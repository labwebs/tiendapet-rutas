<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modals extends CORE_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('mylog');

        //if (! $this->input->is_ajax_request() ) redirect('/');
    }

    public function index() {
        return false;
    }

    public function quickorder() {

        $this->load->model('customers');
        $this->load->model('addresses');
        $this->load->model('products');
        $this->load->model('drivers');

        $this->load->library('utils');
        $this->load->library('Bsale');


        //$data['customers'] = $this->customers->all(true);
        $data['customers'] = array();
        $data['comunas']   = $this->addresses->comunas();
        $data['products']  = $this->products->all();
        $data['drivers']   = $this->drivers->all(false);

        $bs = new Bsale();
        $bodegas = $bs->getSucursales();
        $empresa = $this->orders->getEmpresaBsale();

        $data['bodegas']   = $bodegas;
        $data['empresa']   = $empresa;

        $this->load->view('modals/quick-order', $data);

    }

    public function customers(){

        $this->load->model('customers');

        //$post = $this->input->get();
        $post = $this->input->post();

        $customers = (array)$this->customers->find($post);

        echo json_encode($customers);
    }

    public function customerLoadAddressesHasorders(){

        $this->load->model('customers');

        $customer_id = $this->input->post('customer_id');
        $customer_id = intval($customer_id);
        //$customer_id = 0;
        $customer = $customer_id ? $this->customers->loadAddressesHasorders($customer_id) : new stdObject();

        echo json_encode($customer);
    }


    public function order($id) {
        $this->load->helper('form');
        $this->load->model( array('orders', 'payments', 'descuentos_model') );

        $order = $this->orders->get($id, true);
        $discount_mail = $order->discounts_mails_id ? $this->descuentos_model->get($order->discounts_mails_id) : array();

        $this->load->view('modals/order', array(
            'order'   => $order,
            'pagos'   => $this->payments->all(),
            'comuna'  => $order->comuna ? $order->comuna->nombre : $order->address->comuna ? $order->address->comuna : '',
            'discount_mail' => $discount_mail
        ));
    }

    public function agregar() {
        $this->load->model('products');

        $this->load->view('modals/agregar', array(
            'products' => $this->products->all()
        ));
    }

}