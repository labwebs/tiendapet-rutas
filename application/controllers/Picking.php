<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Picking extends CORE_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('mylog');
        $this->load->library('notificacion');

        /*
        if ($this->user->is_driver && $this->user->is_tomador_pedidos && $this->user->is_tomador_pedidos_despacho) {
        //redirect("/");
        }
         */

        if (!$this->user->is_picking) {
            redirect("/dashboard");
        }
    }

    public function index()
    {
        $this->load->helper('form');

        $this->load->model(array(
            'customers',
            'drivers',
        ));

        $accion = $this->input->post('accion') ? $this->input->post('accion') : '';
        if ($accion == "search") {
            $search = $this->input->post('search');
            $this->session->set_userdata('search', $search);
        }
        $search = $this->session->userdata('search');

        $search['order_delivered_null'] = true;
        $search['order_canceled'] = true;

        $orders_pending = $this->orders->pending_dashboard();

        $stats_dashboard = $this->orders->stats_dashboard();

        $data = array(
            "title" => "Entregas para hoy",
            'search' => $search,
            'drivers' => $this->drivers->overview(null, $search),
            'orders_pending' => $orders_pending,
            'locales' => $this->orders->getLocales(),
            'stats_dashboard' => $stats_dashboard,
        );

        //$this->render('dashboard', $data);

        $this->renderPicking('picking/index', $data);
    }
}
