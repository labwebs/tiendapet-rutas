<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pedidos extends CORE_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('orders');
        $this->load->model('addresses');
        $this->load->model('drivers');
        $this->load->model('descuentos_model');
        $this->load->library('bsale');
        $this->load->library('mylog');
    }

    public function index()
    {
        redirect('pedidos/hoy');
    }

    public function hoy()
    {
        $data = array(
            'pedidos' => $this->orders->today(),
        );

        $this->render('pedidos/listado', $data);
    }

    public function confirmar()
    {
        $data = array(
            'pedidos' => $this->orders->pending(),
        );

        $this->render('pedidos/listado', $data);
    }

    public function entregar()
    {
        $data = array(
            'pedidos' => $this->orders->for_delivery(),
        );

        $this->render('pedidos/listado', $data);
    }

    public function historico()
    {
        $data = array(
            'pedidos' => $this->orders->all(),
        );

        $this->render('pedidos/listado', $data);
    }

    public function buscar()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('drivers');

        $post = $this->input->post();

        /*
        $this->mylog->add(
        array(
        'action' => 'BUSCAR_PEDIDO',
        'description' => 'Dentro de la sección del buscador.',
        'data' => $post
        )
        );
         */

        $data = array(
            'pedidos' => $this->orders->find($post),
            'drivers' => $this->drivers->all(),
            'desde' => $this->input->post('desde'),
            'hasta' => $this->input->post('hasta'),
            'chofer' => $this->input->post('chofer'),
            'monto' => $this->input->post('monto'),
            'query' => $this->input->post('query'),
        );

        $this->render('pedidos/buscar', $data);
    }

    public function cerrar()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $post = $this->input->post();

        $accion = $this->input->post('accion') ? $this->input->post('accion') : '';

        $search = array();
        if ($accion == "search_cerrar") {
            $this->session->set_userdata('search_cerrar', $post);
        }
        $search = $this->session->userdata('search_cerrar');

        $thead = array(
            'id' => '#',
            'order_delivery_date' => 'Fecha',
            'cliente' => 'Cliente',
            'direccion' => 'Direcci&oacute;n',
            'chofer' => 'Chofer',
            'monto' => 'Monto',
            'estado' => 'Estado'
        );

        $drivers_selects = $this->drivers->selects();

        $data = array(
            'logs' => array(),
            'desde' => $search['desde'],
            'hasta' => $search['hasta'],
            'driver_id' => $search['driver_id'],
            'query' => $search['query'],
            'drivers_selects' => $drivers_selects,
            'thead' => $thead,
        );

        $this->render('pedidos/cerrar', $data);
    }

    public function transacciones()
    {
        $post = $this->input->post();

        $accion = $this->input->post('accion') ? $this->input->post('accion') : '';

        $search = array();
        if ($accion == "search_transacciones") {
            $this->session->set_userdata('search_transacciones', $post);
        }
        $search = $this->session->userdata('search_transacciones');

        $thead = array(
            'id' => '#',
            'client' => 'Cliente',
            'method_of_delivery' => 'Método de Entrega',
            'paym_name' => 'Método de Pago',
            'display_price' => 'Monto',
            'date' => 'Fecha',
            'paid_norm' => 'Estado'
        );

        //$drivers_selects = $this->drivers->selects();

        $data = array(
            'logs' => array(),
            'desde' => $search['desde'],
            'hasta' => $search['hasta'],
            //'driver_id' => $search['driver_id'],
            'query' => $search['query'],
            //'drivers_selects' => $drivers_selects,
            'thead' => $thead,
        );

        $this->render('pedidos/transacciones', $data);
    }

    public function log()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $post = $this->input->post();

        $accion = $this->input->post('accion') ? $this->input->post('accion') : '';

        $search = array();
        if ($accion == "search_log") {
            $this->session->set_userdata('search_log', $post);
        }
        $search = $this->session->userdata('search_log');

        $thead = array(
            'id' => '#',
            'created_at' => 'Fecha',
            'order_id' => 'Nro.Pedido',
            'admin_name' => 'Usuario',
            'action' => 'Acci&oacute;n',
            'description' => 'Descripci&oacute;n',
            //'http_referer' => 'Referer',
            'http_user_agent' => 'User Agent',
        );

        $data = array(
            //'logs' => $this->orders->searchLog($post),
            'logs' => array(),
            'desde' => $search['desde'],
            'hasta' => $search['hasta'],
            'query' => $search['query'],
            'thead' => $thead,
        );

        $this->render('pedidos/log', $data);
    }

    public function transacciones_listajax()
    {
        $search = $this->session->userdata('search_transacciones');

        //$search = $this->input->post();
        $req = $this->input->get();

        $order_columns = array(
            'o.id',
            'cus.cust_name',
            'addr.local_id',
            'p.paym_name',
            'o.order_total',
            'o.order_created',
            'o.paid'
        );

        $thead = array(
            'id' => '#',
            'client' => 'Cliente',
            'method_of_delivery' => 'Método de Entrega',
            'paym_name' => 'Método de Pago',
            'display_price' => 'Monto',
            'date' => 'Fecha',
            'paid_norm' => 'Estado'
        );

        //$search = $this->session->userdata('search_autocompra');

        $orderByColumnIndex = @$req['order'][0]['column']; // index of the sorting column (0 index based - i.e. 0 is the first record)
        $orderBy = @$order_columns[$orderByColumnIndex]; //Get name of the sorting column from its index
        $orderType = @$req['order'][0]['dir']; // A

        $page = @intval($req['start']);
        $mostrar = isset($req['length']) && $req['length'] ? intval($req['length']) : 10;

        $search['order'] = array(
            'orderby' => $orderBy,
            'orderType' => $orderType,
        );
        $search['start'] = $page;
        $search['length'] = $mostrar;

        ini_set('memory_limit', '-1');

        $count = $this->orders->srchTransaccionesCount($search);

        $ids = $this->orders->srchTransacciones($search);

        //$ini = $page ? ($page-1)*$mostrar : 0;
        $ini = $page;
        $total = $count;

        //$ids = array_slice($ids, $ini, $mostrar);

        $data = array();
        foreach ($ids as $id) {
            $row = $this->orders->getSrchTransaccion($id);

            $link_view = base_url('pedidos/detalletransaccion/' . $id);

            $columns = array();
            foreach ($thead as $key => $t) {
                if ($key == 'paid_norm') {
                    $columns[] = '<span class="label label-sm label-'.($row->paid==1 ? 'success' : 'danger').'">'.$row->$key.'</span>';
                } else {
                    $columns[] = $row->$key;
                }
            }

            $columns[] = '<div class="btn-inline text-center"><a href="'.$link_view.'" class="btn btn-outline btn-circle btn-sm blue"><i class="fa fa-eye"></i> Ver detalle</a></div>';

            /*
            $htm = '';
            if (! $row->order_delivered ) {
                $htm .= '
                <div class="form-inline -btn-group text-center">
                    <a href="'.base_url("pedidos/editar/{$id}").'" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-pencil"></i> '.($row->status == "Pendiente de confirmación" ? "Confirmar" : "Modificar").'</a>
                    <a href="'.base_url("pedidos/eliminar/{$id}").'" class="btn btn-xs btn-danger" onclick="return confirm(\'Estás seguro que desea eliminar ó anular el pedido?\');">
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                </div>';
            }else{
                $htm .= '
                <div class="form-inline -btn-group text-center">
                    <a href="'.base_url("pedidos/editar/{$id}").'" class="btn btn-xs btn-success"><i class="fa fa-fw fa-check"></i> Ver detalle</a>
                    <a href="'.base_url("pedidos/eliminar/{$id}").'" class="btn btn-xs btn-danger" onclick="return confirm(\'Estás seguro que desea eliminar ó anular el pedido?\');">
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                </div>';
            }
            $columns[] = $htm;
            */

            $data[] = $columns;
        }

        $json_data = array(
            "draw" => isset($req['draw']) ? intval($req['draw']) : 0,
            "recordsTotal" => intval($total),
            "recordsFiltered" => intval($total),
            "data" => $data, // total data array
        );

        echo json_encode($json_data); // send data as json format
        exit;
    }

    public function cerrar_listajax()
    {
        $search = $this->session->userdata('search_cerrar');

        //$search = $this->input->post();
        $req = $this->input->get();

        $order_columns = array(
            'o.id',
            'o.order_delivery_date',
            'cus.cust_name',
            'addr.direccion',
            'a.admin_name',
            'o.order_total',
            ''
        );

        $thead = array(
            'id' => '#',
            'order_delivery_date' => 'Fecha',
            'cliente' => 'Cliente',
            'direccion' => 'Direcci&oacute;n',
            'chofer' => 'Chofer',
            'monto' => 'Monto',
            'estado' => 'Estado'
        );

        //$search = $this->session->userdata('search_autocompra');

        $orderByColumnIndex = @$req['order'][0]['column']; // index of the sorting column (0 index based - i.e. 0 is the first record)
        $orderBy = @$order_columns[$orderByColumnIndex]; //Get name of the sorting column from its index
        $orderType = @$req['order'][0]['dir']; // A

        /*if ($orderBy == 'date_scheduled_format') {
        $orderBy = 'date_scheduled';
        }*/

        $page = @intval($req['start']);
        $mostrar = isset($req['length']) && $req['length'] ? intval($req['length']) : 10;

        $search['order'] = array(
            'orderby' => $orderBy,
            'orderType' => $orderType,
        );
        $search['start'] = $page;
        $search['length'] = $mostrar;

        ini_set('memory_limit', '-1');

        $count = $this->orders->srchCerrarCount($search);

        $ids = $this->orders->srchCerrar($search);

        //$ini = $page ? ($page-1)*$mostrar : 0;
        $ini = $page;
        $total = $count;

        //$ids = array_slice($ids, $ini, $mostrar);

        $data = array();
        foreach ($ids as $id) {
            $row = $this->orders->get($id);

            $link_detalle = base_url('pedidos/detallelog/' . $id);

            $columns = array();
            foreach ($thead as $key => $t) {
                if ($key == 'cliente') {
                    $columns[] = $row->customer->name;
                } else if ($key == 'id') {
                    $htm = '
                    <input id="check-'.$id.'" type="checkbox" name="checks[]" value="'.$id.'" class="check-child"> <label for="check-'.$id.'">'.$id.'</label>
                    ';
                    $columns[] = $htm;
                } else if ($key == 'direccion') {
                    $columns[] = $row->address->direccion.', '.$row->address->comuna;
                } else if ($key == 'chofer') {
                    $columns[] = $row->driver->name;
                } else if ($key == 'monto') {
                    $columns[] = $row->display_price;
                } else if ($key == 'estado') {
                    $columns[] = $row->status;
                } else {
                    $columns[] = $row->$key;
                }
            }
            
            /*
            $htm = '';
            if (! $row->order_delivered ) {
                $htm .= '
                <div class="form-inline -btn-group text-center">
                    <a href="'.base_url("pedidos/editar/{$id}").'" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-pencil"></i> '.($row->status == "Pendiente de confirmación" ? "Confirmar" : "Modificar").'</a>
                    <a href="'.base_url("pedidos/eliminar/{$id}").'" class="btn btn-xs btn-danger" onclick="return confirm(\'Estás seguro que desea eliminar ó anular el pedido?\');">
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                </div>';
            }else{
                $htm .= '
                <div class="form-inline -btn-group text-center">
                    <a href="'.base_url("pedidos/editar/{$id}").'" class="btn btn-xs btn-success"><i class="fa fa-fw fa-check"></i> Ver detalle</a>
                    <a href="'.base_url("pedidos/eliminar/{$id}").'" class="btn btn-xs btn-danger" onclick="return confirm(\'Estás seguro que desea eliminar ó anular el pedido?\');">
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                </div>';
            }
            $columns[] = $htm;
            */

            $data[] = $columns;
        }

        $json_data = array(
            "draw" => isset($req['draw']) ? intval($req['draw']) : 0,
            "recordsTotal" => intval($total),
            "recordsFiltered" => intval($total),
            "data" => $data, // total data array
        );

        echo json_encode($json_data); // send data as json format
        exit;
    }

    public function log_listajax()
    {
        $search = $this->session->userdata('search_log');

        //$search = $this->input->post();
        $req = $this->input->get();

        $order_columns = array(
            'id',
            'created_at',
            'order_id',
            'admin_name',
            'action',
            'description',
            'http_referer',
            'http_user_agent',
        );

        $thead = array(
            'id' => '#',
            'created_at' => 'Fecha',
            'order_id' => 'Nro.Pedido',
            'admin_name' => 'Usuario',
            'action' => 'Acci&oacute;n',
            'description' => 'Descripci&oacute;n',
            //'http_referer' => 'Referer',
            'http_user_agent' => 'User Agent',
        );

        //$search = $this->session->userdata('search_autocompra');

        $orderByColumnIndex = @$req['order'][0]['column']; // index of the sorting column (0 index based - i.e. 0 is the first record)
        $orderBy = @$order_columns[$orderByColumnIndex]; //Get name of the sorting column from its index
        $orderType = @$req['order'][0]['dir']; // A

        /*if ($orderBy == 'date_scheduled_format') {
        $orderBy = 'date_scheduled';
        }*/

        $page = @intval($req['start']);
        $mostrar = isset($req['length']) && $req['length'] ? intval($req['length']) : 10;

        $search['order'] = array(
            'orderby' => $orderBy,
            'orderType' => $orderType,
        );
        $search['start'] = $page;
        $search['length'] = $mostrar;

        ini_set('memory_limit', '-1');

        $count = $this->orders->srchLogCount($search);

        $ids = $this->orders->srchLog($search);

        //$ini = $page ? ($page-1)*$mostrar : 0;
        $ini = $page;
        $total = $count;

        //$ids = array_slice($ids, $ini, $mostrar);

        $data = array();
        foreach ($ids as $id) {
            $row = $this->orders->getSrchLog($id);

            $link_detalle = base_url('pedidos/detallelog/' . $id);

            $columns = array();
            foreach ($thead as $key => $t) {
                if ($key == 'created_at') {
                    $columns[] = isset($row->$key) ? date('d-m-Y H:i:s', strtotime($row->$key)) : '';
                } else {
                    $columns[] = $row->$key;
                }
            }
            $columns[] = '<a href="' . $link_detalle . '" class="btn btn-xs btn-success"><i class="fa fa-fw fa-check"></i> Ver detalle</a>';

            $data[] = $columns;
        }

        $json_data = array(
            "draw" => isset($req['draw']) ? intval($req['draw']) : 0,
            "recordsTotal" => intval($total),
            "recordsFiltered" => intval($total),
            "data" => $data, // total data array
        );

        echo json_encode($json_data); // send data as json format
        exit;
    }

    public function editar($id)
    {
        $this->load->model('drivers');
        $this->load->helper('form');

        $bs = new Bsale();
        $bodegas = $bs->getSucursales();
        $empresa = $this->orders->getEmpresaBsale();

        $pedido = $this->orders->get($id, true);
        $discount_mail = $pedido->discounts_mails_id ? $this->descuentos_model->get($pedido->discounts_mails_id) : array();

        $despachoatupinta_texto = $this->orders->getMsjeDespachoatuPinta($id);

        $selects_region = $this->orders->getSelectsRegion();
        $selects_comuna = $this->orders->getSelectsComunas();
        $selects_local  = $this->orders->getSelectsLocales();
        //print_a($selects_region);
        //exit;
        $data = array(
            'pedido' => $pedido,
            'despachoatupinta_texto' => $despachoatupinta_texto,
            'choferes' => $this->drivers->all(),
            'bodegas' => $bodegas,
            'empresa' => $empresa,
            'locales' => $this->orders->getLocales(),
            'discount_mail' => $discount_mail,
            'selects_region' => $selects_region,
            'selects_comuna' => $selects_comuna,
            'selects_local' => $selects_local,
        );

        $this->render('pedidos/confirmar', $data);
    }

    public function detalletransaccion($id)
    {
        $this->load->model('drivers');
        
        $pedido = $this->orders->get($id, true);
        
        $pedido->detalle_webpay = $pedido->paym_id == 4 ? $this->orders->getDetalleWebpay($id) : array();
        $pedido->detalle_oneclick = $pedido->paym_id == 5 || $pedido->paym_id == 6 ? $this->orders->getDetalleOneclick($id) : array();
        $pedido->detalle_puntopago = $pedido->paym_id == 7 ? $this->orders->getDetallePuntopago($id) : array();

        $data = array(
            'pedido' => $pedido,
            'locales' => $this->orders->getLocales()
        );

        $this->render('pedidos/detalletransaccion', $data);
    }

    public function detallelog($id)
    {
        $this->load->helper('form');

        $log = $this->orders->getLog($id);
        $data = array(
            'log' => $log,
            'pedido' => $this->orders->get($log->order_id, true),
        );

        $this->render('pedidos/detallelog', $data);
    }

    public function modificar()
    {
        $this->load->library('form_validation');

        $msje = '';
        $data_log = array();

        $rules = array(
            /*
            array(
            'field' => 'chofer',
            'label' => 'Chofer',
            'rules' => 'required|integer'
            ),
            array(
            'field' => 'delivery_date',
            'label' => 'Fecha',
            'rules' => 'required'
            ),
             */
            /*
            array(
            'field' => 'delivery_time',
            'label' => 'Hora',
            'rules' => 'required'
            ),*/
            array(
                'field' => 'order_id',
                'label' => 'Pedido',
                'rules' => 'required|integer',
            ),
        );
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run()) {
            $id = $this->input->post('order_id');

            $data = array(
                'order_delivery_date' => $this->input->post('delivery_date'),
                //'order_delivery_time' => $this->input->post('delivery_time'),
                'order_comment' => $this->input->post('order_comment'),
                'driver_id' => $this->input->post('chofer'),
            );
            $this->orders->update($id, $data);

            $data_log = $data;
            $msje = "Se modificó con éxito el Pedido #$id";
            $this->session->set_flashdata('success', $msje);
        } else {
            $msje = validation_errors();
            $this->session->set_flashdata('error', $msje);
        }

        $this->mylog->add(
            array(
                'order_id' => $id,
                'action' => 'ACTUALIZAR_PEDIDO',
                'description' => $msje,
                'data' => $data_log,
            )
        );

        $this->goback();
    }

    public function actualizar()
    {
        $accion = $this->input->post('accion');
        if ($accion == 'modificar') {
            $this->modificar();
        } elseif ($accion == 'emitir') {
            $this->emitir();
        } else {
            $this->goback();
        }
        //$this->$accion();
    }

    public function emitir($from_order_manual = false, $data = array())
    {
        $data_log = array();

        if ($from_order_manual) {
            $_REQUEST = $data;
            $_POST = $data;
            $data_log['request'] = $data;
        }

        $this->load->library('form_validation');

        $rules = array(
            array(
                'field' => 'chofer',
                'label' => 'Chofer',
                'rules' => 'required|integer',
            ),
            array(
                'field' => 'delivery_date',
                'label' => 'Fecha',
                'rules' => 'required',
            ),
            /*
            array(
            'field' => 'delivery_time',
            'label' => 'Hora',
            'rules' => 'required'
            ),*/
            array(
                'field' => 'sucursal',
                'label' => 'Bodega',
                'rules' => 'required',
            ),
            array(
                'field' => 'order_id',
                'label' => 'Pedido',
                'rules' => 'required|integer',
            ),
        );
        $this->form_validation->set_rules($rules);

        $id = $this->input->post('order_id');

        if ($this->form_validation->run()) {
            $sucursal_id = $this->input->post('sucursal');

            //$time = intval(time());
            $time = intval(strtotime(date('Y-m-d H:i:s')));

            //$fdespacho = intval(strtotime($this->input->post('delivery_date')));
            $fdespacho = intval(strtotime(date('Y-m-d H:i:s')));

            $order = $this->orders->get($id, true);

            $discount_mail = $order->discounts_mails_id ? $this->descuentos_model->get($order->discounts_mails_id) : array();

            $driver_id = $this->input->post('chofer');
            $driver = $this->drivers->get($driver_id, false);

            /***** Integrar con Bsale *******/
            $bs = new Bsale();
            $client = $bs->getClientByMail($order->customer->email);

            $skus_nofound = array();
            $skus_nostock = array();
            $skus = array();
            //$skus_stock = array();
            if (!empty($order->products)) {
                foreach ($order->products as $indice => $p) {
                    
                    $sku = $bs->getProductBySku($p->sku_id);
                    if (isset($sku->variant->product->pack_details) && !empty($sku->variant->product->pack_details)) {
                        foreach ($sku->variant->product->pack_details as $indice => $item) {
                            $sku_product = $bs->getProductByVariantid($item->variant->id);
                            $sku_stock = $bs->getStockProductByVariantid($item->variant->id, $sucursal_id);
                            if (isset($sku_stock->id) && !empty($sku_stock->id) && ($item->quantity*$p->qty) > $sku_stock->quantity) {
                                $skus_nostock[] = $sku_product->code;
                            }
                        }
                        if( empty($skus_nostock) ){
                            $skus[] = array('variant_id' => $sku->variant->id, 'qty' => $p->qty, 'name' => $p->description->brand. ' - ' . $p->description->name, 'price' => $p->description->price, 'price_offer' => $p->description->price_offer);
                        }
                    }else{
                        if (isset($sku->variant->id) && !empty($sku->variant->id)) {
	                        
                            $sku_stock = $bs->getStockProductBySku($p->sku_id, $sucursal_id);
                            
                            if (isset($sku_stock->id) && !empty($sku_stock->id) && $p->qty <= $sku_stock->quantity) {
                                $skus[] = array('variant_id' => $sku->variant->id, 'qty' => $p->qty, 'name' => $sku->variant->product->name . ' - ' . $sku->variant->description, 'price' => $p->description->price, 'price_offer' => $p->description->price_offer);
								//$skus_stock[] = $sku_stock->id;
                            } else {
                                $skus_nostock[] = $p->sku_id;
                            }
                        } else {
                            $skus_nofound[] = $p->sku_id;
                        }
                    }
                }
            }

            if (empty($skus_nofound)) {
                if (empty($skus_nostock)) {
                    $document = array(
                        "documentTypeId" => intval(1),
                        "officeId" => intval($sucursal_id),
                        "priceListId" => $bs->getPriceListId(),
                        "emissionDate" => $time,
                        "expirationDate" => $time,
                        "declareSii" => intval(1), // Si se desea declarar al servicio de impuesto interno colocar 1
                        "sendEmail" => intval(1),
                    );

                    $err_insert_client = '';
                    if (isset($client->id) && !empty($client->id)) {
                        $document['clientId'] = intval($client->id);
                    } else {
                        //Crear Cliente
                        $insert_client = array(
                            'firstName' => $order->customer->name,
                            'lastName' => '.',
                            'email' => $order->customer->email,
                        );
                        $client = $bs->insertClient($insert_client);
                        $document['clientId'] = isset($client->id) && !empty($client->id) ? intval($client->id) : '';
                        $err_insert_client = isset($client->error) && !empty($client->error) ? $client->error : '';
                    }

                    if (isset($document['clientId']) && !empty($document['clientId'])) {
                        #$date = DateTime::createFromFormat('d/m/Y', $this->input->post('delivery_date') );

                        $details = array();
                        if (!empty($skus)) {
                            foreach ($skus as $indice => $_sku) {
                                $price = $_sku['price_offer'] ? $_sku['price_offer'] : $_sku['price'];

                                if (!empty($discount_mail)) {
                                    $discount = $price * ($discount_mail['percentage'] / 100);
                                    $price = $price > $discount ? $price - $discount : $price;
                                }

                                // Pedido Autocompra
                                if ($order->paym_id == 5) {
                                    $desc = round($price * 0.05); //5%
                                    $price = $price - $desc;
                                }

                                $price_neto = round($price / 1.19);

                                $details[] = array(
                                    "variantId" => intval($_sku['variant_id']),
                                    "netUnitValue" => intval($price_neto),
                                    "quantity" => intval($_sku['qty']),
                                    "taxId" => "[1]",
                                    "comment" => "", //$_sku['name'],
                                    "discount" => intval(0),
                                );
                            }

                            if ($order->order_shipping && $order->order_shipping > 0) {
                                $price_neto = round($order->order_shipping / 1.19);
                                $details[] = array(
                                    "variantId" => $bs->getIdProShipping(), // intval(29866) intval(633)
                                    "netUnitValue" => intval($price_neto),
                                    "quantity" => intval(1),
                                    "taxId" => "[1]",
                                    "comment" => "", //$_sku['name'],
                                    "discount" => intval(0),
                                );
                            }

                            $document['details'] = $details;
                        }

                        $amount = $order->order_total + $order->order_shipping;

                        $boleta = $bs->generateDocument($document);
                        if (isset($boleta->id) && !empty($boleta->id)) {
                            $details_document = $bs->getDetailsDocuments($boleta->id);

                            $details_despacho = array();
                            if (!empty($details_document)) {
                                foreach ($details_document as $indice => $detail) {
                                    $details_despacho[] = array(
                                        "detailId" => $detail->id,
                                        "quantity" => $detail->quantity,
                                    );
                                }
                            }

                            //Generar Guía de Despacho
                            $despacho = array(
                                "documentTypeId" => $bs->getDespachoId(), //21,  21=>RyA y 18=>Macdog
                                "emissionDate" => $fdespacho,
                                "expirationDate" => $fdespacho,
                                "shippingTypeId" => 1,
                                "officeId" => $sucursal_id,
                                "priceListId" => $bs->getPriceListId(),
                                "municipality" => "Las Condes", //$order->address->comuna,
                                "city" => "Santiago",
                                "address" => "Apoquindo 7910", //$order->address->direccion,
                                "declareSii" => 0,
                                "recipient" => mb_convert_case($driver->name, MB_CASE_UPPER, "UTF-8"), //Sacar de la BD al despachador
                                "client" => array(
                                    'firstName' => "Allan", //$order->customer->name,
                                    'lastName' => "Turski",
                                    "municipality" => "Las Condes", //$order->address->comuna,
                                    "code" => "25250770-1",
                                    "activity" => "Sin dato",
                                    "company" => "Particular",
                                    "city" => "Santiago",
                                    "address" => "Apoquindo 7910", //$order->address->direccion
                                ),
                                "details" => $details_despacho,
                            );

                            $shipping = $bs->generateShipping($despacho);

                            $data = array(
                                'order_delivery_date' => $this->input->post('delivery_date'),
                                //'order_delivery_time' => $this->input->post('delivery_time'),
                                'bsale_empresas_id' => $this->input->post('bsale_empresas_id'),
                                'bsale_sucursal_id' => $sucursal_id,
                                'order_comment' => $this->input->post('order_comment'),
                                'driver_id' => $this->input->post('chofer'),
                                'bsale_nro_boleta' => $boleta->number,
                                'bsale_url_print_boleta' => $boleta->urlPublicView,
                            );

                            $data_log['data'] = $data;
                            $data_log['data_document'] = $document;
                            $data_log['data_despacho'] = $despacho;

                            $msje = "Se generó con éxito la boleta en Bsale para el Pedido #$id";
                            $this->orders->update($id, $data);
                            $this->session->set_flashdata('success', $msje);
                        } else {
                            $msje = "Se produjo un error al generar la boleta en Bsale.";
                            $this->session->set_flashdata('error', $msje);
                        }
                    } else {
                        $msje = "Se produjo un error al relacionar al cliente.";
                        if (isset($err_insert_client) && !empty($err_insert_client)) {
                            $msje = 'Se produjo un error al crear en Bsale el cliente : ' . $err_insert_client;
                        }

                        $this->session->set_flashdata('error', $msje);
                    }
                } else {
                    $msje = "No hay stock para los siguientes SKU en BSale : " . implode(',', $skus_nostock);
                    $this->session->set_flashdata('error', $msje);
                }
            } else {
                $msje = "Los siguientes SKU no se encontraron en BSale : " . implode(',', $skus_nofound);
                $this->session->set_flashdata('error', $msje);
            }
        } else {
            $msje = validation_errors();
            $this->session->set_flashdata('error', $msje);
        }

        $this->mylog->add(
            array(
                'order_id' => $id,
                'action' => 'EMITIR_BOLETA_BSALE',
                'description' => $msje,
                'data' => $data_log,
            )
        );

        if ($from_order_manual) {
            redirect('pedidos/editar/' . $id);
        } else {
            $this->goback();
        }
    }

    //Crear formulario desde modal
    public function crear()
    {
        $data_log = array();

        $this->load->model(array(
            'customers',
            'addresses',
            'orders',
            'products',
        ));

        $post = $this->input->post();

        # CLIENTE
        if (isset($post['create_customer'])) {
            $customer_id = $this->customers->create(array(
                'cust_login' => $post['cust_email'],
                'cust_email' => $post['cust_email'],
                'cust_name' => $post['cust_name'],
                'cust_phone' => $post['cust_phone'],
            ));
        } else {
            $customer_id = $post['customer_id'];
        }
        # FIN CLIENTE

        # DIRECCIÓN
        if (isset($post['create_address']) || isset($post['create_customer'])) {
            $comuna = $this->addresses->get_comuna($post['comuna']);

            $address_id = $this->addresses->create(array(
                'direccion' => $post['direccion'],
                'comuna' => $comuna->nombre,
                'comuna_id' => $comuna->id,
                'ciudad' => $post['ciudad'],
                'comentarios' => $post['comentarios'],
                'cust_id' => $customer_id,
                'created_at' => date('Y-m-d H:i:s'),
                'activo' => 1,
            ));
        } else {
            $address_id = $post['address_id'];
        }
        # FIN DIRECCIÓN

        # TOTAL PEDIDO
        $total = 0;
        foreach ($post['sku']['ids'] as $index => $sku) {
            $sku = $this->products->by_sku($sku);
            $qty = $post['sku']['qty'][$index];

            $total += intval($sku->price) * intval($qty);
        }

        $total -= intval($post['discount']);
        # FIN TOTAL PEDIDO

        # PEDIDO
        //if ($post['driver_id'] == 'Por confirmar') $post['driver_id'] = null;

        $data_order = array(
            'order_created' => date('Y-m-d H:i:s'),
            'order_delivery_date' => $post['delivery_date'],
            //'order_delivery_time' => $post['delivery_time'],
            'cust_id' => $customer_id,
            'addr_id' => $address_id,
            'driver_id' => $post['driver_id'],
            'order_total' => $total,
            'order_comment' => $post['comment'],
            'order_shipping' => $post['shipping'],
            'paym_id' => 1,
            'paid' => 1,
        );

        $data_log = $data_order;

        $order_id = $this->orders->create($data_order);
        # FIN PEDIDO

        # PRODUCTOS
        foreach ($post['sku']['ids'] as $index => $sku) {
            $this->orders->append_sku(array(
                'sku_id' => $sku,
                'order_id' => $order_id,
                'qty' => $post['sku']['qty'][$index],
            ));
        }
        # FIN PRODUCTOS

        if ($post['accion'] == "emitir") {
            $data = array(
                'chofer' => $post['driver_id'],
                'delivery_date' => $post['delivery_date'],
                'sucursal' => $post['sucursal2'],
                'order_id' => $order_id,
                'order_comment' => $post['comment'],
            );
            Pedidos::emitir(true, $data);
        } else {
            $this->mylog->add(
                array(
                    'order_id' => $order_id,
                    'action' => 'CREAR_PEDIDO_MANUAL',
                    'description' => 'El pedido se ha creado correctamente.',
                    'data' => $data_order,
                )
            );

            $this->session->set_flashdata('success', 'El pedido se ha creado correctamente');
            redirect($this->input->server('HTTP_REFERER'));
        }
    }

    public function eliminar($id)
    {
        $this->db->delete('sku_orders', array('order_id' => $id));
        $this->db->delete('orders', array('id' => $id));

        $msje = 'El pedido se ha eliminado correctamente';
        $this->session->set_flashdata('error', $msje);
        //$this->goback();

        $this->mylog->add(
            array(
                'order_id' => $id,
                'action' => 'ELIMINAR_PEDIDO',
                'description' => $msje,
                'data' => array('order_id' => $id),
            )
        );

        if (strpos($this->input->server('HTTP_REFERER'), "editar")) {
            redirect('pedidos/hoy');
        } else {
            redirect($this->input->server('HTTP_REFERER'));
        }
    }

    public function anular($id)
    {
        $this->orders->update($id, array('order_canceled' => 1));

        $msje = 'El pedido se ha anulado correctamente';

        $this->mylog->add(
            array(
                'order_id' => $id,
                'action' => 'ANULAR_PEDIDO',
                'description' => $msje,
                'data' => array('order_id' => $id),
            )
        );

        $this->session->set_flashdata('error', $msje);
        //$this->goback();
        redirect('pedidos/hoy');
    }

    public function imprimirPorConfirmar()
    {

        //$this->load->model( array('drivers', 'orders') );

        //$search = $this->session->userdata('search');
        //$overview = $this->drivers->overview($id, $search);

        /*
        $this->mylog->add(
        array(
        'action' => 'IMPRIMIR_POR_CONFIRMAR',
        'description' => 'Dashboard imprimir por confirmar',
        'data' => ''
        )
        );
         */

        $payments = $this->orders->getPayments();

        $locales = $this->orders->getLocales();

        $orders_pending = $this->orders->pending_dashboard(true, false);
        foreach ($orders_pending as &$o) {
            $o = $this->orders->get_products($o);
        }

        $this->load->view("pedidos/print", array(
            'orders_pending' => $orders_pending,
            'payments' => $payments,
            'locales' => $locales,
        ));
    }

    public function detalle()
    {
        $skus = array();

        $ids = $this->input->post('sku_id');
        $qtys = $this->input->post('qty');

        foreach ($ids as $ix => $id) {
            $skus[] = array('id' => $id, 'qty' => $qtys[$ix]);
        }

        $this->orders->sync_products($this->input->post('order_id'), $skus);

        $this->mylog->add(
            array(
                'order_id' => $this->input->post('order_id'),
                'action' => 'GUARDAR_DETALLE_PRODUCTOS',
                'description' => 'El usuario decide modificar los productos o agregar nuevos productos para el pedido.',
                'data' => $skus,
            )
        );

        redirect($this->input->server('HTTP_REFERER'));
    }

    public function metodoentrega(){
        
        $me = $this->input->post('metodo_entrega');
        $order_id = $this->input->post('order_id');
        
        $pedido = $this->orders->get($order_id);
        
        if( isset($pedido->id) && $pedido->id ){
            $address_id = $pedido->address->id;
            if ($me == 'retiro_local') {
                $local_id = $this->input->post('me_local_id');

                $data_update = array(
                            'direccion' => 'Retiro en local,',
                            'comuna' => '',
                            'ciudad' => '',
                            'comuna_id' => '493',
                            'local_id' => $local_id
                        );
                $this->addresses->update($address_id, $data_update);
            } elseif ($me == 'despacho_normal') {
                $direccion = $this->input->post('me_direccion');
                $comuna_id = $this->input->post('me_comuna_id');
                $comentarios = $this->input->post('me_comentarios');

                $comuna = $this->addresses->get_comuna($comuna_id);

                $data_update = array(
                    'direccion' => $direccion,
                    'comentarios' => $comentarios,
                    'comuna' => $comuna->nombre,
                    'ciudad' => $comuna->nombre,
                    'comuna_id' => $comuna_id,
                    'local_id' => ""
                );
                $this->addresses->update($address_id, $data_update);
            }

            $this->mylog->add(
                array(
                    'order_id' => $order_id,
                    'action' => 'GUARDAR_METODO_ENTREGA',
                    'description' => 'El usuario decide modificar el metodo de entrega.',
                    'data' => $data_update,
                )
            );
        }

        $this->session->set_flashdata('success', 'El pedido fue modificado correctamente.');
        redirect($this->input->server('HTTP_REFERER'));
    }
    
    public function cerrarOrders()
    {
        $this->load->helper('form');

        $this->load->model(array(
            'orders',
            'products'
        ));

        $this->load->library('mylog');

        $date_current_format = date('Y-m-d');

        $orders_ids = $this->input->post('checks');
        
        $res = 0;
        if( ! empty($orders_ids) ){
            foreach($orders_ids as $indice => $order_id){
                $order = $this->orders->get($order_id);

                $data = array(
                    //"order_paid" => $order->paid,
                    'order_delivered' => $date_current_format.' 00:00:00'
                );    
                if( $this->orders->update($order_id, $data) ){
                    $msje = "Se cerró correctamente el Pedido #$order_id";
                    $this->mylog->add(
                        array(
                            'order_id' => $order_id,
                            'action' => 'PEDIDO_CERRADO',
                            'description' => $msje,
                            'data' => '',
                        )
                    );
                }
            }
            $res = 1;
            $msje = "Se cerraron los pedidos exitosamente.";
        }else{
            $msje = "No se encontraron pedidos.";
        }

        if ($res == 1) {
            $this->session->set_flashdata('success', $msje);
        } else {
            $this->session->set_flashdata('error', $msje);
        }
        
        redirect('pedidos/cerrar');        
    }

}
