<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Dashboard extends CORE_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        
        $this->load->library('mylog');

        $this->load->library('simpliroute');
        
        $this->load->model('simpliroute_model');
        $this->load->model('payments');

        if ($this->user->is_driver && !$this->user->is_tomador_pedidos && !$this->user->is_tomador_pedidos_despacho) {
            redirect("/rutero/presente");
        }
        
        if ($this->user->is_picking) {
            redirect("/picking");
        }
    }
    
    public function indicadores()
    {
        if (!$this->user->is_super_admin) {
            redirect("/");
        }
        
        $this->load->helper('form');
        
        $this->load->model(array(
        'orders',
        'products',
        'customers',
        'drivers',
        'indicators_model',
        ));
        
        $date_current_format = date('Y-m-d');
        
        // INDICADORES RETENCION DE CLIENTES
        
        $less_30_days = strtotime('-30 days');
        $less_90_days = strtotime('-90 days');
        $less_180_days = strtotime('-180 days');
        $less_270_days = strtotime('-270 days');
        
        $less_30_days_format = date('Y-m-d', $less_30_days);
        $less_90_days_format = date('Y-m-d', $less_90_days);
        $less_180_days_format = date('Y-m-d', $less_180_days);
        $less_270_days_format = date('Y-m-d', $less_270_days);
        
        /*
        $srch = array(
        'orders_start_date' => $less_30_days_format,
        'orders_end_date' => $date_current_format,
        'orders_paid' => 1,
        //'group_by' => 'cus.cust_email'
        );
        $orders_0y30days = $this->indicators_model->srchOrdersBuy($srch);
        
        $orders_0y30days_count = count($orders_0y30days);
        
        $orders_x_bodega_0y30days_count = 0;
        $orders_x_local_0y30days_count = 0;
        $order_x_bodegas_0y30days = array();
        if (!empty($orders_0y30days)) {
        foreach ($orders_0y30days as $row) {
        //if( in_array($row->bodega_id, array(11, 12) )) continue;
        if ($row->addr_local_id == 0 || is_null($row->addr_local_id)) {
        $order_x_bodegas_0y30days['despacho'][$row->bodega_id]['orders'][] = $row;
        $order_x_bodegas_0y30days['despacho'][$row->bodega_id]['monto'] = isset($order_x_bodegas_0y30days['despacho'][$row->bodega_id]['monto']) ? $order_x_bodegas_0y30days['despacho'][$row->bodega_id]['monto'] + ($row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc) : 0;
        $bodegas_ids_all[$row->bodega_id] = $row->bodega_id;
        $orders_x_bodega_0y30days_count++;
        } elseif ($row->bodega_id == 12) {
        $order_x_bodegas_0y30days['retiro_local'][$row->addr_local_id]['count'] = isset($order_x_bodegas_0y30days['retiro_local'][$row->addr_local_id]['count']) ? intval($order_x_bodegas_0y30days['retiro_local'][$row->addr_local_id]['count']) + 1 : 1;
        $order_x_bodegas_0y30days['retiro_local'][$row->addr_local_id]['monto'] = isset($order_x_bodegas_0y30days['retiro_local'][$row->addr_local_id]['monto']) ? $order_x_bodegas_0y30days['retiro_local'][$row->addr_local_id]['monto'] + ($row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc) : ($row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc);
        $locales_ids_all[$row->addr_local_id] = $row->addr_local_id;
        $orders_x_local_0y30days_count++;
        //if( $row->order_id == 253724 ){
        if( $row->addr_local_id == 15 ){
        print_a($row);
        }
        }
        }
        }
        
        print_a($orders_0y30days_count);
        print_a($order_x_bodegas_0y30days['retiro_local']);
        exit;
        
        die("jeans");
        exit;
        */
        
        
        
        
        $tiempo_inicial = microtime(true);
        
        $srch = array(
        'orders_start_date' => $less_270_days_format,
        'orders_end_date' => $less_180_days_format,
        'orders_paid' => 1,
        //'paid' => 1,
        'group_by' => 'cus.cust_email',
        );
        $customers_mail_between180y270days = $this->indicators_model->srchCustomersBuy($srch, 'cust_email');
        
        $less_179_days = strtotime('-179 days');
        $less_179_days_format = date('Y-m-d', $less_179_days);
        
        $srch = array(
        'orders_start_date' => $less_179_days_format,
        'orders_end_date' => $less_90_days_format,
        'orders_paid' => 1,
        //'paid' => 1,
        'in_customers_mails' => $customers_mail_between180y270days,
        'group_by' => 'cus.cust_email',
        );
        $customers_mail_between90y179days = $this->indicators_model->srchCustomersBuy($srch, 'cust_email');
        
        $less_89_days = strtotime('-89 days');
        $less_89_days_format = date('Y-m-d', $less_89_days);
        
        $srch = array(
        'orders_start_date' => $less_89_days_format,
        'orders_end_date' => $date_current_format,
        'orders_paid' => 1,
        //'paid' => 1,
        'in_customers_mails' => $customers_mail_between90y179days,
        'group_by' => 'cus.cust_email',
        );
        $customers_mail_between0y89days = $this->indicators_model->srchCustomersBuy($srch, 'cust_email');
        
        // Clientes x Bodegas
        $filters = array(
        'orders_start_date' => $less_270_days_format,
        'orders_end_date' => $less_180_days_format,
        'orders_paid' => 1,
        //'paid' => 1,
        'group_by' => 'cus.cust_email',
        );
        $customers_por_bodegas_180y270days = $this->indicators_model->getCustomersPorBodega($customers_mail_between180y270days, 'cust_email', $filters);
        
        $filters = array(
        'orders_start_date' => $less_179_days_format,
        'orders_end_date' => $less_90_days_format,
        'orders_paid' => 1,
        //'paid' => 1,
        'group_by' => 'cus.cust_email',
        );
        $customers_por_bodegas_90y179days = $this->indicators_model->getCustomersPorBodega($customers_mail_between90y179days, 'cust_email', $filters);
        
        $filters = array(
        'orders_start_date' => $less_89_days_format,
        'orders_end_date' => $date_current_format,
        'orders_paid' => 1,
        //'paid' => 1,
        'group_by' => 'cus.cust_email',
        );
        $customers_por_bodegas_0y89days = $this->indicators_model->getCustomersPorBodega($customers_mail_between0y89days, 'cust_email', $filters, true);
        
        $bodegas = $this->indicators_model->getBodegas();
        
        $indicator_clients_x_bodega = array();
        if (!empty($bodegas)) {
            foreach ($bodegas as $bodega_id => $bodega) {
                $count0y90 = isset($customers_por_bodegas_0y89days[$bodega_id]) && !empty($customers_por_bodegas_0y89days[$bodega_id]) ? count($customers_por_bodegas_0y89days[$bodega_id]) : 0;
                $count90y180 = isset($customers_por_bodegas_90y179days[$bodega_id]) && !empty($customers_por_bodegas_90y179days[$bodega_id]) ? count($customers_por_bodegas_90y179days[$bodega_id]) : 0;
                $count180y270 = isset($customers_por_bodegas_180y270days[$bodega_id]) && !empty($customers_por_bodegas_180y270days[$bodega_id]) ? count($customers_por_bodegas_180y270days[$bodega_id]) : 0;
                
                $percent0y90 = $count90y180 ? round((($count0y90 / $count90y180) * 100), 1) : 0;
                $percent90y180 = $count180y270 ? round((($count90y180 / $count180y270) * 100), 1) : 0;
                
                $indicator_clients_x_bodega[$bodega_id]['nombre'] = $bodega['nombre'];
                $indicator_clients_x_bodega[$bodega_id]['0y90']['count'] = $count0y90;
                $indicator_clients_x_bodega[$bodega_id]['90y180']['count'] = $count90y180;
                $indicator_clients_x_bodega[$bodega_id]['180y270']['count'] = $count180y270;
                $indicator_clients_x_bodega[$bodega_id]['0y90']['percent'] = $percent0y90;
                $indicator_clients_x_bodega[$bodega_id]['90y180']['percent'] = $percent90y180;
            }
        }
        $indicator_clients_retencion_percent = count($customers_mail_between90y179days) ? round(((count($customers_mail_between0y89days) / count($customers_mail_between90y179days)) * 100), 1) : 0;
        
        // SIN BODEGA
        /*
        $count0y90 = isset($customers_por_bodegas_0y89days['sin-bodega']) && ! empty($customers_por_bodegas_0y89days['sin-bodega']) ? count($customers_por_bodegas_0y89days['sin-bodega']) : 0;
        $count90y180 = isset($customers_por_bodegas_90y179days['sin-bodega']) && ! empty($customers_por_bodegas_90y179days['sin-bodega']) ? count($customers_por_bodegas_90y179days['sin-bodega']) : 0;
        $count180y270 = isset($customers_por_bodegas_180y270days['sin-bodega']) && ! empty($customers_por_bodegas_180y270days['sin-bodega'])? count($customers_por_bodegas_180y270days['sin-bodega']) : 0;
        
        $percent0y90 = $count90y180 ? ($count0y90/$count90y180)*100 : 0;
        $percent90y180 = $count180y270 ? ($count90y180/$count180y270)*100 : 0;
        
        $indicator_clients_x_bodega['sin-bodega']['nombre'] = $bodega['nombre'];
        $indicator_clients_x_bodega['sin-bodega']['0y90']['count'] = $count0y90;
        $indicator_clients_x_bodega['sin-bodega']['90y180']['count'] = $count90y180;
        $indicator_clients_x_bodega['sin-bodega']['180y270']['count'] = $count180y270;
        $indicator_clients_x_bodega['sin-bodega']['0y90']['percent'] = $percent0y90;
        $indicator_clients_x_bodega['sin-bodega']['90y180']['percent'] = $percent90y180;
        */
        
        // INDICADORES CLIENTES NUEVOS A 30 DÍAS Y ANTERIOR
        
        $less_30_days = strtotime('-30 days');
        $less_31_days = strtotime('-31 days');
        $less_60_days = strtotime('-60 days');
        $less_61_days = strtotime('-61 days');
        $less_90_days = strtotime('-90 days');
        $less_91_days = strtotime('-91 days');
        $less_120_days = strtotime('-120 days');
        
        $less_30_days_format = date('Y-m-d', $less_30_days);
        $less_31_days_format = date('Y-m-d', $less_31_days);
        $less_60_days_format = date('Y-m-d', $less_60_days);
        $less_61_days_format = date('Y-m-d', $less_61_days);
        $less_90_days_format = date('Y-m-d', $less_90_days);
        $less_91_days_format = date('Y-m-d', $less_91_days);
        $less_120_days_format = date('Y-m-d', $less_120_days);
        
        $customersCountAll = $this->customers->count_all();
        
        $srch = array(
        'orders_start_date' => $less_30_days_format,
        'orders_end_date' => $date_current_format,
        'orders_paid' => 1,
        //'paid' => 1,
        'group_by' => 'cus.cust_email',
        );
        $customers_mail_between0y30days = $this->indicators_model->srchCustomersBuy($srch, 'cust_email');
        
        $srch = array(
        //'orders_start_date' => $less_60_days_format,
        'orders_end_date' => $less_31_days_format,
        'orders_paid' => 1,
        //'paid' => 1,
        'in_customers_mails' => $customers_mail_between0y30days,
        'group_by' => 'cus.cust_email',
        );
        $customers_mail_finds_0y30 = $this->indicators_model->srchCustomersBuy($srch, 'cust_email');
        
        $customers_mail_news_0y30_count = intval(count($customers_mail_between0y30days)) - intval(count($customers_mail_finds_0y30));
        
        $srch = array(
        'orders_start_date' => $less_60_days_format,
        'orders_end_date' => $less_31_days_format,
        'orders_paid' => 1,
        //'paid' => 1,
        'group_by' => 'cus.cust_email',
        );
        $customers_mail_between31y60days = $this->indicators_model->srchCustomersBuy($srch, 'cust_email');
        
        $srch = array(
        //'orders_start_date' => $less_60_days_format,
        'orders_end_date' => $less_61_days_format,
        'orders_paid' => 1,
        //'paid' => 1,
        'in_customers_mails' => $customers_mail_between31y60days,
        'group_by' => 'cus.cust_email',
        );
        $customers_mail_finds_31y60 = $this->indicators_model->srchCustomersBuy($srch, 'cust_email');
        
        $customers_mail_news_31y60_count = intval(count($customers_mail_between31y60days)) - intval(count($customers_mail_finds_31y60));
        
        $srch = array(
        'orders_start_date' => $less_90_days_format,
        'orders_end_date' => $less_61_days_format,
        'orders_paid' => 1,
        //'paid' => 1,
        'group_by' => 'cus.cust_email',
        );
        $customers_mail_between61y90days = $this->indicators_model->srchCustomersBuy($srch, 'cust_email');
        
        $srch = array(
        //'orders_start_date' => $less_120_days_format,
        'orders_end_date' => $less_91_days_format,
        'orders_paid' => 1,
        //'paid' => 1,
        'in_customers_mails' => $customers_mail_between61y90days,
        'group_by' => 'cus.cust_email',
        );
        $customers_mail_finds_61y90 = $this->indicators_model->srchCustomersBuy($srch, 'cust_email');
        
        $customers_mail_news_61y90_count = intval(count($customers_mail_between61y90days)) - intval(count($customers_mail_finds_61y90));
        
        /** Clientes nuevos por Bodega **/
        
        /** New 61y90days **/
        $customers_mail_news_61y90days = array();
        if( ! empty($customers_mail_between61y90days) ){
            $temp_customers_mail_finds = array();
            foreach ($customers_mail_finds_61y90 as $email_find) {
                $temp_customers_mail_finds[$email_find] = $email_find;
            }
            foreach ($customers_mail_between61y90days as $_email) {
                if( isset($temp_customers_mail_finds[$_email]) ){
                    continue;
                }else{
                    $customers_mail_news_61y90days[] = $_email;
                }
            }
        }
        $filters = array(
        'orders_start_date' => $less_90_days_format,
        'orders_end_date' => $less_61_days_format,
        'orders_paid' => 1,
        'group_by' => 'cus.cust_email',
        );
        $customers_news_por_bodegas_61y90days = $this->indicators_model->getCustomersPorBodega($customers_mail_news_61y90days, 'cust_email', $filters);
        
        /** New 31y60days **/
        $customers_mail_news_31y60days = array();
        if( ! empty($customers_mail_between31y60days) ){
            $temp_customers_mail_finds = array();
            foreach ($customers_mail_finds_31y60 as $email_find) {
                $temp_customers_mail_finds[$email_find] = $email_find;
            }
            foreach ($customers_mail_between31y60days as $_email) {
                if( isset($temp_customers_mail_finds[$_email]) ){
                    continue;
                }else{
                    $customers_mail_news_31y60days[] = $_email;
                }
            }
        }
        $filters = array(
        'orders_start_date' => $less_60_days_format,
        'orders_end_date' => $less_31_days_format,
        'orders_paid' => 1,
        'group_by' => 'cus.cust_email',
        );
        $customers_news_por_bodegas_31y60days = $this->indicators_model->getCustomersPorBodega($customers_mail_news_31y60days, 'cust_email', $filters);
        
        /** New 0y30days **/
        $customers_mail_news_0y30days = array();
        if( ! empty($customers_mail_between0y30days) ){
            $temp_customers_mail_finds = array();
            foreach ($customers_mail_finds_0y30 as $email_find) {
                $temp_customers_mail_finds[$email_find] = $email_find;
            }
            foreach ($customers_mail_between0y30days as $_email) {
                if( isset($temp_customers_mail_finds[$_email]) ){
                    continue;
                }else{
                    $customers_mail_news_0y30days[] = $_email;
                }
            }
        }
        $filters = array(
        'orders_start_date' => $less_30_days_format,
        'orders_end_date' => $date_current_format,
        'orders_paid' => 1,
        'group_by' => 'cus.cust_email',
        );
        $customers_news_por_bodegas_0y30days = $this->indicators_model->getCustomersPorBodega($customers_mail_news_0y30days, 'cust_email', $filters);
        
        //print_a(count($customers_mail_between0y30days), count($customers_mail_finds_0y30), $customers_mail_news_0y30_count);
        
        $bodegas = $this->indicators_model->getBodegas();
        
        $customers_mail_between61y90days_count = intval(count($customers_mail_between61y90days));
        $customers_mail_between31y60days_count = intval(count($customers_mail_between31y60days));
        $customers_mail_between0y30days_count = intval(count($customers_mail_between0y30days));
        
        $indicator_clients_news_x_bodega = array();
        if (!empty($bodegas)) {
            foreach ($bodegas as $bodega_id => $bodega) {
                $news_count61y90 = isset($customers_news_por_bodegas_61y90days[$bodega_id]) && !empty($customers_news_por_bodegas_61y90days[$bodega_id]) ? count($customers_news_por_bodegas_61y90days[$bodega_id]) : 0;
                $news_percent61y90 = $customers_mail_between61y90days_count ? round((($news_count61y90 / $customers_mail_between61y90days_count) * 100), 1) : 0;
                
                $news_count31y60 = isset($customers_news_por_bodegas_31y60days[$bodega_id]) && !empty($customers_news_por_bodegas_31y60days[$bodega_id]) ? count($customers_news_por_bodegas_31y60days[$bodega_id]) : 0;
                $news_percent31y60 = $customers_mail_between31y60days_count ? round((($news_count31y60 / $customers_mail_between31y60days_count) * 100), 1) : 0;
                
                $news_count0y30 = isset($customers_news_por_bodegas_0y30days[$bodega_id]) && !empty($customers_news_por_bodegas_0y30days[$bodega_id]) ? count($customers_news_por_bodegas_0y30days[$bodega_id]) : 0;
                $news_percent0y30 = $customers_mail_between0y30days_count ? round((($news_count0y30 / $customers_mail_between0y30days_count) * 100), 1) : 0;
                
                $indicator_clients_news_x_bodega[$bodega_id]['nombre'] = $bodega['nombre'];
                $indicator_clients_news_x_bodega[$bodega_id]['61y90']['count'] = $news_count61y90;
                $indicator_clients_news_x_bodega[$bodega_id]['61y90']['percent'] = $news_percent61y90;
                
                $indicator_clients_news_x_bodega[$bodega_id]['31y60']['count'] = $news_count31y60;
                $indicator_clients_news_x_bodega[$bodega_id]['31y60']['percent'] = $news_percent31y60;
                
                $indicator_clients_news_x_bodega[$bodega_id]['0y30']['count'] = $news_count0y30;
                $indicator_clients_news_x_bodega[$bodega_id]['0y30']['percent'] = $news_percent0y30;
            }
        }
        
        /*
        hasta aqui solo toma 4 a 5 segundos
        $tiempo_final = microtime(true);
        $tiempo = $tiempo_final - $tiempo_inicial;
        echo $tiempo.' en segundos';
        exit;
        */
        
        //print_a(($customers_por_bodegas_61y90days));
        //print_a($indicator_clients_news_x_bodega);
        //exit;
        
        $totalCountNews = $customers_mail_news_0y30_count + $customers_mail_news_31y60_count + $customers_mail_news_61y90_count;
        
        $indicator_clients_news = array(
        'indicator_clients_news_x_bodega' => $indicator_clients_news_x_bodega,
        'total' => array(
        'countNews' => $totalCountNews,
        'percent' => ($customersCountAll ? round((($totalCountNews / $customersCountAll) * 100), 1) : 0),
        'totalCustomers' => $customersCountAll,
        ),
        );
        
        // INDICADORES % SKU DISPONIBLES E ASIGNADO POR BODEGAS
        
        $skus = $this->indicators_model->srchSku(array());
        $skus_count = count($skus);
        
        $filters = array('sku_available' => 1);
        $skus_available_por_bodegas = $this->indicators_model->getSkuPorBodega('id', array(), $filters);
        
        $indicator_skus = array();
        if (!empty($bodegas)) {
            foreach ($bodegas as $bodega_id => $bodega) {
                $count_skuxbodega = isset($skus_available_por_bodegas[$bodega_id]) && !empty($skus_available_por_bodegas[$bodega_id]) ? count($skus_available_por_bodegas[$bodega_id]) : 0;
                $percent = $skus_count ? round(($count_skuxbodega / $skus_count) * 100, 1) : 0;
                
                $indicator_skus['bodegas'][$bodega_id]['nombre'] = $bodega['nombre'];
                $indicator_skus['bodegas'][$bodega_id]['count'] = $count_skuxbodega;
                $indicator_skus['bodegas'][$bodega_id]['percent'] = $percent;
            }
        }
        
        $filters = array();
        $skus_total_por_bodegas = $this->indicators_model->getSkuPorBodega('id', array(), $filters);
        
        if (!empty($bodegas)) {
            foreach ($bodegas as $bodega_id => $bodega) {
                $count_skuxbodega = isset($skus_total_por_bodegas[$bodega_id]) && !empty($skus_total_por_bodegas[$bodega_id]) ? count($skus_total_por_bodegas[$bodega_id]) : 0;
                $percent = $skus_count ? round(($count_skuxbodega / $skus_count) * 100, 1) : 0;
                
                $indicator_skus['bodegas'][$bodega_id]['count_total'] = $count_skuxbodega;
                $indicator_skus['bodegas'][$bodega_id]['percent_total'] = $percent;
            }
        }
        
        $sku_notavailable = $this->indicators_model->srchSku(array('sku_available' => 0));
        $sku_notavailable_count = count($sku_notavailable);
        
        $skus_percent_disponible = round((($skus_count - $sku_notavailable_count) / $skus_count) * 100, 1);
        
        $indicator_skus['skus_percent_disponible'] = $skus_percent_disponible;
        
        // INDICADORES ALIMENTOS PERRO Y GATO
        
        $categorias = $this->products->getArbolCategories();
        
        $cats_perro = array();
        $cats_perro_adulto = array();
        $cats_perro_cachorro = array();
        $cats_perro_alimentos = array();
        $cats_perro_accesorio = array();
        $cats_perro_juguetes = array();
        $cats_perro_farmacia = array();
        
        $cats_perro_y_gato = array();
        
        $cats_gato = array();
        $cats_gato_farmacia = array();
        $cats_gato_accesorio = array();
        $cats_gato_juguetes = array();
        $cats_gato_adulto = array();
        $cats_gato_gatito = array();
        $cats_gato_alimentos = array();
        
        if (!empty($categorias)) {
            foreach ($categorias as $cat_id => $c) {
                $paths = explode('/', $c['path']);
                
                // Perro
                if (in_array(9, $paths)) {
                    $cats_perro_y_gato[] = $cat_id;
                }
                
                if (in_array(10, $paths)) {
                    $cats_perro_y_gato[] = $cat_id;
                }
                
                // Perro
                if (in_array(10, $paths)) {
                    $cats_perro[] = $cat_id;
                }
                
                /*
                Perro
                15 Alimentos
                5 Adultos
                6 Cahorros
                7 Necesidades especiales
                8 medicados
                48 Golosinas
                */
                
                // Perro - Adulto
                if (in_array(5, $paths)) {
                    $cats_perro_adulto[] = $cat_id;
                }
                
                // Perro - Cachorro
                if (in_array(6, $paths)) {
                    $cats_perro_cachorro[] = $cat_id;
                }
                
                // Perro - Necesidades especiales
                if (in_array(7, $paths)) {
                    $cats_perro_alimentos[] = $cat_id;
                }
                // Perro - Medicados
                if (in_array(8, $paths)) {
                    $cats_perro_alimentos[] = $cat_id;
                }
                // Perro - Golosinas
                if (in_array(48, $paths)) {
                    $cats_perro_alimentos[] = $cat_id;
                }
                
                // Perro - Accesorios
                if (in_array(19, $paths)) {
                    $cats_perro_accesorio[] = $cat_id;
                }
                
                // Perro - Juguetes
                if (in_array(21, $paths)) {
                    $cats_perro_juguetes[] = $cat_id;
                }
                
                // Perro - Farmacia
                if (in_array(17, $paths)) {
                    $cats_perro_farmacia[] = $cat_id;
                }
                
                // Gato
                if (in_array(9, $paths)) {
                    $cats_gato[] = $cat_id;
                }
                
                // Gato - Adulto
                if (in_array(1, $paths)) {
                    $cats_gato_adulto[] = $cat_id;
                }
                
                // Gato - gatitos
                if (in_array(2, $paths)) {
                    $cats_gato_gatito[] = $cat_id;
                }
                
                // Gato - Necesidades especiales
                if (in_array(3, $paths)) {
                    $cats_gato_alimentos[] = $cat_id;
                }
                
                // Gato - Medicados
                if (in_array(4, $paths)) {
                    $cats_gato_alimentos[] = $cat_id;
                }
                
                // Gato - golosinas
                if (in_array(103, $paths)) {
                    $cats_gato_alimentos[] = $cat_id;
                }
                
                // Gato - Farmacia
                if (in_array(18, $paths)) {
                    $cats_gato_farmacia[] = $cat_id;
                }
                
                // Gato - Accesorio
                if (in_array(20, $paths)) {
                    $cats_gato_accesorio[] = $cat_id;
                }
                
                // Gato - Juguetes
                if (in_array(22, $paths)) {
                    $cats_gato_juguetes[] = $cat_id;
                }
            }
        }
        
        $less_91_days = strtotime('-90 days');
        $less_91_days_format = date('Y-m-d', $less_91_days);
        
        $less_180_days = strtotime('-180 days');
        $less_180_days_format = date('Y-m-d', $less_180_days);
        
        // Total
        $srch = array(
        'orders_start_date' => $less_90_days_format,
        'orders_end_date' => $date_current_format,
        'orders_paid' => 1,
        //'paid' => 1,
        //'group_by' => 'cus.cust_email'
        );
        $customers_mail_between0y90days = $this->indicators_model->srchOrdersBuyByCategories($srch);
        
        $customers_mail_between0y90days_count = count($customers_mail_between0y90days);
        
        $order_cat_perro = array();
        $order_cat_perro_adulto = array();
        $order_cat_perro_cachorro = array();
        $order_cat_perro_alimentos = array();
        $order_cat_perro_accesorio = array();
        $order_cat_perro_farmacia = array();
        $order_cat_perro_juguetes = array();
        
        $order_cat_gato = array();
        $order_cat_gato_adulto = array();
        $order_cat_gato_gatito = array();
        $order_cat_gato_alimentos = array();
        $order_cat_gato_accesorio = array();
        $order_cat_gato_farmacia = array();
        $order_cat_gato_juguetes = array();
        
        $order_cat_perro_y_gato = array();
        
        $order_cat_perro_0y90days_valor_venta = 0;
        $order_cat_perro_adulto_0y90days_valor_venta = 0;
        $order_cat_perro_cachorro_0y90days_valor_venta = 0;
        $order_cat_perro_alimentos_0y90days_valor_venta = 0;
        $order_cat_perro_accesorio_0y90days_valor_venta = 0;
        $order_cat_perro_farmacia_0y90days_valor_venta = 0;
        $order_cat_perro_juguetes_0y90days_valor_venta = 0;
        
        $order_cat_gato_0y90days_valor_venta = 0;
        $order_cat_gato_adulto_0y90days_valor_venta = 0;
        $order_cat_gato_gatito_0y90days_valor_venta = 0;
        $order_cat_gato_alimentos_0y90days_valor_venta = 0;
        $order_cat_gato_accesorio_0y90days_valor_venta = 0;
        $order_cat_gato_farmacia_0y90days_valor_venta = 0;
        $order_cat_gato_juguetes_0y90days_valor_venta = 0;
        
        if (!empty($customers_mail_between0y90days)) {
            foreach ($customers_mail_between0y90days as $row) {
                
                // Perro
                if (in_array($row->cat_id, $cats_perro)) {
                    $order_cat_perro[] = $row;
                    $order_cat_perro_0y90days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                if (in_array($row->cat_id, $cats_perro_adulto)) {
                    $order_cat_perro_adulto[] = $row;
                    $order_cat_perro_adulto_0y90days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                if (in_array($row->cat_id, $cats_perro_cachorro)) {
                    $order_cat_perro_cachorro[] = $row;
                    $order_cat_perro_cachorro_0y90days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                if (in_array($row->cat_id, $cats_perro_alimentos)) {
                    $order_cat_perro_alimentos[] = $row;
                    $order_cat_perro_alimentos_0y90days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                if (in_array($row->cat_id, $cats_perro_accesorio)) {
                    $order_cat_perro_accesorio[] = $row;
                    $order_cat_perro_accesorio_0y90days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                if (in_array($row->cat_id, $cats_perro_farmacia)) {
                    $order_cat_perro_farmacia[] = $row;
                    $order_cat_perro_farmacia_0y90days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                if (in_array($row->cat_id, $cats_perro_juguetes)) {
                    $order_cat_perro_juguetes[] = $row;
                    $order_cat_perro_juguetes_0y90days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                // Gato
                if (in_array($row->cat_id, $cats_gato)) {
                    $order_cat_gato[] = $row;
                    $order_cat_gato_0y90days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                if (in_array($row->cat_id, $cats_gato_adulto)) {
                    $order_cat_gato_adulto[] = $row;
                    $order_cat_gato_adulto_0y90days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                if (in_array($row->cat_id, $cats_gato_gatito)) {
                    $order_cat_gato_gatito[] = $row;
                    $order_cat_gato_gatito_0y90days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                if (in_array($row->cat_id, $cats_gato_alimentos)) {
                    $order_cat_gato_alimentos[] = $row;
                    $order_cat_gato_alimentos_0y90days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                if (in_array($row->cat_id, $cats_gato_accesorio)) {
                    $order_cat_gato_accesorio[] = $row;
                    $order_cat_gato_accesorio_0y90days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                if (in_array($row->cat_id, $cats_gato_farmacia)) {
                    $order_cat_gato_farmacia[] = $row;
                    $order_cat_gato_farmacia_0y90days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                if (in_array($row->cat_id, $cats_gato_juguetes)) {
                    $order_cat_gato_juguetes[] = $row;
                    $order_cat_gato_juguetes_0y90days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                if (in_array($row->cat_id, $cats_gato) && in_array($row->cat_id, $cats_perro)) {
                    $order_cat_perro_y_gato[] = $row;
                }
            }
        }
        
        // Simplemente porque la relacion de productos con las categorias estan como perro y alimentos
        //print_a($cats_perro);
        //print_a(($cats_perro_adulto), ($cats_perro_cachorro), ($cats_perro_alimentos), ($cats_perro_accesorio), ($cats_perro_juguetes), ($cats_perro_farmacia));
        //print_a(count($order_cat_perro), count($order_cat_perro_adulto), count($cats_perro_cachorro), count($cats_perro_alimentos), count($cats_perro_accesorio), count($cats_perro_farmacia), count($cats_perro_juguetes));
        //exit;
        
        $order_cat_perro_0y90days_count = count($order_cat_perro);
        $order_cat_perro_adulto_0y90days_count = count($order_cat_perro_adulto);
        $order_cat_perro_cachorro_0y90days_count = count($order_cat_perro_cachorro);
        $order_cat_perro_alimentos_0y90days_count = count($order_cat_perro_alimentos);
        $order_cat_perro_accesorio_0y90days_count = count($order_cat_perro_accesorio);
        $order_cat_perro_farmacia_0y90days_count = count($order_cat_perro_farmacia);
        $order_cat_perro_juguetes_0y90days_count = count($order_cat_perro_juguetes);
        
        $order_cat_perro_0y90days_percent = $customers_mail_between0y90days_count ? round(($order_cat_perro_0y90days_count / $customers_mail_between0y90days_count) * 100, 1) : 0;
        
        $order_cat_perro_adulto_0y90days_percent = $customers_mail_between0y90days_count ? round(($order_cat_perro_adulto_0y90days_count / $customers_mail_between0y90days_count) * 100, 1) : 0;
        $order_cat_perro_cachorro_0y90days_percent = $customers_mail_between0y90days_count ? round(($order_cat_perro_cachorro_0y90days_count / $customers_mail_between0y90days_count) * 100, 1) : 0;
        $order_cat_perro_alimentos_0y90days_percent = $customers_mail_between0y90days_count ? round(($order_cat_perro_alimentos_0y90days_count / $customers_mail_between0y90days_count) * 100, 1) : 0;
        $order_cat_perro_accesorio_0y90days_percent = $customers_mail_between0y90days_count ? round(($order_cat_perro_accesorio_0y90days_count / $customers_mail_between0y90days_count) * 100, 1) : 0;
        $order_cat_perro_farmacia_0y90days_percent = $customers_mail_between0y90days_count ? round(($order_cat_perro_farmacia_0y90days_count / $customers_mail_between0y90days_count) * 100, 1) : 0;
        $order_cat_perro_juguetes_0y90days_percent = $customers_mail_between0y90days_count ? round(($order_cat_perro_juguetes_0y90days_count / $customers_mail_between0y90days_count) * 100, 1) : 0;
        
        $order_cat_gato_0y90days_count = count($order_cat_gato);
        $order_cat_gato_adulto_0y90days_count = count($order_cat_gato_adulto);
        $order_cat_gato_gatito_0y90days_count = count($order_cat_gato_gatito);
        $order_cat_gato_alimentos_0y90days_count = count($order_cat_gato_alimentos);
        $order_cat_gato_accesorio_0y90days_count = count($order_cat_gato_accesorio);
        $order_cat_gato_farmacia_0y90days_count = count($order_cat_gato_farmacia);
        $order_cat_gato_juguetes_0y90days_count = count($order_cat_gato_juguetes);
        
        $order_cat_gato_0y90days_percent = $customers_mail_between0y90days_count ? round(($order_cat_gato_0y90days_count / $customers_mail_between0y90days_count) * 100, 1) : 0;
        
        $order_cat_gato_adulto_0y90days_percent = $customers_mail_between0y90days_count ? round(($order_cat_gato_adulto_0y90days_count / $customers_mail_between0y90days_count) * 100, 1) : 0;
        $order_cat_gato_gatito_0y90days_percent = $customers_mail_between0y90days_count ? round(($order_cat_gato_gatito_0y90days_count / $customers_mail_between0y90days_count) * 100, 1) : 0;
        $order_cat_gato_alimentos_0y90days_percent = $customers_mail_between0y90days_count ? round(($order_cat_gato_alimentos_0y90days_count / $customers_mail_between0y90days_count) * 100, 1) : 0;
        $order_cat_gato_accesorio_0y90days_percent = $customers_mail_between0y90days_count ? round(($order_cat_gato_accesorio_0y90days_count / $customers_mail_between0y90days_count) * 100, 1) : 0;
        $order_cat_gato_farmacia_0y90days_percent = $customers_mail_between0y90days_count ? round(($order_cat_gato_farmacia_0y90days_count / $customers_mail_between0y90days_count) * 100, 1) : 0;
        $order_cat_gato_juguetes_0y90days_percent = $customers_mail_between0y90days_count ? round(($order_cat_gato_juguetes_0y90days_count / $customers_mail_between0y90days_count) * 100, 1) : 0;
        
        // 91 Y 180 DAYS
        $less_91_days = strtotime('-90 days');
        $less_91_days_format = date('Y-m-d', $less_91_days);
        
        $less_180_days = strtotime('-180 days');
        $less_180_days_format = date('Y-m-d', $less_180_days);
        
        // Total
        $srch = array(
        'orders_start_date' => $less_180_days_format,
        'orders_end_date' => $less_91_days_format,
        'orders_paid' => 1,
        //'paid' => 1,
        //'group_by' => 'cus.cust_email'
        );
        $customers_mail_between91y180days = $this->indicators_model->srchOrdersBuyByCategories($srch);
        
        $customers_mail_between91y180days_count = count($customers_mail_between91y180days);
        
        $order_cat_perro = array();
        $order_cat_perro_adulto = array();
        $order_cat_perro_cachorro = array();
        $order_cat_perro_alimentos = array();
        $order_cat_perro_accesorio = array();
        $order_cat_perro_farmacia = array();
        $order_cat_perro_juguetes = array();
        
        $order_cat_gato = array();
        $order_cat_gato_adulto = array();
        $order_cat_gato_gatito = array();
        $order_cat_gato_alimentos = array();
        $order_cat_gato_accesorio = array();
        $order_cat_gato_farmacia = array();
        $order_cat_gato_juguetes = array();
        
        $order_cat_perro_y_gato = array();
        
        $order_cat_perro_90y180days_valor_venta = 0;
        $order_cat_perro_adulto_90y180days_valor_venta = 0;
        $order_cat_perro_cachorro_90y180days_valor_venta = 0;
        $order_cat_perro_alimentos_90y180days_valor_venta = 0;
        $order_cat_perro_accesorio_90y180days_valor_venta = 0;
        $order_cat_perro_farmacia_90y180days_valor_venta = 0;
        $order_cat_perro_juguetes_90y180days_valor_venta = 0;
        
        $order_cat_gato_90y180days_valor_venta = 0;
        $order_cat_gato_adulto_90y180days_valor_venta = 0;
        $order_cat_gato_gatito_90y180days_valor_venta = 0;
        $order_cat_gato_alimentos_90y180days_valor_venta = 0;
        $order_cat_gato_accesorio_90y180days_valor_venta = 0;
        $order_cat_gato_farmacia_90y180days_valor_venta = 0;
        $order_cat_gato_juguetes_90y180days_valor_venta = 0;
        
        
        if (!empty($customers_mail_between91y180days)) {
            foreach ($customers_mail_between91y180days as $row) {
                // Perro
                if (in_array($row->cat_id, $cats_perro)) {
                    $order_cat_perro[] = $row;
                    $order_cat_perro_90y180days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                if (in_array($row->cat_id, $cats_perro_adulto)) {
                    $order_cat_perro_adulto[] = $row;
                    $order_cat_perro_adulto_90y180days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                    
                }
                
                if (in_array($row->cat_id, $cats_perro_cachorro)) {
                    $order_cat_perro_cachorro[] = $row;
                    $order_cat_perro_cachorro_90y180days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                if (in_array($row->cat_id, $cats_perro_alimentos)) {
                    $order_cat_perro_alimentos[] = $row;
                    $order_cat_perro_alimentos_90y180days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                if (in_array($row->cat_id, $cats_perro_accesorio)) {
                    $order_cat_perro_accesorio[] = $row;
                    $order_cat_perro_accesorio_90y180days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                if (in_array($row->cat_id, $cats_perro_farmacia)) {
                    $order_cat_perro_farmacia[] = $row;
                    $order_cat_perro_farmacia_90y180days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                if (in_array($row->cat_id, $cats_perro_juguetes)) {
                    $order_cat_perro_juguetes[] = $row;
                    $order_cat_perro_juguetes_90y180days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                // Gato
                if (in_array($row->cat_id, $cats_gato)) {
                    $order_cat_gato[] = $row;
                    $order_cat_gato_90y180days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                if (in_array($row->cat_id, $cats_gato_adulto)) {
                    $order_cat_gato_adulto[] = $row;
                    $order_cat_gato_adulto_90y180days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                if (in_array($row->cat_id, $cats_gato_gatito)) {
                    $order_cat_gato_gatito[] = $row;
                    $order_cat_gato_gatito_90y180days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                if (in_array($row->cat_id, $cats_gato_alimentos)) {
                    $order_cat_gato_alimentos[] = $row;
                    $order_cat_gato_alimentos_90y180days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                if (in_array($row->cat_id, $cats_gato_accesorio)) {
                    $order_cat_gato_accesorio[] = $row;
                    $order_cat_gato_accesorio_90y180days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                if (in_array($row->cat_id, $cats_gato_farmacia)) {
                    $order_cat_gato_farmacia[] = $row;
                    $order_cat_gato_farmacia_90y180days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                if (in_array($row->cat_id, $cats_gato_juguetes)) {
                    $order_cat_gato_juguetes[] = $row;
                    $order_cat_gato_juguetes_90y180days_valor_venta += $row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc;
                }
                
                if (in_array($row->cat_id, $cats_gato) && in_array($row->cat_id, $cats_perro)) {
                    $order_cat_perro_y_gato[] = $row;
                }
            }
        }
        
        $order_cat_perro_90y180days_count = count($order_cat_perro);
        $order_cat_perro_adulto_90y180days_count = count($order_cat_perro_adulto);
        $order_cat_perro_cachorro_90y180days_count = count($order_cat_perro_cachorro);
        $order_cat_perro_alimentos_90y180days_count = count($order_cat_perro_alimentos);
        $order_cat_perro_accesorio_90y180days_count = count($order_cat_perro_accesorio);
        $order_cat_perro_farmacia_90y180days_count = count($order_cat_perro_farmacia);
        $order_cat_perro_juguetes_90y180days_count = count($order_cat_perro_juguetes);
        
        $order_cat_perro_adulto_90y180days_percent = $customers_mail_between91y180days_count ? round(($order_cat_perro_adulto_90y180days_count / $customers_mail_between91y180days_count) * 100, 1) : 0;
        $order_cat_perro_cachorro_90y180days_percent = $customers_mail_between91y180days_count ? round(($order_cat_perro_cachorro_90y180days_count / $customers_mail_between91y180days_count) * 100, 1) : 0;
        $order_cat_perro_alimentos_90y180days_percent = $customers_mail_between91y180days_count ? round(($order_cat_perro_alimentos_90y180days_count / $customers_mail_between91y180days_count) * 100, 1) : 0;
        $order_cat_perro_accesorio_90y180days_percent = $customers_mail_between91y180days_count ? round(($order_cat_perro_accesorio_90y180days_count / $customers_mail_between91y180days_count) * 100, 1) : 0;
        $order_cat_perro_farmacia_90y180days_percent = $customers_mail_between91y180days_count ? round(($order_cat_perro_farmacia_90y180days_count / $customers_mail_between91y180days_count) * 100, 1) : 0;
        $order_cat_perro_juguetes_90y180days_percent = $customers_mail_between91y180days_count ? round(($order_cat_perro_juguetes_90y180days_count / $customers_mail_between91y180days_count) * 100, 1) : 0;
        
        $order_cat_gato_90y180days_count = count($order_cat_gato);
        $order_cat_gato_adulto_90y180days_count = count($order_cat_gato_adulto);
        $order_cat_gato_gatito_90y180days_count = count($order_cat_gato_gatito);
        $order_cat_gato_alimentos_90y180days_count = count($order_cat_gato_alimentos);
        $order_cat_gato_accesorio_90y180days_count = count($order_cat_gato_accesorio);
        $order_cat_gato_farmacia_90y180days_count = count($order_cat_gato_farmacia);
        $order_cat_gato_juguetes_90y180days_count = count($order_cat_gato_juguetes);
        
        $order_cat_gato_adulto_90y180days_percent = $customers_mail_between91y180days_count ? round(($order_cat_gato_adulto_90y180days_count / $customers_mail_between91y180days_count) * 100, 1) : 0;
        $order_cat_gato_gatito_90y180days_percent = $customers_mail_between91y180days_count ? round(($order_cat_gato_gatito_90y180days_count / $customers_mail_between91y180days_count) * 100, 1) : 0;
        $order_cat_gato_alimentos_90y180days_percent = $customers_mail_between91y180days_count ? round(($order_cat_gato_alimentos_90y180days_count / $customers_mail_between91y180days_count) * 100, 1) : 0;
        $order_cat_gato_accesorio_90y180days_percent = $customers_mail_between91y180days_count ? round(($order_cat_gato_accesorio_90y180days_count / $customers_mail_between91y180days_count) * 100, 1) : 0;
        $order_cat_gato_farmacia_90y180days_percent = $customers_mail_between91y180days_count ? round(($order_cat_gato_farmacia_90y180days_count / $customers_mail_between91y180days_count) * 100, 1) : 0;
        $order_cat_gato_juguetes_90y180days_percent = $customers_mail_between91y180days_count ? round(($order_cat_gato_juguetes_90y180days_count / $customers_mail_between91y180days_count) * 100, 1) : 0;
        
        $indicators_perro = array(
        'total' => array(
        'count' => $order_cat_perro_0y90days_count,
        'percent' => $order_cat_perro_0y90days_percent,
        ),
        'categorias' => array(
        array(
        'nombre' => 'Adulto',
        '0y90days' => array(
        'count' => $order_cat_perro_adulto_0y90days_count,
        'percent' => $order_cat_perro_adulto_0y90days_percent,
        'monto' => $order_cat_perro_adulto_0y90days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_perro_adulto_0y90days_valor_venta)
        ),
        '90y180days' => array(
        'count' => $order_cat_perro_adulto_90y180days_count,
        'percent' => $order_cat_perro_adulto_90y180days_percent,
        'monto' => $order_cat_perro_adulto_90y180days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_perro_adulto_90y180days_valor_venta)
        ),
        ),
        array(
        'nombre' => 'Cachorro',
        '0y90days' => array(
        'count' => $order_cat_perro_cachorro_0y90days_count,
        'percent' => $order_cat_perro_cachorro_0y90days_percent,
        'monto' => $order_cat_perro_cachorro_0y90days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_perro_cachorro_0y90days_valor_venta)
        ),
        '90y180days' => array(
        'count' => $order_cat_perro_cachorro_90y180days_count,
        'percent' => $order_cat_perro_cachorro_90y180days_percent,
        'monto' => $order_cat_perro_cachorro_90y180days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_perro_cachorro_90y180days_valor_venta)
        ),
        ),
        array(
        'nombre' => 'Alimentos',
        '0y90days' => array(
        'count' => $order_cat_perro_alimentos_0y90days_count,
        'percent' => $order_cat_perro_alimentos_0y90days_percent,
        'monto' => $order_cat_perro_alimentos_0y90days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_perro_alimentos_0y90days_valor_venta)
        ),
        '90y180days' => array(
        'count' => $order_cat_perro_alimentos_90y180days_count,
        'percent' => $order_cat_perro_alimentos_90y180days_percent,
        'monto' => $order_cat_perro_alimentos_90y180days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_perro_alimentos_90y180days_valor_venta)
        ),
        ),
        array(
        'nombre' => 'Accesorio',
        '0y90days' => array(
        'count' => $order_cat_perro_accesorio_0y90days_count,
        'percent' => $order_cat_perro_accesorio_0y90days_percent,
        'monto' => $order_cat_perro_accesorio_0y90days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_perro_accesorio_0y90days_valor_venta)
        ),
        '90y180days' => array(
        'count' => $order_cat_perro_accesorio_90y180days_count,
        'percent' => $order_cat_perro_accesorio_90y180days_percent,
        'monto' => $order_cat_perro_accesorio_90y180days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_perro_accesorio_90y180days_valor_venta)
        ),
        ),
        array(
        'nombre' => 'Farmacia',
        '0y90days' => array(
        'count' => $order_cat_perro_farmacia_0y90days_count,
        'percent' => $order_cat_perro_farmacia_0y90days_percent,
        'monto' => $order_cat_perro_farmacia_0y90days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_perro_farmacia_0y90days_valor_venta)
        ),
        '90y180days' => array(
        'count' => $order_cat_perro_farmacia_90y180days_count,
        'percent' => $order_cat_perro_farmacia_90y180days_percent,
        'monto' => $order_cat_perro_farmacia_90y180days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_perro_farmacia_90y180days_valor_venta)
        ),
        ),
        array(
        'nombre' => 'Juguetes',
        '0y90days' => array(
        'count' => $order_cat_perro_juguetes_0y90days_count,
        'percent' => $order_cat_perro_juguetes_0y90days_percent,
        'monto' => $order_cat_perro_juguetes_0y90days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_perro_juguetes_0y90days_valor_venta)
        ),
        '90y180days' => array(
        'count' => $order_cat_perro_juguetes_90y180days_count,
        'percent' => $order_cat_perro_juguetes_90y180days_percent,
        'monto' => $order_cat_perro_juguetes_90y180days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_perro_juguetes_90y180days_valor_venta)
        ),
        ),
        ),
        );
        
        $indicators_gato = array(
        'total' => array(
        'count' => $order_cat_gato_0y90days_count,
        'percent' => $order_cat_gato_0y90days_percent,
        ),
        'categorias' => array(
        array(
        'nombre' => 'Adulto',
        '0y90days' => array(
        'count' => $order_cat_gato_adulto_0y90days_count,
        'percent' => $order_cat_gato_adulto_0y90days_percent,
        'monto' => $order_cat_gato_adulto_0y90days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_gato_adulto_0y90days_valor_venta)
        ),
        '90y180days' => array(
        'count' => $order_cat_gato_adulto_90y180days_count,
        'percent' => $order_cat_gato_adulto_90y180days_percent,
        'monto' => $order_cat_gato_adulto_90y180days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_gato_adulto_90y180days_valor_venta)
        ),
        ),
        array(
        'nombre' => 'Gatito',
        '0y90days' => array(
        'count' => $order_cat_gato_gatito_0y90days_count,
        'percent' => $order_cat_gato_gatito_0y90days_percent,
        'monto' => $order_cat_gato_gatito_0y90days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_gato_gatito_0y90days_valor_venta)
        ),
        '90y180days' => array(
        'count' => $order_cat_gato_gatito_90y180days_count,
        'percent' => $order_cat_gato_gatito_90y180days_percent,
        'monto' => $order_cat_gato_gatito_90y180days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_gato_gatito_90y180days_valor_venta)
        ),
        ),
        array(
        'nombre' => 'Alimentos',
        '0y90days' => array(
        'count' => $order_cat_gato_alimentos_0y90days_count,
        'percent' => $order_cat_gato_alimentos_0y90days_percent,
        'monto' => $order_cat_gato_alimentos_0y90days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_gato_alimentos_0y90days_valor_venta)
        ),
        '90y180days' => array(
        'count' => $order_cat_gato_alimentos_90y180days_count,
        'percent' => $order_cat_gato_alimentos_90y180days_percent,
        'monto' => $order_cat_gato_alimentos_90y180days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_gato_alimentos_90y180days_valor_venta)
        ),
        ),
        array(
        'nombre' => 'Accesorio',
        '0y90days' => array(
        'count' => $order_cat_gato_accesorio_0y90days_count,
        'percent' => $order_cat_gato_accesorio_0y90days_percent,
        'monto' => $order_cat_gato_accesorio_0y90days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_gato_accesorio_0y90days_valor_venta)
        ),
        '90y180days' => array(
        'count' => $order_cat_gato_accesorio_90y180days_count,
        'percent' => $order_cat_gato_accesorio_90y180days_percent,
        'monto' => $order_cat_gato_accesorio_90y180days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_gato_accesorio_90y180days_valor_venta)
        ),
        ),
        array(
        'nombre' => 'Farmacia',
        '0y90days' => array(
        'count' => $order_cat_gato_farmacia_0y90days_count,
        'percent' => $order_cat_gato_farmacia_0y90days_percent,
        'monto' => $order_cat_gato_farmacia_0y90days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_gato_farmacia_0y90days_valor_venta)
        ),
        '90y180days' => array(
        'count' => $order_cat_gato_farmacia_90y180days_count,
        'percent' => $order_cat_gato_farmacia_90y180days_percent,
        'monto' => $order_cat_gato_farmacia_90y180days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_gato_farmacia_90y180days_valor_venta)
        ),
        ),
        array(
        'nombre' => 'Juguetes',
        '0y90days' => array(
        'count' => $order_cat_gato_juguetes_0y90days_count,
        'percent' => $order_cat_gato_juguetes_0y90days_percent,
        'monto' => $order_cat_gato_juguetes_0y90days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_gato_juguetes_0y90days_valor_venta)
        ),
        '90y180days' => array(
        'count' => $order_cat_gato_juguetes_90y180days_count,
        'percent' => $order_cat_gato_juguetes_90y180days_percent,
        'monto' => $order_cat_gato_juguetes_90y180days_valor_venta,
        'monto_formato' => $this->utils->numformat($order_cat_gato_juguetes_90y180days_valor_venta)
        ),
        ),
        ),
        );
        
        // INDICADORES POR RETIRO LOCAL Y POR DESPACHO
        
        $less_30_days = strtotime('-30 days');
        $less_31_days = strtotime('-31 days');
        $less_60_days = strtotime('-60 days');
        $less_61_days = strtotime('-61 days');
        $less_90_days = strtotime('-90 days');
        $less_91_days = strtotime('-91 days');
        $less_120_days = strtotime('-120 days');
        
        $less_30_days_format = date('Y-m-d', $less_30_days);
        $less_31_days_format = date('Y-m-d', $less_31_days);
        $less_60_days_format = date('Y-m-d', $less_60_days);
        $less_61_days_format = date('Y-m-d', $less_61_days);
        $less_90_days_format = date('Y-m-d', $less_90_days);
        $less_91_days_format = date('Y-m-d', $less_91_days);
        $less_120_days_format = date('Y-m-d', $less_120_days);
        
        $locales_ids_all = array();
        $bodegas_ids_all = array();
        
        $srch = array(
        'orders_start_date' => $less_30_days_format,
        'orders_end_date' => $date_current_format,
        'orders_paid' => 1,
        //'group_by' => 'cus.cust_email'
        );
        $orders_0y30days = $this->indicators_model->srchOrdersBuy($srch);
        
        $orders_0y30days_count = count($orders_0y30days);
        
        $orders_x_bodega_0y30days_count = 0;
        $orders_x_local_0y30days_count = 0;
        $order_x_bodegas_0y30days = array();
        if (!empty($orders_0y30days)) {
            foreach ($orders_0y30days as $row) {
                //if( in_array($row->bodega_id, array(11, 12) )) continue;
                if ($row->addr_local_id == 0 || is_null($row->addr_local_id)) {
                    $order_x_bodegas_0y30days['despacho'][$row->bodega_id]['orders'][] = $row;
                    $order_x_bodegas_0y30days['despacho'][$row->bodega_id]['monto'] = isset($order_x_bodegas_0y30days['despacho'][$row->bodega_id]['monto']) ? $order_x_bodegas_0y30days['despacho'][$row->bodega_id]['monto'] + ($row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc) : ($row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc);
                    $bodegas_ids_all[$row->bodega_id] = $row->bodega_id;
                    $orders_x_bodega_0y30days_count++;
                } elseif ($row->bodega_id == 12 || ($row->addr_local_id != 0 && ! is_null($row->addr_local_id) ) ) {
                    $order_x_bodegas_0y30days['retiro_local'][$row->addr_local_id]['count'] = isset($order_x_bodegas_0y30days['retiro_local'][$row->addr_local_id]['count']) ? intval($order_x_bodegas_0y30days['retiro_local'][$row->addr_local_id]['count']) + 1 : 1;
                    $order_x_bodegas_0y30days['retiro_local'][$row->addr_local_id]['monto'] = isset($order_x_bodegas_0y30days['retiro_local'][$row->addr_local_id]['monto']) ? $order_x_bodegas_0y30days['retiro_local'][$row->addr_local_id]['monto'] + ($row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc) : ($row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc);
                    $locales_ids_all[$row->addr_local_id] = $row->addr_local_id;
                    $orders_x_local_0y30days_count++;
                }
            }
        }
        
        $srch = array(
        'orders_start_date' => $less_60_days_format,
        'orders_end_date' => $less_31_days_format,
        'orders_paid' => 1,
        //'group_by' => 'cus.cust_email'
        );
        $orders_30y60days = $this->indicators_model->srchOrdersBuy($srch);
        
        $orders_30y60days_count = count($orders_30y60days);
        
        $orders_x_bodega_30y60days_count = 0;
        $orders_x_local_30y60days_count = 0;
        $order_x_bodegas_30y60days = array();
        if (!empty($orders_30y60days)) {
            foreach ($orders_30y60days as $row) {
                if ($row->addr_local_id == 0 || is_null($row->addr_local_id)) {
                    $order_x_bodegas_30y60days['despacho'][$row->bodega_id]['orders'][] = $row;
                    $order_x_bodegas_30y60days['despacho'][$row->bodega_id]['monto'] = isset($order_x_bodegas_30y60days['despacho'][$row->bodega_id]['monto']) ? $order_x_bodegas_30y60days['despacho'][$row->bodega_id]['monto'] + ($row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc) : ($row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc);
                    $bodegas_ids_all[$row->bodega_id] = $row->bodega_id;
                    $orders_x_bodega_30y60days_count++;
                //} elseif ($row->bodega_id == 12) {
                } elseif ($row->bodega_id == 12 || ($row->addr_local_id != 0 && ! is_null($row->addr_local_id) ) ) {
                    $order_x_bodegas_30y60days['retiro_local'][$row->addr_local_id]['count'] = isset($order_x_bodegas_30y60days['retiro_local'][$row->addr_local_id]['count']) ? intval($order_x_bodegas_30y60days['retiro_local'][$row->addr_local_id]['count']) + 1 : 1;
                    $order_x_bodegas_30y60days['retiro_local'][$row->addr_local_id]['monto'] = isset($order_x_bodegas_30y60days['retiro_local'][$row->addr_local_id]['monto']) ? $order_x_bodegas_30y60days['retiro_local'][$row->addr_local_id]['monto'] + ($row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc) : ($row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc);
                    $locales_ids_all[$row->addr_local_id] = $row->addr_local_id;
                    $orders_x_local_30y60days_count++;
                }
            }
        }
        
        $srch = array(
        'orders_start_date' => $less_90_days_format,
        'orders_end_date' => $less_61_days_format,
        'orders_paid' => 1,
        //'group_by' => 'cus.cust_email'
        );
        $orders_60y90days = $this->indicators_model->srchOrdersBuy($srch);
        
        $orders_60y90days_count = count($orders_60y90days);
        
        $orders_x_bodega_60y90days_count = 0;
        $orders_x_local_60y90days_count = 0;
        $order_x_bodegas_60y90days = array();
        if (!empty($orders_60y90days)) {
            foreach ($orders_60y90days as $row) {
                if ($row->addr_local_id == 0 || is_null($row->addr_local_id)) {
                    $order_x_bodegas_60y90days['despacho'][$row->bodega_id]['orders'][] = $row;
                    $order_x_bodegas_60y90days['despacho'][$row->bodega_id]['monto'] = isset($order_x_bodegas_60y90days['despacho'][$row->bodega_id]['monto']) ? $order_x_bodegas_60y90days['despacho'][$row->bodega_id]['monto'] + ($row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc) : ($row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc);
                    $bodegas_ids_all[$row->bodega_id] = $row->bodega_id;
                    $orders_x_bodega_60y90days_count++;
                //} elseif ($row->bodega_id == 12) {
                } elseif ($row->bodega_id == 12 || ($row->addr_local_id != 0 && ! is_null($row->addr_local_id) ) ) {
                    $order_x_bodegas_60y90days['retiro_local'][$row->addr_local_id]['count'] = isset($order_x_bodegas_60y90days['retiro_local'][$row->addr_local_id]['count']) ? intval($order_x_bodegas_60y90days['retiro_local'][$row->addr_local_id]['count']) + 1 : 1;
                    $order_x_bodegas_60y90days['retiro_local'][$row->addr_local_id]['monto'] = isset($order_x_bodegas_60y90days['retiro_local'][$row->addr_local_id]['monto']) ? $order_x_bodegas_60y90days['retiro_local'][$row->addr_local_id]['monto'] + ($row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc) : ($row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc);
                    $locales_ids_all[$row->addr_local_id] = $row->addr_local_id;
                    $orders_x_local_60y90days_count++;
                }
            }
        }
        
        $srch = array(
        'orders_start_date' => $less_120_days_format,
        'orders_end_date' => $less_91_days_format,
        'orders_paid' => 1,
        //'group_by' => 'cus.cust_email'
        );
        $orders_90y120days = $this->indicators_model->srchOrdersBuy($srch);
        $orders_90y120days_count = count($orders_90y120days);
        
        $orders_x_bodega_90y120days_count = 0;
        $orders_x_local_90y120days_count = 0;
        $order_x_bodegas_90y120days = array();
        if (!empty($orders_90y120days)) {
            foreach ($orders_90y120days as $row) {
                //if( in_array($row->bodega_id, array(11, 12) )) continue;
                if ($row->addr_local_id == 0 || is_null($row->addr_local_id)) {
                    $order_x_bodegas_90y120days['despacho'][$row->bodega_id]['orders'][] = $row;
                    $order_x_bodegas_90y120days['despacho'][$row->bodega_id]['monto'] = isset($order_x_bodegas_90y120days['despacho'][$row->bodega_id]['monto']) ? $order_x_bodegas_90y120days['despacho'][$row->bodega_id]['monto'] + ($row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc) : ($row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc);
                    $bodegas_ids_all[$row->bodega_id] = $row->bodega_id;
                    $orders_x_bodega_90y120days_count++;
                //} elseif ($row->bodega_id == 12) {
                } elseif ($row->bodega_id == 12 || ($row->addr_local_id != 0 && ! is_null($row->addr_local_id) ) ) {
                    $order_x_bodegas_90y120days['retiro_local'][$row->addr_local_id]['count'] = isset($order_x_bodegas_90y120days['retiro_local'][$row->addr_local_id]['count']) ? intval($order_x_bodegas_90y120days['retiro_local'][$row->addr_local_id]['count']) + 1 : 1;
                    $order_x_bodegas_90y120days['retiro_local'][$row->addr_local_id]['monto'] = isset($order_x_bodegas_90y120days['retiro_local'][$row->addr_local_id]['monto']) ? $order_x_bodegas_90y120days['retiro_local'][$row->addr_local_id]['monto'] + ($row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc) : ($row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc);
                    $locales_ids_all[$row->addr_local_id] = $row->addr_local_id;
                    $orders_x_local_90y120days_count++;
                }
            }
        }
        
        $locales = $this->indicators_model->getLocales();
        
        $indicators_order_x_local = array();
        if (!empty($locales)) {
            foreach ($locales_ids_all as $local_id => $l) {
                $count0y30 = isset($order_x_bodegas_0y30days['retiro_local'][$local_id]['count']) && $order_x_bodegas_0y30days['retiro_local'][$local_id]['count'] ? $order_x_bodegas_0y30days['retiro_local'][$local_id]['count'] : 0;
                $percent0y30 = $orders_0y30days_count ? round((($count0y30 / $orders_0y30days_count) * 100), 1) : 0;
                
                $count30y60 = isset($order_x_bodegas_30y60days['retiro_local'][$local_id]['count']) && $order_x_bodegas_30y60days['retiro_local'][$local_id]['count'] ? $order_x_bodegas_30y60days['retiro_local'][$local_id]['count'] : 0;
                $percent30y60 = $orders_30y60days_count ? round((($count30y60 / $orders_30y60days_count) * 100), 1) : 0;
                
                $count60y90 = isset($order_x_bodegas_60y90days['retiro_local'][$local_id]['count']) && $order_x_bodegas_60y90days['retiro_local'][$local_id]['count'] ? $order_x_bodegas_60y90days['retiro_local'][$local_id]['count'] : 0;
                $percent60y90 = $orders_60y90days_count ? round((($count60y90 / $orders_60y90days_count) * 100), 1) : 0;
                
                $count90y120 = isset($order_x_bodegas_90y120days['retiro_local'][$local_id]['count']) && $order_x_bodegas_90y120days['retiro_local'][$local_id]['count'] ? $order_x_bodegas_90y120days['retiro_local'][$local_id]['count'] : 0;
                $percent90y120 = $orders_90y120days_count ? round((($count90y120 / $orders_90y120days_count) * 100), 1) : 0;
                
                $indicators_order_x_local['locales'][$local_id]['nombre'] = isset($locales[$local_id]['alias']) ? $locales[$local_id]['alias'] : 'local-id-'.$local_id;
                
                $indicators_order_x_local['locales'][$local_id]['0y30']['count'] = $count0y30;
                $indicators_order_x_local['locales'][$local_id]['0y30']['percent'] = $percent0y30;
                $indicators_order_x_local['locales'][$local_id]['0y30']['monto'] = isset($order_x_bodegas_0y30days['retiro_local'][$local_id]['monto']) ? $order_x_bodegas_0y30days['retiro_local'][$local_id]['monto'] : 0;
                $indicators_order_x_local['locales'][$local_id]['0y30']['monto_formato'] = isset($order_x_bodegas_0y30days['retiro_local'][$local_id]['monto']) ? $this->utils->numformat($order_x_bodegas_0y30days['retiro_local'][$local_id]['monto']) : 0;
                
                $indicators_order_x_local['locales'][$local_id]['30y60']['count'] = $count30y60;
                $indicators_order_x_local['locales'][$local_id]['30y60']['percent'] = $percent30y60;
                $indicators_order_x_local['locales'][$local_id]['30y60']['monto'] = isset($order_x_bodegas_30y60days['retiro_local'][$local_id]['monto']) ? $order_x_bodegas_30y60days['retiro_local'][$local_id]['monto'] : 0;
                $indicators_order_x_local['locales'][$local_id]['30y60']['monto_formato'] = isset($order_x_bodegas_30y60days['retiro_local'][$local_id]['monto']) ? $this->utils->numformat($order_x_bodegas_30y60days['retiro_local'][$local_id]['monto']) : 0;
                
                $indicators_order_x_local['locales'][$local_id]['60y90']['count'] = $count60y90;
                $indicators_order_x_local['locales'][$local_id]['60y90']['percent'] = $percent60y90;
                $indicators_order_x_local['locales'][$local_id]['60y90']['monto'] = isset($order_x_bodegas_60y90days['retiro_local'][$local_id]['monto']) ? $order_x_bodegas_60y90days['retiro_local'][$local_id]['monto'] : 0;
                $indicators_order_x_local['locales'][$local_id]['60y90']['monto_formato'] = isset($order_x_bodegas_60y90days['retiro_local'][$local_id]['monto']) ? $this->utils->numformat($order_x_bodegas_60y90days['retiro_local'][$local_id]['monto']) : 0;
                
                $indicators_order_x_local['locales'][$local_id]['90y120']['count'] = $count90y120;
                $indicators_order_x_local['locales'][$local_id]['90y120']['percent'] = $percent90y120;
                $indicators_order_x_local['locales'][$local_id]['90y120']['monto'] = isset($order_x_bodegas_90y120days['retiro_local'][$local_id]['monto']) ? $order_x_bodegas_90y120days['retiro_local'][$local_id]['monto'] : 0;
                $indicators_order_x_local['locales'][$local_id]['90y120']['monto_formato'] = isset($order_x_bodegas_90y120days['retiro_local'][$local_id]['monto']) ? $this->utils->numformat($order_x_bodegas_90y120days['retiro_local'][$local_id]['monto']) : 0;
            }
        }
        
        $indicators_order_x_retiro = array(
        '0y30days' => array(
        'count' => $orders_x_local_0y30days_count,
        'percent' => $orders_0y30days_count ? round(($orders_x_local_0y30days_count / $orders_0y30days_count) * 100, 1) : 0,
        ),
        'indicators_order_x_local' => $indicators_order_x_local
        /*
        '30y60days' => array(
        'count' => $order_x_bodegas_30y60days['retiro_local']['count'],
        'percent' => $orders_30y60days_count ? round(($order_x_bodegas_30y60days['retiro_local']['count'] / $orders_30y60days_count) * 100, 1) : 0,
        ),
        '60y90days' => array(
        'count' => $order_x_bodegas_60y90days['retiro_local']['count'],
        'percent' => $orders_60y90days_count ? round(($order_x_bodegas_60y90days['retiro_local']['count'] / $orders_60y90days_count) * 100, 1) : 0,
        ),
        '90y120days' => array(
        'count' => $order_x_bodegas_90y120days['retiro_local']['count'],
        'percent' => $orders_90y120days_count ? round(($order_x_bodegas_90y120days['retiro_local']['count'] / $orders_90y120days_count) * 100, 1) : 0,
        )
        */
        );
        
        $indicators_order_x_despacho = array();
        
        $sum_0y30days_count = 0;
        if (!empty($bodegas_ids_all)) {
            foreach ($bodegas_ids_all as $bodega_id => $b) {
                
                if( in_array($bodega_id, array(11, 12) )) continue;
                
                $count0y30 = isset($order_x_bodegas_0y30days['despacho'][$bodega_id]['orders']) && !empty($order_x_bodegas_0y30days['despacho'][$bodega_id]['orders']) ? count($order_x_bodegas_0y30days['despacho'][$bodega_id]['orders']) : 0;
                $percent0y30 = $orders_0y30days_count ? round((($count0y30 / $orders_0y30days_count) * 100), 1) : 0;
                
                $count30y60 = isset($order_x_bodegas_30y60days['despacho'][$bodega_id]['orders']) && !empty($order_x_bodegas_30y60days['despacho'][$bodega_id]['orders']) ? count($order_x_bodegas_30y60days['despacho'][$bodega_id]['orders']) : 0;
                $percent30y60 = $orders_30y60days_count ? round((($count30y60 / $orders_30y60days_count) * 100), 1) : 0;
                
                $count60y90 = isset($order_x_bodegas_60y90days['despacho'][$bodega_id]['orders']) && !empty($order_x_bodegas_60y90days['despacho'][$bodega_id]['orders']) ? count($order_x_bodegas_60y90days['despacho'][$bodega_id]['orders']) : 0;
                $percent60y90 = $orders_60y90days_count ? round((($count60y90 / $orders_60y90days_count) * 100), 1) : 0;
                
                $count90y120 = isset($order_x_bodegas_90y120days['despacho'][$bodega_id]['orders']) && !empty($order_x_bodegas_90y120days['despacho'][$bodega_id]['orders']) ? count($order_x_bodegas_90y120days['despacho'][$bodega_id]['orders']) : 0;
                $percent90y120 = $orders_90y120days_count ? round((($count90y120 / $orders_90y120days_count) * 100), 1) : 0;
                
                $indicators_order_x_despacho['bodegas'][$bodega_id]['nombre'] = isset($bodegas[$bodega_id]['nombre']) ? $bodegas[$bodega_id]['nombre'] : 'bodega-id-'.$bodega_id;
                
                $indicators_order_x_despacho['bodegas'][$bodega_id]['0y30']['count'] = $count0y30;
                $indicators_order_x_despacho['bodegas'][$bodega_id]['0y30']['percent'] = $percent0y30;
                $indicators_order_x_despacho['bodegas'][$bodega_id]['0y30']['monto'] = isset($order_x_bodegas_0y30days['despacho'][$bodega_id]['monto']) ? $order_x_bodegas_0y30days['despacho'][$bodega_id]['monto'] : 0;
                $indicators_order_x_despacho['bodegas'][$bodega_id]['0y30']['monto_formato'] = isset($order_x_bodegas_0y30days['despacho'][$bodega_id]['monto']) ? $this->utils->numformat($order_x_bodegas_0y30days['despacho'][$bodega_id]['monto']) : 0;
                
                $indicators_order_x_despacho['bodegas'][$bodega_id]['30y60']['count'] = $count30y60;
                $indicators_order_x_despacho['bodegas'][$bodega_id]['30y60']['percent'] = $percent30y60;
                $indicators_order_x_despacho['bodegas'][$bodega_id]['30y60']['monto'] = isset($order_x_bodegas_30y60days['despacho'][$bodega_id]['monto']) ? $order_x_bodegas_30y60days['despacho'][$bodega_id]['monto'] : 0;
                $indicators_order_x_despacho['bodegas'][$bodega_id]['30y60']['monto_formato'] = isset($order_x_bodegas_30y60days['despacho'][$bodega_id]['monto']) ? $this->utils->numformat($order_x_bodegas_30y60days['despacho'][$bodega_id]['monto']) : 0;
                
                $indicators_order_x_despacho['bodegas'][$bodega_id]['60y90']['count'] = $count60y90;
                $indicators_order_x_despacho['bodegas'][$bodega_id]['60y90']['percent'] = $percent60y90;
                $indicators_order_x_despacho['bodegas'][$bodega_id]['60y90']['monto'] = isset($order_x_bodegas_60y90days['despacho'][$bodega_id]['monto']) ? $order_x_bodegas_60y90days['despacho'][$bodega_id]['monto'] : 0;
                $indicators_order_x_despacho['bodegas'][$bodega_id]['60y90']['monto_formato'] = isset($order_x_bodegas_60y90days['despacho'][$bodega_id]['monto']) ? $this->utils->numformat($order_x_bodegas_60y90days['despacho'][$bodega_id]['monto']) : 0;
                
                $indicators_order_x_despacho['bodegas'][$bodega_id]['90y120']['count'] = $count90y120;
                $indicators_order_x_despacho['bodegas'][$bodega_id]['90y120']['percent'] = $percent90y120;
                $indicators_order_x_despacho['bodegas'][$bodega_id]['90y120']['monto'] = isset($order_x_bodegas_90y120days['despacho'][$bodega_id]['monto']) ? $order_x_bodegas_90y120days['despacho'][$bodega_id]['monto'] : 0;
                $indicators_order_x_despacho['bodegas'][$bodega_id]['90y120']['monto_formato'] = isset($order_x_bodegas_90y120days['despacho'][$bodega_id]['monto']) ? $this->utils->numformat($order_x_bodegas_90y120days['despacho'][$bodega_id]['monto']) : 0;
                
                $sum_0y30days_count += $count0y30;
            }
        }
        
        $indicators_order_x_despacho['total']['0y30days']['count'] = $orders_x_bodega_0y30days_count;
        $indicators_order_x_despacho['total']['0y30days']['percent'] = $orders_0y30days_count ? round(($orders_x_bodega_0y30days_count / $orders_0y30days_count) * 100, 1) : 0;
        
        $data = array(
        'indicator_clients_x_bodega' => $indicator_clients_x_bodega,
        'indicator_clients_retencion_percent' => $indicator_clients_retencion_percent,
        'indicator_clients_news' => $indicator_clients_news,
        'indicator_skus' => $indicator_skus,
        'indicators_perro' => $indicators_perro,
        'indicators_gato' => $indicators_gato,
        'indicators_order_x_retiro' => $indicators_order_x_retiro,
        'indicators_order_x_despacho' => $indicators_order_x_despacho,
        //'user' => $this->session->userdata('user')
        );
        
        $this->render('indicadores', $data);
    }
    
    public function index()
    {
        $this->load->helper('form');
        
        $this->load->model(array(
        'customers',
        'drivers',
        ));
        
        /*
        $datetemp = "2019-01-16T20:00:13.609244Z";
        $datetemp = str_replace('T', ' ', $datetemp);
        $datetemp = str_replace('Z', '', $datetemp);
        echo date('Y-m-d H:i:s', strtotime($datetemp));
        exit;
        */
        
        $accion = $this->input->post('accion') ? $this->input->post('accion') : '';
        if ($accion == "search") {
            $search = $this->input->post('search');
            $this->session->set_userdata('search', $search);
        }
        $search = $this->session->userdata('search');
        
        $search['order_delivered_null'] = true;
        $search['order_canceled'] = true;
        
        $orders_pending = $this->orders->pending_dashboard();
        $orders_simpleroute = $this->drivers->byChoferSimpliroute($search);
        
        foreach ($orders_pending as &$o) {
            $o = $this->orders->get_products($o);
        }
        
        $stats_dashboard = $this->orders->stats_dashboard();
        
        $drivers_selects = $this->drivers->selects();
        
        $tipo_opt_sr = array();
        $tipo_opt_sr[] = array('nombre'=>'Balance', 'tipo'=>'balance', 'texto'=>'Balancea la cantidad de visitas con la cantidad de vehículos.' );
        $tipo_opt_sr[] = array('nombre'=>'All Vehicles', 'tipo'=>'all_vehicles', 'texto'=>'Se usará la menor cantidad de vehículos posible para atender las visitas ingresadas a plataforma.' );
        $tipo_opt_sr[] = array('nombre'=>'Join', 'tipo'=>'join', 'texto'=>'Si tienes mas de una visita con la misma dirección, la plataforma las considerará como una sola visita al optimizar.' );
        $tipo_opt_sr[] = array('nombre'=>'Open Ended', 'tipo'=>'open_ended', 'texto'=>'Las rutas propuestas terminarán en el ultimo punto de entrega, sin considerar la vuelta de los vehículos a la bodega.' );
        $tipo_opt_sr[] = array('nombre'=>'Single Tour', 'tipo'=>'single_tour', 'texto'=>'Una de las propuestas de rutas puede considerar mas de una salida por vehículo. Al activar esta opción indicarás a la plataforma que deseas 
programar una sola salida por optimización.' );
        $tipo_opt_sr[] = array('nombre'=>'Beauty', 'tipo'=>'beauty', 'texto'=>'Algoritmo experimental que entrega rutas mas zonificadas.' );
        

        //Obtenemos a todos los choferes y vehículos
        $driversAllSR = $this->drivers->AllVehiculosSR();
        
        $data = array(
        'search' => $search,
        //'customers' => $this->customers->count_all(),
        'tipo_opt_sr' => $tipo_opt_sr,
        'drivers_selects' => $drivers_selects,
        'drivers' => $this->drivers->overview(null, $search),
        'orders_pending' => $orders_pending,
        'orders_simpleroute' => $orders_simpleroute,
        'locales' => $this->orders->getLocales(),
        'stats_dashboard' => $stats_dashboard,
        'driversAllSR' => $driversAllSR,
        //'user' => $this->session->userdata('user')
        );
        
        $this->render('dashboard', $data);
    }
    
    public function assignOrderToDriver()
    {
        $this->load->model(array(
        'customers',
        'drivers',
        'descuentos_model',
        ));
        
        $this->load->library('bsale');
        $this->load->library('mylog');
        
        $chofer_id = $this->input->post('driver_id_assign');
        $delivery_date = $this->input->post('date_assign');
        
        //$chofer_id=8;
        
        $driver = $this->drivers->find($chofer_id);
        $empresa = $this->orders->getEmpresaBsale();
        
        $drivers_comunas = $this->drivers->getComunas($chofer_id);
        
        $comuna_ids = array();
        if (!empty($drivers_comunas)) {
            foreach ($drivers_comunas as $indice => $c) {
                $comuna_ids[] = $c->comuna_id;
            }
        }

        $res = 0;
        if (!empty($comuna_ids)) {
            $orders = $this->orders->getOrdersByComunas($comuna_ids);
            
            if (!empty($orders)) {
                $driver_id = $driver->id;
                $sucursal_id = $driver->chofer_bodega_id;
                $bsale_empresas_id = $empresa->bsale_empresas_id;
                $driver = $this->drivers->get($driver_id, false);
                
                $fcurrent = date('Y-m-d');
                $fcurrent_hour = date('H');
                $fcurrent_minute = date('i');
                
                /*
                if( ($fcurrent_hour == 12 && $fcurrent_minute >= 30) || ($fcurrent_hour > 12) ){
                $delivery_date = date('Y-m-d', strtotime("+1 day"));
                }else{
                $delivery_date = date('Y-m-d');
                }
                */
                
                $msje = array();
                foreach ($orders as $indice => $order) {
                    $data_log = array();
                    
                    $id = $order->id;
                    
                    $order = $this->orders->get($id, true);
                    
                    $fdespacho = intval(strtotime(date('Y-m-d H:i:s')));
                    //$fdespacho = intval(strtotime($this->input->post('delivery_date')));
                    $time = intval(strtotime(date('Y-m-d H:i:s')));
                    
                    $discount_mail = $order->discounts_mails_id ? $this->descuentos_model->get($order->discounts_mails_id) : array();
                    
                    /***** Integrar con Bsale *******/
                    $bs = new Bsale();
                    $client = $bs->getClientByMail($order->customer->email);
                    
                    $skus_nofound = array();
                    $skus_nostock = array();
                    $skus = array();
                    //$skus_stock = array();
                    if (!empty($order->products)) {
                        foreach ($order->products as $indice => $p) {
                            $sku = $bs->getProductBySku($p->sku_id);
                            /*
                            if (isset($sku->variant->id) && !empty($sku->variant->id)) {
                                $sku_stock = $bs->getStockProductBySku($p->sku_id, $sucursal_id);
                                if (isset($sku_stock->id) && !empty($sku_stock->id) && $p->qty <= $sku_stock->quantity) {
                                    $skus[] = array('variant_id' => $sku->variant->id, 'qty' => $p->qty, 'name' => $sku->variant->product->name . ' - ' . $sku->variant->description, 'price' => $p->description->price, 'price_offer' => $p->description->price_offer);
                                } else {
                                    $skus_nostock[] = $p->sku_id;
                                }
                            } else {
                                $skus_nofound[] = $p->sku_id;
                            }
                            */

                            if (isset($sku->variant->product->pack_details) && !empty($sku->variant->product->pack_details)) {
                                foreach ($sku->variant->product->pack_details as $indice => $item) {
                                    $sku_product = $bs->getProductByVariantid($item->variant->id);
                                    $sku_stock = $bs->getStockProductByVariantid($item->variant->id, $sucursal_id);
                                    if (isset($sku_stock->id) && !empty($sku_stock->id) && ($item->quantity*$p->qty) > $sku_stock->quantity) {
                                        $skus_nostock[] = $sku_product->code;
                                    }
                                }
                                if( empty($skus_nostock) ){
                                    $skus[] = array('variant_id' => $sku->variant->id, 'qty' => $p->qty, 'name' => $p->description->brand. ' - ' . $p->description->name, 'price' => $p->description->price, 'price_offer' => $p->description->price_offer);
                                }
                            }else{
                                if (isset($sku->variant->id) && !empty($sku->variant->id)) {
                                    $sku_stock = $bs->getStockProductBySku($p->sku_id, $sucursal_id);
                                    if (isset($sku_stock->id) && !empty($sku_stock->id) && $p->qty <= $sku_stock->quantity) {
                                        $skus[] = array('variant_id' => $sku->variant->id, 'qty' => $p->qty, 'name' => $sku->variant->product->name . ' - ' . $sku->variant->description, 'price' => $p->description->price, 'price_offer' => $p->description->price_offer);
                                    //$skus_stock[] = $sku_stock->id;
                                    } else {
                                        $skus_nostock[] = $p->sku_id;
                                    }
                                } else {
                                    $skus_nofound[] = $p->sku_id;
                                }
                            }
                        }
                    }
                    
                    if (empty($skus_nofound)) {
                        if (empty($skus_nostock)) {
                            $document = array(
                            "documentTypeId" => intval(1),
                            "officeId" => intval($sucursal_id),
                            "priceListId" => $bs->getPriceListId(),
                            "emissionDate" => $time,
                            "expirationDate" => $time,
                            "declareSii" => intval(1), // Si se desea declarar al servicio de impuesto interno colocar 1
                            "sendEmail" => intval(1),
                            );
                            
                            $err_insert_client = '';
                            if (isset($client->id) && !empty($client->id)) {
                                $document['clientId'] = intval($client->id);
                            } else {
                                //Crear Cliente
                                $insert_client = array(
                                'firstName' => $order->customer->name,
                                'lastName' => '.',
                                'email' => $order->customer->email,
                                );
                                $client = $bs->insertClient($insert_client);
                                $document['clientId'] = isset($client->id) && !empty($client->id) ? intval($client->id) : '';
                                $err_insert_client = isset($client->error) && !empty($client->error) ? $client->error : '';
                            }
                            
                            if (isset($document['clientId']) && !empty($document['clientId'])) {
                                #$date = DateTime::createFromFormat('d/m/Y', $this->input->post('delivery_date') );
                                
                                $details = array();
                                if (!empty($skus)) {
                                    foreach ($skus as $indice => $_sku) {
                                        $price = $_sku['price_offer'] ? $_sku['price_offer'] : $_sku['price'];
                                        
                                        if (!empty($discount_mail)) {
                                            $discount = $price * ($discount_mail['percentage'] / 100);
                                            $price = $price > $discount ? $price - $discount : $price;
                                        }
                                        
                                        // Pedido Autocompra
                                        if ($order->paym_id == 5) {
                                            $desc = round($price * 0.05); //5%
                                            $price = $price - $desc;
                                        }
                                        
                                        $price_neto = round($price / 1.19);
                                        
                                        $details[] = array(
                                        "variantId" => intval($_sku['variant_id']),
                                        "netUnitValue" => intval($price_neto),
                                        "quantity" => intval($_sku['qty']),
                                        "taxId" => "[1]",
                                        "comment" => "", //$_sku['name'],
                                        "discount" => intval(0),
                                        );
                                    }
                                    
                                    if ($order->order_shipping && $order->order_shipping > 0) {
                                        $price_neto = round($order->order_shipping / 1.19);
                                        $details[] = array(
                                        "variantId" => $bs->getIdProShipping(), // intval(633)
                                        "netUnitValue" => intval($price_neto),
                                        "quantity" => intval(1),
                                        "taxId" => "[1]",
                                        "comment" => "", //$_sku['name'],
                                        "discount" => intval(0),
                                        );
                                    }
                                    
                                    $document['details'] = $details;
                                }
                                
                                $amount = $order->order_total + $order->order_shipping;
                                
                                $boleta = $bs->generateDocument($document);
                                
                                if (isset($boleta->id) && !empty($boleta->id)) {
                                    $details_document = $bs->getDetailsDocuments($boleta->id);
                                    
                                    $details_despacho = array();
                                    if (!empty($details_document)) {
                                        foreach ($details_document as $indice => $detail) {
                                            $details_despacho[] = array(
                                            "detailId" => $detail->id,
                                            "quantity" => $detail->quantity,
                                            );
                                        }
                                    }
                                    
                                    //Generar Guía de Despacho
                                    $despacho = array(
                                    "documentTypeId" => $bs->getDespachoId(), //21,  21=>RyA y 18=>Macdog
                                    "emissionDate" => $fdespacho,
                                    "expirationDate" => $fdespacho,
                                    "shippingTypeId" => 1,
                                    "officeId" => $sucursal_id,
                                    "priceListId" => $bs->getPriceListId(),
                                    "municipality" => "Las Condes", //$order->address->comuna,
                                    "city" => "Santiago",
                                    "address" => "Apoquindo 7910", //$order->address->direccion,
                                    "declareSii" => 0,
                                    "recipient" => mb_convert_case($driver->name, MB_CASE_UPPER, "UTF-8"), //Sacar de la BD al despachador
                                    "client" => array(
                                    'firstName' => "Allan", //$order->customer->name,
                                    'lastName' => "Turski",
                                    "municipality" => "Las Condes", //$order->address->comuna,
                                    "code" => "25250770-1",
                                    "activity" => "Sin dato",
                                    "company" => "Particular",
                                    "city" => "Santiago",
                                    "address" => "Apoquindo 7910", //$order->address->direccion
                                    ),
                                    "details" => $details_despacho,
                                    );
                                    
                                    $shipping = $bs->generateShipping($despacho);
                                    
                                    $data = array(
                                    'order_delivery_date' => $delivery_date,
                                    //'order_delivery_time' => $this->input->post('delivery_time'),
                                    'bsale_empresas_id' => $bsale_empresas_id,
                                    'bsale_sucursal_id' => $sucursal_id,
                                    'order_comment' => '',
                                    'driver_id' => $chofer_id,
                                    'bsale_nro_boleta' => $boleta->number,
                                    'bsale_url_print_boleta' => $boleta->urlPublicView,
                                    );
                                    
                                    $data_log['data'] = $data;
                                    $data_log['data_document'] = $document;
                                    $data_log['data_despacho'] = $despacho;
                                    
                                    $msje[] = "Se generó con éxito la boleta en Bsale para el Pedido #$id";
                                    $this->orders->update($id, $data);
                                    $res = 1;
                                    
                                    //$this->session->set_flashdata('success', $msje);
                                } else {
                                    $msje[] = "Se produjo un error al generar la boleta en Bsale para el Pedido #$id";
                                    //$this->session->set_flashdata('error', $msje);
                                }
                            } else {
                                $msje[] = "Se produjo un error al relacionar al cliente para el Pedido #$id";
                                if (isset($err_insert_client) && !empty($err_insert_client)) {
                                    $msje = 'Se produjo un error al crear en Bsale el cliente para el Pedido #' . $id . ': ' . $err_insert_client;
                                }
                                //$this->session->set_flashdata('error', $msje);
                            }
                        } else {
                            $msje[] = "Para el Pedido #$id, no hay stock para los siguientes SKU en BSale : " . implode(',', $skus_nostock);
                            //$this->session->set_flashdata('error', $msje);
                        }
                    } else {
                        $msje[] = "Para el Pedido #$id, los siguientes SKU no se encontraron en BSale : " . implode(',', $skus_nofound);
                        //$this->session->set_flashdata('error', $msje);
                    }
                    
                    $this->mylog->add(
                    array(
                    'order_id' => $id,
                    'action' => 'EMITIR_BOLETA_BSALE',
                    'description' => (!empty($msje) ? implode('<br/>', $msje) : ''),
                    'data' => $data_log,
                    )
                    );
                }
            } else {
                $msje[] = "No se encontraron pedidos para las comunas que tiene asignada el chofer.";
            }
        } else {
            $msje[] = "No tiene comunas asignadas, favor de asignarles comunas al chofer.";
        }
        
        $msje = !empty($msje) ? implode('<br/>', $msje) : '';
        if ($res == 1) {
            $this->session->set_flashdata('success', $msje);
        } else {
            $this->session->set_flashdata('error', $msje);
        }
        
        redirect('dashboard');
    }
    
    public function assignOrderConfirmedToDriver()
    {
        $this->load->model(array(
        'customers',
        'drivers',
        'descuentos_model',
        ));
        
        $this->load->library('bsale');
        $this->load->library('mylog');
        
        $chofer_id = $this->input->post('driver_id_assign_order_confirmed');
        $delivery_date = $this->input->post('date_assign_order_confirmed');
        
        $driver = $this->drivers->find($chofer_id);
        $empresa = $this->orders->getEmpresaBsale();
        
        $orders_ids = $this->input->post('checks');
        
        $res = 0;
        if (! empty($chofer_id) && ! empty($delivery_date)) {
            if ( !empty($orders_ids) ) {
                
                $driver_id = $driver->id;
                $sucursal_id = $driver->chofer_bodega_id;
                $bsale_empresas_id = $empresa->bsale_empresas_id;
                $driver = $this->drivers->get($driver_id, false);
                
                $fcurrent = date('Y-m-d');
                $fcurrent_hour = date('H');
                $fcurrent_minute = date('i');
                
                $msje = array();
                foreach ($orders_ids as $indice => $id) {
                    $data_log = array();
                    
                    $order = $this->orders->get($id, true);
                    
                    $fdespacho = intval(strtotime(date('Y-m-d H:i:s')));
                    $time = intval(strtotime(date('Y-m-d H:i:s')));
                    
                    $discount_mail = $order->discounts_mails_id ? $this->descuentos_model->get($order->discounts_mails_id) : array();
                    
                    /***** Integrar con Bsale *******/
                    $bs = new Bsale();
                    $client = $bs->getClientByMail($order->customer->email);
                    
                    $skus_nofound = array();
                    $skus_nostock = array();
                    $skus = array();
                    
                    if (!empty($order->products)) {
                        foreach ($order->products as $indice => $p) {
                            $sku = $bs->getProductBySku($p->sku_id);
                            /*
                            if (isset($sku->variant->id) && !empty($sku->variant->id)) {
                                $sku_stock = $bs->getStockProductBySku($p->sku_id, $sucursal_id);
                                if (isset($sku_stock->id) && !empty($sku_stock->id) && $p->qty <= $sku_stock->quantity) {
                                    $skus[] = array('variant_id' => $sku->variant->id, 'qty' => $p->qty, 'name' => $sku->variant->product->name . ' - ' . $sku->variant->description, 'price' => $p->description->price, 'price_offer' => $p->description->price_offer);
                                } else {
                                    $skus_nostock[] = $p->sku_id;
                                }
                            } else {
                                $skus_nofound[] = $p->sku_id;
                            }
                            */
                            if (isset($sku->variant->product->pack_details) && !empty($sku->variant->product->pack_details)) {
                                foreach ($sku->variant->product->pack_details as $indice => $item) {
                                    $sku_product = $bs->getProductByVariantid($item->variant->id);
                                    $sku_stock = $bs->getStockProductByVariantid($item->variant->id, $sucursal_id);
                                    if (isset($sku_stock->id) && !empty($sku_stock->id) && ($item->quantity*$p->qty) > $sku_stock->quantity) {
                                        $skus_nostock[] = $sku_product->code;
                                    }
                                }
                                if( empty($skus_nostock) ){
                                    $skus[] = array('variant_id' => $sku->variant->id, 'qty' => $p->qty, 'name' => $p->description->brand. ' - ' . $p->description->name, 'price' => $p->description->price, 'price_offer' => $p->description->price_offer);
                                }
                            }else{
                                if (isset($sku->variant->id) && !empty($sku->variant->id)) {
                                    $sku_stock = $bs->getStockProductBySku($p->sku_id, $sucursal_id);
                                    if (isset($sku_stock->id) && !empty($sku_stock->id) && $p->qty <= $sku_stock->quantity) {
                                        $skus[] = array('variant_id' => $sku->variant->id, 'qty' => $p->qty, 'name' => $sku->variant->product->name . ' - ' . $sku->variant->description, 'price' => $p->description->price, 'price_offer' => $p->description->price_offer);
                                    //$skus_stock[] = $sku_stock->id;
                                    } else {
                                        $skus_nostock[] = $p->sku_id;
                                    }
                                } else {
                                    $skus_nofound[] = $p->sku_id;
                                }
                            }
                        }
                    }
                    
                    if (empty($skus_nofound)) {
                        if (empty($skus_nostock)) {
                            $document = array(
                            "documentTypeId" => intval(1),
                            "officeId" => intval($sucursal_id),
                            "priceListId" => $bs->getPriceListId(),
                            "emissionDate" => $time,
                            "expirationDate" => $time,
                            "declareSii" => intval(1), // Si se desea declarar al servicio de impuesto interno colocar 1
                            "sendEmail" => intval(1),
                            );
                            
                            $err_insert_client = '';
                            if (isset($client->id) && !empty($client->id)) {
                                $document['clientId'] = intval($client->id);
                            } else {
                                //Crear Cliente
                                $insert_client = array(
                                'firstName' => $order->customer->name,
                                'lastName' => '.',
                                'email' => $order->customer->email,
                                );
                                $client = $bs->insertClient($insert_client);
                                $document['clientId'] = isset($client->id) && !empty($client->id) ? intval($client->id) : '';
                                $err_insert_client = isset($client->error) && !empty($client->error) ? $client->error : '';
                            }
                            
                            if (isset($document['clientId']) && !empty($document['clientId'])) {
                                #$date = DateTime::createFromFormat('d/m/Y', $this->input->post('delivery_date') );
                                
                                $details = array();
                                if (!empty($skus)) {
                                    foreach ($skus as $indice => $_sku) {
                                        $price = $_sku['price_offer'] ? $_sku['price_offer'] : $_sku['price'];
                                        
                                        if (!empty($discount_mail)) {
                                            $discount = $price * ($discount_mail['percentage'] / 100);
                                            $price = $price > $discount ? $price - $discount : $price;
                                        }
                                        
                                        // Pedido Autocompra
                                        if ($order->paym_id == 5) {
                                            $desc = round($price * 0.05); //5%
                                            $price = $price - $desc;
                                        }
                                        
                                        $price_neto = round($price / 1.19);
                                        
                                        $details[] = array(
                                        "variantId" => intval($_sku['variant_id']),
                                        "netUnitValue" => intval($price_neto),
                                        "quantity" => intval($_sku['qty']),
                                        "taxId" => "[1]",
                                        "comment" => "", //$_sku['name'],
                                        "discount" => intval(0),
                                        );
                                    }
                                    
                                    if ($order->order_shipping && $order->order_shipping > 0) {
                                        $price_neto = round($order->order_shipping / 1.19);
                                        $details[] = array(
                                        "variantId" => $bs->getIdProShipping(), // intval(633)
                                        "netUnitValue" => intval($price_neto),
                                        "quantity" => intval(1),
                                        "taxId" => "[1]",
                                        "comment" => "", //$_sku['name'],
                                        "discount" => intval(0),
                                        );
                                    }
                                    
                                    $document['details'] = $details;
                                }
                                
                                $amount = $order->order_total + $order->order_shipping;
                                
                                $boleta = $bs->generateDocument($document);
                                
                                if (isset($boleta->id) && !empty($boleta->id)) {
                                    $details_document = $bs->getDetailsDocuments($boleta->id);
                                    
                                    $details_despacho = array();
                                    if (!empty($details_document)) {
                                        foreach ($details_document as $indice => $detail) {
                                            $details_despacho[] = array(
                                            "detailId" => $detail->id,
                                            "quantity" => $detail->quantity,
                                            );
                                        }
                                    }
                                    
                                    //Generar Guía de Despacho
                                    $despacho = array(
                                    "documentTypeId" => $bs->getDespachoId(), //21,  21=>RyA y 18=>Macdog
                                    "emissionDate" => $fdespacho,
                                    "expirationDate" => $fdespacho,
                                    "shippingTypeId" => 1,
                                    "officeId" => $sucursal_id,
                                    "priceListId" => $bs->getPriceListId(),
                                    "municipality" => "Las Condes", //$order->address->comuna,
                                    "city" => "Santiago",
                                    "address" => "Apoquindo 7910", //$order->address->direccion,
                                    "declareSii" => 0,
                                    "recipient" => mb_convert_case($driver->name, MB_CASE_UPPER, "UTF-8"), //Sacar de la BD al despachador
                                    "client" => array(
                                    'firstName' => "Allan", //$order->customer->name,
                                    'lastName' => "Turski",
                                    "municipality" => "Las Condes", //$order->address->comuna,
                                    "code" => "25250770-1",
                                    "activity" => "Sin dato",
                                    "company" => "Particular",
                                    "city" => "Santiago",
                                    "address" => "Apoquindo 7910", //$order->address->direccion
                                    ),
                                    "details" => $details_despacho,
                                    );
                                    
                                    $shipping = $bs->generateShipping($despacho);
                                    
                                    $data = array(
                                    'order_delivery_date' => $delivery_date,
                                    //'order_delivery_time' => $this->input->post('delivery_time'),
                                    'bsale_empresas_id' => $bsale_empresas_id,
                                    'bsale_sucursal_id' => $sucursal_id,
                                    'order_comment' => '',
                                    'driver_id' => $chofer_id,
                                    'bsale_nro_boleta' => $boleta->number,
                                    'bsale_url_print_boleta' => $boleta->urlPublicView,
                                    );
                                    
                                    $data_log['data'] = $data;
                                    $data_log['data_document'] = $document;
                                    $data_log['data_despacho'] = $despacho;
                                    
                                    $msje[] = "Se generó con éxito la boleta en Bsale para el Pedido #$id, con boleta Nro.".$boleta->number;
                                    $this->orders->update($id, $data);
                                    $res = 1;
                                } else {
                                    $msje[] = "Se produjo un error al generar la boleta en Bsale para el Pedido #$id";
                                }
                            } else {
                                $msje[] = "Se produjo un error al relacionar al cliente para el Pedido #$id";
                                if (isset($err_insert_client) && !empty($err_insert_client)) {
                                    $msje = 'Se produjo un error al crear en Bsale el cliente para el Pedido #' . $id . ': ' . $err_insert_client;
                                }
                            }
                        } else {
                            $msje[] = "Para el Pedido #$id, no hay stock para los siguientes SKU en BSale : " . implode(',', $skus_nostock);
                        }
                    } else {
                        $msje[] = "Para el Pedido #$id, los siguientes SKU no se encontraron en BSale : " . implode(',', $skus_nofound);
                    }
                    
                    $this->mylog->add(
                    array(
                    'order_id' => $id,
                    'action' => 'EMITIR_BOLETA_BSALE',
                    'description' => 'Se genera desde el dashboard tabla pedidos por confirmar.<br/>'.(!empty($msje) ? implode('<br/>', $msje) : ''),
                    'data' => $data_log,
                    )
                    );
                }
                
            } else {
                $msje[] = "No tiene pedidos seleccionados, favor de seleccionar los pedidos que desee procesar.";
            }
        } else {
            $msje[] = "Se produjo un error, favor de completar bien los datos para la asignación del chofer y fecha de entrega.";
        }
        
        $msje = !empty($msje) ? implode('<br/>', $msje) : '';
        if ($res == 1) {
            $this->session->set_flashdata('success', $msje);
        } else {
            $this->session->set_flashdata('error', $msje);
        }
        
        redirect('dashboard');
    }
    
    public function moveOrderToDriver()
    {
        $this->load->model(array(
        'customers',
        'drivers',
        'descuentos_model',
        ));
        
        $this->load->library('mylog');
        
        $chofer_id = $this->input->post('driver_id_move');
        $delivery_date = $this->input->post('date_move');
        $orders_ids = $this->input->post('checks');
        
        $res = 0;
        if (!empty($orders_ids) && ! empty($chofer_id) && ! empty($delivery_date)) {
            
            foreach( $orders_ids as $order_id ){
                $order = $this->orders->get($order_id);
                
                $data = array(
                'order_delivery_date' => $delivery_date,
                'driver_id' => $chofer_id,
                );
                
                $data_log['old_order_delivery_date'] = $order->order_delivery_date;
                $data_log['old_driver_id'] = $order->driver_id;
                
                $data_log['new_order_delivery_date'] = $delivery_date;
                $data_log['new_driver_id'] = $chofer_id;
                
                if( $this->orders->update($order_id, $data) ){
                    $msje = "Se Actualizó con éxito los datos el chofer y la fecha de entrega para el Pedido #$order_id";
                    $this->mylog->add(
                    array(
                    'order_id' => $order_id,
                    'action' => 'ASIGNO_CHOFER_DASHBOARD',
                    'description' => $msje,
                    'data' => $data_log,
                    )
                    );
                }
            }
            $res = 1;
            $msje = "Se realizó con éxito la asignación del chofer y fecha de entrega para los pedidos seleccionados.";
            
        } else {
            $msje = "Se produjo un error, favor de completar bien los datos para la asignación del chofer y fecha de entrega.";
        }
        
        if ($res == 1) {
            $this->session->set_flashdata('success', $msje);
        } else {
            $this->session->set_flashdata('error', $msje);
        }
        
        redirect('dashboard');
    }
    
    public function printAll()
    {
        $this->load->model(array('drivers', 'orders'));
        
        $search = $this->session->userdata('search');
        
        $locales = $this->orders->getLocales();
        
        $orders_pending = $this->orders->pending_dashboard(true, true);
        foreach ($orders_pending as &$o) {
            $o = $this->orders->get_products($o);
        }
        
        $result_drivers = array();
        
        $drivers = $this->drivers->overview(null, $search);
        if (!empty($drivers)) {
            foreach ($drivers as $d) {
                if ($d->inactivo == 1) {
                    continue;
                }
                if (!empty($d->orders)) {
                    foreach ($d->orders as &$o) {
                        $o = $this->orders->get_products($o);
                    }
                }
                $result_drivers[$d->id] = $d;
            }
        }
        
        $payments = $this->orders->getPayments();
        
        $this->load->view("printall", array(
        'orders_pending' => $orders_pending,
        'result_drivers' => $result_drivers,
        'payments' => $payments,
        'locales' => $locales,
        ));
    }
    
    public function export($task = null)
    {
        $this->load->model(array(
        'orders',
        'products',
        'customers',
        'drivers',
        'indicators_model',
        ));
        
        $date_current_format = date('Y-m-d');
        
        $less_30_days = strtotime('-30 days');
        $less_31_days = strtotime('-31 days');
        
        $less_90_days = strtotime('-90 days');
        $less_180_days = strtotime('-180 days');
        $less_270_days = strtotime('-270 days');
        
        $less_30_days_format = date('Y-m-d', $less_30_days);
        $less_31_days_format = date('Y-m-d', $less_31_days);
        
        $less_90_days_format = date('Y-m-d', $less_90_days);
        $less_180_days_format = date('Y-m-d', $less_180_days);
        $less_270_days_format = date('Y-m-d', $less_270_days);
        
        switch ($task) {
            
            case 'retencion-de-clientes':
                
                // INDICADORES RETENCION DE CLIENTES
                
                // Exportar los mails por bodega
                
                $srch = array(
                'orders_start_date' => $less_270_days_format,
                'orders_end_date' => $less_180_days_format,
                'orders_paid' => 1,
                'group_by' => 'cus.cust_email',
                );
                $customers_mail_between180y270days = $this->indicators_model->srchCustomersBuy($srch, 'cust_email');
                
                $less_179_days = strtotime('-179 days');
                $less_179_days_format = date('Y-m-d', $less_179_days);
                
                $srch = array(
                'orders_start_date' => $less_179_days_format,
                'orders_end_date' => $less_90_days_format,
                'orders_paid' => 1,
                'in_customers_mails' => $customers_mail_between180y270days,
                'group_by' => 'cus.cust_email',
                );
                $customers_mail_between90y179days = $this->indicators_model->srchCustomersBuy($srch, 'cust_email');
                
                $less_89_days = strtotime('-89 days');
                $less_89_days_format = date('Y-m-d', $less_89_days);
                
                $srch = array(
                'orders_start_date' => $less_89_days_format,
                'orders_end_date' => $date_current_format,
                'orders_paid' => 1,
                'in_customers_mails' => $customers_mail_between90y179days,
                'group_by' => 'cus.cust_email',
                );
                $customers_mail_between0y89days = $this->indicators_model->srchCustomersBuy($srch, 'cust_email');
                
                $customers_mail_between0y89days_not_buy = array();
                if( ! empty($customers_mail_between90y179days) ){
                    $temp_customers_mail_between0y89days = array();
                    foreach($customers_mail_between0y89days as $email){
                        $email = trim($email);
                        $temp_customers_mail_between0y89days[$email] = $email;
                    }
                    //$customers_mail_between0y89days = array_combine(array_values($customers_mail_between0y89days), array_values($customers_mail_between0y89days));
                    foreach($customers_mail_between90y179days as $email){
                        $email = trim($email);
                        if( isset($temp_customers_mail_between0y89days[$email]) ){
                            continue;
                        }else{
                            $customers_mail_between0y89days_not_buy[] = $email;
                        }
                    }
                }
            
                $filters = array(
                    'orders_start_date' => $less_179_days_format,
                    'orders_end_date' => $less_90_days_format,
                    'orders_paid' => 1,
                    //'paid' => 1,
                    'group_by' => 'cus.cust_email',
                );
                $customers_mail_between0y89days_not_buy = $this->indicators_model->getCustomersPorBodega($customers_mail_between0y89days_not_buy, 'cust_email', $filters);

                $objPHPExcel = new PHPExcel();
                $cell_values = array(
                'A',
                'B',
                'C',
                'D',
                'E',
                'F',
                'G',
                'H',
                'I',
                'J',
                'K',
                'L',
                'M',
                'N',
                'O',
                'P',
                'Q',
                'R',
                'S',
                'T',
                );
            
                $cellvalue = 'A1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'Bodega');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);
                $cellvalue = 'B1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'E-mail');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);

                $bodegas = $this->indicators_model->getBodegas();

                if (!empty($customers_mail_between0y89days_not_buy)) {
                    $ini = 0;
                    $can = 1000;
                    //$total = count($customers_mail_between0y89days_not_buy);
                    $col = 2;

                    /*
                    for ($i = 1; $ini <= $total; $i++) {
                        $ini = $i ? ($i - 1) * $can : 0;
                        $results = array_slice($customers_mail_between0y89days_not_buy, $ini, $can);
                        if (!empty($results)) {
                            foreach ($results as $value) {
                                $indice = 0;
                                $cellvalue = $cell_values[$indice] . $col;
                                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $value);
                                $col++;
                            }
                        }
                    }
                    */
                    foreach ($customers_mail_between0y89days_not_buy as $bodega_id => $orders) {
                        $bodega = isset($bodegas[$bodega_id]['id']) ? $bodegas[$bodega_id]['nombre'] : '';
                        foreach ($orders as $indice => $o) {
                            $cellvalue = $cell_values[0] . $col;
                            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $bodega);
                            $cellvalue = $cell_values[1] . $col;
                            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $o->cust_email);
                            $col++;
                        }
                    }
                }
            
                $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="clientes-no-compraron-0y90dias-' . date('d-m-Y') . '.xls"');
                $objWriter->save('php://output');
                
                break;
        
            case 'skus-por-bodega-available':
                //$sku_notavailable = $this->indicators_model->srchSku(array('sku_available' => 0));
                $bodegas = $this->indicators_model->getBodegas();
                
                //$filters = array('sku_available' => 1);
                $filters = array();
                $skus_por_bodegas = $this->indicators_model->getSkuPorBodega('id', array(), $filters);
                
                $objPHPExcel = new PHPExcel();
                $cell_values = array(
                'A',
                'B',
                'C',
                'D',
                'E',
                'F',
                'G',
                'H',
                'I',
                'J',
                'K',
                'L',
                'M',
                'N',
                'O',
                'P',
                'Q',
                'R',
                'S',
                'T',
                );
                
                $cellvalue = 'A1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'Bodega');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);
                $cellvalue = 'B1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'SKU');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);
                $cellvalue = 'C1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'Producto');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);
                $cellvalue = 'D1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'SKU Descripción');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);
                $cellvalue = 'E1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'Marca');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);
                $cellvalue = 'F1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'Categoría');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);
                $cellvalue = 'G1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'Disponible');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);
                
                if (!empty($skus_por_bodegas)) {
                    $col = 2;
                    foreach ($skus_por_bodegas as $bodega_id => $skus) {
                        $bodega = isset($bodegas[$bodega_id]['id']) ? $bodegas[$bodega_id]['nombre'] : '';
                        foreach ($skus as $sku_id => $p) {
                            $cellvalue = $cell_values[0] . $col;
                            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $bodega);
                            $cellvalue = $cell_values[1] . $col;
                            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $sku_id);
                            $cellvalue = $cell_values[2] . $col;
                            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $p->prod_name);
                            $cellvalue = $cell_values[3] . $col;
                            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $p->sku_description);
                            $cellvalue = $cell_values[4] . $col;
                            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $p->marca);
                            $cellvalue = $cell_values[5] . $col;
                            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $p->categoria);
                            $cellvalue = $cell_values[6] . $col;
                            $disponible = $p->prod_available && $p->sku_available ? 'Si' : 'No';
                            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $disponible);
                            $col++;
                        }
                    }
                }
            
                $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="skus-por-bodegas-disponibles-' . date('d-m-Y') . '.xls"');
                $objWriter->save('php://output');
                
                break;
    
            case 'skus-no-disponibles':
                //$sku_notavailable = $this->indicators_model->srchSku(array('sku_available' => 0));
                $bodegas = $this->indicators_model->getBodegas();
                $bodegas['sin-bodega'] = array('nombre'=>'sin-Bodega');
                
                $filters = array('sku_available' => 1);
                $skus_por_bodegas = $this->indicators_model->getSkuPorBodega('id', array(), $filters);
                
                $skus_por_bodega_santiago = isset($skus_por_bodegas[1]) && !empty($skus_por_bodegas[1]) ? $skus_por_bodegas[1] : array();
                $skus_por_bodega_antofagasta = isset($skus_por_bodegas[3]) && !empty($skus_por_bodegas[3]) ? $skus_por_bodegas[3] : array();
                
                $skus_nodisponibles_en_bodega_y_si_en_santiago = array();
                $skus_disponibles_en_bodega_y_no_en_santiago = array();
                if( ! empty($skus_por_bodega_santiago) ){
                    foreach ($skus_por_bodega_santiago as $sku_id => $sku_santiago) {
                        foreach ($bodegas as $bodega_id => $bodega) {
                            if ($bodega_id == 'sin-bodega') {
                                continue;
                            }
                        
                            if (! @isset($skus_por_bodegas[$bodega_id][$sku_id])) {
                                $skus_nodisponibles_en_bodega_y_si_en_santiago[$bodega_id][$sku_id] = $sku_santiago;
                            }
                        }
                    }
                    foreach ($skus_por_bodegas as $bodega_id => $skus) {
                        if ($bodega_id == 1) continue;
                        foreach ($skus as $sku_id => $sku) {
                            if (! @isset($skus_por_bodega_santiago[$sku_id])) {
                                $skus_disponibles_en_bodega_y_no_en_santiago[$bodega_id][$sku_id] = $sku;
                            }
                        }
                    }
                }
    
                $objPHPExcel = new PHPExcel();
                $cell_values = array(
                'A',
                'B',
                'C',
                'D',
                'E',
                'F',
                'G',
                'H',
                'I',
                'J',
                'K',
                'L',
                'M',
                'N',
                'O',
                'P',
                'Q',
                'R',
                'S',
                'T',
                );
                
                $cellvalue = 'A1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'Bodega');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);
                $cellvalue = 'B1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'SKU');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);
                $cellvalue = 'C1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'Producto');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);
                $cellvalue = 'D1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'SKU Descripción');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);
                $cellvalue = 'E1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'Marca');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);
                $cellvalue = 'F1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'Categoría');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);
                $cellvalue = 'G1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'Disponible en Santiago');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);
            
                if (!empty($skus_nodisponibles_en_bodega_y_si_en_santiago)) {
                    $col = 2;
                    foreach ($skus_nodisponibles_en_bodega_y_si_en_santiago as $bodega_id => $skus) {
                        $bodega = isset($bodegas[$bodega_id]['id']) ? $bodegas[$bodega_id]['nombre'] : '';
                        foreach ($skus as $sku_id => $p) {
                            $cellvalue = $cell_values[0] . $col;
                            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $bodega);
                            $cellvalue = $cell_values[1] . $col;
                            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $sku_id);
                            $cellvalue = $cell_values[2] . $col;
                            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $p->prod_name);
                            $cellvalue = $cell_values[3] . $col;
                            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $p->sku_description);
                            $cellvalue = $cell_values[4] . $col;
                            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $p->marca);
                            $cellvalue = $cell_values[5] . $col;
                            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $p->categoria);
                            $cellvalue = $cell_values[6] . $col;
                            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'Si');
                            $col++;
                        }
                    }
                }
            
                if (!empty($skus_disponibles_en_bodega_y_no_en_santiago)) {
                    $col = 2;
                    foreach ($skus_disponibles_en_bodega_y_no_en_santiago as $bodega_id => $skus) {
                        $bodega = isset($bodegas[$bodega_id]['id']) ? $bodegas[$bodega_id]['nombre'] : '';
                        foreach ($skus as $sku_id => $p) {
                            $cellvalue = $cell_values[0] . $col;
                            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $bodega);
                            $cellvalue = $cell_values[1] . $col;
                            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $sku_id);
                            $cellvalue = $cell_values[2] . $col;
                            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $p->prod_name);
                            $cellvalue = $cell_values[3] . $col;
                            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $p->sku_description);
                            $cellvalue = $cell_values[4] . $col;
                            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $p->marca);
                            $cellvalue = $cell_values[5] . $col;
                            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $p->categoria);
                            $cellvalue = $cell_values[6] . $col;
                            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'No');
                            $col++;
                        }
                    }
                }
                
                $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="skus-no-disponibles-' . date('d-m-Y') . '.xls"');
                $objWriter->save('php://output');
        
                break;

            case 'clients-news':
    
                $srch = array(
                'orders_start_date' => $less_30_days_format,
                'orders_end_date' => $date_current_format,
                'orders_paid' => 1,
                //'paid' => 1,
                'group_by' => 'cus.cust_email',
                );
                $customers_mail_between0y30days = $this->indicators_model->srchCustomersBuy($srch, 'cust_email');
                
                $srch = array(
                //'orders_start_date' => $less_60_days_format,
                'orders_end_date' => $less_31_days_format,
                'orders_paid' => 1,
                //'paid' => 1,
                'in_customers_mails' => $customers_mail_between0y30days,
                'group_by' => 'cus.cust_email',
                );
                $customers_mail_finds_0y30 = $this->indicators_model->srchCustomersBuy($srch, 'cust_email');
                
                /** New 0y30days **/
                $customers_mail_news_0y30days = array();
                if( ! empty($customers_mail_between0y30days) ){
                    $temp_customers_mail_finds = array();
                    foreach ($customers_mail_finds_0y30 as $email_find) {
                        $temp_customers_mail_finds[$email_find] = $email_find;
                    }
                    foreach ($customers_mail_between0y30days as $_email) {
                        if( isset($temp_customers_mail_finds[$_email]) ){
                            continue;
                        }else{
                            $customers_mail_news_0y30days[] = $_email;
                        }
                    }
                }
                $filters = array(
                    'orders_start_date' => $less_30_days_format,
                    'orders_end_date' => $date_current_format,
                    'orders_paid' => 1,
                    'group_by' => 'cus.cust_email',
                );
                $customers_news_por_bodegas_0y30days = $this->indicators_model->getCustomersPorBodega($customers_mail_news_0y30days, 'cust_email', $filters);

                $customers_news_por_bodegas = array();
                if( ! empty($customers_news_por_bodegas_0y30days) ){
                    foreach ($customers_news_por_bodegas_0y30days as $bodega_id => $rows) {
                        foreach ($rows as $row) {
                            if( ! empty($row->bodega_id) ) $customers_news_por_bodegas[] = $row;
                        }
                    }
                }

                $bodegas = $this->indicators_model->getBodegas();

                $objPHPExcel = new PHPExcel();
                $cell_values = array(
                'A',
                'B',
                'C',
                'D',
                'E',
                'F',
                'G',
                'H',
                'I',
                'J',
                'K',
                'L',
                'M',
                'N',
                'O',
                'P',
                'Q',
                'R',
                'S',
                'T',
                );

                $cellvalue = 'A1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'E-mail');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);

                $cellvalue = 'B1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'Bodega');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);

                if (!empty($customers_news_por_bodegas)) {
                    $ini = 0;
                    $can = 1000;
                    $total = count($customers_news_por_bodegas);
                    $col = 2;
                    for ($i = 1; $ini <= $total; $i++) {
                        $ini = $i ? ($i - 1) * $can : 0;
                        $results = array_slice($customers_news_por_bodegas, $ini, $can);
                        if (!empty($results)) {
                            foreach ($results as $indice => $r) {
                                $indice = 0;
                                $cellvalue = $cell_values[$indice] . $col;
                                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $r->cust_email);
                                
                                $indice = 1;
                                $cellvalue = $cell_values[$indice] . $col;
                                $name_bodega = isset($bodegas[$r->bodega_id]['nombre']) ? $bodegas[$r->bodega_id]['nombre'] : '';
                                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $name_bodega);
                                
                                $col++;
                            }
                        }
                    }
                }

                $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="clientes-nuevos-' . date('d-m-Y') . '.xls"');
                $objWriter->save('php://output');

                break;

            case 'clients-buy-products-dog':
    
                $categorias = $this->products->getArbolCategories();
                
                $cats_perro = array();
                
                if (!empty($categorias)) {
                    foreach ($categorias as $cat_id => $c) {
                        $paths = explode('/', $c['path']);
                        // Perro
                        if (in_array(10, $paths)) {
                            $cats_perro[] = $cat_id;
                        }
                    }
                }

                // Total
                $srch = array(
                'orders_start_date' => $less_90_days_format,
                'orders_end_date' => $date_current_format,
                'orders_paid' => 1,
                );
                $customers_mail_between0y90days = $this->indicators_model->srchOrdersBuyByCategories($srch);

                $order_cat_perro = array();
                $order_cat_gato = array();

                if (!empty($customers_mail_between0y90days)) {
                    foreach ($customers_mail_between0y90days as $row) {
                        // Perro
                        if (in_array($row->cat_id, $cats_perro)) {
                            $order_cat_perro[] = $row;
                        }
                    }
                }

                $objPHPExcel = new PHPExcel();
                $cell_values = array(
                'A',
                'B',
                'C',
                'D',
                'E',
                'F',
                'G',
                'H',
                'I',
                'J',
                'K',
                'L',
                'M',
                'N',
                'O',
                'P',
                'Q',
                'R',
                'S',
                'T',
                );

                $cellvalue = 'A1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'E-mail');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);
                $cellvalue = 'B1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'Categoría');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);

                if (!empty($order_cat_perro)) {
                    $ini = 0;
                    $can = 1000;
                    $total = count($order_cat_perro);
                    $col = 2;
                    for ($i = 1; $ini <= $total; $i++) {
                        $ini = $i ? ($i - 1) * $can : 0;
                        $results = array_slice($order_cat_perro, $ini, $can);
                        if (!empty($results)) {
                            foreach ($results as $r) {
                                /*
                                // Perro - Adulto
                                if (in_array(5, $paths)) {
                                    $cats_perro_adulto[] = $cat_id;
                                }
                                
                                // Perro - Cachorro
                                if (in_array(6, $paths)) {
                                    $cats_perro_cachorro[] = $cat_id;
                                }
                                
                                // Perro - Necesidades especiales
                                if (in_array(7, $paths)) {
                                    $cats_perro_alimentos[] = $cat_id;
                                }
                                // Perro - Medicados
                                if (in_array(8, $paths)) {
                                    $cats_perro_alimentos[] = $cat_id;
                                }
                                // Perro - Golosinas
                                if (in_array(48, $paths)) {
                                    $cats_perro_alimentos[] = $cat_id;
                                }
                                
                                // Perro - Accesorios
                                if (in_array(19, $paths)) {
                                    $cats_perro_accesorio[] = $cat_id;
                                }
                                
                                // Perro - Juguetes
                                if (in_array(21, $paths)) {
                                    $cats_perro_juguetes[] = $cat_id;
                                }
                                
                                // Perro - Farmacia
                                if (in_array(17, $paths)) {
                                    $cats_perro_farmacia[] = $cat_id;
                                }

                                */
                                $indice = 0;
                                $cellvalue = $cell_values[$indice] . $col;
                                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $r->cust_email);
                                
                                $indice = 1;
                                $cellvalue = $cell_values[$indice] . $col;
                                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $r->cat_name);
                                
                                $col++;
                            }
                        }
                    }
                }

                $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="clientes-compran-productos-perro-' . date('d-m-Y') . '.xls"');
                $objWriter->save('php://output');

                break;

            case 'clients-buy-products-cat':
                $categorias = $this->products->getArbolCategories();
                
                $cats_gato = array();
                
                if (!empty($categorias)) {
                    foreach ($categorias as $cat_id => $c) {
                        $paths = explode('/', $c['path']);
                        // Gato
                        if (in_array(9, $paths)) {
                            $cats_gato[] = $cat_id;
                        }
                    }
                }

                // Total
                $srch = array(
                'orders_start_date' => $less_90_days_format,
                'orders_end_date' => $date_current_format,
                'orders_paid' => 1,
                );
                $customers_mail_between0y90days = $this->indicators_model->srchOrdersBuyByCategories($srch);

                $order_cat_gato = array();

                if (!empty($customers_mail_between0y90days)) {
                    foreach ($customers_mail_between0y90days as $row) {
                        // Gato
                        if (in_array($row->cat_id, $cats_gato)) {
                            $order_cat_gato[] = $row;
                        }
                    }
                }

                $objPHPExcel = new PHPExcel();
                $cell_values = array(
                'A',
                'B',
                'C',
                'D',
                'E',
                'F',
                'G',
                'H',
                'I',
                'J',
                'K',
                'L',
                'M',
                'N',
                'O',
                'P',
                'Q',
                'R',
                'S',
                'T',
                );

                $cellvalue = 'A1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'E-mail');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);
                $cellvalue = 'B1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'Categoría');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);

                if (!empty($order_cat_gato)) {
                    $ini = 0;
                    $can = 1000;
                    $total = count($order_cat_gato);
                    $col = 2;
                    for ($i = 1; $ini <= $total; $i++) {
                        $ini = $i ? ($i - 1) * $can : 0;
                        $results = array_slice($order_cat_gato, $ini, $can);
                        if (!empty($results)) {
                            foreach ($results as $r) {
                                $indice = 0;
                                $cellvalue = $cell_values[$indice] . $col;
                                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $r->cust_email);
                                
                                $indice = 1;
                                $cellvalue = $cell_values[$indice] . $col;
                                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $r->cat_name);
                                
                                $col++;
                            }
                        }
                    }
                }

                $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="clientes-compran-productos-gato-' . date('d-m-Y') . '.xls"');
                $objWriter->save('php://output');

                break;

            case 'clients-retiro-local':
                $srch = array(
                'orders_start_date' => $less_30_days_format,
                'orders_end_date' => $date_current_format,
                'orders_paid' => 1,
                );
                $orders_0y30days = $this->indicators_model->srchOrdersBuy($srch);
                
                $orders_0y30days_count = count($orders_0y30days);
                
                $locales = $this->indicators_model->getLocales();
                
                $orders_retiro_local = array();
                if (!empty($orders_0y30days)) {
                    foreach ($orders_0y30days as $row) {
                        //if ($row->bodega_id == 12) {
                        if ($row->bodega_id == 12 || ($row->addr_local_id != 0 && ! is_null($row->addr_local_id) ) ) {
                            $orders_retiro_local[] = $row;
                        }
                    }
                }

                $objPHPExcel = new PHPExcel();
                $cell_values = array(
                'A',
                'B',
                'C',
                'D',
                'E',
                'F',
                'G',
                'H',
                'I',
                'J',
                'K',
                'L',
                'M',
                'N',
                'O',
                'P',
                'Q',
                'R',
                'S',
                'T',
                );

                $cellvalue = 'A1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'E-mail');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);

                $cellvalue = 'B1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'Local');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);

                if (!empty($orders_retiro_local)) {
                    $ini = 0;
                    $can = 1000;
                    $total = count($orders_retiro_local);
                    $col = 2;
                    for ($i = 1; $ini <= $total; $i++) {
                        $ini = $i ? ($i - 1) * $can : 0;
                        $results = array_slice($orders_retiro_local, $ini, $can);
                        if (!empty($results)) {
                            foreach ($results as $r) {
                                $indice = 0;
                                $cellvalue = $cell_values[$indice] . $col;
                                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $r->cust_email);
                                
                                $local_nombre = isset($locales[$r->addr_local_id]) ? $locales[$r->addr_local_id]['alias'] : 'local-id-'.$r->addr_local_id;
                                
                                $indice = 1;
                                $cellvalue = $cell_values[$indice] . $col;
                                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $local_nombre);
                                
                                $col++;
                            }
                        }
                    }
                }

                $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="clientes-retiro-local' . date('d-m-Y') . '.xls"');
                $objWriter->save('php://output');

                break;

            case 'clients-despacho':
    
                $srch = array(
                'orders_start_date' => $less_30_days_format,
                'orders_end_date' => $date_current_format,
                'orders_paid' => 1,
                );
                $orders_0y30days = $this->indicators_model->srchOrdersBuy($srch);
                
                $orders_0y30days_count = count($orders_0y30days);
                
                $bodegas = $this->indicators_model->getBodegas();
                
                $orders_despacho = array();
                
                if (!empty($orders_0y30days)) {
                    foreach ($orders_0y30days as $row) {
                        if ($row->addr_local_id == 0) {
                            $orders_despacho[] = $row;
                        }
                    }
                }

                $objPHPExcel = new PHPExcel();
                $cell_values = array(
                'A',
                'B',
                'C',
                'D',
                'E',
                'F',
                'G',
                'H',
                'I',
                'J',
                'K',
                'L',
                'M',
                'N',
                'O',
                'P',
                'Q',
                'R',
                'S',
                'T',
                );

                $cellvalue = 'A1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'E-mail');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);
                $cellvalue = 'B1';
                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'Bodega');
                $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);
                //$cellvalue = 'C1';
                //$objPHPExcel->getActiveSheet()->setCellValue($cellvalue, 'Producto');
                //$objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);

                if (!empty($orders_despacho)) {
                    $ini = 0;
                    $can = 1000;
                    $total = count($orders_despacho);
                    $col = 2;
                    for ($i = 1; $ini <= $total; $i++) {
                        $ini = $i ? ($i - 1) * $can : 0;
                        $results = array_slice($orders_despacho, $ini, $can);
                        if (!empty($results)) {
                            foreach ($results as $r) {
                                
                                if( in_array($r->bodega_id, array(11, 12) )) continue;
                                
                                $indice = 0;
                                $cellvalue = $cell_values[$indice] . $col;
                                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $r->cust_email);
                                
                                $bodega_nombre = isset($bodegas[$r->bodega_id]) ? $bodegas[$r->bodega_id]['nombre'] : 'bodega-id-'.$r->bodega_id;
                                
                                $indice = 1;
                                $cellvalue = $cell_values[$indice] . $col;
                                $objPHPExcel->getActiveSheet()->setCellValue($cellvalue, $bodega_nombre);
                                
                                $col++;
                            }
                        }
                    }
                }

                $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="clientes-despacho-por-bodega' . date('d-m-Y') . '.xls"');
                $objWriter->save('php://output');

                break;

            default:
                # code...
                break;
        }
    }

    public function closeOrdersNotDelivered()
    {
        //echo phpinfo();
        //exit;
        if (!$this->user->is_super_admin) {
            $this->session->set_flashdata('error', 'No tienes permiso para ejecutar esa acción.');
            redirect("/");
        }
        
        $this->load->helper('form');
        
        $this->load->model(array(
        'orders',
        'products'
        ));
        
        $order_end_date = '2018-07-31';
        $date_current_format = date('Y-m-d');
        
        $srch = array(
        //'orders_start_date' => $less_270_days_format,
        'orders_end_date' => $order_end_date,
        );
        $ids = $this->orders->srchOrdersNotDelivered($srch);
        
        $res = 0;
        $cont = 0;
        if( ! empty($ids) ){
            foreach($ids as $indice => $row){
                $order = $this->orders->get($row->id);
                $data = array(
                "order_paid" => $order->paid,
                //'order_delivered' => '2018-07-31 00:00:00',
                
                //'driver_id' => $chofer_id,
                );
                $this->orders->update($row->id, $data);
            }
            $res = 1;
            $msje = "Se cerraron los pedidos exitosamente.";
        }else{
            $msje = "No se encontraron pedidos.";
        }
        
        if ($res == 1) {
            $this->session->set_flashdata('success', $msje);
        } else {
            $this->session->set_flashdata('error', $msje);
        }
        
        redirect('dashboard');
    }

    public function geocodeSimpliroute(){
        
        $ids = $this->input->post('checks');
        
        $orders = !empty($ids) ? $this->orders->getById($ids) : array();
        
        if( ! empty($orders) ){
            $geocode = array();
            foreach ($orders as $key => $o) {
                $o = $this->orders->load($o);
                $dir = $o->address->direccion.', '.$o->address->comuna.', '.$o->address->region.', Chile';
                $geocode[$o->id]['address'] = mb_convert_case($dir, MB_CASE_TITLE, "UTF-8");
            }
            
            $sr = new Simpliroute();
            $data = array_values($geocode);
            $result_geocode = $sr->geocode($data);
            
            if (! $result_geocode['error']) {
                $i=0;
                foreach ($orders as $key => $o) {
                    if( isset($result_geocode['response'][$i]) && $result_geocode['response'][$i]->ref_id ){
                        $response = $result_geocode['response'][$i];
                        $this->addresses->update($o->address->id, array('latitude'=>@$response->latitude, 'longitude'=>@$response->longitude));
                    }
                    $i++;
                }
                $this->session->set_flashdata('success', 'Para los pedidos seleccionados se realizó la georeferencia exitosamente.');
            }else{
                $this->session->set_flashdata('error', 'Ocurrio un error en la comunicación con Simpliroute.');
            }
        }else{
            $this->session->set_flashdata('error', 'No se encontraron pedidos seleccionados.');
        }
        
        redirect("dashboard");
    }

    public function sendToSimpliroute(){
    
        $this->load->model(array(
            'customers',
            'drivers',
            'orders',
        ));

        $ids = $this->input->post('checks');
        $tipo_opt = $this->input->post('tipo_opt');
        
        $orders = !empty($ids) ? $this->orders->getById($ids) : array();
        
        if( ! empty($orders) ){
            $validGeocode = true;
            foreach ($orders as $key => $o) {
                $o = $this->orders->load($o);
                if( ! $o->address->latitude || ! $o->address->latitude ){
                    $validGeocode = false;
                }
                if( $validGeocode == false ) break;
            }
            
            if( $validGeocode == true ){
            
                //Obtenemos a todos los choferes y vehículos
                $drivers = $this->drivers->getAllVehiculosSR();
                
                $json_optimize = array(
                    'vehicles' => array(),
                    'nodes' => array(),
                    'balance' => ($tipo_opt == 'balance' ? true : false),
                    'all_vehicles' => ($tipo_opt == 'all_vehicles' ? true : false),
                    'join' => ($tipo_opt == 'join' ? true : false),
                    'open_ended' => ($tipo_opt == 'open_ended' ? true : false),
                    'single_tour' => ($tipo_opt == 'single_tour' ? true : false),
                    'beauty' => ($tipo_opt == 'beauty' ? true : false),
                );
                
                foreach ($drivers as $key => $driver) {
                    $json_optimize['vehicles'][] = array(
                        "ident" => $driver->api_vehiculo_id,
                        "location_start" => array(
                            "ident"=>"vehicle-start-".$driver->api_vehiculo_id,
                            "lat"=>$driver->latitude_start,
                            "lon"=>$driver->longitude_start
                        ),
                        "location_end" => array(
                            "ident"=>"vehicle-end-".$driver->api_vehiculo_id,
                            "lat"=>$driver->latitude_end,
                            "lon"=>$driver->longitude_end
                        ),
                        "capacity"=>2000,
                        "shift_start"=>($driver->shift_start ? $driver->shift_start : '09:00'),
                        "shift_end"=>($driver->shift_end ? $driver->shift_end : '19:00'),
                        "skills"=>array(),
                        "refill"=>0,
                        "zones"=>array()
                    );
                }
            
                // La hora de inicio y termino de cada visita que sea dinamico

                $window_start = intval(date('G')) < 9 ? "09:00" : date("H:i");
                $window_end   = "23:59";

                foreach($orders as $key => $o){
                    $json_optimize['nodes'][] = array(
                        "ident" => $o->id,
                        "name" => $o->id,
                        "lat" => $o->address->latitude,
                        "lon" => $o->address->longitude,
                        "window_start" => $window_start,
                        "window_end" => $window_end,
                        "duration" => 2,
                        "load" => 0,
                        "priority_level" => 0,
                        "pickLoad" => 0,
                        "zones" => array()
                    );
                }

                //Obtimizamos los pedidos y choferes

                $sr = new Simpliroute();
                $result_optimize = $sr->optimize($json_optimize);

                $this->mylog->add(
                    array(
                        'action' => 'OPTIMIZE_RUTAS',
                        'description' => 'Estos son los datos que se envia los choferes y pedidosr, nos retorna por cada chofer y sus visitas segun la optmización de rutas en la plataforma de Simpliroute.',
                        'data' => $json_optimize,
                    )
                );

                $planned_date = date('Y-m-d');

                //Creamos el plan 
                $plan = array(
                    "name" => date('d-m-Y H:i:s'),
                    "start_date" => $planned_date,
                    "end_date" => $planned_date,
                    "routes" => array()
                );

                $plan_assignment = array();

                if (! $result_optimize['error']) {
                    $vehicles = $result_optimize['response']->vehicles;

                    $k=0;
                    foreach($vehicles as $indice => $vehicle){
                        if(isset($vehicle->tours) && !empty($vehicle->tours)){
                            $driverSR = $this->drivers->findVehicleSimpliroute($vehicle->ident);                            

                            if( isset($driverSR->id) && $driverSR->id ){
                                
                                $driver = $drivers[$driverSR->chofer_id];
                                
                                $plan['routes'][$k] = array(
                                    "driver" => $driverSR->api_chofer_id,
                                    "vehicle" => $driverSR->api_vehiculo_id,
                                    "planned_date" => $planned_date,
                                    //"total_distance" => "34647",
                                    //"total_duration" => "01 =>41 =>00",
                                    //"total_load" => 0,
                                    //"total_load_percentage" => 0,
                                    //"total_load_2" => 0,
                                    //"total_load_2_percentage" => 0,
                                    //"total_load_3" => 0,
                                    //"total_load_3_percentage" => 0,
                                    //"estimated_time_start" => "10 =>00",
                                    //"estimated_time_end" => "11 =>41",
                                    //"comment" => null,
                                    "location_start_address" => $driver->address_start,
                                    "location_start_latitude" => $driver->latitude_start,
                                    "location_start_longitude" => $driver->longitude_start,
                                    "location_end_address" => $driver->address_end,
                                    "location_end_latitude" => $driver->latitude_end,
                                    "location_end_longitude" => $driver->longitude_end,  
                                );

                                $tours = $vehicle->tours[0]->nodes;
                                foreach($tours as $key => $t){
                                    if( $t->ident == 'vehicle-start-'.$vehicle->ident ){
                                        $plan['routes'][$k]['estimated_time_start']= $t->departure;
                                    }else if( $t->ident == 'vehicle-end-'.$vehicle->ident ){
                                        $plan['routes'][$k]['estimated_time_end']= $t->arrival;
                                    }else{
                                        $order = $orders[$t->ident];
                                        $this->orders->get_products($order);
                                        
                                        $tipopago = $this->payments->get($order->paym_id);

                                        $mensaje = '
                                        Referencia Direccion: '.($order->address->comentarios ? utf8_decode($order->address->comentarios) : '-')."\n"."\n".'Comentario: '.$order->order_comment."\n"."\n".'Productos: '."\n".'--------------'."\n";
                                        foreach ($order->products as $indice => $p) {
                                            $mensaje .= $p->qty.' x '.utf8_decode($p->description->brand).' '.utf8_decode($p->description->name).' '.utf8_decode($p->description->size)."\n";
                                        }
                                        $mensaje .= '---------------'."\n"."\n".'Total: '."\n".'----------'."\n".'Subtotal: '.number_format($order->order_total, 0, ',', '.')."\n".'Envio: '.number_format($order->order_shipping, 0, ',', '.')."\n".'Total: '.number_format($order->order_total + $order->order_shipping - $order->order_purcharse_valor_desc, 0, ',', '.')."\n".'Tipo de pago: '.$tipopago->name."\n";

                                        $contact_name = mb_convert_case($order->customer->name, MB_CASE_UPPER, "UTF-8");
                                        $dir = $order->address->direccion.', '.$order->address->comuna.', '.$order->address->region.', Chile';
                                        $plan['routes'][$k]['visits'][] = array(
                                            "id" => null,
                                            "tracking_id" => $t->ident,
                                            "reference" => $t->ident,
                                            "title" => "Pedido-".$t->ident,
                                            "address" => $dir,
                                            "contact_name" => $contact_name,
                                            "contact_phone" => $order->customer->phone,
                                            "contact_email" => $order->customer->email,
                                            //"duration" => "10:00", 10min por minuto
                                            "estimated_time_arrival" => $t->arrival,
                                            "estimated_time_departure" => $t->departure,
                                            "latitude" => $order->address->latitude,
                                            "longitude" => $order->address->longitude,
                                            "load" => 0,
                                            "load_2" => 0,
                                            "load_3" => 0,
                                            "order" => $t->order,
                                            "notes" => utf8_encode($mensaje),
                                            "priority" => false,
                                            "priority_level" => 0,
                                            "skills_optional" => [],
                                            "skills_required" => [],

                                            "window_start" => $driver->shift_start,
                                            "window_end" => $driver->window_start_2,
                                            "window_start_2" => $driver->window_end_2,
                                            "window_end_2" => $driver->shift_end,
                                            "duration" => 120,
                                            //"duration" => ($driver->duration ? $driver->duration * 60 : 240),

                                            //"sms_enabled" => false,
                                            //"window_end" => "12:30", fin de la primera ventana de entrega (colacion)
                                            //"window_end_2" => "19:00", fin de la segunda ventana de entrga (termino de horario laboral)
                                            //"window_start" => "10:00", inicio de la primera ventana de entrega (inicio de horario laboral)
                                            //"window_start_2" => "15:00" inicio de la segunda ventana de entrega (cuando termine de la colacion)
                                        );

                                        $plan_assignment[] = array(
                                            'order_id' => $t->ident,
                                            'chofer_id' => $driverSR->chofer_id,
                                            'api_chofer_id' => $driverSR->api_chofer_id,
                                            'api_vehiculo_id' => $driverSR->api_vehiculo_id,
                                            'planned_date' => $planned_date,
                                            //'created_at' => date('Y-m-d H:i:s'),
                                        );
                                    }
                                }

                                $k++;
                            }
                        }
                    }

                    //print_a(json_encode($plan));
                    //exit;
                    
                    if( ! empty($plan['routes']) ){
                        $result_plan = $sr->createPlan($plan);

                        if (! $result_plan['error']) {

                            $response = $result_plan['response'];

                            $created_tmp = $response->created;
                            $created_tmp = str_replace('T', ' ', $created_tmp);
                            $created_tmp = str_replace('Z', '', $created_tmp);
                            $created_tmp = date('Y-m-d H:i:s', strtotime($created_tmp));
                            
                            $modified_tmp = $response->modified;
                            $modified_tmp = str_replace('T', ' ', $modified_tmp);
                            $modified_tmp = str_replace('Z', '', $modified_tmp);
                            $modified_tmp = date('Y-m-d H:i:s', strtotime($modified_tmp));

                            $data_plan_insert = array(
                                "ident" => $response->id,
                                "name" => $response->name,
                                "start_date" => $response->start_date,
                                "end_date" => $response->end_date,
                                "reset_day" => $response->reset_day,
                                "created" => $created_tmp,
                                "modified" => $modified_tmp,
                                "is_cluster" => $response->is_cluster,
                            );
                            
                            if( $plan_id = $this->simpliroute_model->create($data_plan_insert) ){
                                if( ! empty($response->routes) ){
                                    foreach ($response->routes as $key => $route) {
                                        $data_route_insert = array(
                                            "sr_plan_id" => $plan_id,
                                            "route" => $route
                                        );
                                        $this->simpliroute_model->createRoute($data_route_insert);
                                    }
                                }
                                if( ! empty($plan_assignment) ){
                                    foreach ($plan_assignment as $key => $pa) {
                                        $data_assignment_insert = array(
                                            'sr_plan_id' => $plan_id,
                                            'order_id' => $pa['order_id'],
                                            'chofer_id' => $pa['chofer_id'],
                                            'api_chofer_id' => $pa['api_chofer_id'],
                                            'api_vehiculo_id' => $pa['api_vehiculo_id'],
                                            'planned_date' => $pa['planned_date'],
                                            'created_at' => date('Y-m-d H:i:s'),
                                        );
                                        $this->simpliroute_model->createAssignment($data_assignment_insert);
                                        $this->orders->update($pa['order_id'], array('driver_id'=>$pa['chofer_id']));
                                    }
                                }
                                $this->mylog->add(
                                    array(
                                        'action' => 'CREAR_PLAN_SR',
                                        'description' => 'Se creó el plan correctamente en la plataforma de Simpliroute.',
                                        'data' => $plan,
                                    )
                                );
                                $this->session->set_flashdata('success', 'Se creó correctamente las rutas de los pedidos en la plataforma de Simpliroute.');
                                //print_a($result_plan);
                                //exit;
                            }
                        }else{
                            $this->mylog->add(
                                array(
                                    'action' => 'CREAR_PLAN_SR',
                                    'description' => 'Error al crear el plan en la plataforma de Simpliroute.',
                                    'data' => $plan,
                                )
                            );
                            $msje = "Error al crear el plan en Simpliroute : ".$result_plan['message'];
                            $this->session->set_flashdata('error', $msje);
                        }
                    }else{
                        $this->session->set_flashdata('error', 'No se encontraron rutas para poder crear un plan en Simpliroute.');
                    }
                }else{
                    $this->session->set_flashdata('error', 'Ocurrio un error en la comunicación con Simpliroute al optmizar las rutas de los pedidos.');
                }
            }else{
                $this->session->set_flashdata('error', 'Para los pedidos seleccionados no cuentan con la georefencia, favor de georeferenciarlo antes de enviarlo a Simpliroute.');
            }
        }else{
            $this->session->set_flashdata('error', 'No tienes pedidos seleccionados para enviar a Simppliroute.');
        }

        redirect("dashboard");
    }
}