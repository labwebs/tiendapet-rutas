<?php
/**
 * Created by PhpStorm.
 * User: jeans
 * Date: 08-06-17
 * Time: 12:28
 */
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH .'/libraries/Rest_Controller.php';
require APPPATH .'/libraries/Format.php';

class Routes extends REST_Controller{

    public function __construct() {
        parent::__construct();

        $this->load->library(array('ion_auth', 'notificacion'));
        $this->load->model('orders');
        $this->load->model('drivers');
        $this->load->model('simpliroute_model');
    }
    public function index_get(){

        die("entre");

        $idComuna = $this->comunas_model->all();
        $com = $this->comunas_model->get($idComuna);

        echo $this->response($com, 200);
    }

    public function visit_post(){

        //$post = $this->input->post();
        $post = json_decode(file_get_contents('php://input'), true);
        
        //print_a($post);
        //exit;
        
        $message = '';
        if( isset($post['id']) && $post['id'] ){
            
            $order_id = $post['reference'];

            if( $post['status'] == 'completed' ){
                // Actualizamos
                $data = array(
                    "order_delivered" => date('Y-m-d'),
                );
                $this->orders->update($order_id, $data);
                
                $driver = $this->simpliroute_model->findChoferByOrderAndPlannedDate($order_id, date('Y-m-d'));
                if( isset($driver->chofer_id) && $driver->chofer_id ){
                    $this->drivers->update($driver->chofer_id, array(
                        'last_delivery' => $order_id,
                    ));
                }
                $notificacion = new Notificacion();
                $notificacion->qualifyService($order_id);
                $msje = "ACTUALIZANDO EL PEDIDO COMPLETADO #".$order_id."...";
                $message .= "[STATUS: Completed, DRIVER_ID: ".$driver->chofer_id.", ORDER_DELIVERED: ".$driver->planned_date."] " . $msje . "\r\n";    
            }else{
                // Actualizamos
                /*
                $driver = $this->drivers->findByUserRolChofer('pedidosporconseguir');
                if (isset($driver->id) && $driver->id) {
                    $data = array(
                        //"order_delivery_date" => $date_current,
                        "driver_id" => $driver->id,
                    );
                    $this->orders->update($order_id, $data);
                }
                */
                $msje = "NO ACTUALIZANDO LA EL PEDIDO NO COMPLETADO #".$order_id."...";
                $message .= "[STATUS: ".ucfirst($post['status']).", DRIVER_ID: ".$driver->id.", ORDER_DELIVERY_DATE: ".date('Y-m-d')."] " . $msje . "\r\n";    
            }
        }

        $data = array('msje' => $message);
        $this->response($data, 200);
    }
    //$this->response(null, 400);
}