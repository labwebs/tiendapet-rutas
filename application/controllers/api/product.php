<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  
class Product extends CI_Controller {
  
  private $product_id;
  
  public function index($product_id, $method = 'show') {
    $this->product_id = $product_id;
    call_user_func_array(array($this, $method), array());
  }
  
  public function show() {
    $this->load->model('products');
    
    $product = $this->products->by_sku($this->product_id);
    
    $this->_put($product);
  }
  
  private function _put($data = null) {
    
    if ($data) {
      header('Content-Type: application/json');
      echo json_encode($data);
    }
    
  }
  
}