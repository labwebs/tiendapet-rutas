<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  
class Users extends CI_Controller {
  
  private $user_id;
  
  public function index($user_id, $method) {
    $this->user_id = $user_id;
    call_user_func_array(array($this, $method), array());
  }
  
  public function orders() {
    
    $orders = $this->db
      ->select('orders.*')
      ->join('orders', 'orders.id = sku_orders.order_id')
      ->where('orders.cust_id', $this->user_id)
      ->order_by('orders.order_created', 'desc')
      ->group_by('sku_orders.order_id')
      ->get('sku_orders')
      ->result();
    
    foreach ($orders as $o)
      $o->products = $this->db
        ->select('sku.id, brands.name, products.prod_name, sku.sku_description, sku.sku_price, sku_orders.qty')
        ->join('sku', 'sku_orders.sku_id = sku.id')
        ->join('products', 'sku.prod_id = products.id')
        ->join('brands', 'products.brand_id = brands.id')
        ->where('order_id', $o->id)
        ->get('sku_orders')
        ->result();
        
    $this->_put( $orders );   
  }
  
  private function _put($data = null) {
    
    if ($data) {
      header('Content-Type: application/json');
      echo json_encode($data);
    }
    
  }
  
}