<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CORE_Controller
{

    public $site_name = "TiendaPet &mdash; Gestión de rutas";

    function index()
    {

    }

    function login()
    {
        $this->load->helper('form');
        $this->load->view('login');
    }

    function logout()
    {
        $this->session->unset_userdata('user');
        $this->gohome();
    }

    function validate()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('users');

        $rules = array(
            array(
                'field' => 'username',
                'label' => 'Nombre de usuario',
                'rules' => 'required|trim'
            ),
            array(
                'field' => 'password',
                'label' => 'Contraseña',
                'rules' => 'required|trim'
            )
        );

        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run()) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $user = $this->users->get($username, $password);

            if ( isset($user->id) && $user->id ) {

                $user->choferes = $user->is_tomador_pedidos || $user->is_tomador_pedidos_despacho ? $this->users->getChoferes($user->id) : array();
                $user->bodegas  = $user->is_tomador_pedidos || $user->is_tomador_pedidos_despacho ? $this->users->getBodegas($user->id) : array();
                //$user->comunas  = $user->is_tomador_pedidos || $user->is_tomador_pedidos_despacho ? $this->users->getComunas($user->id) : array();
                //$user->comunas = array(46);
                $this->session->set_userdata('user', $user);

                if ($user->is_tomador_pedidos || $user->is_tomador_pedidos_despacho) {
                    $this->gohome();
                } else {
                    if ($user->is_driver) {
                        redirect("rutero");
                    } else {
                        $this->gohome();
                    }
                }
            }else {
                $this->load->view('login', array('error' => 'No se encontró un usuario con estos datos'));
            }
        }else{
            $this->load->view('login', array('error' => 'Todos los campos son obligatorios'));
        }
    }
}