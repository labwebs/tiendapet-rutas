<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rutero extends CORE_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('mylog');
        $this->load->library('notificacion');

        if ($this->user->is_driver) {
            if ($this->user->is_tomador_pedidos) {
                redirect("/");
            }
        } else {
            redirect("/");
        }
        /*
    if ($this->user->is_picking) {
    redirect("/picking");
    }
     */
    }

    public function index()
    {
        redirect("rutero/presente");
    }

    public function presente()
    {
        $this->load->model('orders');

        $params = array(
            "order_delivery_date" => date('Y-m-d'),
            "driver_id" => $this->user->id,
        );

        $this->renderRutero('rutero/listado', array(
            "pedidos" => $this->orders->search($params),
            "title" => "Entregas para hoy",
        ));
    }

    public function pasado()
    {
        $this->load->model('orders');

        $params = array(
            "order_delivery_date <" => date('Y-m-d'),
            "order_delivery_date >=" => date('Y-m-d', strtotime("-2 days")),
            "driver_id" => $this->user->id,
        );

        $this->renderRutero('rutero/listado', array(
            "pedidos" => $this->orders->search($params),
            "title" => "Entregas anteriores",
            "text" => "Mostrando sólo las últimas 100",
        ));
    }

    public function futuro()
    {
        $this->load->model('orders');

        $params = array(
            "order_delivery_date >" => date('Y-m-d'),
            "driver_id" => $this->user->id,
        );

        $this->renderRutero('rutero/listado', array(
            "pedidos" => $this->orders->search($params),
            "title" => "Próximas entregas",
        ));
    }

    public function confirmar()
    {
        /*
        $notificacion = new Notificacion();
        $notificacion->qualifyService(167793);
        exit;
         */

        $this->load->model(array('orders', 'drivers'));

        $id = $this->input->post('order_id');

        $order = $this->orders->get($id, true);

        $data = array(
            "order_delivered" => $order->order_delivery_date,
            "order_paid" => 1,
            //"order_paid" => $this->input->post('payment') == '1' ? 1 : 0
            //"paym_id" => $this->input->post('payment')
        );

        $this->orders->update($id, $data);

        $this->drivers->update($this->user->id, array(
            'last_delivery' => $id,
        ));

        $notificacion = new Notificacion();
        $notificacion->qualifyService($id);

        $this->mylog->add(
            array(
                'order_id' => $id,
                'action' => 'CONFIRMAR_PEDIDO_CHOFER',
                'description' => 'Chofer confirma el pedido.',
                'data' => $data,
            )
        );

        $this->goback();
    }

    public function reporte($anterior = false)
    {
        $this->load->model(array(
            'orders',
            'payments',
        ));

        $date = $anterior ? date("Y-m-d", strtotime("-1 day")) : date("Y-m-d");

        $report = array();
        $payments = $this->payments->all();

        foreach ($payments as $p):

            $orders = $this->orders->search(array(
                "driver_id" => $this->user->id,
                "paym_id" => $p->id,
                "DATE(order_delivered)" => $date,
            ));

            $report[] = array(
                'name' => $p->name,
                'orders' => $orders,
                'total' => array_reduce($orders, function ($carry, $item) {
                    return $carry + $item->order_total;
                }),
            );

        endforeach;

        $this->renderRutero('rutero/reporte', array(
            "report" => $report,
            "fecha" => date("d/m/Y", strtotime($date)),
        ));
    }
}
