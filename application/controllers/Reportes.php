<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  
class Reportes extends CORE_Controller {
  
  function __construct() {
    parent::__construct();

    if( $this->user->is_driver || $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ){
      redirect("/");
    }
  }
  
  function index() {
    
    $this->load->helper('form');
    $this->load->model( array(
      'drivers'
    ));
    
    $this->render('reportes/index', array(
      'drivers' => $this->drivers->all(false),
      'date'    => date('Y-m-d')
    ));  
  }
  
  function generar() {
    
    $this->load->model( array(
      'drivers',
      'orders',
      'payments'
    ));
    
    $date   = $this->input->post('date') ?: null;
    $driver = $this->input->post('driver') ?: null;
    
    $report   = array();
    $payments = $this->payments->all();
    
    foreach($payments as $p) :
      
      $orders = $this->orders->search(array(
        "driver_id"             => $driver,
        "paym_id"               => $p->id,
        "DATE(order_delivered)" => $date
      ));
      
      $report[] = array(
        'name' => $p->name,
        'orders'  => $orders,
        'total'   => array_reduce($orders, function ($carry, $item) {
          return $carry + ($item->order_total + $item->order_shipping);
        })
      );
      
    endforeach;
    
    $this->render('reportes/show', array(
      "driver"    => $this->drivers->get($driver, false),
      "report"    => $report,
      "date"      => date('d/m/Y', strtotime($this->input->post('date')))
    )); 
  }
  
}