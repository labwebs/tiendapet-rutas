<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Ajax extends CI_Controller
{
    public function picking()
    {
        $sku_orders_id = $this->input->post('sku_orders_id');

        $userid = $this->input->post('userid');
        $checked = $this->input->post('checked');
        
        $this->load->model('orders');
        
        $sku_orders_id = strpos($sku_orders_id, ',') === false ? array($sku_orders_id) : explode(',', $sku_orders_id);
        
        //$res = $this->orders->updCheckPicking($sku_orders_id, array('picking_check' => $checked, 'picking_userid' => $userid, 'picking_updated_at' => date('Y-m-d H:i:s')));
        
        $res = 0;
        if( ! empty($sku_orders_id) ){
            foreach( $sku_orders_id as $sku_order_id ){
                $res = $this->orders->updCheckPicking($sku_order_id, array('picking_check' => $checked, 'picking_userid' => $userid, 'picking_updated_at' => date('Y-m-d H:i:s')));
            }
        }

        $data = array('res' => $res);
        
        echo json_encode($data);
    }

    public function updStatusChoferSR()
    {

        $this->load->model('drivers');

        $chofer_id = $this->input->post('chofer_id');
        $checked = $this->input->post('checked');
        $inactivo = $checked == "true" ? 0 : 1;
        
        $res = $this->drivers->update($chofer_id, array('inactivo_sr' => $inactivo));
        
        $data = array('res' => $res, 'inactivo'=>$inactivo, 'chofer_id'=>$chofer_id);
        
        echo json_encode($data);
    }

    public function selComunas()
    {
        $this->load->model('orders');

        $region_id = $this->input->post('me_region_id');
        $comunas = $region_id ? $this->orders->getSelectsComunasPorRegion($region_id) : array();
        
        foreach ($comunas as $key => $c) {
            $comunas[$key]->nombre = htmlentities(mb_convert_case(mb_strtolower($comunas[$key]->nombre,'UTF-8'),MB_CASE_TITLE,'UTF-8'));
        }
        echo json_encode($comunas);
    }

    public function selSucursalBsale()
    {
        $this->load->library('bsale');
        $this->load->model('bodegas');

        $empresa_id = $this->input->post('empresa_id');
        $bodegasBsale = $empresa_id ? $this->bodegas->getBsaleBodegasByEmpresa($empresa_id) : array();
        
        $bodegas = array();
        if( ! empty($bodegasBsale) ){
            foreach ($bodegasBsale as $indice => $bodega) {
                $bodegas[$bodega->id] = new stdClass();
                $bodegas[$bodega->id]->id = $bodega->id;
                $bodegas[$bodega->id]->nombre = htmlentities(mb_convert_case(mb_strtolower($bodega->bsaje_bodega_nombre,'UTF-8'),MB_CASE_TITLE,'UTF-8'));
            }
        }
        
        echo json_encode($bodegas);
    }

    public function bsaleSearchBySku()
    {
        $this->load->library('bsale');
        $this->load->model('products');
        $this->load->model('bodegas');

        $skuid = $this->input->post('skuid');
        $empresa_id = $this->input->post('empresa_id');
        $bodega_id  = $this->input->post('bodega_id');

        $sku = $skuid ? $this->products->by_sku($skuid) : array();

        $bodegasBsale = $empresa_id ? $this->bodegas->getBsaleBodegasByEmpresa($empresa_id) : array();
        
        /*
        $bodegas = array();
        if( ! empty($bodegasBsale) ){
            foreach ($bodegasBsale as $indice => $bodega) {
                print_a($bodega);
                exit;
                $bodegas[$bodega->id] = new stdClass();
                $bodegas[$bodega->id]->id = $bodega->id;
                $bodegas[$bodega->id]->nombre = htmlentities(mb_convert_case(mb_strtolower($bodega->bsaje_bodega_nombre,'UTF-8'),MB_CASE_TITLE,'UTF-8'));
            }
        }
        */

        $html = '';
        
        if( isset($sku->prod_id) && $sku->prod_id ){
            
            $html = '
            <fieldset class="form-group">
                <label class="col-sm-3 control-label">Producto</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" disabled value="'.$sku->name.'" />
                </div>
            </fieldset>
            
            <fieldset class="form-group">
                <label class="col-sm-3 control-label">Marca</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" disabled value="'.$sku->brand.'" />
                </div>
            </fieldset>

            <fieldset class="form-group">
                <label class="col-sm-3 control-label">Tamaño</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" disabled value="'.$sku->size.'" />
                </div>
            </fieldset>
            
            <fieldset class="form-group">
                <label class="col-sm-3 control-label">Precio</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" disabled value="$ '.number_format($sku->price, 0, ',', '.').'" />
                </div>
            </fieldset>
            ';

            $empresa = $this->bodegas->getEmpresa($empresa_id);
            $bodega = $this->bodegas->getBodegaEmpresa($bodega_id);

            $bs = new Bsale();
            $bs->setToken($empresa->token);

            //$skuBsale = $bs->getStockProductBySku($skuid, $bodega->bsale_bodega_id);
            //print_a($skuBsale);
            //exit;
            $html .= '
                <h4 class="media-heading text-center"><a>Bsale Stock</a></h4>
            ';

            if( ! empty($bodegasBsale) ){
                foreach($bodegasBsale as $indice => $bodega){

                    $skuBsale = $bs->getStockProductBySku($skuid, $bodega->bsale_bodega_id);
                    if( isset($skuBsale->id) && $skuBsale->id ){
                        $html .= '
                        <fieldset class="form-group text-center col-md-3">
                            <label class="control-label">'.$bodega->bsaje_bodega_nombre.'</label>
                            <div class="">
                                <input type="text" class="form-control" disabled value="'.(isset($skuBsale->quantity) ? intval($skuBsale->quantity) : '-').'" />
                            </div>
                        </fieldset>
                        ';
                    }else{
                        $html .= '
                        <fieldset class="form-group text-center col-md-3">
                            <label class="control-label">'.$bodega->bsaje_bodega_nombre.'</label>
                            <div class="">
                                <input type="text" class="form-control" disabled value="No existe SKU" />
                            </div>
                        </fieldset>
                        ';
                    }
                }
            }

            
        }else{
            $html = '
            <fieldset class="form-group text-center">
                <label class="col-md-12 control-label">No se encontras registrado en el sistema.</label>
            </fieldset>
            ';
        }


        $res = array(
            'html' => $html
        );

        echo json_encode($res);
    }
}