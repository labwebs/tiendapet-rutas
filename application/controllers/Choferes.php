<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Choferes extends CORE_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('drivers');
        $this->load->model('addresses');
        $this->load->library('bsale');
        $this->load->library('simpliroute');
        $this->load->library('mylog');

        if ($this->router->fetch_method() != 'imprimir' && $this->router->fetch_method() != 'imprimirproductosagrupados') {
            if ($this->user->is_driver || $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho) {
                redirect("/");
            }
            if ($this->user->is_picking) {
                redirect("/picking");
            }
        }
    }

    public function index()
    {
        $data = array(
            'choferes' => $this->drivers->all(),
        );
        $this->render('choferes/listado', $data);
    }

    public function crear()
    {
        $this->load->helper('form');
        $selectsRegiones = $this->addresses->selectsRegiones();

        $bs = new Bsale();
        $bodegas = $bs->getSucursales();

        //$bodegas = $this->drivers->getBodegas();

        $this->render('choferes/crear', array(
            'selectsRegiones' => $selectsRegiones,
            'selectsBodegas' => $bodegas,
        ));
    }

    public function editar($id)
    {
        $this->load->helper('form');
        $this->load->model('drivers');

        $selectsRegiones = $this->addresses->selectsRegiones();

        $comunas_rel_chofer = $this->drivers->getComunas($id);

        $comuna_ids = array();
        if (!empty($comunas_rel_chofer)) {
            foreach ($comunas_rel_chofer as $indice => $comuna) {
                $comuna_ids[] = $comuna->comuna_id;
            }
        }

        $bs = new Bsale();
        $bodegas = $bs->getSucursales();

        $driver = $this->drivers->find($id);
        $driver->simpliroute = $this->drivers->findChoferSimpliroute($id);
        
        //$bodegas = $this->drivers->getBodegas();

        $this->render('choferes/editar', array(
            'driver' => $driver,
            'comuna_ids' => $comuna_ids,
            'selectsRegiones' => $selectsRegiones,
            'selectsBodegas' => $bodegas,
        ));
    }

    public function imprimir($id = false)
    {
        if (!$id) {
            redirect($this->input->server('HTTP_REFERER'));
        }

        $this->load->model(array('drivers', 'orders'));

        $search = $this->session->userdata('search');

        $overview = $this->drivers->overview($id, $search);

        $payments = $this->orders->getPayments();

        foreach ($overview->orders as &$o) {
            $o = $this->orders->get_products($o);
        }

        $locales = $this->orders->getLocales();

        $this->load->view("choferes/print", array(
            'driver' => $overview,
            'payments' => $payments,
            'locales' => $locales,
        ));
    }

    public function imprimirproductosagrupados($id = false)
    {
        if (!$id) {
            redirect($this->input->server('HTTP_REFERER'));
        }

        $this->load->model(array('drivers', 'orders'));

        $search = $this->session->userdata('search');

        $overview = $this->drivers->overview($id, $search);

        $payments = $this->orders->getPayments();

        foreach ($overview->orders as &$o) {
            $o = $this->orders->get_products($o);
        }

        $locales = $this->orders->getLocales();
        
        $products = array();
        if( ! empty($overview) ){
            foreach ($overview->orders as $key => $value) {
                foreach ($value->products as $p){
                    $products[$p->sku_id]['producto'] = array('brand'=>$p->description->brand, 'name'=>$p->description->name, 'size'=>$p->description->size );
                    @$products[$p->sku_id]['qty'] += @$p->qty;
                    @$products[$p->sku_id]['orders'][$p->id] = @$p->id;
                    @$products[$p->sku_id]['pinkings'][$p->id] = @$p->picking_check;
                }
            }
        }
        uasort($products, function($a, $b) {
            return strcmp($a['qty'], $b['qty']);
        });
        $products = array_reverse($products);
        
        $this->load->view("choferes/printproductsgrouped", array(
            'driver' => $overview,
            'products' => $products,
            'payments' => $payments,
            'locales' => $locales,
        ));
    }

    public function picking($id = false)
    {
        if (!$id) {
            redirect($this->input->server('HTTP_REFERER'));
        }

        $this->load->model(array('drivers', 'orders'));

        $search = $this->session->userdata('search');

        $overview = $this->drivers->overview($id, $search);

        $payments = $this->orders->getPayments();

        foreach ($overview->orders as &$o) {
            $o = $this->orders->get_products($o);
        }

        $this->load->view("choferes/picking", array(
            'driver' => $overview,
            'payments' => $payments,
        ));
    }

    public function actualizar()
    {
        $this->load->library('form_validation');
        $this->load->model('drivers');

        $form_data = $this->input->post();
        
        if( !isset($form_data['id']) && !$form_data['id'] ){
            redirect("choferes");
        }

        $this->form_validation->set_error_delimiters('<label class="error">', '</label>');
        $this->form_validation->set_rules(array(
            array(
                "field" => "admin_name",
                "label" => "<b>NOMBRE COMPLETO</b>",
                "rules" => "required",
            ),
            array(
                "field" => "admin_login",
                "label" => "<b>NOMBRE DE USUARIO</b>",
                "rules" => "required",
            ),
            array(
                "field" => "email",
                "label" => "<b>CORREO ELECTRÓNICO</b>",
                "rules" => "required|valid_email",
            ),
            array(
                "field" => "phone",
                "label" => "<b>TELÉFONO</b>",
                "rules" => "required",
            ),
            array(
                "field" => "password",
                "label" => "<b>CONTRASEÑA</b>",
                "rules" => "min_length[6]",
            ),
            array(
                "field" => "password_match",
                "label" => "<b>CONFIRMACIÓN DE CONTRASEÑA</b>",
                "rules" => "matches[password]",
            ),
            array(
                "field" => "id",
                "label" => "<b>ID DE CHOFER</b>",
                "rules" => "integer|required",
            ),
        ));

        if ($this->form_validation->run()){
            //print_a($form_data);
            //exit;

            $chofer_id = $this->input->post('id');
            $comunas = $this->input->post('comuna_id');

            if (!empty($comunas)) {
                $comunas_rel_chofer = $this->drivers->getComunas($chofer_id);

                if (!empty($comunas_rel_chofer)) {
                    $comuna_ids = array();
                    if (!empty($comunas_rel_chofer)) {
                        foreach ($comunas_rel_chofer as $indice => $comuna) {
                            if (!in_array($comuna->comuna_id, $comunas)) {
                                $this->drivers->deleteComuna($comuna->id);
                            }

                            $comuna_ids[] = $comuna->comuna_id;
                        }
                    }

                    //Insertamos
                    foreach ($comunas as $indice => $com_id) {
                        if (!in_array($com_id, $comuna_ids)) {
                            $this->drivers->insertComuna(array('chofer_id' => $chofer_id, 'comuna_id' => $com_id));
                        }
                    }
                } else {
                    foreach ($comunas as $indice => $com_id) {
                        $this->drivers->insertComuna(array('chofer_id' => $chofer_id, 'comuna_id' => $com_id));
                    }
                }
            }

            $latitude_start = $this->input->post('latitude_start') ? round($this->input->post('latitude_start'), 6) : '';
            $longitude_start = $this->input->post('longitude_start') ? round($this->input->post('longitude_start'), 6) : '';

            $latitude_end = $this->input->post('latitude_end') ? round($this->input->post('latitude_end'), 6) : '';
            $longitude_end = $this->input->post('longitude_end') ? round($this->input->post('longitude_end'), 6) : '';

            $data = array(
                'admin_name' => $this->input->post('admin_name'),
                'admin_login' => $this->input->post('admin_login'),
                'role_id' => 1,
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'chofer_region_id' => $this->input->post('region_id'),
                'chofer_bodega_id' => $this->input->post('chofer_bodega_id'),
                'address_start' => $this->input->post('address_start'),
                'latitude_start' => $latitude_start,
                'longitude_start' => $longitude_start,
                'address_end' => $this->input->post('address_end'),
                'latitude_end' => $latitude_end,
                'longitude_end' => $longitude_end,
                'shift_start' => $this->input->post('shift_start'),
                'shift_end' => $this->input->post('shift_end'),
                'window_start_2' => $this->input->post('window_start_2'),
                'window_end_2' => $this->input->post('window_end_2'),
                'duration' => $this->input->post('duration'),
                'inactivo' => $this->input->post('inactivo'),  
                'inactivo_sr' => $this->input->post('inactivo_sr')  
            );

            if ($pass = $this->input->post('password')) {
                $data['password'] = md5($pass);
            }

            $id = $this->input->post('id');
            $driver = $this->drivers->find($id);
            $driver->simpliroute = $this->drivers->findChoferSimpliroute($id);

            if( $this->drivers->update($this->input->post('id'), $data) ){
                
                $this->mylog->add(
                    array(
                        'action' => 'ACTUALIZAR_CHOFER',
                        'description' => 'Dentro de la sección Choferes.',
                        'data' => $data,
                    )
                );

                $error_sr = false;
                
                $sr = new Simpliroute();
                //$delete_repartidor = $sr->deleteRepartidor(array('id'=>17883));

                $action_sr = isset($form_data['checksr']) && $form_data['checksr'] ? $form_data['checksr'] : '';
                if( $action_sr == 'create' ){

                    /** SimpliRoute **/
                    //Crear el repartidor
                    $data_insert_chofer = array(
                        'username' => 'tp_'.$form_data['admin_login'],
                        //'username' => $form_data['admin_login'],
                        'name' => $form_data['admin_name'],
                        'is_admin' => false,
                        'is_driver' => true,
                        'password' => $form_data['password']
                    );

                    $sr = new Simpliroute();
                    $result_repartidor = $sr->createRepartidor($data_insert_chofer);
                    
                    if( ! $result_repartidor['error'] ){
                        
                        //Creamos el Vehiculo para el repartidor
                        $data_insert_vehiculo = array(
                            'name' => 'tp_'.$form_data['admin_login'].'-v',
                            'capacity' => 2000,
                            'default_driver' => $result_repartidor['response']->id,
                            'location_start_address' => $form_data['address_start'],
                            'location_start_latitude' => $latitude_start,
                            'location_start_longitude' => $longitude_start,
                            'location_end_address' => $form_data['address_end'],
                            'location_end_latitude' => $latitude_end,
                            'location_end_longitude' => $longitude_end,
                            'shift_start' => $form_data['shift_start'],
                            'shift_end' => $form_data['shift_end'],
                            'skills' => array()
                        );

                        // Crear Vehiculo
                        $result_vehiculo = $sr->createVehiculo($data_insert_vehiculo);
                        if ( ! $result_vehiculo['error']) {
                            $data = array(
                                'chofer_id' => $form_data['id'],
                                'api_chofer_id' => $result_repartidor['response']->id,
                                'api_vehiculo_id' => $result_vehiculo['response']->id,
                                'created_at' => date('Y-m-d H:i:s'),
                            );
                            $this->drivers->insert_srChofer($data);
                        }else{
                            $error_sr = true;

                            $sr->deleteRepartidor(array('id'=>$result_repartidor['response']->id));
                            $msje = "Error al crear el vehiculo : ".$result_vehiculo['message'];
                        }
                    }else{
                        $error_sr = true;
                        $msje = "Error al crear al repartidor : ".$result_repartidor['message'];
                    }
                    
                }else if( $action_sr == 'update' ){

                    /** SimpliRoute **/
                    //Crear el repartidor
                    $data_update_chofer = array(
                        'username' => 'tp_'.$form_data['admin_login'],
                        'name' => $form_data['admin_name'],
                        'is_admin' => false,
                        'is_driver' => true,
                    );

                    if ($pass = $this->input->post('password')) {
                        $data_update_chofer['password'] = $pass;
                    }

                    $sr = new Simpliroute();
                    $result_repartidor = $sr->updateRepartidor($driver->simpliroute->api_chofer_id, $data_update_chofer);
                    if ( ! $result_repartidor['error'] ) {
                        $data_update_vehiculo = array(
                            'name' => 'tp_'.$form_data['admin_login'].'-v',
                            'capacity' => 2000,
                            'default_driver' => $result_repartidor['response']->id,
                            'location_start_address' => $form_data['address_start'],
                            'location_start_latitude' => $latitude_start,
                            'location_start_longitude' => $longitude_start,
                            'location_end_address' => $form_data['address_end'],
                            'location_end_latitude' => $latitude_end,
                            'location_end_longitude' => $longitude_end,
                            'shift_start' => $form_data['shift_start'],
                            'shift_end' => $form_data['shift_end'],
                            'skills' => array()
                        );
                        $result_vehiculo = $sr->updateVehiculo($driver->simpliroute->api_vehiculo_id, $data_update_vehiculo);
                        if ( ! $result_vehiculo['error'] ) {
                        }else{
                            $error_sr = true;
                            $msje = "Error al actualizar los datos del vehiculo : ".$result_vehiculo['message'];
                        }
                    }else{
                        $error_sr = true;
                        $msje = "Error al actualizar los datos del repartidor : ".$result_vehiculo['message'];
                    }                    
                }

                if( $error_sr ){
                    $this->session->set_flashdata('error', "Error con la API de Simpleroute<br>".$msje);
                }else{
                    $msje = "Se modificó con éxito los datos.";
                    $this->session->set_flashdata('success', $msje);
                }

            }else{
                $msje = "Se produjo un error al actualizar los datos.";
                $this->session->set_flashdata('error', $msje);
            }   
            //$this->editar($this->input->post('id'));
            //redirect("choferes/editar/".$this->input->post('id'));
        }
        redirect("choferes/editar/".$this->input->post('id'));
    }

    public function guardar()
    {
        $this->load->library('form_validation');
        $this->load->model('drivers');

        $this->form_validation->set_error_delimiters('<label class="error">', '</label>');
        $this->form_validation->set_rules(array(
            array(
                "field" => "admin_name",
                "label" => "<b>NOMBRE COMPLETO</b>",
                "rules" => "required",
            ),
            array(
                "field" => "admin_login",
                "label" => "<b>NOMBRE DE USUARIO</b>",
                "rules" => "required|is_unique[admins.admin_login]",
            ),
            array(
                "field" => "email",
                "label" => "<b>CORREO ELECTRÓNICO</b>",
                "rules" => "required|valid_email",
            ),
            array(
                "field" => "phone",
                "label" => "<b>TELÉFONO</b>",
                "rules" => "required",
            ),
            array(
                "field" => "password",
                "label" => "<b>CONTRASEÑA</b>",
                "rules" => "min_length[6]|required",
            ),
            array(
                "field" => "password_match",
                "label" => "<b>CONFIRMACIÓN DE CONTRASEÑA</b>",
                "rules" => "required|matches[password]",
            ),
        ));

        $form_data = $this->input->post();

        if ( $this->form_validation->run() ){

            $latitude_start = $this->input->post('latitude_start') ? round($this->input->post('latitude_start'), 6) : '';
            $longitude_start = $this->input->post('longitude_start') ? round($this->input->post('longitude_start'), 6) : '';

            $latitude_end = $this->input->post('latitude_end') ? round($this->input->post('latitude_end'), 6) : '';
            $longitude_end = $this->input->post('longitude_end') ? round($this->input->post('longitude_end'), 6) : '';

            $data = array(
                'admin_name' => $this->input->post('admin_name'),
                'admin_login' => $this->input->post('admin_login'),
                'role_id' => 1,
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'password' => md5($this->input->post('password')),
                'inactivo' => $this->input->post('inactivo'),
                'chofer_region_id' => $this->input->post('region_id'),
                'chofer_bodega_id' => $this->input->post('chofer_bodega_id'),

                'address_start' => $this->input->post('address_start'),
                'latitude_start' => $latitude_start,
                'longitude_start' => $longitude_start,
                'address_end' => $this->input->post('address_end'),
                'latitude_end' => $latitude_end,
                'longitude_end' => $longitude_end,
                'shift_start' => $this->input->post('shift_start'),
                'shift_end' => $this->input->post('shift_end'),
                'window_start_2' => $this->input->post('window_start_2'),
                'window_end_2' => $this->input->post('window_end_2'),
                'duration' => $this->input->post('duration'),
                'inactivo_sr' => $this->input->post('inactivo_sr')  
            );

            $id = $this->drivers->create($data);

            $comunas = $this->input->post('comuna_id');

            if (!empty($comunas)) {
                foreach ($comunas as $indice => $com_id) {
                    $this->drivers->insertComuna(array('chofer_id' => $id, 'comuna_id' => $com_id));
                }
            }

            $this->mylog->add(
                array(
                    'action' => 'GUARDAR_CHOFER',
                    'description' => 'Dentro de la sección Choferes.',
                    'data' => $data,
                )
            );

            $error_sr = false;
            $msje = '';

            if( $id ){
                
                $sr = new Simpliroute();
                //$delete_repartidor = $sr->deleteRepartidor(array('id'=>17883));

                $action_sr = isset($form_data['checksr']) && $form_data['checksr'] ? $form_data['checksr'] : '';
                if ($action_sr == 'create') {

                    /** SimpliRoute **/
                    //Crear el repartidor
                    $data_insert_chofer = array(
                        'username' => 'tp_'.$form_data['admin_login'],
                        'name' => $form_data['admin_name'],
                        'is_admin' => false,
                        'is_driver' => true,
                        'password' => $form_data['password'],
                    );

                    $sr = new Simpliroute();
                    $result_repartidor = $sr->createRepartidor($data_insert_chofer);
                    
                    if (! $result_repartidor['error']) {
                        
                        //Creamos el Vehiculo para el repartidor
                        $data_insert_vehiculo = array(
                            'name' => 'tp_'.$form_data['admin_login'].'-v',
                            'capacity' => 2000,
                            'default_driver' => $result_repartidor['response']->id,
                            'location_start_address' => $form_data['address_start'],
                            'location_start_latitude' => $latitude_start,
                            'location_start_longitude' => $longitude_start,
                            'location_end_address' => $form_data['address_end'],
                            'location_end_latitude' => $latitude_end,
                            'location_end_longitude' => $longitude_end,
                            'shift_start' => $form_data['shift_start'],
                            'shift_end' => $form_data['shift_end'],
                            'skills' => array()
                        );

                        // Crear Vehiculo
                        $result_vehiculo = $sr->createVehiculo($data_insert_vehiculo);
                        if (! $result_vehiculo['error']) {
                            $data = array(
                                'chofer_id' => $id,
                                'api_chofer_id' => $result_repartidor['response']->id,
                                'api_vehiculo_id' => $result_vehiculo['response']->id,
                                'created_at' => date('Y-m-d H:i:s'),
                            );
                            $this->drivers->insert_srChofer($data);
                        } else {
                            $error_sr = true;

                            $sr->deleteRepartidor(array('id'=>$result_repartidor['response']->id));
                            $msje = "Error al crear el vehiculo : ".$result_vehiculo['message'];
                        }
                    } else {
                        $error_sr = true;
                        $msje = "Error al crear al repartidor : ".$result_repartidor['message'];
                    }
                } 

                if ($error_sr) {
                    $this->session->set_flashdata('error', "Error con la API de Simpleroute<br>".$msje);
                } else {
                    $msje = "Se creó el chofer con éxito.";
                    $this->session->set_flashdata('success', $msje);
                }
            }

            redirect("choferes");
        }
    }

    public function selComunas()
    {
        $region_id = $this->input->post('region_id');

        $comunas = $region_id ? $this->addresses->getComunasByRegion($region_id) : array();

        echo json_encode($comunas);
    }
}
