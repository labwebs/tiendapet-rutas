<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  
class Bsaleapi extends CORE_Controller {
  
    function __construct() {
        parent::__construct();

        //if( $this->user->is_driver || $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ){
        //    redirect("/");
        //}

        $this->load->library('bsale');

        $this->load->model( array(
            'orders',
            'bodegas'
        ));
    }
  
    function index() {
        
        redirect("searchbysku");

        //$this->load->helper('form');
    }

    function searchbysku() {
        
        $empresas = $this->bodegas->getBsaleEmpresasAll();
    
        $this->render('bsaleapi/searchbysku', array(
            "empresas"    => $empresas,
        )); 
    }
}