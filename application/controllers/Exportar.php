<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  
class Exportar extends CORE_Controller {
  
    function __construct() {
        parent::__construct();

        if( $this->user->is_driver || $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ){
            redirect("/");
        }
    }
  
    function index() {
        
        $this->load->helper('form');
        $this->load->model(array('drivers'));
    
        $this->render('exportar/index', array(
            'drivers' => $this->drivers->all(false),
            'date'    => date('Y-m-d'),
            "paid"    => 1
        ));  
    }

    function generarXLS(){
        
        $this->load->model(array(
            'drivers',
            'orders',
            //'payments'
        ));

        $search = $this->session->userdata('search_exportar');

        $orders = $this->orders->search(array(
            "driver_id"             => $search['driver_id'],
            "DATE(order_delivery_date)" => $search['date'],
            "paid" => 1
            //"paym_id"               => $p->id,
        ));

        ini_set('memory_limit', '-1');
        $objPHPExcel = new PHPExcel();
        $cell_values = array(
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'L',
            'M',
            'N',
            'O',
            'P',
            'Q',
            'R'
        );

        $thead = array(
            'nombre' => 'Nombre',
            'direccion' => 'Dirección',
            'comuna' => 'Comuna',
            'd' => '',
            'num_24' => 'Número',
            'pagado' => 'Pagado',
            'uno' => 'Uno',
            'nro_boleta' => 'Nro.Boleta',
            'i' => '',
            'monto_total' => 'Monto Total',
            'k' => '',
            'l' => '',
            'm' => '',
            'n' => '',
            'o' => '',
            'p' => '',
            'telefono' => 'Teléfono',
            'email' => 'E-mail'
        );

        $col=1;
        $indice = 0;
        /*
        foreach ($thead as $key => $th){
            $cellvalue = $cell_values[$indice].$col;
            $objPHPExcel->getActiveSheet()->setCellValue($cellvalue,$th);
            $objPHPExcel->getActiveSheet()->getStyle($cellvalue)->getFont()->setBold(true);
            $indice++;
        }
        */

        if( ! empty($orders) ){
            $col = 1;
            foreach ($orders as $o){
                $indice = 0;
                $value = '';
                foreach ($thead as $key => $th) {
                    if( $key == 'nombre' ) {
                        $value = $o->customer->name;
                    }elseif( $key == 'direccion' ){
                        $value = $o->address->direccion;
                    }elseif( $key == 'comuna' ){
                        $value = $o->address->comuna;
                    }elseif( $key == 'num_24' ){
                        $value = 24;
                    }elseif( $key == 'pagado' ){
                        $value = 'P';
                    }elseif( $key == 'uno' ){
                        $value = 1;
                    }elseif( $key == 'nro_boleta' ){
                        $value = $o->bsale_nro_boleta;
                    }elseif( $key == 'monto_total' ){
                        $value = ($o->order_total + $o->order_shipping - $o->order_purcharse_valor_desc);
                    }elseif( $key == 'telefono' ){
                        $value = $o->customer->phone;
                    }else{
	                    $value = '';
                    }

                    $cellvalue = $cell_values[$indice].$col;
                    $objPHPExcel->getActiveSheet()->setCellValue($cellvalue,$value);
                    $indice++;
                }
                $col++;
            }
        }
        
        $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="exportPedidosPorDespachar.xls"');
        $objWriter->save('php://output');
    }

    function generar() {
    
        $this->load->model( array(
        'drivers',
        'orders',
        'payments'
        ));
    
        $accion = $this->input->post('accion') ? $this->input->post('accion') : '';
        if( $accion=="exportXls" ){
            $search = $this->input->post('search_exportar');
            $this->session->set_userdata('search_exportar',$search);
        }else{
            $search = $this->session->userdata('search_exportar');
        }
        $search = $this->session->userdata('search_exportar');

        //$date   = $this->input->post('date') ?: null;
        //$driver_id = $this->input->post('driver_id') ?: null;
        
        $orders = $this->orders->search(array(
            "driver_id"             => $search['driver_id'],
            "DATE(order_delivery_date)" => $search['date'],
            "paid"               => 1,
        ));
      
        //array_reduce($orders, function ($carry, $item) {
        //    return $carry + ($item->order_total + $item->order_shipping);
        //});

        $this->render('exportar/show', array(
            //"driver"    => $this->drivers->get($driver, false),
            "search"    => $search,
            "date"      => $this->input->post('date'),
            "orders"    => $orders
        )); 
    }
}