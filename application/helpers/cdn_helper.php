<?php
/*01-11-2017: Nuevo HOST para imagenes en CDN nuevo: tiendapet01.akamaized.net  (Este host responde por HTTP y HTTPS)*/
/*01-11-2017: Se modifica para tener opcion a utilizar tres host CND distintos de forma aleatoria*/


function base_url_cdn($image_path) {

    //Cambiamos todas eligiendo un servidor CDN aleatorio
    $numero_cdn = rand(1,3);
    switch ($numero_cdn) {
        case 1:
            //$image_path = str_replace("rutas.tiendapet.cl","tiendapet01.akamaized.net",base_url($image_path));
            $image_path = str_replace("rutas.tiendapet.cl","tiendapet01.akamaized.net",base_url($image_path));
            break;
        case 2:
            $image_path = str_replace("rutas.tiendapet.cl","tiendapet02.akamaized.net",base_url($image_path));
            break;
        case 3:
            $image_path = str_replace("rutas.tiendapet.cl","tiendapet03.akamaized.net",base_url($image_path));
            break;
    }

    return $image_path;
}


/* Creado por dkTronics Ltda 21-05-2016 */
/* End of file cdn_helper.php */
/* Location: ./application/helpers/cdn_helper.php */
