<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class CORE_Controller extends CI_Controller
{

    public $site_name = "TiendaPet &mdash; Gestión de rutas";
    public $icons = array(
        'pedidos' => 'fa-shopping-cart',
        'clientes' => 'fa-users',
        'choferes' => 'fa-truck',
        'rutero' => 'fa-truck',
        'dashboard' => 'fa-dashboard',
        'reportes' => 'fa-clipboard',
        'exportar' => 'fa-file-excel-o',
        'bsaleapi' => 'fa fa-dot-circle-o',
    );

    public function __construct()
    {
        parent::__construct();

        setlocale(LC_TIME, "es_ES");
        $this->load->model('orders');
        $allowed = array('login', 'validate');

        if( ! isset($_SESSION['cron']) || ! $_SESSION['cron'] ){
            if ( !$this->session->userdata('user') && !in_array($this->router->fetch_method(), $allowed) ) {
                redirect('auth/login');
            }
            $this->user = $this->session->userdata('user');
        }

#    if ($this->user && $this->user->role_id == 1 && $this->router->fetch_class() != 'rutero')
        #      redirect('rutero');
    }

    public function renderRutero($view, $data = null)
    {

        $controller = $this->router->fetch_class();
        $method = $this->router->fetch_method();

        $this->load->view('rutero/header', array(
            'hoy' => date('d/m/Y'),
            'ayer' => date('d/m/Y', strtotime("-1 day")),
        ));

        $this->load->view($view, $data);
        $this->load->view('rutero/footer');

    }

    public function renderPicking($view, $data = null)
    {

        $controller = $this->router->fetch_class();
        $method = $this->router->fetch_method();

        $this->load->view('picking/header', array(
            'hoy' => date('d/m/Y'),
            'ayer' => date('d/m/Y', strtotime("-1 day")),
        ));

        $this->load->view($view, $data);
        $this->load->view('picking/footer');

    }

    public function render($view, $data = null)
    {
        $this->load->model('orders');

        $controller = $this->router->fetch_class();
        $method = $this->router->fetch_method();
        $this->stats = $this->orders->stats();
        //$this->stats_dashboard   = $this->orders->stats_dashboard();

        $this->load->view('common/header');
        $this->load->view('common/menu', array(
            'current' => $controller,
            'method' => $method,
            'icon' => $this->icons[$controller],
            'stats' => $this->stats,
            //'stats_dashboard' => $this->stats_dashboard
        ));
        $this->load->view('common/page-header', array('current' => $controller));
        $this->load->view($view, $data);
        $this->load->view('common/footer');
    }

    public function gohome()
    {
        redirect();
    }

    public function goback()
    {
        redirect($this->input->server('HTTP_REFERER'));
    }
}
