<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Notificacion
{
    protected $ci;
    private $_ajustes;

    public function __construct()
    {

        $this->ci =& get_instance();
        $this->ci->load->library('sendmail');
        $this->ci->load->model('orders');
        $this->ci->load->model('customers');

        $query = $this->ci->db->get_where('ajustes', array('id' => 1));
        $result = $query->result();

        $this->_ajustes = $result[0];
    }

    public function qualifyService($order_id)
    {
        $detalle = $order_id ? $this->ci->orders->get($order_id) : new stdClass();
        $customer = $this->ci->customers->get($detalle->cust_id);

        $url = "https://www.tiendapet.cl/";
        //$url = "http://tiendapet-new.test/";

        /****** Enviar Email ******/
        $data = array(
            'ajustes' => $this->_ajustes,
            'detalle' => $detalle,
            'order_id' => $order_id,
            'customer' => $customer,
            'url' => $url
        );
        $subject = utf8_decode("Tiendapet.cl su pedido fue entregado, por favor evalue nuestro servicio");
        $body = $this->ci->load->view('mail/qualify_service', $data, TRUE);
        $info = array('subject' => $subject, 'body' => $body, 'to' => $customer->email, 'reply_to' => $this->_ajustes->mail);
        $sendmail = new Sendmail();
        $sendmail->send($info);
        //$info = array('subject'=>$subject, 'body'=>$body, 'to'=>'rodolfo@tiendapet.cl');
        //$info = array('subject'=>$subject, 'body'=>$body, 'to'=>'jeans.jave@gmail.com');
        /******** Fin Email *******/
    }
}