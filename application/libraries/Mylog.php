<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mylog {
  
    private $user;

    function __construct()
    {
        $CI = &get_instance();

        // Load the session, CI2 as a library, CI3 uses it as a driver
        if (substr(CI_VERSION, 0, 1) == '2')
        {
            $CI->load->library('session');
        }
        else
        {
            $CI->load->driver('session');
        }

        $CI->load->library('user_agent');

        $user = $CI->session->userdata('user');
        if( isset($user->id) && $user->id ){
            //$query = $CI->db->select("*")->from('admins')->join('bsale_empresas', 'bsale_empresas.id = admins.bsale_empresas_id', 'left')->where('admins.id',$user->id)->get();
            //$result = $query->row();
            $this->user = $user;
        }else{
            $this->user = new stdClass();
        }
    }

    public function add($data=array()){

        $CI = &get_instance();

        $data['user_id'] = $this->user->id;
        $data['http_referer'] = $CI->agent->referrer();
        $data['http_user_agent'] = $CI->agent->browser().' '.$CI->agent->version();
        $data['created_at'] = date('Y-m-d H:i:s');

        $data['data'] = var_export($data['data'], true);

        $CI->db->insert("log", $data);
    }
}
