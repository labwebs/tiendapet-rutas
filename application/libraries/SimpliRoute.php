<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Simpliroute {
  
    private $url = 'https://api.simpliroute.com/v1';
    private $url_optimize = 'https://optimizator.simpliroute.com/vrp';
    private $access_token = 'd27f31bf1d745903621597069e1c9334ae718ab3';
    
    function __construct()
    {
        $CI = &get_instance();

        // Load the session, CI2 as a library, CI3 uses it as a driver
        if (substr(CI_VERSION, 0, 1) == '2')
        {
            $CI->load->library('session');
        }
        else
        {
            $CI->load->driver('session');
        }
    }

    private function get($url) {

        // Inicia cURL
        $session = curl_init($url);

        // Indica a cURL que retorne data
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

        // Configura cabeceras
        $headers = array(
            'access_token: '.$this->access_token,
            'Accept: application/json',
            'Content-Type: application/json'
        );
        curl_setopt($session, CURLOPT_HTTPHEADER, $headers);

        // Ejecuta cURL
        $response = curl_exec($session);
        $code = curl_getinfo($session, CURLINFO_HTTP_CODE);

        // Cierra la sesión cURL
        curl_close($session);

        //Esto es sólo para poder visualizar lo que se está retornando

        return $response;
    }

    private function post($method, $data, $optmize = false){

        // Parsea a JSON
        $data =  json_encode( $data );

        $endpoint = $optmize == true ? $this->url_optimize.'/'.$method : $this->url.'/'.$method;

        // Inicia cURL
        $session = curl_init($endpoint);

        // Indica a cURL que retorne data
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

        // Activa SSL
        //curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, true);

        // Configura cabeceras
        /*
        $headers = array(
            'access_token: '.$this->access_token,
            'Accept: application/json',
            'Content-Type: application/json'
        );
        */
        $headers = array(
            'Authorization: Token '.$this->access_token,
            'Content-Type: application/json'
        );
        curl_setopt($session, CURLOPT_HTTPHEADER, $headers);

        // Indica que se va ser una petición POST
        curl_setopt($session, CURLOPT_POST, true);

        // Agrega parámetros
        curl_setopt($session, CURLOPT_POSTFIELDS, $data);

        // Ejecuta cURL
        $response = curl_exec($session);
        $code = curl_getinfo($session, CURLINFO_HTTP_CODE);
        
        // Cierra la sesión cURL
        curl_close($session);

        $error = false;
        $message = '';

        switch ($code) {

            case '400':
                $error = true;
                $message = var_export($response, true);
                $response = "";
                break;

            case '401':
                $error = true;
                $response = "";
                $message = "No estás usando el token correcto para hacer la llamada.";
                break;

            case '404':
                $error = true;
                $response = "";
                $message = "La llamada que hiciste no arrojó resultados.";
                break;

            case '405':
                $error = true;
                $response = "";
                $message = "Intentaste acceder al endpoint con un método inválido.";
                break;

            case '406':
                $error = true;
                $response = "";
                $message = "Hiciste una llamada con un formato que no es JSON.";
                break;

            case '500':
                $error = true;
                $response = "";
                $message = "Hubo un problema con nuestro servidor de SimpliRoute. Inténtalo más tarde o comunícate con nosotros.";
                break;

            case '500':
                $error = true;
                $response = "";
                $message = "Estamos temporalmente fuera por mantenimiento. Por favor inténtalo más tarde.";
                break;
            
            default:
                # code...
                break;
        }


        $result = array(
            'code' => $code,
            'response' => json_decode($response),
            'error' => $error,
            'message' => $message
        );

        //Esto es sólo para poder visualizar lo que se está retornando
        //print_r($response);
        return $result;
    }

    private function call($method, $api, $data, $optmize = false){

        // Parsea a JSON
        $data =  json_encode( $data );

        $endpoint = $optmize == true ? $this->url_optimize.'/'.$api : $this->url.'/'.$api;
        
        // Inicia cURL
        $session = curl_init($endpoint);

        // Indica a cURL que retorne data
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        
        // Configura cabeceras
        /*
        $headers = array(
            'access_token: '.$this->access_token,
            'Accept: application/json',
            'Content-Type: application/json'
        );
        */
        $headers = array(
            'Authorization: Token '.$this->access_token,
            'Content-Type: application/json'
        );
        curl_setopt($session, CURLOPT_HTTPHEADER, $headers);
        
        switch ($method) {
            case "GET":
                curl_setopt($session, CURLOPT_CUSTOMREQUEST, "GET");
                break;
            case "POST":
                curl_setopt($session, CURLOPT_CUSTOMREQUEST, "POST");
                break;
            case "PUT":
                curl_setopt($session, CURLOPT_CUSTOMREQUEST, "PUT");
                break;
            case "DELETE":
                curl_setopt($session, CURLOPT_CUSTOMREQUEST, "DELETE"); 
                break;
        }

        // Agrega parámetros
        curl_setopt($session, CURLOPT_POSTFIELDS, $data);

        // Ejecuta cURL
        $response = curl_exec($session);
        
        $code = curl_getinfo($session, CURLINFO_HTTP_CODE);
        
        // Cierra la sesión cURL
        curl_close($session);

        $error = false;
        $message = '';

        switch ($code) {

            case '400':
                $error = true;
                $message = var_export($response, true);
                $response = "";
                break;

            case '401':
                $error = true;
                $response = "";
                $message = "No estás usando el token correcto para hacer la llamada.";
                break;

            case '404':
                $error = true;
                $response = "";
                $message = "La llamada que hiciste no arrojó resultados.";
                break;

            case '405':
                $error = true;
                $response = "";
                $message = "Intentaste acceder al endpoint con un método inválido.";
                break;

            case '406':
                $error = true;
                $response = "";
                $message = "Hiciste una llamada con un formato que no es JSON.";
                break;

            case '500':
                $error = true;
                $response = "";
                $message = "Hubo un problema con nuestro servidor de SimpliRoute. Inténtalo más tarde o comunícate con nosotros.";
                break;

            case '500':
                $error = true;
                $response = "";
                $message = "Estamos temporalmente fuera por mantenimiento. Por favor inténtalo más tarde.";
                break;
            
            default:
                # code...
                break;
        }


        $result = array(
            'code' => $code,
            'response' => json_decode($response),
            'error' => $error,
            'message' => $message
        );

        //Esto es sólo para poder visualizar lo que se está retornando
        //print_r($response);
        return $result;
    }

    public function getSucursales(){
        $url = 'https://api.bsale.cl/v1/offices.json';
        $json = $this->get($url);

        $json = json_decode($json);

        
        //print '<pre>';
        //print_r($json->items);
        //exit;

        $lists = array();
        if( isset($json->items) && !empty($json->items) ){
            $CI = &get_instance();
            $user = $CI->session->userdata('user');
            if( isset($user->id) && $user->id && $this->access_token && ($user->is_tomador_pedidos || $user->is_tomador_pedidos_despacho) ){
                $query = $CI->db->select("*")->from('admins_rel_bsale_empresas_bodegas')->join('bsale_empresas_bodegas', 'admins_rel_bsale_empresas_bodegas.bsale_empresas_bodegas_id = bsale_empresas_bodegas.id')->join('bsale_empresas', 'bsale_empresas_bodegas.bsale_empresa_id = bsale_empresas.id')->where('admins_rel_bsale_empresas_bodegas.admins_id',$user->id)->where('bsale_empresas.token', $this->access_token)->get();
                $result = $query->result();

                $bsale_bodega_id = array();
                if( ! empty($result) ){
                    foreach ($result as $indice => $r) $bsale_bodega_id[$r->bsale_bodega_id] = $r->bsale_bodega_id;
                }
                if( ! empty($bsale_bodega_id) ){
                    foreach ($json->items as $indice => $item){
                        if( in_array($item->id, array_values($bsale_bodega_id)) ) $lists[$item->id] = $item->name;
                    }
                }
            }else{
                foreach ($json->items as $indice => $item){
                    $lists[$item->id] = $item->name;
                }
            }
        }

        return $lists;
    }

    public function createRepartidor($data){

        $result = $this->call('POST', 'accounts/drivers/', $data);

        //$result = json_decode($json);
        
        return $result;
    }

    public function updateRepartidor($id, $data){
        
        $result = $this->call('PUT', 'accounts/drivers/'.$id.'/', $data);

        //$result = json_decode($json);
        
        return $result;
    }
    
    public function deleteRepartidor($data){

        $result = $this->call('DELETE', 'accounts/users/'.$data['id'].'/', $data);
        
        //$result = json_decode($json);
        
        return $result;
    }

    public function createVehiculo($data){

        $result = $this->call('POST', 'routes/vehicles/', $data);

        //$result = json_decode($json);
        
        return $result;
    }

    public function updateVehiculo($id, $data){

        $result = $this->call('PUT', 'routes/vehicles/'.$id.'/', $data);

        //$result = json_decode($json);
        
        return $result;
    }

    public function sendOrders($data){

        $result = $this->call('POST', 'routes/vehicles/', $data);

        //$result = json_decode($json);
        
        return $result;
    }
    
    public function geocode($data){

        $result = $this->call('POST', 'directory/geocode/', $data);

        //$result = json_decode($json);
        
        return $result;
    }

    public function optimize($data){

        $result = $this->call('POST', 'optimize/sync/', $data, true);

        //$result = json_decode($json);
        
        return $result;
    }
    
    public function createPlan($data){

        $result = $this->call('POST', 'plans/create-plan/', $data);

        //$result = json_decode($json);
        
        return $result;
    }

    public function consultRoutes($route){

        $result = $this->call('GET', 'plans/routes/'.$route.'/visits/', array());

        //$result = json_decode($json);
        
        return $result;
    }
}
