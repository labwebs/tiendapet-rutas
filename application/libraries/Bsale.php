<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bsale {
  
    private $access_token;
    private $precio_lista_id;
    private $despacho_id;
    private $shipping_pro_id;

    //private $CI =& get_instance();

    //$token="49f120f4ef772f701bea2622d21dfc5262c2220d"
    function __construct()
    {
        $CI = &get_instance();

        //$CI->load->config('ion_auth', TRUE);
        //$CI->load->library(array('email'));

        // Load the session, CI2 as a library, CI3 uses it as a driver
        if (substr(CI_VERSION, 0, 1) == '2')
        {
            $CI->load->library('session');
        }
        else
        {
            $CI->load->driver('session');
        }

        //$CI->load->library('session');

        /*
        $query = $CI->db->select("*")->from('ajustes')->join('bsale_empresas', 'bsale_empresas.id = ajustes.bsale_empresas_id', 'left')->where('ajustes.id',1)->get();
        $result = $query->row();
        */

        $user = $CI->session->userdata('user');
        if( isset($user->id) && $user->id ){
            $query = $CI->db->select("*")->from('admins')->join('bsale_empresas', 'bsale_empresas.id = admins.bsale_empresas_id', 'left')->where('admins.id',$user->id)->get();
            $result = $query->row();    
            $this->access_token = $result->token;
            $this->precio_lista_id = $result->precio_lista_id;
            $this->despacho_id = $result->despacho_id;
            $this->shipping_pro_id = $result->shipping_pro_id;
        }else{
            $this->access_token = 0;
            $this->despacho_id = 0;
        }
    }

    private function get($url) {

        // Inicia cURL
        $session = curl_init($url);

        // Indica a cURL que retorne data
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

        // Configura cabeceras
        $headers = array(
            'access_token: '.$this->access_token,
            'Accept: application/json',
            'Content-Type: application/json'
        );
        curl_setopt($session, CURLOPT_HTTPHEADER, $headers);

        // Ejecuta cURL
        $response = curl_exec($session);
        $code = curl_getinfo($session, CURLINFO_HTTP_CODE);

        // Cierra la sesión cURL
        curl_close($session);

        //Esto es sólo para poder visualizar lo que se está retornando

        return $response;
    }

    private function post($url, $data){

        // Parsea a JSON
        $data =  json_encode( $data );

        // Inicia cURL
        $session = curl_init($url);


        // Indica a cURL que retorne data
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

        // Activa SSL
        //curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, true);

        // Configura cabeceras
        $headers = array(
            'access_token: '.$this->access_token,
            'Accept: application/json',
            'Content-Type: application/json'
        );
        curl_setopt($session, CURLOPT_HTTPHEADER, $headers);

        // Indica que se va ser una petición POST
        curl_setopt($session, CURLOPT_POST, true);

        // Agrega parámetros
        curl_setopt($session, CURLOPT_POSTFIELDS, $data);

        // Ejecuta cURL
        $response = curl_exec($session);
        $code = curl_getinfo($session, CURLINFO_HTTP_CODE);

        // Cierra la sesión cURL
        curl_close($session);

        //Esto es sólo para poder visualizar lo que se está retornando
        //print_r($response);
        return $response;
    }

    public function setToken($token){
        $this->access_token = $token;
    }

    public function getSucursales(){
        $url = 'https://api.bsale.cl/v1/offices.json';
        $json = $this->get($url);

        $json = json_decode($json);

        //print '<pre>';
        //print_r($json->items);
        //exit;

        $lists = array();
        if( isset($json->items) && !empty($json->items) ){
            $CI = &get_instance();
            $user = $CI->session->userdata('user');
            if( isset($user->id) && $user->id && $this->access_token && ($user->is_tomador_pedidos || $user->is_tomador_pedidos_despacho) ){
                $query = $CI->db->select("*")->from('admins_rel_bsale_empresas_bodegas')->join('bsale_empresas_bodegas', 'admins_rel_bsale_empresas_bodegas.bsale_empresas_bodegas_id = bsale_empresas_bodegas.id')->join('bsale_empresas', 'bsale_empresas_bodegas.bsale_empresa_id = bsale_empresas.id')->where('admins_rel_bsale_empresas_bodegas.admins_id',$user->id)->where('bsale_empresas.token', $this->access_token)->get();
                $result = $query->result();

                $bsale_bodega_id = array();
                if( ! empty($result) ){
                    foreach ($result as $indice => $r) $bsale_bodega_id[$r->bsale_bodega_id] = $r->bsale_bodega_id;
                }
                if( ! empty($bsale_bodega_id) ){
                    foreach ($json->items as $indice => $item){
                        if( in_array($item->id, array_values($bsale_bodega_id)) ) $lists[$item->id] = $item->name;
                    }
                }
            }else{
                foreach ($json->items as $indice => $item){
                    $lists[$item->id] = $item->name;
                }
            }
        }

        return $lists;
    }

    public function getClientByMail($email){
        //$email = 'faninadiaz@gmail.com';
        $url='https://api.bsale.cl/v1/clients.json?email='.$email;
        $json = $this->get($url);

        $json = json_decode($json);

        $result = isset($json->items) && ! empty($json->items[0]) ? $json->items[0] : new stdClass();

        return $result;
    }

    public function getProductBySku($sku){
        $url='https://api.bsale.cl/v1/price_lists/'.$this->precio_lista_id.'/details.json?code='.$sku.'&expand=[variant,product]';
        $json = $this->get($url);

        $json = json_decode($json);

        $result = isset($json->items) && ! empty($json->items[0]) ? $json->items[0] : new stdClass();

        return $result;
    }

    public function getProductByVariantid($variantid){
        $url='https://api.bsale.cl/v1/variants/'.$variantid.'.json?expand=[product]';
        $json = $this->get($url);

        $json = json_decode($json);
        
        //$result = isset($json->items) && ! empty($json->items[0]) ? $json->items[0] : new stdClass();

        return $json;
    }

    public function getStockProductBySku($sku, $officeid){
        $url='https://api.bsale.cl/v1/stocks.json?code='.$sku.'&officeid='.$officeid; // Consultar el stock
        $json = $this->get($url);

        $json = json_decode($json);

        $result = isset($json->items) && ! empty($json->items[0]) ? $json->items[0] : new stdClass();

        return $result;
    }
    
    public function getStockProductByVariantid($variantid, $officeid){
        $url='https://api.bsale.cl/v1/stocks.json?variantid='.$variantid.'&officeid='.$officeid; // Consultar el stock
        $json = $this->get($url);

        $json = json_decode($json);

        $result = isset($json->items) && ! empty($json->items[0]) ? $json->items[0] : new stdClass();

        return $result;
    }

    public function getTypeDocuments(){
        $url='https://api.bsale.cl/v1/document_types.json?fields=[name,state]';
        $json = $this->get($url);

        $json = json_decode($json);

        $result = isset($json->items) && ! empty($json->items) ? $json->items : array();

        return $result;
    }

    public function getDetailsDocuments($document_id){
        $url='https://api.bsale.cl/v1/documents/'.$document_id.'/details.json';
        $json = $this->get($url);

        $json = json_decode($json);

        $result = isset($json->items) && ! empty($json->items) ? $json->items : array();

        return $result;
    }

    public function insertClient($data){
        $url='https://api.bsale.cl/v1/clients.json';
        $json = $this->post($url, $data);

        $result = json_decode($json);

        return $result;
    }

    public function generateDocument($data){
        $url='https://api.bsale.cl/v1/documents.json';
        $json = $this->post($url, $data);

        $result = json_decode($json);

        return $result;
    }

    public function generateShipping($data){
        $url='https://api.bsale.cl/v1/shippings.json';
        $json = $this->post($url, $data);

        $result = json_decode($json);

        return $result;
    }

    public function discountStock($data){
        $url='https://api.bsale.cl/v1/stocks/consumptions.json';
        $json = $this->post($url, $data);

        $result = json_decode($json);

        return $result;
    }

    public function getPriceListId(){
        return $this->precio_lista_id;
    }

    public function getDespachoId(){
        return $this->despacho_id;
    }

    public function getIdProShipping(){
        return $this->shipping_pro_id;
    }
    
}
