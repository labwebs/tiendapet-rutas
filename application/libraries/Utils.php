<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Utils {
  function numformat($int) {
    
    return '$ ' . number_format($int, 0, ',','.');
    
  }
}

if( ! function_exists('print_a') ){
    function print_a(){
        $argumentos = func_get_args();
        if ( empty( $argumentos ) ) $argumentos = '';
        $argumentos = is_array( $argumentos ) && count( $argumentos ) == 1 ? array_shift( $argumentos ) : $argumentos;
        print "<pre>";
        print_r( $argumentos );
        print "</pre>";
    }
}