<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sendmail
{
    private $sendMail;

    public function __construct(){

        //require FCPATH."vendor/phpmailer/phpmailer/PHPMailerAutoload";

        $this->sendMail = new \PHPMailer(true);

        $from = "info@tiendapet.cl";
        $from_name = "TiendaPet.cl";

        $this->sendMail->IsSMTP(); // enable SMTP
        $this->sendMail->SMTPDebug = 0;  // debugging: 1 = errors and messages, 2 = messages only
        $this->sendMail->SMTPAuth = true;  // authentication enabled
        $this->sendMail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
        //$this->sendMail->Host = 'mail.tiendapet.cl';
        $this->sendMail->Host = 'srv7.dktronics.cl';
        $this->sendMail->Port = 465;
        $this->sendMail->Username = 'info@tiendapet.cl';//getenv('PHPMAILER_USERNAME');
        //$this->sendMail->Username = 'ventas@tiendapet.cl';//getenv('PHPMAILER_USERNAME');
        $this->sendMail->Password = 'tpet.cl16';//getenv('PHPMAILER_PASSWORD');
        //$this->sendMail->Password = 'vt.tpet16';//getenv('PHPMAILER_PASSWORD');
        $this->sendMail->IsHTML(true);
        $this->sendMail->SetFrom($from, $from_name);
    }

    public function send($info){
        $send = 0;
        try {
            $this->sendMail->Subject = $info['subject'];
            $this->sendMail->Body = $info['body'];
            $this->sendMail->AddAddress($info['to']);
            //if( $info['to'] != 'ventas@tiendapet.cl' ) $this->sendMail->AddBCC('ventas@tiendapet.cl');
            if (!empty($info['reply_to'])) {
                $this->sendMail->AddReplyTo($info['reply_to']);
            }
            $send = $this->sendMail->Send();
        }
        catch (phpmailerException $e)
        {
            $send = 0;
        }

        return $send;
    }
}
?>