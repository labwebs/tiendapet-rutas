<?php

$lang['form_validation_required']			= "El campo <b>%s</b> es obligatorio.";
$lang['form_validation_isset']				= "El campo <b>%s</b> debe contener un valor.";
$lang['form_validation_valid_email']		= "El campo <b>%s</b> debe contener una dirección de correo válida.";
$lang['form_validation_valid_emails']		= "El campo <b>%s</b> debe contener todas las direcciones de correo válidas.";
$lang['form_validation_valid_url']			= "El campo <b>%s</b> debe contener una URL válida.";
$lang['form_validation_valid_ip']			= "El campo <b>%s</b> debe contener una dirección IP válida.";
$lang['form_validation_min_length']			= "El campo <b>%s</b> debe contener al menos <b>%s</b> caracteres de longitud.";
$lang['form_validation_max_length']			= "El campo <b>%s</b> no debe exceder los <b>%s</b> caracteres de longitud.";
$lang['form_validation_exact_length']		= "El campo <b>%s</b> debe tener exactamente <b>%s</b> carácteres.";
$lang['form_validation_alpha']				= "El campo <b>%s</b> sólo puede contener carácteres alfabéticos.";
$lang['form_validation_alpha_numeric']		= "El campo <b>%s</b> sólo puede contener carácteres alfanuméricos.";
$lang['form_validation_alpha_dash']			= "El campo <b>%s</b> sólo puede contener carácteres alfanuméricos, guiones bajos '_' o guiones '-'.";
$lang['form_validation_numeric']			= "El campo <b>%s</b> sólo puede contener números.";
$lang['form_validation_is_numeric']			= "El campo <b>%s</b> sólo puede contener carácteres numéricos.";
$lang['form_validation_integer']			= "El campo <b>%s</b> debe contener un número entero.";
$lang['regex_match']		= "El campo <b>%s</b> no tiene el formato correcto.";
$lang['matches']			= "El campo <b>%s</b> no concuerda con el campo <b>%s</b> .";
$lang['is_natural']			= "El campo <b>%s</b> debe contener sólo números positivos.";
$lang['is_natural_no_zero']	= "El campo <b>%s</b> debe contener un número mayor que 0.";
$lang['decimal']			= "El campo <b>%s</b> debe contener un número decimal.";
$lang['less_than']			= "El campo <b>%s</b> debe contener un número menor que <b>%s</b>.";
$lang['greater_than']		= "El campo <b>%s</b> debe contener un número mayor que <b>%s</b>.";
/* Added after 2.0.2 */
$lang['is_unique'] 			= "El valor del campo <b><b>%s</b></b> ya se encuentra registrado, el valor deber ser único.";

/* End of file form_validation_lang.php */
/* Location: ./system/language/spanish/form_validation_lang.php */