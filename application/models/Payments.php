<?php  if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}
  
class Payments extends CI_Model
{
    public function get($id)
    {
        $payment = $this->db
      ->select('paym_name as name')
      ->where('id', $id)
      ->get('payments')
      ->row();
      
        return $payment;
    }
  
    public function all()
    {
        return $this->db
      ->select("*, paym_name as name")
      ->get('payments')
      ->result();
    }
}
