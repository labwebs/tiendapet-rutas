<?php 
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Orders extends CI_Model
{
    public function __construct()
    {
        $this->load->model('users');
    }

    public function get($id, $with_products = false)
    {
        $order = $this->db
            ->where('id', $id)
            ->get('orders')
            ->row();

        if ($with_products) {
            $order = $this->get_products($order);
        }

        return $this->load($order);
    }

    public function getById($id)
    {
        $ids = is_array( $id ) ? $id : array( $id );

        if ( empty( $ids ) ) return array();

        $sql = "
			SELECT o.*  
			FROM orders AS o 
			WHERE 
			      o.id IN(".( implode( ', ', $ids ) ).")
		";
        $query = $this->db->query($sql);

        $rows = $query->result();

        $results = array();
        foreach( $rows as $row ){
            $results[$row->id] = $row;
        }

        return is_array( $id ) ? $results : $results[$id];
    }

    public function getDetalleWebpay($id)
    {
        $order = $this->db
            ->where('Tbk_orden_compra', $id)
            ->get('webpay')
            ->row();
        return $order;
    }
    
    public function getDetalleOneclick($id)
    {
        $order = $this->db
            ->where('buyOrder', $id)
            ->get('tbk_oneclick_authorizes')
            ->row();
        return $order;
    }

    public function getDetallePuntopago($id)
    {
        $order = $this->db
            ->where('order_id', $id)
            ->get('puntopagos')
            ->row();
        return $order;
    }

    public function getBloquesDespachoatupinta($id)
    {
        $ids = is_array($id) ? $id : array($id);

        if (empty($ids)) {
            return array();
        }

        $sql = "
			SELECT
			    dpb.*
            FROM
			    despacho_a_tu_pinta_bloques AS dpb
			WHERE
			      dpb.id IN(" . (implode(', ', $ids)) . ")

		";
        $query = $this->db->query($sql);
        $rows = $query->result();

        $results = array();
        foreach ($rows as $row) {
            $results[$row->id] = $row;
        }

        return is_array($id) ? $results : $results[$id];
    }

    public function getMsjeDespachoatuPinta($order_id)
    {
        if (!$order_id) {
            return "";
        }

        $detalle = $this->get($order_id);

        $despachoatupinta_bloque_id = $detalle->despachoatupinta_bloque_id;

        $texto = '';
        if ($despachoatupinta_bloque_id) {
            $despachoatupinta_bloque = $this->getBloquesDespachoatupinta($despachoatupinta_bloque_id);
            if (isset($despachoatupinta_bloque->id) && $despachoatupinta_bloque->id) {
                $fecha = $detalle->despachoatupinta_fecha ? date('d/m/Y', strtotime($detalle->despachoatupinta_fecha)) : '';
                $texto = "El pedido debe ser despachado, para la fecha que seleccionó <b>&quot;" . $fecha . "&quot;</b> y franja horaria seleccionado <b>&quot;" . $despachoatupinta_bloque->nombre . " (" . $despachoatupinta_bloque->hora_inicio . " - " . $despachoatupinta_bloque->hora_termino . ")&quot;</b>.";
            }
        }

        return $texto;
    }

    public function getLog($id)
    {
        $log = $this->db
            ->select('log.*, admins.admin_name', false)
            ->join('orders', 'log.order_id = orders.id', 'LEFT')
            ->join('admins', 'log.user_id = admins.id', 'LEFT')
            ->where('log.id', $id)
            ->get('log')
            ->row();

        return $log;
    }

    public function all($limit = null, $offset = null)
    {

        //$user = $this->session->userdata('user');

        if ($this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho) {
            $comunas = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getComunas($this->user->id) : array();
            $locales = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getLocales($this->user->id) : array();

            $bodega_retiro = false;
            if( ! empty($comunas) ){
                foreach($comunas as $indice => $com_id ){
                    if($com_id == 493){
                        $bodega_retiro = true;    
                    }
                }
            }
            $locales = $bodega_retiro ? $locales : array();

            $orders = $this->db
                ->select('orders.*')
                ->join('addresses', 'orders.addr_id = addresses.id')
                ->join('locales', 'locales.id = addresses.local_id', 'LEFT');
                //->where('( ( CASE WHEN addresses.local_id=0 THEN addresses.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) OR ( CASE WHEN addresses.local_id!=0 THEN locales.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) )');
                //->get('orders', $limit, $offset)
                //->result();
            
            if( ! empty($locales) ){
                $orders->where('( ( CASE WHEN addresses.local_id!=0 THEN locales.id IN (' . (implode(',', $locales)) . ') ELSE false END ) )', null, false);
            }else{
                $orders->where('( ( CASE WHEN addresses.local_id=0 THEN addresses.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) OR ( CASE WHEN addresses.local_id!=0 THEN locales.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) )', null, false);
            }

            $orders = $orders->get('orders', $limit, $offset)->result();

        } else {
            $orders = $this->db
                ->get('orders', $limit, $offset)
                ->result();
        }

        foreach ($orders as $o) {
            $o = $this->load($o);
        }

        return $orders;
    }

    public function search($filters, $count = false, $offset = 0, $single_driver = false)
    {
        if ($count) {
            $this->db->limit($count, $offset);
        }

        //$user = $this->session->userdata('user');

        if ($this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho) {
            $comunas = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getComunas($this->user->id) : array();
            $locales = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getLocales($this->user->id) : array();

            $bodega_retiro = false;
            if( ! empty($comunas) ){
                foreach($comunas as $indice => $com_id ){
                    if($com_id == 493){
                        $bodega_retiro = true;    
                    }
                }
            }
            $locales = $bodega_retiro ? $locales : array();

            if (!$single_driver) {
                unset($filters['driver_id']);
            }

            $orders = $this->db
                ->select('orders.*')
                ->from('orders')
                ->join('addresses', 'orders.addr_id = addresses.id')
                ->join('locales', 'locales.id = addresses.local_id', 'LEFT')
                //->where('( ( CASE WHEN addresses.local_id=0 THEN addresses.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) OR ( CASE WHEN addresses.local_id!=0 THEN locales.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) )', null, false)
                ->where($filters)
                ->order_by('addresses.comuna_id, orders.order_delivery_date, orders.order_delivery_time', 'asc');
                //->get()
                //->result();
            
            if( ! empty($locales) ){
                $orders->where('( ( CASE WHEN addresses.local_id!=0 THEN locales.id IN (' . (implode(',', $locales)) . ') ELSE false END ) )', null, false);
            }else{
                $orders->where('( ( CASE WHEN addresses.local_id=0 THEN addresses.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) OR ( CASE WHEN addresses.local_id!=0 THEN locales.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) )', null, false);
            }

            $orders = $orders->get()->result();

        } else {

            //if(!$single_driver) unset($filters['driver_id']);

            $orders = $this->db
                ->select('orders.*')
                ->from('orders')
                ->where($filters)
                ->order_by('addresses.comuna_id, order_delivery_date, order_delivery_time', 'asc')
                ->join('addresses', 'orders.addr_id = addresses.id')
                ->get()
                ->result();
        }

        foreach ($orders as $o) {
            $o = $this->load($o);
        }

        return $orders;
    }

    public function today()
    {

        //$user = $this->session->userdata('user');

        if ($this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho) {
            $comunas = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getComunas($this->user->id) : array();
            $locales = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getLocales($this->user->id) : array();

            $bodega_retiro = false;
            if( ! empty($comunas) ){
                foreach($comunas as $indice => $com_id ){
                    if($com_id == 493){
                        $bodega_retiro = true;    
                    }
                }
            }
            $locales = $bodega_retiro ? $locales : array();

            $orders = $this->db
                ->select('orders.*')
                ->from('orders')
                ->join('addresses', 'orders.addr_id = addresses.id')
                ->join('locales', 'locales.id = addresses.local_id', 'LEFT')
                //->where('( ( CASE WHEN addresses.local_id=0 THEN addresses.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) OR ( CASE WHEN addresses.local_id!=0 THEN locales.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) )', null, false)
                ->where('order_delivery_date', date('Y-m-d'))
                ->where('order_canceled', 0)
                ->where('paid', 1);

            if( ! empty($locales) ){
                $orders->where('( ( CASE WHEN addresses.local_id!=0 THEN locales.id IN (' . (implode(',', $locales)) . ') ELSE false END ) )', null, false);
            }else{
                $orders->where('( ( CASE WHEN addresses.local_id=0 THEN addresses.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) OR ( CASE WHEN addresses.local_id!=0 THEN locales.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) )', null, false);
            }

            $orders = $orders->get()->result();

        } else {
            $orders = $this->db
                ->where('paid', 1)
                ->where('order_delivery_date', date('Y-m-d'))
                ->where('order_canceled', 0)
                ->get('orders')
                ->result();
        }

        foreach ($orders as $o) {
            $o = $this->load($o);
        }

        return $orders;
    }

    public function by_date($desde = null, $hasta = null, $driver = null)
    {
        if (!$desde && !$hasta) {
            return array();
        }

        if ($desde) {
            $this->db->where('orders.order_delivery_date >=', $desde);
        }

        if ($hasta) {
            $this->db->where('orders.order_delivery_date <=', $hasta);
        }

        if ($driver) {
            $this->db->where('orders.driver_id', $driver);
        }

        //$user = $this->session->userdata('user');

        if ($this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho) {
            $comunas = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getComunas($this->user->id) : array();
            $locales = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getLocales($this->user->id) : array();

            $bodega_retiro = false;
            if( ! empty($comunas) ){
                foreach($comunas as $indice => $com_id ){
                    if($com_id == 493){
                        $bodega_retiro = true;    
                    }
                }
            }
            $locales = $bodega_retiro ? $locales : array();

            $orders = $this->db
                ->select('orders.*')
                ->from('orders')
                ->join('addresses', 'orders.addr_id = addresses.id')
                ->join('locales', 'locales.id = addresses.local_id', 'LEFT');
                //->where('( ( CASE WHEN addresses.local_id=0 THEN addresses.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) OR ( CASE WHEN addresses.local_id!=0 THEN locales.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) )', null, false);
                
            if( ! empty($locales) ){
                $orders->where('( ( CASE WHEN addresses.local_id!=0 THEN locales.id IN (' . (implode(',', $locales)) . ') ELSE false END ) )', null, false);
            }else{
                $orders->where('( ( CASE WHEN addresses.local_id=0 THEN addresses.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) OR ( CASE WHEN addresses.local_id!=0 THEN locales.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) )', null, false);
            }

            $orders = $orders->get()->result();

        } else {
            $orders = $this->db
                ->get('orders')
                ->result();
        }

        foreach ($orders as $o) {
            $o = $this->load($o);
        }

        return $orders;
    }

    public function find($params)
    {
        $comunas = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getComunas($this->user->id) : array();
        $locales = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getLocales($this->user->id) : array();

        $bodega_retiro = false;
        if( ! empty($comunas) ){
            foreach($comunas as $indice => $com_id ){
                if($com_id == 493){
                    $bodega_retiro = true;    
                }
            }
        }
        $locales = $bodega_retiro ? $locales : array();

        /*
        print '<pre>';
        print_r($comunas);
        exit;
         */

        if (!$params) {
            return array();
        }

        if ($desde = $params['desde']) {
            $this->db->where('order_delivery_date >=', $desde);
        }

        if ($hasta = $params['hasta']) {
            $this->db->where('order_delivery_date <=', $hasta);
        }

        if ($driver = $params['chofer']) {
            $this->db->where('driver_id', $driver);
        }

        if ($monto = $params['monto']) {
            if (is_numeric($monto)) {
                $this->db->where('order_total', intval($monto));
            }
        }

        if ($query = $params['query']) {
            if (is_numeric($query)) {
                $this->db->where('orders.id', intval($query));
            } else {
                $this->db
                    ->join('customers', 'orders.cust_id = customers.id')
                    ->join('addresses as addresses2', 'orders.addr_id = addresses2.id')
                    ->where('( (customers.cust_name LIKE \'%' . $query . '%\') OR (customers.cust_email LIKE \'%' . $query . '%\') OR (customers.cust_phone LIKE \'%' . $query . '%\') OR (addresses2.direccion LIKE \'%' . $query . '%\') OR (addresses2.comuna LIKE \'%' . $query . '%\') OR (addresses2.ciudad LIKE \'%' . $query . '%\') OR (addresses2.comentarios LIKE \'%' . $query . '%\') OR (orders.order_comment LIKE \'%' . $query . '%\') )');
                /*->like(array(
            'customers.cust_name'    => $query,
            'customers.cust_email'   => $query,
            'customers.cust_phone'   => $query,
            'addresses2.direccion'   => $query,
            'addresses2.comuna'      => $query,
            'addresses2.ciudad'      => $query,
            'addresses2.comentarios'  => $query,
            'orders.order_comment'  => $query
            ));*/
            }
        }

        # TODO: FILTRAR POR QUERY

        //$user = $this->session->userdata('user');

        if ($this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho) {
            $orders = $this->db
                ->select('orders.*, orders.id as id', false)
                ->from('orders')
                ->join('addresses', 'orders.addr_id = addresses.id')
                ->join('locales', 'locales.id = addresses.local_id', 'LEFT')
                //->where('( ( CASE WHEN addresses.local_id=0 THEN addresses.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) OR ( CASE WHEN addresses.local_id!=0 THEN locales.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) )', null, false)
                ->group_by('orders.id');
                
            if( ! empty($locales) ){
                $orders->where('( ( CASE WHEN addresses.local_id!=0 THEN locales.id IN (' . (implode(',', $locales)) . ') ELSE false END ) )', null, false);
            }else{
                $orders->where('( ( CASE WHEN addresses.local_id=0 THEN addresses.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) OR ( CASE WHEN addresses.local_id!=0 THEN locales.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) )', null, false);
            }

            $orders = $orders->get()->result();

        } else {
            $orders = $this->db
                ->select('*, orders.id as id', false)
                ->group_by('orders.id')
                ->get('orders')
                ->result();
        }

        foreach ($orders as $o) {
            $o = $this->load($o);
        }

        return $orders;
    }

    public function searchLog($params)
    {

        //$comunas = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getComunas($this->user->id) : array();

        //if (! $params) return array();

        $query = isset($params['query']) ? $params['query'] : '';

        if ($desde = @$params['desde']) {
            $this->db->where('created_at >=', $desde);
        }

        if ($hasta = @$params['hasta']) {
            $this->db->where('created_at <=', $hasta);
        }

        if ($query) {
            if (is_numeric($query)) {
                $this->db->where('orders.id', intval($query));
            } else {
                $this->db
                    ->join('customers', 'orders.cust_id = customers.id')
                    ->join('addresses as addresses2', 'orders.addr_id = addresses2.id')
                    ->where('( (log.action LIKE \'%' . $query . '%\') OR (log.description LIKE \'%' . $query . '%\') OR (customers.cust_name LIKE \'%' . $query . '%\') OR (customers.cust_email LIKE \'%' . $query . '%\') OR (customers.cust_phone LIKE \'%' . $query . '%\') OR (addresses2.direccion LIKE \'%' . $query . '%\') OR (addresses2.comuna LIKE \'%' . $query . '%\') OR (addresses2.ciudad LIKE \'%' . $query . '%\') OR (addresses2.comentarios LIKE \'%' . $query . '%\') OR (orders.order_comment LIKE \'%' . $query . '%\') )');
            }
        }

        # TODO: FILTRAR POR QUERY

        //$user = $this->session->userdata('user');

        /*
        if ($this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho) {
        $orders = $this->db
        ->select('orders.*, orders.id as id', false)
        ->from('orders')
        ->join('addresses', 'orders.addr_id = addresses.id')
        ->where_in('addresses.comuna_id',$comunas)
        ->group_by('orders.id')
        ->get()
        ->result();
        }else{
        $orders = $this->db
        ->select('*, orders.id as id', false)
        ->group_by('orders.id')
        ->get('orders')
        ->result();
        }
         */

        $logs = $this->db
            ->select('log.*, admins.admin_name', false)
            ->join('orders', 'log.order_id = orders.id', 'LEFT')
            ->join('admins', 'log.user_id = admins.id', 'LEFT')
            ->get('log')
            ->result();

        return $logs;
    }

    public function srchTransacciones($srch)
    {
        $wheres = array();

        if (isset($srch['query']) && $srch['query']) {
            if (is_numeric($srch['query'])) {
                $wheres[] = "o.id ='" . $srch['query'] . "'";
            } else {
                $wheres[] = " (cus.cust_name LIKE '%" . $srch['query'] . "%') OR (cus.cust_email LIKE '%" . $srch['query'] . "%') OR (cus.cust_phone LIKE '%" . $srch['query'] . "%') OR (addr.direccion LIKE '%" . $srch['query'] . "%') OR (addr.comuna LIKE '%" . $srch['query'] . "%') OR (addr.ciudad LIKE '%" . $srch['query'] . "%') OR (addr.comentarios LIKE '%" . $srch['query'] . "%') OR (o.order_comment LIKE '%" . $srch['query'] . "%')";
            }
        }

        if (isset($srch['desde']) && $srch['desde']) {
            $wheres[] = "o.created_at >='" . $srch['desde'] ."'";
        }

        if (isset($srch['hasta']) && $srch['hasta']) {
            $wheres[] = "o.created_at <='" . $srch['hasta'] . "'";
        }
        
        $condicion = $wheres ? ' and ( ' . implode(') and (', $wheres) . ') ' : '';

        $sql = '
        SELECT
            o.id
        FROM
            orders AS o LEFT JOIN customers AS cus ON( cus.id = o.cust_id )
                        LEFT JOIN addresses AS addr ON( addr.id = o.addr_id )
                        LEFT JOIN payments AS pay ON( pay.id = o.paym_id )
                        
        WHERE
            o.paym_id IN(4, 5, 6, 7)
        ';

        $sql .= $condicion;

        if (isset($srch['order']['orderby'])) {
            $sql .= '
                ORDER BY ' . $srch['order']['orderby'] . ' ' . $srch['order']['orderType'] . '
            ';
        } else {
            $sql .= '
                ORDER BY o.id DESC
            ';
        }

        $sql .= '
            LIMIT ' . $srch['start'] . ', ' . $srch['length'];

        $query = $this->db->query($sql);

        $rows = $query->result();

        $ids = array();
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $ids[$row->id] = 1;
            }
        }

        return !empty($ids) ? array_keys($ids) : array();
    }

    public function srchTransaccionesCount($srch)
    {
        $wheres = array();

        if (isset($srch['query']) && $srch['query']) {
            if (is_numeric($srch['query'])) {
                $wheres[] = "o.id ='" . $srch['query'] . "'";
            } else {
                $wheres[] = " (cus.cust_name LIKE '%" . $srch['query'] . "%') OR (cus.cust_email LIKE '%" . $srch['query'] . "%') OR (cus.cust_phone LIKE '%" . $srch['query'] . "%') OR (addr.direccion LIKE '%" . $srch['query'] . "%') OR (addr.comuna LIKE '%" . $srch['query'] . "%') OR (addr.ciudad LIKE '%" . $srch['query'] . "%') OR (addr.comentarios LIKE '%" . $srch['query'] . "%') OR (o.order_comment LIKE '%" . $srch['query'] . "%')";
            }
        }

        if (isset($srch['desde']) && $srch['desde']) {
            $wheres[] = "o.created_at >='" . $srch['desde'] ."'";
        }

        if (isset($srch['hasta']) && $srch['hasta']) {
            $wheres[] = "o.created_at <='" . $srch['hasta'] . "'";
        }
        
        $condicion = $wheres ? ' and ( ' . implode(') and (', $wheres) . ') ' : '';

        $sql = '
        SELECT
            count(o.id) as cant
        FROM
            orders AS o LEFT JOIN customers AS cus ON( cus.id = o.cust_id )
                        LEFT JOIN addresses AS addr ON( addr.id = o.addr_id )
                        LEFT JOIN payments AS pay ON( pay.id = o.paym_id )
                        
        WHERE
            o.paym_id IN(4, 5, 6, 7)
        ';

        $sql .= $condicion;

        $query = $this->db->query($sql);

        $row = $query->row();

        return $row->cant;
    }

    public function getSrchTransaccion($id)
    {
        $ids = is_array($id) ? $id : array($id);

        if (empty($ids)) {
            return array();
        }

        $sql = "
            SELECT
                o.*, (CASE WHEN addr.local_id!=0 THEN 'Retiro en Local' ELSE 'Despacho a Domicilio' END) AS method_of_delivery, pay.paym_name, cus.cust_name 
            FROM
                orders AS o LEFT JOIN customers AS cus ON( cus.id = o.cust_id )
                            LEFT JOIN addresses AS addr ON( addr.id = o.addr_id )
                            LEFT JOIN payments AS pay ON( pay.id = o.paym_id )            
            WHERE
			    o.id IN(" . (implode(', ', $ids)) . ")
			ORDER BY o.id DESC
		";
        $query = $this->db->query($sql);
        $rows = $query->result();

        $results = array();
        foreach ($rows as $row) {
            $results[$row->id] = $this->normTransaccion($row);
        }

        return is_array($id) ? $results : $results[$id];
    }

    public function normTransaccion($row){
        $row->display_price = $this->utils->numformat($row->order_total + $row->order_shipping - $row->order_purcharse_valor_desc);
        $row->date = isset($row->order_created) && $row->order_created ? date('d/m/Y H:i', strtotime($row->order_created)) : '';
        $row->paid_norm = isset($row->paid) && $row->paid ? 'Pagado' : 'No Pagado';
        $row->client = isset($row->cust_name) && $row->cust_name ? mb_convert_case($row->cust_name, MB_CASE_TITLE, "UTF-8") : '';
        return $row;
    }

    public function srchCerrar($srch)
    {
        $wheres = array();

        if (isset($srch['query']) && $srch['query']) {
            if (is_numeric($srch['query'])) {
                $wheres[] = "o.id ='" . $srch['query'] . "'";
            } else {
                $wheres[] = " (a.admin_name LIKE '%" . $srch['query'] . "%') OR (cus.cust_name LIKE '%" . $srch['query'] . "%') OR (cus.cust_email LIKE '%" . $srch['query'] . "%') OR (cus.cust_phone LIKE '%" . $srch['query'] . "%') OR (addr.direccion LIKE '%" . $srch['query'] . "%') OR (addr.comuna LIKE '%" . $srch['query'] . "%') OR (addr.ciudad LIKE '%" . $srch['query'] . "%') OR (addr.comentarios LIKE '%" . $srch['query'] . "%') OR (o.order_comment LIKE '%" . $srch['query'] . "%')";
            }
        }

        if (isset($srch['desde']) && $srch['desde']) {
            $wheres[] = "o.created_at >='" . $srch['desde'] ."'";
        }

        if (isset($srch['hasta']) && $srch['hasta']) {
            $wheres[] = "o.created_at <='" . $srch['hasta'] . "'";
        }
        
        if (isset($srch['driver_id']) && $srch['driver_id']) {
            $wheres[] = "o.driver_id ='" . $srch['driver_id'] . "'";
        }

        $condicion = $wheres ? ' and ( ' . implode(') and (', $wheres) . ') ' : '';

        $sql = '
        SELECT
            o.id
        FROM
            orders AS o LEFT JOIN customers AS cus ON( cus.id = o.cust_id )
                        LEFT JOIN addresses AS addr ON( addr.id = o.addr_id )
                        LEFT JOIN admins a ON( a.id = o.driver_id )
        WHERE
            o.order_canceled = 0 AND 
            o.order_delivered is null AND 
            o.order_delivery_date is not null AND 
            o.driver_id is not null AND 
            o.paid = 1
        ';

        $sql .= $condicion;

        if (isset($srch['order']['orderby'])) {
            $sql .= '
                ORDER BY ' . $srch['order']['orderby'] . ' ' . $srch['order']['orderType'] . '
            ';
        } else {
            $sql .= '
                ORDER BY o.id DESC
            ';
        }

        $sql .= '
            LIMIT ' . $srch['start'] . ', ' . $srch['length'];

        $query = $this->db->query($sql);

        $rows = $query->result();

        $ids = array();
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $ids[$row->id] = 1;
            }
        }

        return !empty($ids) ? array_keys($ids) : array();
    }

    /*
    public function getSrchCerrar($id)
    {
        $sql = "
        SELECT
            o.id
        FROM
            orders AS o LEFT JOIN customers AS cus ON( cus.id = o.cust_id )
                        LEFT JOIN addresses AS addr ON( addr.id = o.addr_id )
                        LEFT JOIN admins a ON( a.id = o.driver_id )
        WHERE
            o.id = '" .$id. "'
        ";
        $query = $this->db->query($sql);

        $orders = $query->result();

        foreach ($orders as $o) {
            $o = $this->load($o);
        }


        $results = array();
        foreach ($rows as $row) {
            $results[$row->id] = $row;
        }

        return is_array($id) ? $results : $results[$id];
    }
    */
    public function srchCerrarCount($srch)
    {
        $wheres = array();

        if (isset($srch['query']) && $srch['query']) {
            if (is_numeric($srch['query'])) {
                $wheres[] = "o.id ='" . $srch['query'] . "'";
            } else {
                $wheres[] = " (a.admin_name LIKE '%" . $srch['query'] . "%') OR (cus.cust_name LIKE '%" . $srch['query'] . "%') OR (cus.cust_email LIKE '%" . $srch['query'] . "%') OR (cus.cust_phone LIKE '%" . $srch['query'] . "%') OR (addr.direccion LIKE '%" . $srch['query'] . "%') OR (addr.comuna LIKE '%" . $srch['query'] . "%') OR (addr.ciudad LIKE '%" . $srch['query'] . "%') OR (addr.comentarios LIKE '%" . $srch['query'] . "%') OR (o.order_comment LIKE '%" . $srch['query'] . "%')";
            }
        }

        if (isset($srch['desde']) && $srch['desde']) {
            $wheres[] = "o.created_at >='" . $srch['desde'] . '"';
        }

        if (isset($srch['hasta']) && $srch['hasta']) {
            $wheres[] = "o.created_at <=" . $srch['hasta'] . "'";
        }

        $condicion = $wheres ? ' and ( ' . implode(') and (', $wheres) . ') ' : '';

        $sql = '
        SELECT
            count(o.id) as cant
        FROM
            orders AS o LEFT JOIN customers AS cus ON( cus.id = o.cust_id )
                        LEFT JOIN addresses AS addr ON( addr.id = o.addr_id )
                        LEFT JOIN admins a ON( a.id = o.driver_id )
        WHERE
            o.order_canceled = 0 AND 
            o.order_delivered is null AND 
            o.order_delivery_date is not null AND 
            o.driver_id is not null AND 
            o.paid = 1
        ';

        $sql .= $condicion;

        $query = $this->db->query($sql);

        $row = $query->row();

        return $row->cant;
    }

    public function srchLog($srch)
    {
        $wheres = array();

        if (isset($srch['query']) && $srch['query']) {
            if (is_numeric($srch['query'])) {
                $wheres[] = "orders.id ='" . $srch['query'] . "'";
            } else {
                $wheres[] = " (log.action LIKE '%" . $srch['query'] . "%') OR (log.description LIKE '%" . $srch['query'] . "%') OR (customers.cust_name LIKE '%" . $srch['query'] . "%') OR (customers.cust_email LIKE '%" . $srch['query'] . "%') OR (customers.cust_phone LIKE '%" . $srch['query'] . "%') OR (addresses2.direccion LIKE '%" . $srch['query'] . "%') OR (addresses2.comuna LIKE '%" . $srch['query'] . "%') OR (addresses2.ciudad LIKE '%" . $srch['query'] . "%') OR (addresses2.comentarios LIKE '%" . $srch['query'] . "%') OR (orders.order_comment LIKE '%" . $srch['query'] . "%')";
            }
        }

        if (isset($srch['desde']) && $srch['desde']) {
            $wheres[] = "log.created_at >='" . $srch['desde'] . '"';
        }

        if (isset($srch['hasta']) && $srch['hasta']) {
            $wheres[] = "log.created_at <=" . $srch['hasta'] . "'";
        }

        $condicion = $wheres ? ' and ( ' . implode(') and (', $wheres) . ') ' : '';

        $sql = '
        SELECT
            log.id
        FROM
            log LEFT JOIN orders ON( orders.id = log.order_id )
                LEFT JOIN customers ON( customers.id = orders.cust_id )
                LEFT JOIN addresses AS addresses2 ON( addresses2.id = orders.addr_id )
                LEFT JOIN admins ON( admins.id = log.user_id )
        WHERE
            1
        ';

        $sql .= $condicion;

        if (isset($srch['order']['orderby'])) {
            $sql .= '
                ORDER BY ' . $srch['order']['orderby'] . ' ' . $srch['order']['orderType'] . '
            ';
        } else {
            $sql .= '
                ORDER BY log.id DESC
            ';
        }

        $sql .= '
            LIMIT ' . $srch['start'] . ', ' . $srch['length'];

        $query = $this->db->query($sql);

        $rows = $query->result();

        $ids = array();
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $ids[$row->id] = 1;
            }
        }

        return !empty($ids) ? array_keys($ids) : array();
    }

    public function srchLogCount($srch)
    {
        $wheres = array();

        if (isset($srch['query']) && $srch['query']) {
            if (is_numeric($srch['query'])) {
                $wheres[] = "orders.id ='" . $srch['query'] . "'";
            } else {
                $wheres[] = " (log.action LIKE '%" . $srch['query'] . "%') OR (log.description LIKE '%" . $srch['query'] . "%') OR (customers.cust_name LIKE '%" . $srch['query'] . "%') OR (customers.cust_email LIKE '%" . $srch['query'] . "%') OR (customers.cust_phone LIKE '%" . $srch['query'] . "%') OR (addresses2.direccion LIKE '%" . $srch['query'] . "%') OR (addresses2.comuna LIKE '%" . $srch['query'] . "%') OR (addresses2.ciudad LIKE '%" . $srch['query'] . "%') OR (addresses2.comentarios LIKE '%" . $srch['query'] . "%') OR (orders.order_comment LIKE '%" . $srch['query'] . "%')";
            }
        }

        if (isset($srch['desde']) && $srch['desde']) {
            $wheres[] = "log.created_at >='" . $srch['desde'] . '"';
        }

        if (isset($srch['hasta']) && $srch['hasta']) {
            $wheres[] = "log.created_at <=" . $srch['hasta'] . "'";
        }

        $condicion = $wheres ? ' and ( ' . implode(') and (', $wheres) . ') ' : '';

        $sql = '
        SELECT
            count(log.id) as cant
        FROM
            log LEFT JOIN orders ON( orders.id = log.order_id )
                LEFT JOIN customers ON( customers.id = orders.cust_id )
                LEFT JOIN addresses AS addresses2 ON( addresses2.id = orders.addr_id )
                LEFT JOIN admins ON( admins.id = log.user_id )
        WHERE
            1
        ';

        $sql .= $condicion;

        $query = $this->db->query($sql);

        $row = $query->row();

        return $row->cant;
    }

    public function getSrchLog($id)
    {
        $ids = is_array($id) ? $id : array($id);

        if (empty($ids)) {
            return array();
        }

        $sql = "
            SELECT
                log.*, admins.admin_name
            FROM
                log LEFT JOIN orders ON( orders.id = log.order_id )
                    LEFT JOIN customers ON( customers.id = orders.cust_id )
                    LEFT JOIN addresses AS addresses2 ON( addresses2.id = orders.addr_id )
                    LEFT JOIN admins ON( admins.id = log.user_id )
			WHERE
			      log.id IN(" . (implode(', ', $ids)) . ")
			ORDER BY log.id DESC
		";
        $query = $this->db->query($sql);
        $rows = $query->result();

        $results = array();
        foreach ($rows as $row) {
            $results[$row->id] = $row;
            //$results[$row->id] = $this->norm($results[$row->id]);
        }

        return is_array($id) ? $results : $results[$id];
    }

    public function pending()
    {

        //$user = $this->session->userdata('user');

        if ($this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho) {
            $comunas = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getComunas($this->user->id) : array();
            $locales = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getLocales($this->user->id) : array();

            $bodega_retiro = false;
            if( ! empty($comunas) ){
                foreach($comunas as $indice => $com_id ){
                    if($com_id == 493){
                        $bodega_retiro = true;    
                    }
                }
            }
            $locales = $bodega_retiro ? $locales : array();

            $orders = $this->db
                ->select('orders.*')
                ->from('orders')
                ->join('addresses', 'orders.addr_id = addresses.id')
                ->join('locales', 'locales.id = addresses.local_id', 'LEFT')
                //->where('( ( CASE WHEN addresses.local_id=0 THEN addresses.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) OR ( CASE WHEN addresses.local_id!=0 THEN locales.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) )', null, false)
                ->where('order_canceled', 0)
                ->where('order_delivered is null')
                ->where('(order_delivery_date IS null or driver_id is null)', null, false)
                ->where('paid', 1)
                ->order_by('orders.id desc');

            if( ! empty($locales) ){
                $orders->where('( ( CASE WHEN addresses.local_id!=0 THEN locales.id IN (' . (implode(',', $locales)) . ') ELSE false END ) )', null, false);
            }else{
                $orders->where('( ( CASE WHEN addresses.local_id=0 THEN addresses.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) OR ( CASE WHEN addresses.local_id!=0 THEN locales.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) )', null, false);
            }

            $orders = $orders->get()->result();
                
        } else {
            $orders = $this->db
                ->where('order_canceled', 0)
                ->where('order_delivered is null')
                ->where('paid', 1)
                ->where('(order_delivery_date IS null or driver_id is null)', null, false)
                ->order_by('orders.id desc')
                ->get('orders')
                ->result();
        }

        /*
        // ADD
        $data = array();
        foreach($orders as $o){
        if( $o->paym_id == 4 ){
        //Chequear que fue pagado
        $webpay = $this->db
        ->where('Tbk_respuesta', 0)
        ->where('Tbk_orden_compra', $o->id)
        ->get('webpay')
        ->row();
        if( isset($webpay->Tbk_orden_compra) && $webpay->Tbk_orden_compra == $o->id && $webpay->Tbk_respuesta != ''){
        $data[] = $o;
        }
        }else{
        $data[] = $o;
        }
        }

        if( ! empty($data) ){
        foreach($data as $indice => $o) $o = $this->load($o);
        }
        // FIN ADD
         */

        foreach ($orders as $o) {
            $o = $this->load($o);
        }

        return $orders;
    }

    public function for_delivery()
    {

        //$user = $this->session->userdata('user');

        if ($this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho) {
            $comunas = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getComunas($this->user->id) : array();
            $locales = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getLocales($this->user->id) : array();

            $bodega_retiro = false;
            if( ! empty($comunas) ){
                foreach($comunas as $indice => $com_id ){
                    if($com_id == 493){
                        $bodega_retiro = true;    
                    }
                }
            }
            $locales = $bodega_retiro ? $locales : array();

            $orders = $this->db
                ->select('orders.*')
                ->from('orders')
                ->join('addresses', 'orders.addr_id = addresses.id')
                ->join('locales', 'locales.id = addresses.local_id', 'LEFT')
                //->where('( ( CASE WHEN addresses.local_id=0 THEN addresses.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) OR ( CASE WHEN addresses.local_id!=0 THEN locales.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) )', null, false)
                ->where('order_canceled', 0)
                ->where('order_delivered is null')
                ->where('order_delivery_date IS not null')
                ->where('driver_id IS not null')
                ->where('paid', 1);

            if( ! empty($locales) ){
                $orders->where('( ( CASE WHEN addresses.local_id!=0 THEN locales.id IN (' . (implode(',', $locales)) . ') ELSE false END ) )', null, false);
            }else{
                $orders->where('( ( CASE WHEN addresses.local_id=0 THEN addresses.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) OR ( CASE WHEN addresses.local_id!=0 THEN locales.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) )', null, false);
            }

            $orders = $orders->get()->result();

        } else {
            $orders = $this->db
                ->where('order_canceled', 0)
                ->where('order_delivered is null')
                ->where('order_delivery_date IS not null')
                ->where('driver_id IS not null')
                ->where('paid', 1)
                ->get('orders')
                ->result();
        }

        foreach ($orders as $o) {
            $o = $this->load($o);
        }

        return $orders;
    }

    public function stats_dashboard()
    {
        $search = $this->session->userdata('search');

        $fecha = isset($search['select_date']) && $search['select_date'] ? $search['select_date'] : date('Y-m-d');

        //$user = $this->session->userdata('user');

        $stats = array();

        if ($this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho) {
            $comunas = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getComunas($this->user->id) : array();
            $locales = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getLocales($this->user->id) : array();

            $bodega_retiro = false;
            if( ! empty($comunas) ){
                foreach($comunas as $indice => $com_id ){
                    if($com_id == 493){
                        $bodega_retiro = true;    
                    }
                }
            }
            $locales = $bodega_retiro ? $locales : array();

            $stats = array(
                'delivery' => (!empty($locales) ? $this->db->select('orders.*') // Por entregar
                    ->where('paid', 1)
                    ->where('order_canceled', 0)
                    ->where('order_delivered is null')
                    ->where('(driver_id is not null AND driver_id !=0 )')
                    ->where('order_delivery_date is not null')
                    ->where('order_delivery_date', $fecha)
                    ->from('orders')
                    ->join('addresses', 'orders.addr_id = addresses.id')
                    ->join('locales', 'locales.id = addresses.local_id', 'LEFT')
                    ->where('( ( CASE WHEN addresses.local_id!=0 THEN locales.id IN (' . (implode(',', $locales)) . ') ELSE false END ) )', null, false)
                    ->count_all_results()
                    : $this->db->select('orders.*') // Por entregar
                    ->where('paid', 1)
                    ->where('order_canceled', 0)
                    ->where('order_delivered is null')
                    ->where('(driver_id is not null AND driver_id !=0 )')
                    ->where('order_delivery_date is not null')
                    ->where('order_delivery_date', $fecha)
                    ->from('orders')
                    ->join('addresses', 'orders.addr_id = addresses.id')
                    ->join('locales', 'locales.id = addresses.local_id', 'LEFT')
                    ->where('( ( CASE WHEN addresses.local_id=0 THEN addresses.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) OR ( CASE WHEN addresses.local_id!=0 THEN locales.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) )', null, false)
                    ->count_all_results()
                ),
                'delivered' => (!empty($locales) ? $this->db->select('orders.*') // Entregados
                    ->where('paid', 1)
                    ->where('order_canceled', 0)
                    ->where('order_delivered is not null')
                    ->where('order_delivered >=', $fecha . ' 00:00:00')
                    ->where('order_delivered <=', $fecha . ' 23:59:59')
                    ->from('orders')
                    ->join('addresses', 'orders.addr_id = addresses.id')
                    ->join('locales', 'locales.id = addresses.local_id', 'LEFT')
                    ->where('( ( CASE WHEN addresses.local_id!=0 THEN locales.id IN (' . (implode(',', $locales)) . ') ELSE false END ) )', null, false)
                    ->count_all_results()
                    : $this->db->select('orders.*') // Entregados
                    ->where('paid', 1)
                    ->where('order_canceled', 0)
                    ->where('order_delivered is not null')
                    ->where('order_delivered >=', $fecha . ' 00:00:00')
                    ->where('order_delivered <=', $fecha . ' 23:59:59')
                    ->from('orders')
                    ->join('addresses', 'orders.addr_id = addresses.id')
                    ->join('locales', 'locales.id = addresses.local_id', 'LEFT')
                    ->where('( ( CASE WHEN addresses.local_id=0 THEN addresses.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) OR ( CASE WHEN addresses.local_id!=0 THEN locales.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) )', null, false)
                    ->count_all_results()
                ),
                'sales' => (!empty($locales) ? $this->db// Entregados
                    ->where('paid', 1)
                    ->select_sum('order_total')
                    ->select_sum('order_shipping')
                    ->where('order_canceled', 0)
                    ->where('order_delivery_date is not null')
                    ->where('order_delivery_date', $fecha)
                    ->from('orders')
                    ->join('addresses', 'orders.addr_id = addresses.id')
                    ->join('locales', 'locales.id = addresses.local_id', 'LEFT')
                    ->where('( ( CASE WHEN addresses.local_id!=0 THEN locales.id IN (' . (implode(',', $locales)) . ') ELSE false END ) )', null, false)
                    ->get()->row_array()
                    : $this->db// Entregados
                    ->where('paid', 1)
                    ->select_sum('order_total')
                    ->select_sum('order_shipping')
                    ->where('order_canceled', 0)
                    ->where('order_delivery_date is not null')
                    ->where('order_delivery_date', $fecha)
                    ->from('orders')
                    ->join('addresses', 'orders.addr_id = addresses.id')
                    ->join('locales', 'locales.id = addresses.local_id', 'LEFT')
                    ->where('( ( CASE WHEN addresses.local_id=0 THEN addresses.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) OR ( CASE WHEN addresses.local_id!=0 THEN locales.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) )', null, false)
                    ->get()->row_array()
                ),
            );
        } else {
            $stats = array(
                'delivery' => $this->db// Por entregar
                    ->where('order_canceled', 0)
                    ->where('paid', 1)
                    ->where('order_delivered is null')
                    ->where('(driver_id is not null AND driver_id !=0 )')
                    ->where('order_delivery_date is not null')
                    ->where('order_delivery_date', $fecha)
                    ->from('orders')->count_all_results(),
                'delivered' => $this->db// Entregados
                    ->where('order_canceled', 0)
                    ->where('paid', 1)
                    ->where('order_delivered is not null')
                    ->where('order_delivered >=', $fecha . ' 00:00:00')
                    ->where('order_delivered <=', $fecha . ' 23:59:59')
                    ->from('orders')->count_all_results(),
                'sales' => $this->db// Entregados
                    ->select_sum('order_total')
                    ->where('paid', 1)
                    ->select_sum('order_shipping')
                    ->where('order_canceled', 0)
                    ->where('order_delivery_date is not null')
                    ->where('order_delivery_date', $fecha)
                    ->get('orders')->row_array(),
            );
        }

        return $stats;
    }

    public function stats()
    {
        $pending = $this->pending_dashboard(false);
        //$pending = array();

        //$user = $this->session->userdata('user');

        if ($this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho) {
            $comunas = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getComunas($this->user->id) : array();
            $locales = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getLocales($this->user->id) : array();

            $bodega_retiro = false;
            if( ! empty($comunas) ){
                foreach($comunas as $indice => $com_id ){
                    if($com_id == 493){
                        $bodega_retiro = true;    
                    }
                }
            }
            $locales = $bodega_retiro ? $locales : array();

            $stats = array(
                'all' => (!empty($locales) ? $this->db->select('orders.*')->where('paid', 1)->from('orders')->join('addresses', 'orders.addr_id = addresses.id')->where_in('addresses.local_id', $locales)->get()->num_rows() : $this->db->select('orders.*')->where('paid', 1)->from('orders')->join('addresses', 'orders.addr_id = addresses.id')->where_in('addresses.comuna_id', $comunas)->get()->num_rows()),

                'today' => (!empty($locales) ? $this->db->select('orders.*')->where('order_delivery_date', date('Y-m-d'))->where('order_canceled', 0)->where('paid', 1)->from('orders')->join('addresses', 'orders.addr_id = addresses.id')->where_in('addresses.local_id', $locales)->count_all_results() : $this->db->select('orders.*')->where('order_delivery_date', date('Y-m-d'))->where('order_canceled', 0)->where('paid', 1)->from('orders')->join('addresses', 'orders.addr_id = addresses.id')->where_in('addresses.comuna_id', $comunas)->count_all_results()),

                'pending' => count($pending),

                'delivery' => (!empty($locales) ? $this->db->select('orders.*')
                    ->where('paid', 1)
                    ->where('order_canceled', 0)
                    ->where('order_delivered is null')
                //->where('driver_id is not null')
                    ->where('(driver_id is not null or driver_id!=0 )')
                    ->where('order_delivery_date is not null')
                    ->from('orders')
                    ->join('addresses', 'orders.addr_id = addresses.id')
                    ->join('locales', 'locales.id = addresses.local_id', 'LEFT')
                    ->where('( ( CASE WHEN addresses.local_id!=0 THEN locales.id IN (' . (implode(',', $locales)) . ') ELSE false END ) )', null, false)
                //->where_in('addresses.comuna_id',$comunas)
                    ->count_all_results()
                : $this->db->select('orders.*')
                ->where('paid', 1)
                ->where('order_canceled', 0)
                ->where('order_delivered is null')
            //->where('driver_id is not null')
                ->where('(driver_id is not null or driver_id!=0 )')
                ->where('order_delivery_date is not null')
                ->from('orders')
                ->join('addresses', 'orders.addr_id = addresses.id')
                ->join('locales', 'locales.id = addresses.local_id', 'LEFT')
                ->where('( ( CASE WHEN addresses.local_id=0 THEN addresses.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) OR ( CASE WHEN addresses.local_id!=0 THEN locales.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) )', null, false)
            //->where_in('addresses.comuna_id',$comunas)
                ->count_all_results()
                ),
                'delivered' => (!empty($locales) ? $this->db->select('orders.*')
                    ->where('paid', 1)
                    ->where('order_canceled', 0)
                    ->where('order_delivered is not null')
                    ->from('orders')
                    ->join('addresses', 'orders.addr_id = addresses.id')
                    ->join('locales', 'locales.id = addresses.local_id', 'LEFT')
                    ->where('( ( CASE WHEN addresses.local_id!=0 THEN locales.id IN (' . (implode(',', $locales)) . ') ELSE false END ) )', null, false)
                    ->count_all_results()
                : $this->db->select('orders.*')
                ->where('paid', 1)
                ->where('order_canceled', 0)
                ->where('order_delivered is not null')
                ->from('orders')
                ->join('addresses', 'orders.addr_id = addresses.id')
                ->join('locales', 'locales.id = addresses.local_id', 'LEFT')
                ->where('( ( CASE WHEN addresses.local_id=0 THEN addresses.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) OR ( CASE WHEN addresses.local_id!=0 THEN locales.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) )', null, false)
            //->where_in('addresses.comuna_id',$comunas)
            ),
            );
        } else {
            $stats = array(
                'all' => 0, //$this->db->count_all('orders'),

                'today' => $this->db->where('order_delivery_date', date('Y-m-d'))->where('order_canceled', 0)->where('paid', 1)->from('orders')->count_all_results(),

                'pending' => count($pending),

                /*'pending' => $this->db
                ->where('order_canceled', 0)
                ->where('order_delivered is null')
                ->where('(order_delivery_date IS null or driver_id is null)', null, false)
                ->from('orders')->count_all_results(),*/

                'delivery' => $this->db
                    ->where('paid', 1)
                    ->where('order_canceled', 0)
                    ->where('order_delivered is null')
                //->where('driver_id is not null')
                    ->where('(driver_id is not null or driver_id!=0 )')
                    ->where('order_delivery_date is not null')
                    ->from('orders')->count_all_results(),

                'delivered' => $this->db
                    ->where('paid', 1)
                    ->where('order_canceled', 0)
                    ->where('order_delivered is not null')
                    ->from('orders')->count_all_results(),
            );
        }

        return $stats;
    }

    public function load($o)
    {
        #    $this->load->library('order_methods');

        $this->load->model(array(
            'addresses',
            'customers',
            'payments',
            'drivers',
        ));

        @$o->address = $this->addresses->get($o->addr_id);
        @$o->customer = $this->customers->get($o->cust_id);
        @$o->payment = $this->payments->get($o->paym_id);
        @$o->driver = $this->drivers->get($o->driver_id, false);
        @$o->comuna = $this->db->where('codigoInterno', $o->address->comuna_id)->get('comunas')->row();

        //@$o->is_cyber  = $this->product_iscyber($o->id);
        @$o->product_count = $this->orders->product_count($o->id);
        @$o->date = date('d/m/Y', strtotime($o->order_created));
        @$o->display_price = $this->utils->numformat($o->order_total + $o->order_shipping - $o->order_purcharse_valor_desc);

        @$o->status = (!$o->order_delivery_date || !$o->driver_id) ? 'Pendiente de confirmación' : 'Pendiente de entrega';
        if (@$o->order_delivered) {
            @$o->status = "Entregado";
        }

        /*
        if( isset($o->despachoatupinta_bloque_id) && $o->despachoatupinta_bloque_id ){
        @$o->despachoatupinta_texto = $this->getMsjeDespachoatuPinta($o->id);
        }else{
        @$o->despachoatupinta_texto = '';
        }
         */
        //$o->despachoatupinta_texto = '';
        #$o->methods   = new Order_Methods();

        return $o;
    }

    public function pending_dashboard($load=true, $orderPorComuna=false)
    {

        //$user = $this->session->userdata('user');

        if ($this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho) {
            $comunas = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getComunas($this->user->id) : array();
            $locales = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getLocales($this->user->id) : array();
            
            $bodega_retiro = false;
            if( ! empty($comunas) ){
                foreach($comunas as $indice => $com_id ){
                    if($com_id == 493){
                        $bodega_retiro = true;    
                    }
                }
            }
            $locales = $bodega_retiro ? $locales : array();
            
            $orders = $this->db
                ->select('orders.*')
                ->from('orders')
                ->where('paid', 1)
                ->where('order_canceled', 0)
                ->where('order_delivered is null')
                ->where('(order_delivery_date IS null or driver_id is null)', null, false)
                ->join('addresses', 'orders.addr_id = addresses.id')
                ->join('locales', 'locales.id = addresses.local_id', 'LEFT');

            if( ! empty($locales) ){
                $orders->where('( ( CASE WHEN addresses.local_id!=0 THEN locales.id IN (' . (implode(',', $locales)) . ') ELSE false END ) )', null, false);
            }else{
                $orders->where('( ( CASE WHEN addresses.local_id=0 THEN addresses.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) OR ( CASE WHEN addresses.local_id!=0 THEN locales.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) )', null, false);
            }

        } else {
            $orders = $this->db
                ->select('orders.*')
                ->from('orders')
                ->where('paid', 1)
                ->where('order_canceled', 0)
                ->where('order_delivered is null')
                ->where('(order_delivery_date IS null or driver_id is null)', null, false)
                ->join('addresses', 'orders.addr_id = addresses.id');
        }

        if ($orderPorComuna == true) {
            $orders->order_by('addresses.comuna_id, orders.id DESC');
        }else{
            $orders->order_by('orders.order_created asc');
        }
        
        $orders = $orders->get()->result();

        /*
        $data = array();
        foreach($orders as $o){
        if( $o->paym_id == 4 ) {
        //Chequear que fue pagado
        $webpay = $this->db
        ->where('Tbk_respuesta', 0)
        ->where('Tbk_orden_compra', $o->id)
        ->get('webpay')
        ->row();
        if (isset($webpay->Tbk_orden_compra) && $webpay->Tbk_orden_compra == $o->id && $webpay->Tbk_respuesta != '') {
        $data[] = $o;
        }

        }else if( $o->paym_id == 6 || $o->paym_id == 5 ){
        //Chequear que fue pagado con webpay oneclick
        $oneclick = $this->db
        ->where('responseCode', '0')
        ->where('buyOrder', $o->id)
        ->get('tbk_oneclick_authorizes')
        ->row();
        if( isset($oneclick->id) && $oneclick->buyOrder == $o->id && $oneclick->responseCode != ''){
        $data[] = $o;
        }
        }else if( $o->paym_id == 7 ){
        //Chequear que fue pagado para punto pago
        $pp = $this->db
        ->where('respuesta', '00')
        ->where('order_id', $o->id)
        ->get('puntopagos')
        ->row();
        if( isset($pp->id) && $pp->order_id == $o->id && $pp->respuesta != ''){
        $data[] = $o;
        }
        }else{
        $data[] = $o;
        }
        }

        if( ! empty($data) && $load == true ){
        foreach($data as $indice => $o) $o = $this->load($o);
        }
         */

        if (!empty($orders) && $load == true) {
            foreach ($orders as $indice => $o) {
                $o = $this->load($o);
            }
        }

        return $orders;
    }

    public function simpliroute($load=true, $orderPorComuna=false)
    {

        //$user = $this->session->userdata('user');

        if ($this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho) {
            $comunas = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getComunas($this->user->id) : array();
            $locales = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getLocales($this->user->id) : array();
            
            $bodega_retiro = false;
            if( ! empty($comunas) ){
                foreach($comunas as $indice => $com_id ){
                    if($com_id == 493){
                        $bodega_retiro = true;    
                    }
                }
            }
            $locales = $bodega_retiro ? $locales : array();
            
            $orders = $this->db
                ->select('orders.*')
                ->from('orders')
                ->where('paid', 1)
                ->where('order_canceled', 0)
                ->where('order_delivered is null')
                ->where('(order_delivery_date IS null or driver_id is null)', null, false)
                ->join('addresses', 'orders.addr_id = addresses.id')
                ->join('locales', 'locales.id = addresses.local_id', 'LEFT');

            if( ! empty($locales) ){
                $orders->where('( ( CASE WHEN addresses.local_id!=0 THEN locales.id IN (' . (implode(',', $locales)) . ') ELSE false END ) )', null, false);
            }else{
                $orders->where('( ( CASE WHEN addresses.local_id=0 THEN addresses.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) OR ( CASE WHEN addresses.local_id!=0 THEN locales.comuna_id IN (' . (implode(',', $comunas)) . ') ELSE false END ) )', null, false);
            }

        } else {
            $orders = $this->db
                ->select('orders.*')
                ->from('orders')
                ->where('paid', 1)
                ->where('order_canceled', 0)
                ->where('order_delivered is null')
                ->where('(order_delivery_date IS null or driver_id is null)', null, false)
                ->join('addresses', 'orders.addr_id = addresses.id');
        }

        if ($orderPorComuna == true) {
            $orders->order_by('addresses.comuna_id, orders.id DESC');
        }else{
            $orders->order_by('orders.order_created asc');
        }
        
        $orders = $orders->get()->result();

        /*
        $data = array();
        foreach($orders as $o){
        if( $o->paym_id == 4 ) {
        //Chequear que fue pagado
        $webpay = $this->db
        ->where('Tbk_respuesta', 0)
        ->where('Tbk_orden_compra', $o->id)
        ->get('webpay')
        ->row();
        if (isset($webpay->Tbk_orden_compra) && $webpay->Tbk_orden_compra == $o->id && $webpay->Tbk_respuesta != '') {
        $data[] = $o;
        }

        }else if( $o->paym_id == 6 || $o->paym_id == 5 ){
        //Chequear que fue pagado con webpay oneclick
        $oneclick = $this->db
        ->where('responseCode', '0')
        ->where('buyOrder', $o->id)
        ->get('tbk_oneclick_authorizes')
        ->row();
        if( isset($oneclick->id) && $oneclick->buyOrder == $o->id && $oneclick->responseCode != ''){
        $data[] = $o;
        }
        }else if( $o->paym_id == 7 ){
        //Chequear que fue pagado para punto pago
        $pp = $this->db
        ->where('respuesta', '00')
        ->where('order_id', $o->id)
        ->get('puntopagos')
        ->row();
        if( isset($pp->id) && $pp->order_id == $o->id && $pp->respuesta != ''){
        $data[] = $o;
        }
        }else{
        $data[] = $o;
        }
        }

        if( ! empty($data) && $load == true ){
        foreach($data as $indice => $o) $o = $this->load($o);
        }
         */

        if (!empty($orders) && $load == true) {
            foreach ($orders as $indice => $o) {
                $o = $this->load($o);
            }
        }

        return $orders;
    }

    public function product_count($order_id)
    {
        return $this->db
            ->select('sum(qty) as count')
            ->where('order_id', $order_id)
            ->get('sku_orders')
            ->row()
            ->count;
    }

    public function product_iscyber($order_id)
    {
        $result = $this->db
            ->select('count(*) as count')
            ->from('sku_orders')
            ->join('sku', 'sku.id = sku_orders.sku_id')
            ->where('sku.cyber', 1)
            ->where('sku_orders.order_id', $order_id)
            ->get()->row();
        return $result->count ? 1 : 0;
    }

    public function by_customer($customer_id, $with_products = false)
    {
        $orders = array();

        $orders_id = $this->db
            ->select('id')
            ->where('cust_id', $customer_id)
            ->get('orders')
            ->result_array();

        foreach ($orders_id as $o) {
            $orders[] = $this->get($o, true);
        }

        return $orders;
    }

    public function update($id, $data)
    {
        return $this->db
            ->where('id', $id)
            ->update('orders', $data);
    }

    public function updCheckPicking($id, $data)
    {
        return $this->db
            ->where('id', $id)
            ->update('sku_orders', $data);
    }

    public function get_products($order)
    {
        $this->load->model('products');

        $products = $this->db
            ->where('order_id', $order->id)
            ->get('sku_orders')
            ->result();

        foreach ($products as $p) {
            $p->description = $this->products->by_sku($p->sku_id);
        }

        $order->products = $products;

        return $order;
    }

    public function create($data)
    {
        $this->db->insert('orders', $data);
        return $this->db->insert_id();
    }
    
    public function append_sku($data)
    {
        $this->db->insert('sku_orders', $data);
        return $this->db->insert_id();
    }

    public function by_driver($driver_id, $today = true, $search = array())
    {
        $params['driver_id'] = $driver_id;
        $params['paid'] = 1; //add by jeans

        if ($today) {
            $params['order_delivery_date'] = isset($search['select_date']) && $search['select_date'] ? $search['select_date'] : date('Y-m-d');
        }

        if (isset($search['order_delivered_null']) && $search['order_delivered_null']) {
            $params['order_delivered'] = null;
        }

        if (isset($search['order_canceled']) && $search['order_canceled']) {
            $params['order_canceled'] = 0;
        }

        $orders = $this->search($params, false, 0, true);

        return $orders;
    }

    public function sync_products($order_id, $skus = array())
    {
        $this->db->delete('sku_orders', array('order_id' => $order_id));

        foreach ($skus as $sku) {
            if ($sku['qty'] > 0) {
                $this->db->insert('sku_orders', array(
                    'sku_id' => $sku['id'],
                    'order_id' => $order_id,
                    'qty' => $sku['qty'],
                ));
            }
        }

        $this->update_total($order_id);
    }

    public function update_total($order_id)
    {
        $order = $this->get($order_id, true);

        $total = 0;
        foreach ($order->products as $p) {
            $total += $p->qty * $p->description->price;
        }

        $this->db->where('id', $order_id)->update('orders', array(
            'order_total' => $total,
        ));

        return $total;
    }

    public function getEmpresaBsale()
    {
        $query = $this->db->select("*")->from('ajustes')->join('bsale_empresas', 'bsale_empresas.id = ajustes.bsale_empresas_id', 'left')->where('ajustes.id', 1)->get();
        $result = $query->row();
        return $result;
    }

    public function getPayments()
    {
        return $this->db->get('payments')->result();
    }

    public function getLocales()
    {
        $rows = $this->db
            ->get('locales')
            ->result();

        $locales = array();
        foreach ($rows as $row) {
            $locales[$row->id] = $row;
        }

        return $locales;
    }

    //Orders pending
    public function getOrdersByComunas($comuna_ids, $filters = array())
    {
        if (empty($comuna_ids)) {
            return array();
        }

        $orders = $this->db
            ->select('orders.*')
            ->from('orders')
            ->join('addresses', 'orders.addr_id = addresses.id')
        //->join('locales', 'locales.id = addresses.local_id', 'LEFT')
            ->where('( CASE WHEN addresses.local_id=0 THEN addresses.comuna_id IN (' . (implode(',', $comuna_ids)) . ') ELSE false END )', null, false)
        //->where($filters)
            ->where('paid', 1)
            ->where('order_canceled', 0)
            ->where('order_delivered is null')
            ->where('(order_delivery_date IS null or driver_id is null)', null, false)
            ->order_by('orders.id', 'desc')
            ->get()
            ->result();

        //foreach($orders as $o) $o = $this->load($o);

        return $orders;
    }

    public function srchOrdersNotDelivered($srch = array())
    {
        $wheres = array();
        $limite = '';

        if (isset($srch['orders_start_date']) && !empty($srch['orders_start_date'])) {
            //$fechainicio = implode('-',array_reverse(explode('-',$search['fechainicio'])));
            $wheres[] = "o.order_created >='" . $srch['orders_start_date'] . " 00:00:00'";
        }
        if (isset($srch['orders_end_date']) && !empty($srch['orders_end_date'])) {
            //$fechafin = implode('-',array_reverse(explode('-',$search['fechafin'])));
            $wheres[] = "o.order_created <='" . $srch['orders_end_date'] . " 23:59:59'";
        }

        //Orders pagados
        if (isset($srch['orders_paid'])) {
            $wheres[] = "o.paid ='" . $srch['orders_paid'] . "'";
        }

        if (isset($srch['in_customers_mails']) && !empty($srch['in_customers_mails'])) {
            $wheres[] = "cus.cust_email IN('" . implode('\',\'', $srch['in_customers_mails']) . "') ";
        }

        if (isset($srch['not_customers_mails']) && !empty($srch['not_customers_mails'])) {
            $wheres[] = "cus.cust_email NOT IN('" . implode('\',\'', $srch['not_customers_mails']) . "') ";
        }

        $condicion = $wheres ? ' and ( ' . implode(') and (', $wheres) . ') ' : '';
        
        $sql = '
        SELECT
            o.id 
        FROM  orders AS o LEFT JOIN customers AS cus ON( o.cust_id = cus.id )
                          LEFT JOIN addresses AS addr ON( addr.id = o.addr_id )
                          LEFT JOIN bodegas_rel_envio_comunas AS breb ON( breb.envio_comuna_id = addr.comuna_id )
                          LEFT JOIN bodegas AS bod ON( bod.id = breb.bodega_id )
        WHERE
            1
        ';
        //o.order_delivered is null
        $sql .= $condicion;

        if (isset($srch['order'])) {
            $sql .= '
            ORDER BY ' . $srch['order']['column'] . ' ' . $srch['order']['dir'] . '
            ';
        }

        $sql .= $limite;

        if (isset($srch['group_by'])) {
            $sql .= '
            GROUP BY ' . $srch['group_by'] . '
            ';
        }
        
        $query = $this->db->query($sql);

        $rows = $query->result();

        return $rows;
    }

    public function getSelectsRegion(){

        $wheres = array();

        $selects = array();
        if ($this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho) {
            $comunas = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getComunas($this->user->id) : array();
            $locales = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getLocales($this->user->id) : array();

            $bodega_retiro = false;
            if (! empty($comunas)) {
                foreach ($comunas as $indice => $com_id) {
                    if ($com_id == 493) {
                        $bodega_retiro = true;
                    }
                }
            }
            $locales = $bodega_retiro ? $locales : array();

            if( empty($locales) ){
                if ( !empty($comunas) ) {
                    $wheres[] = " envio_comunas.id IN('".implode("','", $comunas)."') ";
                }else{
                    $wheres[] = " envio_regiones.id = -1";
                }
            }
        }

        $sql = "
            SELECT
                envio_regiones.id, envio_regiones.nombre
            FROM
                envio_comunas INNER JOIN envio_regiones ON( envio_regiones.id = envio_comunas.region )
			WHERE
			      1
        ";        
        $condicion = $wheres ? ' and ( '.implode(') and (', $wheres).') ' : '';

        $sql .= $condicion;

        $query = $this->db->query($sql);

        $rows = $query->result();

        if( ! empty($rows) ){
            foreach ($rows as $indices => $row) {
                $selects[$row->id] = $row->nombre;
            }
        }

        return $selects;
    }

    public function getSelectsComunasPorRegion($region_id){

        $wheres = array();

        $selects = array();
        /*
        if ($this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho) {
            $comunas = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getComunas($this->user->id) : array();
            $locales = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getLocales($this->user->id) : array();

            $bodega_retiro = false;
            if (! empty($comunas)) {
                foreach ($comunas as $indice => $com_id) {
                    if ($com_id == 493) {
                        $bodega_retiro = true;
                    }
                }
            }
            $locales = $bodega_retiro ? $locales : array();

            if( empty($locales) ){
                if ( !empty($comunas) ) {
                    $wheres[] = " envio_comunas.id IN('".implode("','", $comunas)."') ";
                }else{
                    $wheres[] = " envio_regiones.id = -1";
                }
            }
            die("entre");
        }
        */
        $wheres [] = " envio_comunas.region = ".$region_id;

        $sql = "
            SELECT
                envio_comunas.id, envio_comunas.nombre
            FROM
                envio_comunas INNER JOIN envio_regiones ON( envio_regiones.id = envio_comunas.region )
			WHERE
                1
        ";        
        $condicion = $wheres ? ' and ( '.implode(') and (', $wheres).') ' : '';

        $sql .= $condicion;


        $query = $this->db->query($sql);

        $rows = $query->result();

        if( ! empty($rows) ){
            foreach ($rows as $indices => $row) {
                $selects[$row->id] = $row;
            }
        }

        return $selects;
    }

    public function getSelectsLocales(){

        $wheres = array();

        $selects = array();
        /*
        if ($this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho) {
            $comunas = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getComunas($this->user->id) : array();
            $locales = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getLocales($this->user->id) : array();

            $bodega_retiro = false;
            if (! empty($comunas)) {
                foreach ($comunas as $indice => $com_id) {
                    if ($com_id == 493) {
                        $bodega_retiro = true;
                    }
                }
            }
            $locales = $bodega_retiro ? $locales : array();
            if( !empty($locales) ){
                $wheres[] = " locales.id IN('".implode("','", $locales)."') ";
            }
        }
        */
        $sql = "
            SELECT
                locales.id, locales.alias, locales.nombre
            FROM
                locales 
			WHERE
                1
        ";        
        $condicion = $wheres ? ' and ( '.implode(') and (', $wheres).') ' : '';

        $sql .= $condicion;

        $query = $this->db->query($sql);

        $rows = $query->result();

        if( ! empty($rows) ){
            foreach ($rows as $indices => $row) {
                $selects[$row->id] = $row->alias.' - '.$row->nombre;
            }
        }

        return $selects;
    }

    public function getSelectsComunas(){

        $wheres = array();

        $selects = array();
        if ($this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho) {
            $comunas = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getComunas($this->user->id) : array();
            $locales = $this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho ? $this->users->getLocales($this->user->id) : array();

            $bodega_retiro = false;
            if (! empty($comunas)) {
                foreach ($comunas as $indice => $com_id) {
                    if ($com_id == 493) {
                        $bodega_retiro = true;
                    }
                }
            }
            $locales = $bodega_retiro ? $locales : array();

            if( empty($locales) ){
                if ( !empty($comunas) ) {
                    $wheres[] = " envio_comunas.id IN('".implode("','", $comunas)."') ";
                }else{
                    $wheres[] = " envio_regiones.id = -1";
                }
            }
        }

        $sql = "
            SELECT
                envio_comunas.id, envio_comunas.nombre, envio_comunas.region
            FROM
                envio_comunas INNER JOIN envio_regiones ON( envio_regiones.id = envio_comunas.region )
			WHERE
                1
        ";        
        $condicion = $wheres ? ' and ( '.implode(') and (', $wheres).') ' : '';
        
        $sql .= $condicion;

        $query = $this->db->query($sql);

        $rows = $query->result();

        if( ! empty($rows) ){
            foreach ($rows as $indices => $row) {
                $selects[$row->id] = $row;
            }
        }

        return $selects;
    }

    public function srchExportXLS($search = array())
    {

        $wheres = array();

		$userdata = $this->session->userdata;
		
        /*
		if (isset($search['global']) && $search['global']) {
            $campos = array( 's.name', 'u.first_name', 'u.last_name', 'c.name', 'c.rut' );
            $palabras = preg_split( "/\s+/",  strtoupper( $srch['global'] ), -1, PREG_SPLIT_NO_EMPTY );
            $wheres = array();
            foreach( $palabras as $palabra ){
                $where = array();
                foreach( $campos as $campo ){
                    $where[] = "($campo like '%$palabra%')";
                }
                $wheres[] = implode( ' or ', $where );
            }
        }
        */

        /*
        if( isset($search['fecha_ini']) && $search['fecha_ini'] ){
            $wheres[] = "o.order_delivered >='".date('Y-m-d', strtotime($search['fecha_ini']))."'";
        }else{
            $wheres[] = "o.order_delivered >='".date('Y-m-d')."'";
        }

        if( isset($search['fecha_fin']) && $search['fecha_fin'] ){
            $wheres[] = "o.order_delivered <='".date('Y-m-d', strtotime($search['fecha_fin']))."'";
        }

        if( isset($search['driver_id']) && $search['driver_id'] ){
            $wheres[] = "o.driver_id ='".$search['driver_id']."'";
        }
        */

        if( isset($search['date']) && $search['date'] ){
            $wheres[] = "o.order_delivery_date ='".$search['date']."'";
        }else{
            $wheres[] = "o.order_delivery_date ='".date('Y-m-d')."'";
        }

        $condicion = $wheres ? ' and ( '.implode( ') and (',  $wheres ).') ' : '';

        $sql = "
            SELECT 
			    o.id
			FROM orders AS o
							INNER JOIN admins AS a ON( o.driver_id = a.id )
			WHERE
              	a.role_id = 1
            ";

        $sql .= $condicion;

        $sql .= '
            ORDER BY o.order_delivery_date DESC
        ';
        
        $query = $this->db->query($sql);

        $rows = $query->result();

        $ids = array();
        if( ! empty($rows) )
        {
            foreach ($rows as $row)
            {
                $ids[$row->id] = 1;
            }
        }

        return ! empty($ids) ? array_keys( $ids ) : array();
	}
}
