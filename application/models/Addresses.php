<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Addresses extends CI_Model
{

    function get($id)
    {

        $address = $this->db
            ->select('addresses.*, envio_comunas.nombre as comuna, regiones.nombre as region')
            ->join('envio_comunas', 'addresses.comuna_id = envio_comunas.id', 'left')
            ->join('regiones', 'regiones.codigo = envio_comunas.region', 'left')
            ->where('addresses.id', $id)
            ->get('addresses')
            ->row();

        return $address;
    }

    function by_customer($cId)
    {

        $addresses = $this->db
            ->select('addresses.*, envio_comunas.nombre as comuna, addresses.comuna as old_comuna')
            ->join('envio_comunas', 'addresses.comuna_id = envio_comunas.id', 'left')
            ->where('cust_id', $cId)
            ->get('addresses')
            ->result();

        return $addresses;
    }

    function comunas()
    {

        $comunas = $this->db
            ->get('envio_comunas')
            ->result();

        return $comunas;
    }

    function getComunasByRegion($region_id){

        $sql = "
        SELECT 
            ec.*
        FROM
            envio_comunas AS ec 
        WHERE 
            ec.region = '".$region_id."'
        ";

        $sql .= '
            ORDER BY ec.nombre ASC
        ';

        $query = $this->db->query($sql);

        $rows = $query->result();

        $selects = array();
        if( ! empty($rows) )
        {
            foreach ($rows as $row)
            {
                $selects[$row->id] = $row;
            }
        }

        return $selects;
    }

    function get_comuna($id)
    {

        return $this->db
            ->where('id', $id)
            ->get('envio_comunas')
            ->row();
    }

    public function selectsRegiones(){

        $sql = "
        SELECT
          *
        FROM regiones
        WHERE 
          activo = 1
        ";

        $sql .= '
            ORDER BY `nombre` ASC
        ';

        $query = $this->db->query($sql);

        $rows = $query->result();

        $selects = array();
        if( ! empty($rows) )
        {
            foreach ($rows as $row)
            {
                $selects[$row->codigo] = $row;
            }
        }

        return $selects;
    }

    function create($data)
    {
        $this->db->insert('addresses', $data);
        return $this->db->insert_id();
    }

    function update($id, $data)
    {
        return $this->db
            ->where('id', $id)
            ->update('addresses', $data);
    }

}