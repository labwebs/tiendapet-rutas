<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Bodegas extends CI_Model
{
    function get($id)
    {
        $bodega = $this->db
            ->select('*')
            ->where('id', $id)
            ->get('bodegas')
            ->row();

        return $bodega;
    }

    function getEmpresa($id)
    {
        $bodega = $this->db
            ->select('*')
            ->where('id', $id)
            ->get('bsale_empresas')
            ->row();

        return $bodega;
    }

    function getBodegaEmpresa($id)
    {
        $bodega = $this->db
            ->select('*')
            ->where('id', $id)
            ->get('bsale_empresas_bodegas')
            ->row();

        return $bodega;
    }

    function all()
    {
        $bodegas = $this->db
            ->select('*')
            ->get('bodegas')
            ->result();

        return $bodegas;
    }

    function getBsaleEmpresasAll()
    {
        return $this->db
            ->select('*')
            ->get('bsale_empresas')
            ->result();
    }

    function getBsaleBodegasByEmpresa($empresa_id)
    {
        $bodegas = $this->db
            ->select('*')
            ->where('bsale_empresa_id', $empresa_id)
            ->get('bsale_empresas_bodegas')
            ->result();
        return $bodegas;
    }

    function getBodegasSyncAjustes()
    {
        $bodegas = $this->db
            ->select('*')
            ->where('ajuste_id', 1)
            ->get('ajustes_sync_bodegas')
            ->result_array();
        return $bodegas;
    }

    function getBySku($sku_id)
    {
        $results = $this->db
            ->select('*')
            ->where('sku_id', $sku_id)
            ->get('sku_rel_bodegas')
            ->result_array();
        return $results;
    }

    function deleteBodegaRelSKU($bodega_id, $sku_id){
        return $this->db->delete('sku_rel_bodegas', array('bodega_id' => $bodega_id, 'sku_id' => $sku_id));
    }

    function insertBodegaRelSKU($bodega_id, $sku_id){
        return $this->db->insert('sku_rel_bodegas', array('bodega_id' => $bodega_id, 'sku_id' => $sku_id, 'priority' => 0));
    }

    public function updSku($id, $data)
    {
        return $this->db
            ->where('id', $id)
            ->update('sku', $data);
    }
}