<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Descuentos_model extends CI_Model {

    function all()
    {
        $descuentos = $this->db
            ->select('*')
            ->get('discounts_mails')
            ->result();
        return $descuentos;
    }

    function getByCorreo($correo)
    {
        $fcurrent = date('Y-m-d');

        $row = $this->db
            ->select('*')
            ->where('email', $correo)
            ->where('used < quantity')
            ->where("'".$fcurrent."' BETWEEN starts AND ends")
            ->get('discounts_mails')
            ->row_array();
        return $row;
    }

    function get($id)
    {
        $row = $this->db
            ->select('*')
            ->where('id', $id)
            ->get('discounts_mails')
            ->row_array();
        
        return $row;
    }

    function load($drivers)
    {

        $this->load->model('orders');

        foreach ($drivers as $d) :
            $d->last_seen = false;

            if ($d->last_delivery) $d->last_seen = $this->orders->get($d->last_delivery);

        endforeach;

        return $drivers;

    }

    /*
    function find($id)
    {
        return $this->db
            ->where('id', $id)
            ->get('descuentos')
            ->row();
    }
    */

    function create($data)
    {
        $this->db->insert('discounts_mails', $data);
        return $this->db->insert_id();
    }

    function update($id, $data)
    {
        $this->db
            ->where('id', $id)
            ->update('discounts_mails', $data);

        return $this->db->affected_rows();
    }

}
