<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');  
class Products extends CI_Model {
  
  function by_sku($sku_id) {
    
    $product = $this->db
      ->select('sku.prod_id, sku.sku_description as size, sku.sku_price as price, sku.sku_offer as price_offer, products.prod_name as name, sku.sku_available as available, brands.name as brand')
      ->where('sku.id', $sku_id)
      ->join('products', 'products.id = sku.prod_id')
      ->join('brands', 'brands.id = products.brand_id')
      ->get('sku')
      ->row();
    
    if( isset($product->prod_id) && $product->prod_id ){
        $product->categorias = $this->getProductoCategorias($product->prod_id);
    }else{
        //$product->categorias = new stdclass();
    }

    //$product->categorias = isset($product->prod_id) && $product->prod_id ? @$this->getProductoCategorias($product->prod_id) : array();

    return $product;
  }

    function getProductoCategorias($id){
        if (empty($id)) {
            return array();
        }
            
        $query = "
        SELECT 
        categories.*  
        FROM 
        categories INNER JOIN products_categories ON (products_categories.categories_id = categories.id)  
        WHERE  
        products_categories.products_id = '".$id."' 
        ";
            
        $rows = $this->db->query($query)->result();
        $categorias = array();
        if (! empty($rows)) {
            foreach ($rows as $row) {
                $categorias[$row->id] = $row;
                $categorias[$row->id]->path = $this->getCaminosCategories($row->id);
            }
        }
            
        return $categorias;
    }
  
  function all() {
    $products = $this->db
      ->select('sku.id as id, sku.prod_id, sku.sku_description as size, sku.sku_price as price, products.prod_name as name, sku.sku_available as available, brands.name as brand')
      ->join('products', 'products.id = sku.prod_id')
      ->join('brands', 'brands.id = products.brand_id')
      ->get('sku')
      ->result();
    
    return $products;
  }

    function getArbolCategories(){

        $query = "
			SELECT 
                * 
			FROM 
			    categories 
			ORDER BY cat_parent ASC
		";

        $rows = $this->db->query( $query )->result();
        $rows = (array)($rows);

        $categorias = array();
        foreach ($rows as $indice => $row){
            $row = (array)$row;
            $cat_id = $row['id'];
            $categorias[$cat_id] = $row;
            $padre = isset($row['cat_parent']) ? intval($row['cat_parent']) : $cat_id;
            $row['path'] = isset($categorias[$padre]['path']) ? $categorias[$padre]['path'].'/'.$cat_id : $cat_id;
            $categorias[$cat_id] = $row;
        }

        $j = 0;
        while( !$j ){
            $j = 1;
            $padres = array();
            foreach( $categorias as $i=>$c ){
                $path   = $c['path'];
                $cat_id = $c['id'];
                $padre  = $c['cat_parent'];
                $c['path'] = isset($categorias[$padre]['path']) ? $categorias[$padre]['path'].'/'.$cat_id : $cat_id;
                $categorias[$cat_id] = $c;

                if ( $path != $c['path'] && in_array( $cat_id, $padres ) ) $j = 0;
                if ( $padre ) $padres[] = $padre;
            }
        }

        uasort( $categorias, create_function( '$a, $b', 'return strcmp( $a["path"], $b["path"] );' ) );


        return $categorias;
    }

    function getCaminosCategories( $op ){
        $categorias = $this->getArbolCategories();
        $caminos = explode( '/', $categorias[$op]['path'] );

        $result = array();
        foreach( $caminos as $camino ){
            $result[$camino] = $categorias[$camino];
        }
        return $result;
    }
}