<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Simpliroute_model extends CI_Model
{

    public function srch($srch=array()){

        $wheres = array();

        if( isset($srch['status']) && $srch['status'] != -1 ){
            $wheres[] = "p.status ='".$srch['status']."'";
        }
        
        if( isset($srch['cron']) ){
            $wheres[] = "p.cron ='".$srch['cron']."'";
        }

        if( isset($srch['start_date']) && $srch['start_date'] ){
            $wheres[] = "DATE_FORMAT(p.start_date,'%Y-%m-%d') >= '".$srch['start_date']."'";
        }

        if( isset($srch['end_date']) && $srch['end_date'] ){
            $wheres[] = "DATE_FORMAT(p.end_date,'%Y-%m-%d') <= '".$srch['end_date']."'";
        }

        //if( isset($srch['searchByDate']) && ! empty($srch['searchByDate']) ){
        //    $wheres[] = "DATE_FORMAT(order_created,'%Y-%m-%d') = '".$srch['searchByDate']."'";
        //}

        $condicion = $wheres ? ' and ( '.implode( ') and (',  $wheres ).') ' : '';

        $sql = '
        SELECT
            p.id
        FROM 
            sr_plan AS p INNER JOIN sr_plan_routes AS pr ON( p.id = pr.sr_plan_id )
        WHERE
            p.deleted_at IS NULL  
        ';

        $sql .= $condicion;

        if( isset( $srch['order'] ) ){
            $sql .= '
                ORDER BY '.$srch['order']['orderby'].' '.$srch['order']['orderType'].'
            ';
        }else{
            $sql .= '
                ORDER BY p.id DESC
            ';
        }

        $query = $this->db->query($sql);

        $rows = $query->result();

        $ids = array();
        if( ! empty($rows) )
        {
            foreach ($rows as $row)
            {
                $ids[$row->id] = 1;
            }
        }

        return ! empty($ids) ? array_keys( $ids ) : array();
    }

    function get( $id ){

        $ids = is_array( $id ) ? $id : array( $id );

        if ( empty( $ids ) ) return array();

        $sql = "
			SELECT
                p.*
            FROM 
                sr_plan AS p /*INNER JOIN sr_plan_routes AS pr ON( p.id = pr.sr_plan_id )*/
			WHERE 
			      p.id IN(".( implode( ', ', $ids ) ).")
			ORDER BY p.id DESC
		";
        $query = $this->db->query($sql);
        $rows = $query->result();

        $results = array();
        foreach( $rows as $row ){
            $results[$row->id] = $row;
            $results[$row->id]->routes = $this->findRoutesByPlanId($row->id);
            $results[$row->id]->driver = $this->findDriverByPlanId($row->id);
            //$order_id = $this->getOrderIdByIdPurcharse($row->id);
            //$results[$row->id]->order = $this->ordenes->get($order_id);
        }

        return is_array( $id ) ? $results : $results[$id];
    }

    function findChoferByOrderAndPlannedDate($id, $planned_date)
    {
        return $this->db
            ->where('order_id', $id)
            ->where('planned_date', $planned_date)
            ->get('sr_plan_assignment')
            ->row();
    }

    function findRoutesByPlanId($id)
    {
        return $this->db
            ->where('sr_plan_id', $id)
            ->get('sr_plan_routes')
            ->result();
    }

    function findDriverByPlanId($id)
    {
        $rows = $this->db
            ->where('sr_plan_id', $id)
            ->get('sr_plan_assignment')
            ->result();

        $results = array();
        foreach( $rows as $row ){
            $results[$row->order_id] = $row;
        }

        return $results;
    }

    function create($data)
    {
        $this->db->insert('sr_plan', $data);
        return $this->db->insert_id();
    }
    
    function createRoute($data)
    {
        $this->db->insert('sr_plan_routes', $data);
        return $this->db->insert_id();
    }
    
    function createAssignment($data)
    {
        $this->db->insert('sr_plan_assignment', $data);
        return $this->db->insert_id();
    }

    function find($id)
    {
        return $this->db
            ->where('id', $id)
            ->get('sr_plan')
            ->row();
    }
    
    function update($id, $data)
    {
        return $this->db
            ->where('id', $id)
            ->update('sr_plan', $data);
    }
}