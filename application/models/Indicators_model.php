<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Indicators_model extends CI_Model
{
    public function srchOrdersBuy($srch = array(), $return_key = 'order_id')
    {
        $wheres = array();
        $limite = '';

        if (isset($srch['orders_start_date']) && !empty($srch['orders_start_date'])) {
            //$fechainicio = implode('-',array_reverse(explode('-',$search['fechainicio'])));
            $wheres[] = "o.order_created >='" . $srch['orders_start_date'] . " 00:00:00'";
        }
        if (isset($srch['orders_end_date']) && !empty($srch['orders_end_date'])) {
            //$fechafin = implode('-',array_reverse(explode('-',$search['fechafin'])));
            $wheres[] = "o.order_created <='" . $srch['orders_end_date'] . " 23:59:59'";
        }

        //Orders pagados
        if (isset($srch['orders_paid'])) {
            $wheres[] = "o.paid ='" . $srch['orders_paid'] . "'";
        }

        if (isset($srch['in_customers_mails']) && !empty($srch['in_customers_mails'])) {
            $wheres[] = "cus.cust_email IN('" . implode('\',\'', $srch['in_customers_mails']) . "') ";
        }

        if (isset($srch['not_customers_mails']) && !empty($srch['not_customers_mails'])) {
            $wheres[] = "cus.cust_email NOT IN('" . implode('\',\'', $srch['not_customers_mails']) . "') ";
        }

        $condicion = $wheres ? ' and ( ' . implode(') and (', $wheres) . ') ' : '';
        /*
        $sql = '
        SELECT
        o.id AS order_id, breb.bodega_id, o.paym_id AS order_paym_id, cus.id AS cust_id, cus.cust_email, addr.id AS addr_id, addr.comuna_id AS addr_comuna_id, addr.local_id AS addr_local_id
        FROM  orders AS o INNER JOIN customers AS cus ON( o.cust_id = cus.id )
        INNER JOIN addresses AS addr ON( addr.id = o.addr_id )
        INNER JOIN bodegas_rel_envio_comunas AS breb ON( breb.envio_comuna_id = addr.comuna_id )
        INNER JOIN bodegas AS bod ON( bod.id = breb.bodega_id )
        WHERE
        cus.cust_email != "" AND
        o.order_delivered is not null
        ';
         */
        $sql = '
        SELECT
        o.id AS order_id, o.order_total, o.order_shipping, o.order_purcharse_valor_desc, breb.bodega_id, cus.cust_email, addr.local_id AS addr_local_id
        FROM  orders AS o INNER JOIN customers AS cus ON( o.cust_id = cus.id )
        INNER JOIN addresses AS addr ON( addr.id = o.addr_id )
        INNER JOIN bodegas_rel_envio_comunas AS breb ON( breb.envio_comuna_id = addr.comuna_id )
        INNER JOIN bodegas AS bod ON( bod.id = breb.bodega_id )
        WHERE
        cus.cust_email != "" AND
        o.order_delivered is not null
        ';

        /*
        breb.bodega_id != 11 AND
        breb.bodega_id != 12
         */

        $sql .= $condicion;

        if (isset($srch['order'])) {
            $sql .= '
            ORDER BY ' . $srch['order']['column'] . ' ' . $srch['order']['dir'] . '
            ';
        }

        $sql .= $limite;

        if (isset($srch['group_by'])) {
            $sql .= '
            GROUP BY ' . $srch['group_by'] . '
            ';
        }

        /*$sql .= '
        ORDER BY p.id DESC
        '.$limite;*/

        $query = $this->db->query($sql);

        $rows = $query->result();

        /*
        $results = array();
        if( ! empty($rows) )
        {
        foreach ($rows as $row)
        {
        if( empty( $row->bodega_id ) ){
        //Hay comunas que no estan asignado a una bodega
        $results['sin-bodega'][] = $row;
        }else{
        $results[$row->bodega_id][$row->id] = $row;
        }
        }
        }
         */

        $results = array();
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $value_key = strtolower($row->$return_key);
                if (empty($value_key)) {
                    continue;
                }

                $results[$value_key] = $row;
            }
        }

        return $results;
    }

    public function srchCustomersBuy($srch = array(), $return_key = 'id')
    {
        $wheres = array();
        $limite = '';

        if (isset($srch['orders_start_date']) && !empty($srch['orders_start_date'])) {
            //$fechainicio = implode('-',array_reverse(explode('-',$search['fechainicio'])));
            $wheres[] = "o.order_created >='" . $srch['orders_start_date'] . " 00:00:00'";
        }
        if (isset($srch['orders_end_date']) && !empty($srch['orders_end_date'])) {
            //$fechafin = implode('-',array_reverse(explode('-',$search['fechafin'])));
            $wheres[] = "o.order_created <='" . $srch['orders_end_date'] . " 23:59:59'";
        }

        //Orders pagados
        if (isset($srch['orders_paid'])) {
            $wheres[] = "o.paid ='" . $srch['orders_paid'] . "'";
        }

        if (isset($srch['in_customers_mails']) && !empty($srch['in_customers_mails'])) {
            $wheres[] = "cus.cust_email IN('" . implode('\',\'', $srch['in_customers_mails']) . "') ";
        }

        if (isset($srch['not_customers_mails']) && !empty($srch['not_customers_mails'])) {
            $wheres[] = "cus.cust_email NOT IN('" . implode('\',\'', $srch['not_customers_mails']) . "') ";
        }

        $condicion = $wheres ? ' and ( ' . implode(') and (', $wheres) . ') ' : '';

        $sql = '
        SELECT
        cus.id, cus.cust_email
        FROM customers AS cus INNER JOIN orders AS o ON( o.cust_id = cus.id )
        INNER JOIN addresses AS addr ON( addr.id = o.addr_id )

        LEFT JOIN bodegas_rel_envio_comunas AS breb ON( breb.envio_comuna_id = addr.comuna_id )
        LEFT JOIN bodegas AS bod ON( bod.id = breb.bodega_id )

        WHERE
        cus.cust_email != "" AND
        o.order_delivered is not null AND
        breb.bodega_id != 11 AND
        breb.bodega_id != 12
        ';

        $sql .= $condicion;

        if (isset($srch['order'])) {
            $sql .= '
            ORDER BY ' . $srch['order']['column'] . ' ' . $srch['order']['dir'] . '
            ';
        }

        $sql .= $limite;

        if (isset($srch['group_by'])) {
            $sql .= '
            GROUP BY ' . $srch['group_by'] . '
            ';
        }

        /*$sql .= '
        ORDER BY p.id DESC
        '.$limite;*/

        $query = $this->db->query($sql);

        $rows = $query->result();

        $keys = array();
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $value_key = strtolower($row->$return_key);
                if (empty($value_key)) {
                    continue;
                }

                $keys[$value_key] = 1;
            }
        }

        return !empty($keys) ? array_keys($keys) : array();
    }

    public function srchOrdersBuyByCategories($srch = array(), $return_key = 'order_id')
    {
        $wheres = array();
        $limite = '';

        if (isset($srch['orders_start_date']) && !empty($srch['orders_start_date'])) {
            //$fechainicio = implode('-',array_reverse(explode('-',$search['fechainicio'])));
            $wheres[] = "o.order_created >='" . $srch['orders_start_date'] . " 00:00:00'";
        }
        if (isset($srch['orders_end_date']) && !empty($srch['orders_end_date'])) {
            //$fechafin = implode('-',array_reverse(explode('-',$search['fechafin'])));
            $wheres[] = "o.order_created <='" . $srch['orders_end_date'] . " 23:59:59'";
        }

        //Orders pagados
        if (isset($srch['orders_paid'])) {
            $wheres[] = "o.paid ='" . $srch['orders_paid'] . "'";
        }

        if (isset($srch['in_customers_mails']) && !empty($srch['in_customers_mails'])) {
            $wheres[] = "cus.cust_email IN('" . implode('\',\'', $srch['in_customers_mails']) . "') ";
        }

        if (isset($srch['not_customers_mails']) && !empty($srch['not_customers_mails'])) {
            $wheres[] = "cus.cust_email NOT IN('" . implode('\',\'', $srch['not_customers_mails']) . "') ";
        }

        if (isset($srch['in_categories']) && !empty($srch['in_categories'])) {
            $wheres[] = "categories.id IN('" . implode('\',\'', $srch['in_categories']) . "') ";
        }

        $condicion = $wheres ? ' and ( ' . implode(') and (', $wheres) . ') ' : '';

        $sql = '
        SELECT
        o.id as order_id, o.order_total, o.order_shipping, o.order_purcharse_valor_desc, cus.id, cus.cust_email, categories.`id` as cat_id, categories.`name` as cat_name, categories.`slug` as cat_slug, products.`prod_name`
        FROM customers AS cus INNER JOIN orders AS o ON( o.cust_id = cus.id )
        INNER JOIN addresses AS addr ON( addr.id = o.addr_id )
        INNER JOIN sku_orders AS skuorder ON( o.id = skuorder.order_id )
        INNER JOIN sku ON( skuorder.sku_id = sku.id )
        INNER JOIN products ON ( sku.prod_id = products.id )
        INNER JOIN brands ON ( products.brand_id = brands.id )
        INNER JOIN products_categories ON ( products_categories.products_id = products.id )
        INNER JOIN categories ON ( products_categories.categories_id = categories.id )
        WHERE
        cus.cust_email != "" AND
        o.order_delivered is not null
        ';

        $sql .= $condicion;

        if (isset($srch['order'])) {
            $sql .= '
            ORDER BY ' . $srch['order']['column'] . ' ' . $srch['order']['dir'] . '
            ';
        }

        $sql .= $limite;

        if (isset($srch['group_by'])) {
            $sql .= '
            GROUP BY ' . $srch['group_by'] . '
            ';
        }

        /*$sql .= '
        ORDER BY p.id DESC
        '.$limite;*/
        //print_a($sql);exit;
        $query = $this->db->query($sql);

        $rows = $query->result();

        $keys = array();
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $value_key = strtolower($row->$return_key);
                if (empty($value_key)) {
                    continue;
                }

                $keys[$value_key] = $row;
                //$keys[$value_key] = 1;
            }
        }

        return !empty($keys) ? $keys : array();
        //return ! empty($keys) ? array_keys( $keys ) : array();
    }

    public function srchCustomersBuyByCategories($srch = array(), $return_key = 'id')
    {
        $wheres = array();
        $limite = '';

        if (isset($srch['orders_start_date']) && !empty($srch['orders_start_date'])) {
            //$fechainicio = implode('-',array_reverse(explode('-',$search['fechainicio'])));
            $wheres[] = "o.order_created >='" . $srch['orders_start_date'] . " 00:00:00'";
        }
        if (isset($srch['orders_end_date']) && !empty($srch['orders_end_date'])) {
            //$fechafin = implode('-',array_reverse(explode('-',$search['fechafin'])));
            $wheres[] = "o.order_created <='" . $srch['orders_end_date'] . " 23:59:59'";
        }

        //Orders pagados
        if (isset($srch['orders_paid'])) {
            $wheres[] = "o.paid ='" . $srch['orders_paid'] . "'";
        }

        if (isset($srch['in_customers_mails']) && !empty($srch['in_customers_mails'])) {
            $wheres[] = "cus.cust_email IN('" . implode('\',\'', $srch['in_customers_mails']) . "') ";
        }

        if (isset($srch['not_customers_mails']) && !empty($srch['not_customers_mails'])) {
            $wheres[] = "cus.cust_email NOT IN('" . implode('\',\'', $srch['not_customers_mails']) . "') ";
        }

        if (isset($srch['in_categories']) && !empty($srch['in_categories'])) {
            $wheres[] = "categories.id IN('" . implode('\',\'', $srch['in_categories']) . "') ";
        }

        $condicion = $wheres ? ' and ( ' . implode(') and (', $wheres) . ') ' : '';

        $sql = '
        SELECT
        o.id as order_id, cus.id, cus.cust_email, categories.`id` as cat_id, categories.`name` as cat_name, categories.`slug` as cat_slug, products.`prod_name`
        FROM customers AS cus INNER JOIN orders AS o ON( o.cust_id = cus.id )
        INNER JOIN addresses AS addr ON( addr.id = o.addr_id )
        INNER JOIN sku_orders AS skuorder ON( o.id = skuorder.order_id )
        INNER JOIN sku ON( skuorder.sku_id = sku.id )
        INNER JOIN products ON ( sku.prod_id = products.id )
        INNER JOIN brands ON ( products.brand_id = brands.id )
        INNER JOIN products_categories ON ( products_categories.products_id = products.id )
        INNER JOIN categories ON ( products_categories.categories_id = categories.id )
        WHERE
        cus.cust_email != "" AND
        o.order_delivered is not null
        ';

        $sql .= $condicion;

        if (isset($srch['order'])) {
            $sql .= '
            ORDER BY ' . $srch['order']['column'] . ' ' . $srch['order']['dir'] . '
            ';
        }

        $sql .= $limite;

        if (isset($srch['group_by'])) {
            $sql .= '
            GROUP BY ' . $srch['group_by'] . '
            ';
        }

        /*$sql .= '
        ORDER BY p.id DESC
        '.$limite;*/
        //print_a($sql);
        $query = $this->db->query($sql);

        $rows = $query->result();

        $keys = array();
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $value_key = strtolower($row->$return_key);
                if (empty($value_key)) {
                    continue;
                }

                $keys[$value_key] = $row;
                //$keys[$value_key] = 1;
            }
        }

        return !empty($keys) ? $keys : array();
        //return ! empty($keys) ? array_keys( $keys ) : array();
    }

    public function getCustomersPorBodega($values, $key = 'id', $filters = array(), $show = false)
    {
        $wheres = array();

        $values = is_array($values) ? $values : array($values);

        if (empty($values)) {
            return array();
        }

        $wheres[] = "cus." . $key . " IN('" . implode('\',\'', $values) . "') ";

        if (isset($filters['orders_start_date']) && !empty($filters['orders_start_date'])) {
            //$fechainicio = implode('-',array_reverse(explode('-',$search['fechainicio'])));
            $wheres[] = "o.order_created >='" . $filters['orders_start_date'] . " 00:00:00'";
        }
        if (isset($filters['orders_end_date']) && !empty($filters['orders_end_date'])) {
            //$fechafin = implode('-',array_reverse(explode('-',$search['fechafin'])));
            $wheres[] = "o.order_created <='" . $filters['orders_end_date'] . " 23:59:59'";
        }
        
        //Orders pagados
        if (isset($filters['orders_paid'])) {
            $wheres[] = "o.paid ='" . $filters['orders_paid'] . "'";
        }

        $condicion = $wheres ? ' and ( ' . implode(') and (', $wheres) . ') ' : '';

        $sql = '
        SELECT
        cus.*, breb.bodega_id, o.addr_id as addr_id
        FROM customers AS cus INNER JOIN orders AS o ON( o.cust_id = cus.id )
        INNER JOIN addresses AS addr ON( addr.id = o.addr_id )
        LEFT JOIN bodegas_rel_envio_comunas AS breb ON( breb.envio_comuna_id = addr.comuna_id )
        LEFT JOIN bodegas AS bod ON( bod.id = breb.bodega_id )
        WHERE
        o.order_delivered is not null AND
        breb.bodega_id != 11 AND
        breb.bodega_id != 12
        ';

        $sql .= $condicion;

        if (isset($filters['group_by'])) {
            $sql .= '
            GROUP BY ' . $filters['group_by'] . '
            ';
        }
        $query = $this->db->query($sql);

        $rows = $query->result();

        $results = array();
        if (!empty($rows)) {
            foreach ($rows as $row) {
                if (empty($row->bodega_id)) {
                    //Hay comunas que no estan asignado a una bodega
                    $results['sin-bodega'][] = $row;
                } else {
                    $results[$row->bodega_id][] = $row;
                }
            }
        }

        return $results;
    }

    public function getBuyPorBodega($filters = array(), $key = 'id')
    {
        $wheres = array();

        if (isset($filters['orders_start_date']) && !empty($filters['orders_start_date'])) {
            //$fechainicio = implode('-',array_reverse(explode('-',$search['fechainicio'])));
            $wheres[] = "o.order_created >='" . $filters['orders_start_date'] . " 00:00:00'";
        }
        if (isset($filters['orders_end_date']) && !empty($filters['orders_end_date'])) {
            //$fechafin = implode('-',array_reverse(explode('-',$search['fechafin'])));
            $wheres[] = "o.order_created <='" . $filters['orders_end_date'] . " 23:59:59'";
        }

        //Orders pagados
        if (isset($filters['orders_paid'])) {
            $wheres[] = "o.paid ='" . $filters['orders_paid'] . "'";
        }

        $condicion = $wheres ? ' and ( ' . implode(') and (', $wheres) . ') ' : '';

        $sql = '
        SELECT
        cus.*, breb.bodega_id, o.addr_id as addr_id
        FROM customers AS cus INNER JOIN orders AS o ON( o.cust_id = cus.id )
        INNER JOIN addresses AS addr ON( addr.id = o.addr_id )
        LEFT JOIN bodegas_rel_envio_comunas AS breb ON( breb.envio_comuna_id = addr.comuna_id )
        LEFT JOIN bodegas AS bod ON( bod.id = breb.bodega_id )
        WHERE
        o.order_delivered is not null
        ';

        $sql .= $condicion;

        if (isset($filters['group_by'])) {
            $sql .= '
            GROUP BY ' . $filters['group_by'] . '
            ';
        }
        $query = $this->db->query($sql);

        $rows = $query->result();

        $results = array();
        if (!empty($rows)) {
            foreach ($rows as $row) {
                if (empty($row->bodega_id)) {
                    //Hay comunas que no estan asignado a una bodega
                    $results['sin-bodega'][] = $row;
                } else {
                    $results[$row->bodega_id][] = $row;
                }
            }
        }

        return $results;
    }

    public function getSkuPorBodega($key = 'id', $values = array(), $filters = array())
    {
        $wheres = array();

        $values = is_array($values) ? $values : array($values);

        if (!empty($values)) {
            $wheres[] = "sku." . $key . " IN('" . implode('\',\'', $values) . "') ";
        }

        //Sku Disponible
        if (isset($filters['sku_available'])) {
            $wheres[] = "sku.sku_available ='" . $filters['sku_available'] . "'";
            $wheres[] = "products.prod_available = 1";
        }

        $condicion = $wheres ? ' and ( ' . implode(') and (', $wheres) . ') ' : '';

        $sql = '
        SELECT
        sku.*, sku_rel_bodegas.bodega_id, products.prod_available, products.prod_name, brands.name as marca, categories.name as categoria, categories.slug as cslug, brands.slug as bslug
        FROM
        sku  LEFT JOIN sku_rel_bodegas ON( sku.id = sku_rel_bodegas.sku_id )
        LEFT JOIN products ON (sku.prod_id  = products.id)
        LEFT JOIN brands ON (products.brand_id = brands.id)
        LEFT JOIN products_categories ON (products_categories.products_id = products.id)
        LEFT JOIN categories ON (products_categories.categories_id = categories.id)
        WHERE
        1
        ';

        $sql .= $condicion;

        if (isset($filters['group_by'])) {
            $sql .= '
            GROUP BY ' . $filters['group_by'] . '
            ';
        }

        $query = $this->db->query($sql);

        $rows = $query->result();

        $results = array();
        if (!empty($rows)) {
            foreach ($rows as $row) {
                if (empty($row->bodega_id)) {
                    //Hay comunas que no estan asignado a una bodega
                    $results['sin-bodega'][] = $row;
                } else {
                    $results[$row->bodega_id][$row->id] = $row;
                }
            }
        }

        return $results;
    }

    public function srchSku($srch = array(), $return_key = 'id')
    {
        $wheres = array();
        $limite = '';

        if (isset($srch['sku_available'])) {
            $wheres[] = "sku.sku_available ='" . $srch['sku_available'] . "'";
        }

        $condicion = $wheres ? ' and ( ' . implode(') and (', $wheres) . ') ' : '';

        $sql = '
        SELECT
        sku.id
        FROM
        sku
        WHERE
        1
        ';

        $sql .= $condicion;

        if (isset($srch['order'])) {
            $sql .= '
            ORDER BY ' . $srch['order']['column'] . ' ' . $srch['order']['dir'] . '
            ';
        }

        $sql .= $limite;

        if (isset($srch['group_by'])) {
            $sql .= '
            GROUP BY ' . $srch['group_by'] . '
            ';
        }

        $query = $this->db->query($sql);

        $rows = $query->result();

        $keys = array();
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $value_key = strtolower($row->$return_key);
                if (empty($value_key)) {
                    continue;
                }

                $keys[$value_key] = 1;
            }
        }

        return !empty($keys) ? array_keys($keys) : array();
    }

    public function getBodegas()
    {
        $rows = $this->db
            ->from('bodegas')
            ->select("*")
            ->order_by('bodegas.nombre', 'asc')
            ->get()
            ->result_array();

        $results = array();
        if (!empty($rows)) {
            foreach ($rows as $row) {
                if (in_array($row['id'], array(11, 12))) {
                    continue;
                }

                $results[$row['id']] = $row;
            }
        }

        return $results;
    }

    public function getLocales()
    {
        $rows = $this->db
            ->from('locales')
            ->select("*")
            ->order_by('locales.alias', 'asc')
            ->get()
            ->result_array();

        $results = array();
        if (!empty($rows)) {
            foreach ($rows as $row) {
                /*
                if (in_array($row['id'], array(11, 12))) {
                    continue;
                }
                */
                $results[$row['id']] = $row;
            }
        }

        return $results;
    }

    /*
function get($id)
{

$payment = $this->db
->select('paym_name as name')
->where('id', $id)
->get('payments')
->row();

return $payment;
}

function all()
{
return $this->db
->select("*, paym_name as name")
->get('payments')
->result();
}
 */
}
