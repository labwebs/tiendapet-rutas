<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Drivers extends CI_Model
{
    function all($load = true, $filters=array())
    {
        $user = $this->session->userdata('user');

        if ($this->user->is_tomador_pedidos || $this->user->is_tomador_pedidos_despacho) {
            if( ! empty($filters) ){
                $drivers = $this->db
                    ->select('id,admin_name as name, admin_login as username, last_delivery, email, phone, inactivo')
                    ->where('role_id', 1)
                    ->where($filters)
                    ->where_in('id', $user->choferes)
                    ->get('admins')
                    ->result();
            }else{
                $drivers = $this->db
                    ->select('id,admin_name as name, admin_login as username, last_delivery, email, phone, inactivo')
                    ->where('role_id', 1)
                    ->where_in('id', $user->choferes)
                    ->get('admins')
                    ->result();
            }

        } else {
            if( ! empty($filters) ){
                $drivers = $this->db->select('id,admin_name as name, admin_login as username, last_delivery, email, phone, inactivo')->where('role_id', 1)->where($filters)->get('admins')->result();
            }else{
                $drivers = $this->db->select('id,admin_name as name, admin_login as username, last_delivery, email, phone, inactivo')->where('role_id', 1)->get('admins')->result();
            }

        }

        if ($load) return $this->load($drivers);

        return $drivers;
    }

    function get($driverId, $load = true)
    {
        $driver = $this->db
            ->select('id,admin_name as name, admin_login as username, last_delivery, email, phone')
            ->where('id', $driverId)
            ->get('admins')
            ->row();

        if ($load)
            return $this->load($driver);

        return $driver;
    }

    function load($drivers)
    {

        $this->load->model('orders');

        foreach ($drivers as $d) :
            $d->last_seen = false;

            if ($d->last_delivery) $d->last_seen = $this->orders->get($d->last_delivery);

        endforeach;

        return $drivers;

    }

    function find($id)
    {
        return $this->db
            ->where('id', $id)
            ->get('admins')
            ->row();
    }
    
    function findChoferSimpliroute($id)
    {
        return $this->db
            ->where('chofer_id', $id)
            ->get('sr_chofer')
            ->row();
    }

    function findVehicleSimpliroute($id)
    {
        return $this->db
            ->where('api_vehiculo_id', $id)
            ->get('sr_chofer')
            ->row();
    }

    function create($data)
    {
        $this->db->insert('admins', $data);
        return $this->db->insert_id();
    }
    
    function update($id, $data)
    {
        return $this->db
            ->where('id', $id)
            ->update('admins', $data);
    }

    function insertComuna($data)
    {
        $this->db->insert('chofer_rel_comuna', $data);
        return $this->db->insert_id();
    }
    
    function insert_srChofer($data)
    {
        $this->db->insert('sr_chofer', $data);
        return $this->db->insert_id();
    }

    function deleteComuna($id)
    {
        return $this->db->delete('chofer_rel_comuna', array('id' => $id));
    }

    function getComunas($chofer_id)
    {
        return $this->db
            ->where('chofer_id', $chofer_id)
            ->get('chofer_rel_comuna')
            ->result();
    }

    function getBodegas()
    {
        return $this->db
            ->where('inactivo', 0)
            ->get('bodegas')
            ->result();
    }

    function getComunasAvailableInBodega($chofer_id){

        $sql = "
			SELECT 
			  bc.*
			FROM 
			  bodegas_rel_envio_comunas AS bc LEFT JOIN chofer_rel_comuna AS cc ON(cc.comuna_id = bc.envio_comuna_id) 
			                                  LEFT JOIN admins AS a ON(a.id = cc.chofer_id AND bc.bodega_id = a.chofer_bodega_id)
			WHERE 
			      a.id = '".$chofer_id."' 
		";
        $query = $this->db->query($sql);
        $rows = $query->result();

        $ids = array();
        if( ! empty($rows) ){
            foreach ($rows as $row){
                $ids[] = $row->envio_comuna_id;
            }
        }

        return $ids;
    }

    function overview($driverId = null, $search = array())
    {
        $this->load->model('orders');

        #¿uno o muchos?
        if ($driverId) {
            $driver = $this->get($driverId, false);
            $driver->orders = $this->orders->by_driver($driverId, true, $search);

            return $driver;

        }else {

            $drivers = $this->all(true, array('inactivo' => 0));
            foreach ($drivers as &$d) {
                $d->orders = $this->orders->by_driver($d->id, true, $search);
            }
            return $drivers;

        }
    }

    function byChoferSimpliroute($search)
    {
        $this->load->model('orders');

        $driver = $this->findByUserRolChofer('simpliroute');
        if( isset($driver->id) && $driver->id ){
            $driver = $this->get($driver->id, false);
            $driver->orders = $this->orders->by_driver($driver->id, true, $search);
        }else{
            $driver = new stdClass();
            $driver->orders = array();
        }

        return $driver;
    }

    function findByUserRolChofer($username)
    {
        return $this->db
            ->where('admin_login', $username)
            ->where('role_id', 1)
            ->get('admins')
            ->row();
    }

    public function selects(){

        $sql = "
        SELECT
          	*
		FROM 
            admins
        WHERE
          role_id = 1 AND 
          inactivo = 0
        ";

        $sql .= '
            ORDER BY admin_name ASC
        ';

        $query = $this->db->query($sql);

        $rows = $query->result();

        $selects = array();
        if( ! empty($rows) )
        {
            foreach ($rows as $row)
            {
                $selects[$row->id] = $row;
            }
        }

        return $selects;
    }

    public function getAllVehiculosSR()
    {
        $sql = "
			SELECT sr_c.*, a.admin_name, a.email, a.address_start, a.latitude_start, a.longitude_start, a.address_end, a.latitude_end, a.longitude_end, a.shift_start, a.shift_end, a.inactivo, a.inactivo_sr, a.window_start_2, a.window_end_2, a.duration   
            FROM sr_chofer AS sr_c INNER JOIN admins a ON( sr_c.chofer_id = a.id ) 
            WHERE a.inactivo_sr = 0 
		";
        $query = $this->db->query($sql);

        $rows = $query->result();

        $results = array();
        foreach( $rows as $row ){
            $results[$row->chofer_id] = $row;
        }

        return $results;
    }

    public function AllVehiculosSR()
    {
        $sql = "
			SELECT sr_c.*, a.admin_name, a.email, a.address_start, a.latitude_start, a.longitude_start, a.address_end, a.latitude_end, a.longitude_end, a.shift_start, a.shift_end, a.inactivo, a.inactivo_sr, a.window_start_2, a.window_end_2, a.duration      
            FROM sr_chofer AS sr_c INNER JOIN admins a ON( sr_c.chofer_id = a.id ) 
            WHERE 1
		";
        $query = $this->db->query($sql);

        $rows = $query->result();

        $results = array();
        foreach( $rows as $row ){
            $results[$row->chofer_id] = $row;
        }

        return $results;
    }
}