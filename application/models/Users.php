<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Users extends CI_Model
{
    public function get($username, $password)
    {
        $user = $this->db
        ->select('admin_login,admin_name, role_id, id, perfil_id')
        ->where('admin_login', $username)
        ->where('password', md5($password))
        ->get('admins')
        ->row();
        
        if ($user) {
            $this->log($user->admin_login);
            $user->is_super_admin = $user->role_id == 5 ? true : false;
            $user->is_admin = $user->role_id == 2 ? true : false;
            $user->is_driver = $user->role_id == 1 ? true : false;
            $user->is_picking = $user->role_id == 4 ? true : false;
            $user->is_tomador_pedidos = $user->perfil_id == 1 ? true : false;
            $user->is_tomador_pedidos_despacho = $user->perfil_id == 2 ? true : false;
            return $user;
        } else {
            return false;
        }
    }
    
    public function log($login)
    {
        $this->db
        ->where('admin_login', $login)
        ->update('admins', array(
        'last_login' => date('Y-m-d H:i:s'),
        'last_ip' => $this->input->ip_address(),
        ));
    }
    
    public function getChoferes($user_id)
    {
        $rows = $this->db
        ->select('chofer_id')
        ->where('admin_id', $user_id)
        ->get('admins_rel_chofer')
        ->result_array();
        
        $ids_chofer = array();
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $ids_chofer[$row['chofer_id']] = $row['chofer_id'];
            }
        }
        
        $user = $this->db
        ->select('*')
        ->where('id', $user_id)
        ->get('admins')
        ->row_array();
        if (isset($user['id']) && $user['id']) {
            if ($user['role_id'] == 1) {
                $ids_chofer[$user_id] = $user_id;
            }
        }
        
        /*
        if( ! empty($ids_chofer) ){
        $rows = $this->db
        ->select('admin_name')
        ->from('admins')
        ->where_in('id', array_values($ids_chofer))
        ->get()
        ->result_array();
        }
        */
        
        return !empty($ids_chofer) ? array_values($ids_chofer) : array(-1);
    }
    
    public function getBodegas($user_id)
    {
        $rows = $this->db
        ->select('bodega_id')
        ->where('admin_id', $user_id)
        ->get('admins_rel_bodegas')
        ->result_array();
        
        $ids_bodegas = array();
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $ids_bodegas[$row['bodega_id']] = $row['bodega_id'];
            }
        }
        
        return !empty($ids_bodegas) ? array_values($ids_bodegas) : array(-1);
    }
    
    public function getComunas($user_id)
    {
        $ids_comunas = array();
        
        $rows = $this->db
        ->select('bodega_id')
        ->where('admin_id', $user_id)
        ->get('admins_rel_bodegas')
        ->result_array();
        
        $ids_bodegas = array();
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $ids_bodegas[$row['bodega_id']] = $row['bodega_id'];
            }
        }
        
        if (!empty($ids_bodegas)) {
            $rows = $this->db
            ->select('envio_comuna_id')
            ->where_in('bodega_id', $ids_bodegas)
            ->get('bodegas_rel_envio_comunas')
            ->result_array();
            
            if (!empty($rows)) {
                foreach ($rows as $row) {
                    $ids_comunas[$row['envio_comuna_id']] = $row['envio_comuna_id'];
                }
            }
        }
        
        return !empty($ids_comunas) ? array_values($ids_comunas) : array(-1);
    }

    public function getLocales($user_id)
    {
        $ids_comunas = array();
        
        $rows = $this->db
        ->select('local_id')
        ->where('admin_id', $user_id)
        ->get('admins_rel_locales')
        ->result_array();
        
        $ids_locales = array();
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $ids_locales[$row['local_id']] = $row['local_id'];
            }
        }
        
        return !empty($ids_locales) ? array_values($ids_locales) : array(-1);
    }
}