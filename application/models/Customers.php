<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Customers extends CI_Model
{

    function get($id, $load = false)
    {

        $customer = $this->db
            ->select('id, cust_login as login, cust_email as email, cust_name as name, cust_phone as phone, cust_avatar as avatar')
            ->where('id', $id)
            ->get('customers')
            ->row();

        if ($load) $customer = $this->load($customer);

        return $customer;
    }

    function load($customer)
    {

        $this->load->model('addresses');

        $customer->addresses = $this->addresses->by_customer($customer->id);
        $customer->has_orders = $this->has_orders($customer->id);

        return $customer;
    }

    function all($load = false)
    {

        $customers = $this->db
            ->select('id, cust_login as login, cust_email as email, cust_name as name, cust_phone as phone, cust_avatar as avatar')
            ->get('customers')
            ->result();

        if ($load)
            foreach ($customers as $c)
                $c = $this->load($c);

        return $customers;
    }

    function count_all()
    {
        return $this->db->count_all('customers');
    }

    function has_orders($customer_id)
    {

        return $this->db
            ->where('cust_id', $customer_id)
            ->get('orders')
            ->num_rows();
    }

    function create($data)
    {
        $this->db->insert('customers', $data);
        return $this->db->insert_id();
    }

    function find($params)
    {

        if (!$params) return array();

        if ($query = $params['query'])
            $this->db
                ->or_like(array(
                    'customers.cust_name' => $query,
                    'customers.cust_email' => $query,
                    'customers.cust_phone' => $query
                ));
        # TODO: FILTRAR POR QUERY

        $customers = $this->db
            ->select('*, customers.id as id', false)
            ->group_by('customers.id')
            ->get('customers')
            ->result();

        //foreach($customers as $c) $c = $this->load($c);

        return $customers;
    }

    function loadAddressesHasorders($customer_id)
    {

        if (!$customer_id) return new stdClass();

        $this->load->model('addresses');
        $customer = new stdClass();
        $customer->addresses = $this->addresses->by_customer($customer_id);
        $customer->has_orders = $this->has_orders($customer_id);

        return $customer;
    }

}