


<div class="contentpanel">

    <div class="row">
        <form id="myform-search-sku" action="" method="post" onsubmit="return false;">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h2 class="panel-title">Buscar por Sku Stock en Bsale</h2>
                    </div>
                    <div class="panel-body">
                        <div class="form-inline- col-md-12">
                            <fieldset class="form-group col-md-2"></fieldset>
                            <fieldset class="form-group text-center col-md-4">
                                <?php /*<label class="sr-only">Sku</label>*/?>
                                <div class="row input-group2">
                                    <input type="text" class="form-control" id="skuid" name="skuid" placeholder="Ingrese SKU" value="" />
                                </div>
                            </fieldset>
                                
                            <fieldset class="form-group col-md-4">
                                <select name="empresa_id" id="empresa_id">
                                    <option value="">- Seleccione Empresa -</option>
                                    <?php foreach ($empresas as $indice => $empresa) { ?>
                                        <option value="<?php echo $empresa->id;?>"><?php echo $empresa->nombre;?></option>
                                    <?php } ?>
                                </select>
                            </fieldset>
                            <fieldset class="form-group col-md-2"></fieldset>
                            
                            <?php
                            /*
                            <fieldset class="form-group col-md-4">
                                <select name="bodega_id" id="bodega_id">
                                    <option value="">- Seleccione Bodega -</option>
                                </select>
                            </fieldset>
                            */
                            ?>
                            <fieldset class="form-group text-center col-md-12">
                                <div class="row input-group-">
                                    <button id="btn-search-sku" type="submit" class="btn btn-warning"><i class="glyphicon glyphicon-search" aria-hidden="true"></i> <?php echo "Buscar en Bsale"; ?></button>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>

            </div>
        </form>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                        <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                    </div>
                    <h2 class="panel-title">Detalle</h2>
                </div>

                <div class="panel-body nopadding">
                    <div id="wrapper-info" class="form-horizontal form-bordered">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        
<script>
$(function(){

    jQuery("#myform-search-sku").validate({
        rules:{
            skuid : {
                required: true
            },
            empresa_id : {
                required: true
            },
            //bodega_id : {
            //    required: true
            //}
        },
        messages:{
            skuid : '',
            empresa_id : '',
            //bodega_id : ''
        },
        submitHandler: function(form){
            //form.submit();
            var skuid = $("#skuid").val();
            var empresa_id = $("#empresa_id").val();
            //var bodega_id = $("#bodega_id").val();
            $.ajax({
                type: "POST",
                url: "/ajax/bsaleSearchBySku",
                data: { 
                    'skuid': skuid, 
                    'empresa_id': empresa_id, 
                    //'bodega_id': bodega_id 
                },
                success: function(data){
                    data = jQuery.parseJSON(data);
                    $("#wrapper-info").html(data.html);
                }
            });
        },
        highlight: function(element, errorClass) {
            if (element.type == "select-one") {
                var elem = $(element);
                if (elem.hasClass("select2-offscreen")) {
                    //$("#s2id_"+elem.attr("id")).addClass("errorClass");
                    $("#s2id_"+elem.attr("id")).addClass('has-error');
                } else {
                    jQuery(element).closest('.input-group').addClass('has-error');
                }
            }else{
                jQuery(element).closest('.input-group2').addClass('has-error');
            }
        },
        unhighlight: function(element, errorClass) {
            if (element.type == "select-one") {
                var elem = $(element);
                if (elem.hasClass("select2-offscreen")) {
                    //$("#s2id_"+elem.attr("id")).addClass("errorClass");
                    $("#s2id_"+elem.attr("id")).removeClass('has-error');
                } else {
                    jQuery(element).closest('.input-group').removeClass('has-error');
                }
            }else{
                jQuery(element).closest('.input-group').removeClass('has-error');
            }
        }
    });
    
    jQuery("#btn-search-sku").click(function(e){
        e.preventDefault();

        $("#wrapper-info").html('<fieldset class="form-group col-md-12 text-center"><label class="control-label text-center">Cargando ...</label></fieldset>');

        $("#myform-search-sku").submit();
    });
    /*
    jQuery("#empresa_id").change(function(){
        $("#bodega_id").empty();
        $('#bodega_id').select2('destroy');
        $('#bodega_id').select2().val(null).trigger('change');
        
        var empresa_id = $(this).val();
        var tmp_bodega_id = $("#tmp_bodega_id").val();
        var selected='';
        $.ajax({
            type: "POST",
            url: "/ajax/selSucursalBsale",
            data: { 
                'empresa_id': empresa_id 
            },
            success: function(data){
                // Parse the returned json data
                var opts = $.parseJSON(data);
                // Use jQuery's each to iterate over the opts value
                $('#bodega_id').append('<option value="">- Seleccione Bodega -</option>');
                $.each(opts, function(i, d) {
                    selected = tmp_bodega_id == d.id ? 'selected="selected"' : '';
                    // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                    $('#bodega_id').append('<option value="' + d.id + '" '+selected+'>' + d.nombre + '</option>');
                    if(selected!=''){
                        $('#bodega_id').val(tmp_bodega_id).trigger('change');
                    }
                });
                $('#bodega_id').val("").trigger('change');
            }
        });
    });
    $('#empresa_id').trigger("change");
    */
})
</script>