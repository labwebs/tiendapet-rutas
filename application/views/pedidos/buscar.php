<div class="contentpanel">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo form_open("pedidos/buscar", array("class" => "form-inline form-horizontal")); ?>

                    <div class="form-group">
                        <label class="sr-only" for="query">Búsqueda</label>
                        <input type="text" name="query" class="form-control" id="query"
                               placeholder="Búsqueda Global o por Número de Pedido" value="<?php echo $query; ?>" size="35">
                    </div><!-- form-group -->

                    <div class="form-group">
                        <label class="sr-only" for="desde">Desde</label>
                        <input type="text" name="desde" class="form-control datepicker" id="desde" placeholder="Desde"
                               value="<?php echo $desde; ?>">
                    </div><!-- form-group -->

                    <div class="form-group">
                        <label class="sr-only" for="desde">Hasta</label>
                        <input type="text" name="hasta" class="form-control datepicker" id="hasta" placeholder="Hasta"
                               value="<?php echo $hasta; ?>">
                    </div><!-- form-group -->

                    <div class="form-group">
                        <label class="sr-only" for="desde">Chofer</label>
                        <select name="chofer" class="select" placeholder="Seleccione chofer">
                            <option></option>
                            <?php foreach ($drivers as $d) { ?>
                                <option value="<?php echo $d->id; ?>" <?php echo $d->id == $chofer ? 'selected' : '' ?> ><?php echo $d->name; ?></option>
                            <?php } ?>
                        </select>
                    </div><!-- form-group -->

                    <div class="form-group">
                        <label class="sr-only" for="monto">Monto</label>
                        <input type="text" name="monto" class="form-control" id="monto" placeholder="Monto"
                               value="<?php echo $monto; ?>">
                    </div><!-- form-group -->

                    <button type="submit" class="btn btn-primary mr5">Buscar</button>

                    </form>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table data-table table-striped table-responsive no-footer">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Fecha</th>
                        <th>Cliente</th>

                        <th>Chofer</th>
                        <th>Monto</th>
                        <th>Estado</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($pedidos as $p) { ?>
                        <tr>
                            <td><?php echo @$p->id; ?>
                            <td><?php echo @$p->order_delivery_date ?></td>
                            <td><?php echo @$p->customer->name ?></td>

                            <td><?php echo @$p->driver->name ?></td>
                            <td><?php echo @$p->display_price ?></td>
                            <td><?php echo @$p->status ?></td>
                            <td>
                                <?php if (!$p->order_delivered) { ?>
                                    <div class="btn-group">
                                        <a href="<?php echo base_url("pedidos/editar/{$p->id}"); ?>"
                                           class="btn btn-xs btn-primary"><i
                                                    class="fa fa-fw fa-pencil"></i> <?php echo $p->status == "Pendiente de confirmación" ? "Confirmar" : "Modificar"; ?>
                                        </a>
                                        <button type="button" class="btn btn-xs btn-primary dropdown-toggle"
                                                data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu pull-right" role="menu">
                                            <li class="dropdown-menu-danger"><a
                                                        href="<?php echo base_url("pedidos/eliminar/{$p->id}"); ?>">Eliminar</a>
                                            </li>
                                        </ul>
                                    </div>
                                <?php }else{ ?>
                                    <div class="btn-group">
                                        <a href="<?php echo base_url("pedidos/editar/{$p->id}"); ?>"
                                           class="btn btn-xs btn-success"><i class="fa fa-fw fa-check"></i> Ver detalle</a>
                                        <button type="button" class="btn btn-xs btn-success dropdown-toggle"
                                                data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu pull-right" role="menu">
                                            <li class="dropdown-menu-danger"><a
                                                        href="<?php echo base_url("pedidos/eliminar/{$p->id}"); ?>">Eliminar</a>
                                            </li>
                                        </ul>
                                    </div>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <! .table-responsive >
        </div>
    </div><!-- contentpanel -->