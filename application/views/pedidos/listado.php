<div class="contentpanel">

    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table data-table table-striped table-responsive no-footer">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Fecha</th>
                        <th>Cliente</th>
                        <th>Dirección</th>
                        <th>Chofer</th>
                        <th>Monto</th>
                        <th>Estado</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($pedidos as $p){ ?>
                        <tr>
                            <td><?php echo @$p->id; ?>
                            <td><?php echo @$p->order_delivery_date ?></td>
                            <td><?php echo @$p->customer->name ?></td>
                            <td><?php echo $p->address->direccion ?>, <?php echo $p->address->comuna ?></td>

                            <td><?php echo @$p->driver->name ?></td>
                            <td nowrap="nowrap"><?php echo @$p->display_price ?></td>
                            <td><?php echo @$p->status ?></td>
                            <td nowrap="nowrap">
                                <?php if (! $p->order_delivered ) { ?>
                                    <div class="form-inline -btn-group text-center">
                                        <a href="<?php echo base_url("pedidos/editar/{$p->id}"); ?>" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-pencil"></i> <?php echo $p->status == "Pendiente de confirmación" ? "Confirmar" : "Modificar"; ?></a>

                                        <a href="<?php echo base_url("pedidos/eliminar/{$p->id}");?>" class="btn btn-xs btn-danger" onclick="return confirm('Estás seguro que desea eliminar ó anular el pedido?');">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </a>
                                        <?php
                                        /*
                                        <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">
                                          <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu pull-right" role="menu">
                                          <li class="dropdown-menu-danger"><a href="<?= base_url("pedidos/eliminar/{$p->id}"); ?>">Eliminar</a></li>
                                        </ul>
                                        */
                                        ?>
                                    </div>
                                <?php }else{ ?>
                                    <div class="form-inline -btn-group text-center">
                                        <a href="<?php echo base_url("pedidos/editar/{$p->id}"); ?>" class="btn btn-xs btn-success"><i class="fa fa-fw fa-check"></i> Ver detalle</a>
                                        <a href="<?php echo base_url("pedidos/eliminar/{$p->id}");?>" class="btn btn-xs btn-danger" onclick="return confirm('Estás seguro que desea eliminar ó anular el pedido?');">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </a>
                                        <?php
                                        /*
                                        <button type="button" class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu pull-right" role="menu">
                                            <li class="dropdown-menu-danger"><a href="<?= base_url("pedidos/eliminar/{$p->id}"); ?>">Eliminar</a></li>
                                        </ul>
                                        */
                                        ?>
                                    </div>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>      </div><! .table-responsive >
        </div>
    </div><!-- contentpanel -->