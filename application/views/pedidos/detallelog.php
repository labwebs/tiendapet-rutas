
<div class="contentpanel">
    <div class="row">
        <?php
        $attributes = array('id' => 'myform-pedido');
        echo form_open('pedidos/actualizar', $attributes);
        ?>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                        <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                    </div>
                    <h2 class="panel-title">Información Log</h2>
                </div>

                <div class="panel-body nopadding">
                    <div class="form-horizontal form-bordered">
                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Nro.Pedido</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $log->order_id;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Acci&oacute;n</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $log->action;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Descripci&oacute;n</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $log->description;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Referer</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $log->http_referer;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">User Agent</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $log->http_user_agent;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Par&aacute;metros</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="order_comment" rows="15" disabled><?php echo $log->data;?></textarea>
                            </div>
                        </fieldset>

                    </div>
                </div><! .panel-body >
            </div><! .panel-default >
        </div><! .col-md-6 >

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                        <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                    </div>
                    <h2 class="panel-title">Información de despacho</h2>
                </div>

                <div class="panel-body nopadding">
                    <div class="form-horizontal form-bordered">
                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Cliente</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->customer->name ?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->customer->email ?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Forma de pago</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" disabled value="<?php echo @$pedido->payment->name ?: 'Sin información' ?>" />
                            </div>

                            <label class="col-sm-3 control-label">Estado</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->order_paid ? 'Pagado' : 'Pendiente de pago' ?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Teléfono</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->customer->phone ?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Dirección</label>
                            <div class="col-sm-9">
                                <?php
                                if($pedido->address->comuna_id == 493){
                                    $direccion = isset($locales[$pedido->address->local_id]) && ! empty($locales[$pedido->address->local_id]) ? $pedido->address->direccion.': '.$locales[$pedido->address->local_id]->nombre : '';
                                }else{
                                    $direccion =$pedido->address->direccion;
                                }
                                ?>
                                <input type="text" class="form-control" disabled value="<?php echo $direccion ?>" />
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Comuna</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->address->comuna;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Fecha <?php //echo $pedido->order_delivery_date;?></label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->order_delivery_date != "NULL" && $pedido->order_delivery_date != "" ? date('d/m/Y', strtotime($pedido->order_delivery_date)) : '';?>" />
                            </div>
                            <?php
                            /*
                            <label class="col-sm-3 control-label">Hora</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" disabled value="<?= $pedido->order_delivery_time ?>" />
                            </div>
                            */
                            ?>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Comentario</label>
                            <div class="col-sm-9">
                                <?php
                                $comment = "";
                                if( ! empty( $pedido->order_comment ) ){
                                    $comment = $pedido->order_comment;
                                }else{
                                    if( $pedido->paym_id == 1 ){ // Efectivo o Cheque
                                        $comment = "Cliente debe cancelar con efectivo o cheque // ";
                                    }else if( $pedido->paym_id == 2 ){
                                        $comment = "Entregar solo una vez confirmación previa vía transferencia // ";
                                    }
                                }
                                ?>
                                <textarea class="form-control" name="order_comment" <?php echo $pedido->order_delivered ? 'disabled' : ''; ?> ><?php echo $comment;?></textarea>
                            </div>
                        </fieldset>

                    </div>
                </div><! .panel-body >
            </div><! .panel-default >
        </div><! .col-md-6 >

    </div>


</div><!-- contentpanel -->

<style>
.has-error .select2-container{}
    border-color: #a94442 !important;
    display: block;
}
.form-inline .select2-container {
    width: 100%;
}
</style>