<div class="contentpanel">

  	<div class="row">
    	<div class="col-md-12">
      		<div class="panel panel-default">
        		<div class="panel-body">
          			<?php echo form_open("pedidos/cerrar", array("class" => "form-horizontal")); ?>
					  	<div class="form-filter">
							<div class="col-md-4">
								<div class="form-group">
									<label class="sr-only" for="query">B&uacute;squeda</label>
									<input type="text" name="query" class="form-control" id="query" placeholder="Búsqueda Global o por N&uacute;mero de Pedido" value="<?php echo $query; ?>" size="50">
								</div>
								<!-- form-group -->
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label class="sr-only" for="desde">Desde</label>
									<input type="text" name="desde" class="form-control datepicker" id="desde" placeholder="Desde" value="<?php echo $desde; ?>">
								</div>
								<!-- form-group -->
							</div>
							<div class="col-md-2">
								<div class="form-group">
								<label class="sr-only" for="desde">Hasta</label>
								<input type="text" name="hasta" class="form-control datepicker" id="hasta" placeholder="Hasta" value="<?php echo $hasta; ?>">
								</div>
								<!-- form-group -->
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<select name="driver_id" class="select2 form-control-">
										<option>Chofer</option>
									<?php foreach($drivers_selects as $select){ ?>
										<?php $selected = $select->id == $driver_id ? 'selected' : ''; ?> 
										<option value="<?php echo $select->id;?>" <?php echo $selected;?>><?php echo $select->admin_name; ?></option>
									<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-md-1">
								<button type="submit" name="accion" value="search_cerrar" class="btn btn-primary mr5">Buscar</button>
							</div>
						</div>
            		</form>

					
					<div class="col-md-12">
						<div class="row-">
							<div class="col-md-2-">
								<div class="form-group  text-center">
									<div class="form-check form-check-inline">
										<input class="form-check-input checkAll" type="checkbox" name="checkall" id="checkall">
										<label class="form-check-label" for="checkall"> Check Todos</label>
									</div>
								</div>
							</div>
							<div class="col-md-2-">
								<div class="form-group text-center">
									<button class="btn-group-ok" onclick="$('#myform-cerrar').submit();">
										<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> 
										<span class=""> Cerrar Pedidos</span>
									</button>
								</div>
							</div>
						</div>
					</div>
				
        		</div>
      		</div>

      		<div id="list_action_ajax" class="hide">pedidos/cerrar_listajax</div>
									
			<form id="myform-cerrar" name="myform-cerrar" class="form-move form-horizontal" action="<?php echo base_url('pedidos/cerrarOrders');?>" method="post">
			<div class="table-responsive">

				<table style="width: 100%;" class="table table-list-ajax table-bordered table-hover ">
				<?php
				if (!empty($thead)) {
				$htm = '
				<thead>
				';
				foreach ($thead as $key => $t) {
					$class = '';
					if ($key == 'cantidad_productos' || $key == 'estado') {
						$class = 'class="no-sort "';
					}
					$htm .= '
					<th '. $class . '>'. $t .'</th>
					';
				}
				$htm .= '
				
				</thead>
				';
				echo $htm;
				}
				?>
				</table>
      		</div>
			</form>
    	</div>
    </div>
</div>
<!-- contentpanel -->

<script>
jQuery(".checkAll").each(function(){
	var thisCheckAll = $(this);
	thisCheckAll.change(function(){
		if ( this.checked ) {
			$('.check-child').attr('checked','checked');
		} else {
			$('.check-child').removeAttr('checked');
		}       
	});
});
</script>