<?php
//$search = $this->session->userdata('search');
//$fecha = isset($search['select_date']) && $search['select_date'] ? $search['select_date'] : date('Y-m-d');
$fecha = date('Y-md-d');
?>
<head>
    <link href="<?php echo base_url("assets/css/bootstrap.min.css"); ?>" rel="stylesheet" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<?php echo base_url("assets/css/print.css"); ?>" rel="stylesheet" />
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<div class="container page">
    <div class="page-header">
        <h1>Pedidos por Confirmar <small class="text-right"><?php echo strftime("%d de %B de %Y", strtotime($fecha)); ?></small></h1>
    </div>
    <table class="table tabla__imprimir">
        <thead>
        <tr>
            <th>Cliente</th>
            <th>Direcci&oacute;n</th>
            <th style="width:5%">#</th>
            <th>Productos</th>
            <th>Total</th>
        </tr>
        </thead>

        <tbody>
        <?php $i = 1;?>
        <?php foreach ($orders_pending as $o) {
    ?>
    <?php
    $pago_webpay = 0;
    if ($o->paym_id == 4 || $o->paym_id == 5 || $o->paym_id == 6 || $o->paym_id == 7) {
        $pago_webpay = 1;
    }
    $pago_gifcard = 0;
    if ($o->paym_id == 101) {
        $pago_gifcard = 1;
    }

    /*
    if( $o->paym_id == 4 ) {
    //Chequear que fue pagado
    $webpay = $this->db
    ->where('Tbk_respuesta', 0)
    ->where('Tbk_orden_compra', $o->id)
    ->get('webpay')
    ->row();
    if (isset($webpay->Tbk_orden_compra) && $webpay->Tbk_orden_compra == $o->id && $webpay->Tbk_respuesta != '') {
    $pago_webpay = 1;
    }
    }else if( $o->paym_id == 6 || $o->paym_id == 5 ){
    //Chequear que fue pagado con webpay oneclick
    $oneclick = $this->db
    ->where('responseCode', '0')
    ->where('buyOrder', $o->id)
    ->get('tbk_oneclick_authorizes')
    ->row();
    if( isset($oneclick->id) && $oneclick->buyOrder == $o->id && $oneclick->responseCode != ''){
    $pago_webpay = 1;
    }
    }else if( $o->paym_id == 7 ){
    //Chequear que fue pagado para punto pago
    $pp = $this->db
    ->where('respuesta', '00')
    ->where('order_id', $o->id)
    ->get('puntopagos')
    ->row();
    if( isset($pp->id) && $pp->order_id == $o->id && $pp->respuesta != ''){
    $pago_webpay = 1;
    }
    }
     */
    /*
    if( $o->paym_id == 4 ) {
    //Chequear que fue pagado
    $webpay = $this->db
    ->where('Tbk_respuesta', 0)
    ->where('Tbk_orden_compra', $o->id)
    ->get('webpay')
    ->row();
    if (isset($webpay->Tbk_orden_compra) && $webpay->Tbk_orden_compra == $o->id) {
    $pago_webpay = 1;
    }
    }
     */?>
            <tr>
                <td width="20%">
                    <?php if (isset($o->bsale_nro_boleta) && $o->bsale_nro_boleta) {
        ?><b>Boleta </b><?php echo $o->bsale_nro_boleta; ?><?php
}?>
                    <address>
                        <strong><?php echo $o->customer->name; ?></strong><br />
                        Tel: <?php echo $o->customer->phone; ?><br />
                        <?php /*Mail: <?= $o->customer->email; ?><br />*/?>
                    </address>
                </td>
                <td width="20%">
                    <address>
                        <?php if (isset($o->address->local_id) && $o->address->local_id && $o->address->comuna_id == 493) {
        ?>
                            <strong><?php echo $o->address->direccion; ?>, <?php echo isset($locales[$o->address->local_id]->nombre) ? $locales[$o->address->local_id]->nombre : $o->address->comuna; ?></strong><br />
                        <?php
} else {
        ?>
                            <strong><?php echo $o->address->direccion; ?>, <?php echo $o->address->comuna; ?></strong><br />
                        <?php
}?>

                        <?php /*<p><i><?= $o->address->comentarios; ?></i></p>*/?>
                        <?php
$comment = "";
    if (!empty($o->order_comment)) {
        $comment = $o->order_comment;
    } else {
        if ($o->paym_id == 1) { // Efectivo o Cheque
            $comment = "Cliente debe cancelar con efectivo o cheque // ";
        } elseif ($o->paym_id == 2) {
            $comment = "Entregar solo una vez confirmación previa vía transferencia // ";
        }
    }
    if (!empty($o->comment)) {
        $comment = $comment . '<br/>' . $o->comment;
    }?>
                        <p><i><?php echo $comment; ?></i></p>
                        <?php if (isset($o->despachoatupinta_bloque_id) && $o->despachoatupinta_bloque_id) {
        ?>
                            <?php $text = $this->orders->getMsjeDespachoatuPinta($o->id);?>
                            <p><i><?php echo $text; ?></i></p>
                        <?php
}?>
                    </address>
                </td>
                <td width="10%"><?php echo $i; ?></td>
                <td width="30%">
                    <ul class="item__lista--imprimir">
                        <?php foreach ($o->products as $p) {
        ?>
                            <li><?php echo $p->qty; ?>&times; <?php echo $p->description->brand; ?> <?php echo $p->description->name; ?> <span class="item__lista--descripcion"><?php echo $p->description->size; ?></span></li>
                        <?php
}?>
                    </ul>
                </td>
                <td width="20%">
                    <?php echo $o->display_price; ?>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="3">
                    <ul class="formapago__lista">
                        <li><i class=""></i>Efectivo</li>
                        <li><i class=""></i>Cheque</li>
                        <li><i class=""></i>Transferencia</li>
                        <li><i class="<?php echo $pago_webpay ? 'fa fa-times' : ''; ?>"></i>Webpay Plus</li>
                        <li><i class="<?php echo $pago_gifcard ? 'fa fa-times' : ''; ?>"></i>Gif Card</li>
                    </ul>
                    <?php
/*echo "<ul class='formapago__lista'>";
    if( ! empty($payments) ){
    foreach ($payments as $indice => $p ){
    if( $p->paym_inactivo == 1 ) continue;
    echo '<li><i class="'.( $pago_webpay && $p->id == 4 ? 'fa fa-times' : '' ).'"></i>'.$p->paym_name.'</li>';
    }
    }
    echo "</ul>";
     */
    ?>
                </td>
                <td class="forma__pago--box">
                    <i></i>
                    Entregado
                </td>
            </tr>
        <?php $i++;?>
        <?php
}?>
        </tbody>

    </table>
</div>
</body>