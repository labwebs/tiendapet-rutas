<div class="contentpanel">

  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <?php echo form_open("pedidos/log", array("class" => "form-inline form-horizontal")); ?>

            <div class="form-group">
              <label class="sr-only" for="query">B&uacute;squeda</label>
              <input type="text" name="query" class="form-control" id="query" placeholder="Búsqueda Global o por N&uacute;mero de Pedido" value="<?php echo $query; ?>" size="50">
            </div>
            <!-- form-group -->

            <div class="form-group">
              <label class="sr-only" for="desde">Desde</label>
              <input type="text" name="desde" class="form-control datepicker" id="desde" placeholder="Desde" value="<?php echo $desde; ?>">
            </div>
            <!-- form-group -->

            <div class="form-group">
              <label class="sr-only" for="desde">Hasta</label>
              <input type="text" name="hasta" class="form-control datepicker" id="hasta" placeholder="Hasta" value="<?php echo $hasta; ?>">
            </div>
            <!-- form-group -->

            <button type="submit" name="accion" value="search_log" class="btn btn-primary mr5">Buscar</button>

            </form>
        </div>
      </div>

      <div id="list_action_ajax" class="hide">pedidos/log_listajax</div>

      <div class="table-responsive">

        <table style="width: 100%;" class="table table-list-ajax table-bordered table-hover ">
          <?php
if (!empty($thead)) {
    $htm = '
    <thead>
    ';
    foreach ($thead as $key => $t) {
        $class = '';
        if ($key == 'cantidad_productos' || $key == 'amount') {
            $class = 'class="no-sort "';
        }
        $htm .= '
        <th ' . $class . '>' . $t . '</th>
        ';
    }
    $htm .= '
    <th>Acciones</th>
    </thead>
    ';
    echo $htm;
}
?>
        </table>

        <!--
<table class="table data-table table-striped table-responsive no-footer ">
<thead>
<tr>
<th>#</th>
<th>Fecha</th>
<th>Nro.Pedido</th>
<th>Usuario</th>
<th>Acci&oacute;n</th>
<th>Descripci&oacute;n</th>
<th>Referer</th>
<th>User Agent</th>
<th>Opciones</th>
</tr>
</thead>
<tbody>
<?php /*foreach ($logs as $log) { ?>
<tr>
<td><?php echo @$log->id; ?>
<td nowrap=" "><?php echo isset($log->created_at) ? date('d-m-Y H:i:s', strtotime($log->created_at)) : ''; ?></td>
<td><?php echo @$log->order_id; ?></td>
<td><?php echo @$log->admin_name; ?></td>
<td><?php echo @$log->action; ?></td>
<td><?php echo @$log->description; ?></td>
<td><?php echo @$log->http_referer; ?></td>
<td><?php echo @$log->http_user_agent; ?></td>
<td>
<a href="<?php echo base_url( "pedidos/detallelog/{$log->id}"); ?>" class="btn btn-xs btn-success"><i class="fa fa-fw fa-check"></i> Ver detalle</a>
</td>
</tr>
<?php } */?>
</tbody>
</table>
-->
      </div>
      <! .table-responsive >
    </div>
  </div>
  <!-- contentpanel -->