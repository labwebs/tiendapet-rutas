
<div class="contentpanel">
    <div class="row">
        <?php
        $attributes = array('id' => 'myform-pedido');
        echo form_open('pedidos/actualizar', $attributes);
        ?>
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h2 class="panel-title"><?php echo $pedido->status == "Pendiente de confirmación" ? "Confirmar" : "Modificar"; ?> pedido #<?= $pedido->id ?></h2>
                </div>

                <div class="panel-body">
                    
                    <div class="form-inline- col-md-12">
                        <fieldset class="form-group col-md-4">
                            <?php /*<label class="sr-only">Chofer</label>*/?>
                            <select name="chofer" id="chofer">
                                <option value="">- Seleccione Chofer -</option>
                                <?php foreach ($choferes as $c){ ?>
                                    <?php if( $c->inactivo ) continue; ?>
                                    <option value="<?php echo $c->id; ?>" <?php echo $c->id == $pedido->driver_id ? 'selected' : '' ?>><?php echo $c->name; ?></option>
                                <?php } ?>
                            </select>
                        </fieldset>
                                    
                        <fieldset class="form-group col-md-4">
                            <?php /*<label class="sr-only">Fecha</label>*/?>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                <?php
                                $dia_masuno = (date('H') == 12 && date('i') >= 30) || date('H') > 12 ? date('Y-m-d', strtotime("+1 day")) : date('Y-m-d');
                                $pedido->order_delivery_date = ! empty($pedido->order_delivery_date) && $pedido->order_delivery_date != '0000-00-00' ? $pedido->order_delivery_date : $dia_masuno;
                                ?>
                                <input type="text" class="datepicker form-control" id="delivery_date" name="delivery_date" placeholder="Fecha de despacho" value="<?php echo set_value('delivery_date', $pedido->order_delivery_date); ?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group col-md-4">
                            <?php /*<label class="sr-only">Bodega</label>*/?>
                            <select name="sucursal" id="sucursal">
                                <option value="">- Seleccione Bodega -</option>
                                <?php foreach ($bodegas as $bodega_id => $nombre) { ?>
                                    <option value="<?php echo $bodega_id;?>" <?php echo $bodega_id == $pedido->bsale_sucursal_id ? 'selected' : '' ?>><?php echo $nombre;?></option>
                                <?php } ?>
                            </select>
                        </fieldset>
                        </div>
                        <div class="form-inline col-md-12">
                        <?php
                        /*
                        <fieldset class="form-group">
                            <label class="sr-only">Hora</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
                                <div class="bootstrap-timepicker">
                                    <input type="text" class="timepicker form-control" name="delivery_time" placeholder="Hora de entrega" value="<?= set_value('delivery_time', $pedido->order_delivery_time); ?>" />
                                </div>
                            </div>
                        </fieldset>
                        */
                        ?>
                        
                        <?php if (! $pedido->order_delivered) { ?>
                            <fieldset class="-form-group -pull-right text-center col-md-12">
                                <div class="row input-group-">
                                    <a href="<?php echo $this->input->server('HTTP_REFERER'); ?>" class="btn btn-default">Volver</a>

                                    <?php if( ! $pedido->bsale_nro_boleta ){ ?>
                                        <button type="submit" name="accion" class="btn btn-success" value="emitir" onclick="$('#action').val('emitir');"><i class="fa fa-file-text-o" aria-hidden="true"></i> <?php echo "Emitir Boleta"; //$pedido->status == "Pendiente de confirmación" ? "Emitir Boleta" : "Modificar"; ?></button>
                                        <button type="submit" name="accion" class="btn btn-warning" value="modificar" onclick="$('#action').val('modificar');"><i class="fa fa-file-text-o" aria-hidden="true"></i> <?php echo "Modificar"; ?></button>

                                    <?php }else{ ?>
                                        <a href="<?php echo $pedido->bsale_url_print_boleta;?>" class="btn btn-success" target="_blank"><i class="fa fa-file-text-o" aria-hidden="true"></i> <?php echo "Imprimir Boleta Nro. ".$pedido->bsale_nro_boleta; //$pedido->status == "Pendiente de confirmación" ? "Emitir Boleta" : "Modificar"; ?></a>
                                        <button type="submit" name="accion" class="btn btn-warning" value="modificar" onclick="$('#action').val('modificar')"><i class="fa fa-file-text-o" aria-hidden="true"></i> <?php echo "Modificar"; ?></button>
                                    <?php } ?>


                                    <?php /*<button type="submit" class="btn btn-success"><?= $pedido->status == "Pendiente de confirmación" ? "Confirmar" : "Modificar"; ?></button>*/ ?>
                                    <input type="hidden" id="action" value="" />
                                    <input type="hidden" name="order_id" value="<?php echo $pedido->id;?>" />
                                    <input type="hidden" name="bsale_empresas_id" value="<?php echo $empresa->bsale_empresas_id;?>" />
                                </div>
                            </fieldset>
                        <?php } ?>

                    </div><! .form-horizontal >


                </div><! .panel-body >
            </div>

        </div><! .col-md-6 >

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                        <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                    </div>
                    <h2 class="panel-title">Información de despacho  <?php if( $pedido->bsale_nro_boleta ){ echo " - Boleta Nro. ".$pedido->bsale_nro_boleta; }?></h2>
                </div>

                <div class="panel-body nopadding">
                    <div class="form-horizontal form-bordered">
                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Cliente</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->customer->name ?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->customer->email ?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Forma de pago</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" disabled value="<?php echo @$pedido->payment->name ?: 'Sin información' ?>" />
                            </div>

                            <label class="col-sm-3 control-label">Estado</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->order_paid ? 'Pagado' : 'Pendiente de pago' ?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Teléfono</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->customer->phone ?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Dirección</label>
                            <div class="col-sm-9">
                                <?php
                                //if($pedido->address->comuna_id == 493){
                                if( isset($pedido->address->local_id) && $pedido->address->local_id ){
                                    $direccion = isset($locales[$pedido->address->local_id]) && ! empty($locales[$pedido->address->local_id]) ? $pedido->address->direccion.': '.$locales[$pedido->address->local_id]->nombre : '';
                                }else{
                                    $direccion =$pedido->address->direccion;
                                }
                                ?>
                                <input type="text" class="form-control" disabled value="<?php echo $direccion ?>" />
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Comuna</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->address->comuna;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Fecha <?php //echo $pedido->order_delivery_date;?></label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->order_delivery_date != "NULL" && $pedido->order_delivery_date != "" ? date('d/m/Y', strtotime($pedido->order_delivery_date)) : '';?>" />
                            </div>
                            <?php
                            /*
                            <label class="col-sm-3 control-label">Hora</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" disabled value="<?= $pedido->order_delivery_time ?>" />
                            </div>
                            */
                            ?>
                        </fieldset>

                        <?php if( ! empty($despachoatupinta_texto) ){ ?>
                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Despacho a tu Pinta</label>
                            <div class="col-sm-9">
                                <p><?php echo $despachoatupinta_texto;?></p>
                            </div>
                        </fieldset>
                        <?php } ?>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Comentario</label>
                            <div class="col-sm-9">
                                <?php
                                $comment = "";
                                if( ! empty( $pedido->order_comment ) ){
                                    $comment = $pedido->order_comment;
                                }else{
                                    if( $pedido->paym_id == 1 ){ // Efectivo o Cheque
                                        $comment = "Cliente debe cancelar con efectivo o cheque // ";
                                    }else if( $pedido->paym_id == 2 ){
                                        $comment = "Entregar solo una vez confirmación previa vía transferencia // ";
                                    }
                                }
                                ?>
                                <textarea class="form-control" name="order_comment" <?php echo $pedido->order_delivered ? 'disabled' : ''; ?> ><?php echo $comment;?></textarea>
                            </div>
                        </fieldset>


                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Fecha Pedido</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo date('d/m/Y H:i', strtotime($pedido->order_created));?>" />
                            </div>
                        </fieldset>


                        <?php if (! $pedido->order_delivered) { ?>
                        <fieldset class="form-group">
                            <div class="col-md-12 text-center">
                                <a href="<?php echo base_url("pedidos/eliminar/{$pedido->id}");?>" class="btn btn-xs btn-danger confirmation">
                                    <span class="glyphicon glyphicon-trash"></span> Eliminar
                                </a>
                            </div>
                        </fieldset>
                        <?php } ?>

                    </div>
                </div><! .panel-body >
            </div><! .panel-default >
        </div><! .col-md-6 >
        <?php echo form_close(); ?>

        <?php if( $this->user->is_super_admin || $this->user->is_admin ){ ?>
        <div class="col-sm-12">
            <?php echo form_open("pedidos/metodoentrega", array("class" => "panel panel-default")); ?>
                <div class="panel-heading">
                    <h2 class="panel-title">
                        M&eacute;todo de Entrega
                        <?php if( $this->user->is_super_admin || $this->user->is_admin ){ ?>
                            <?php if (! $pedido->order_delivered) { ?>
                                <div class="btn-group pull-right btn-group-sm">
                                    <button class="btn btn-success btn-sm" ><i class="fa fa-save"></i> Guardar cambios</button>
                                    <input type="hidden" name="order_id" value="<?php echo $pedido->id; ?>" />
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </h2>
                </div>

                <div class="panel-body nopadding table-responsive">
                    <div class="form-horizontal form-bordered">
                        <fieldset class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-8 text-center">
                                <div class="mt-radio-inline">
                                    <label class="mt-radio mt-radio-outline">
                                        <?php $checked = $pedido->address->local_id == 0 ? '' : 'checked="checked"';?>
                                        <input id="retiro" type="radio" name="metodo_entrega" value="retiro_local" <?php echo $checked;?>> Retiro Local
                                        <span></span>
                                    </label>
                                    <label class="mt-radio mt-radio-outline">
                                        <?php $checked = $pedido->address->local_id == 0 ? 'checked="checked"' : '';?>
                                        <input id="despacho" type="radio" name="metodo_entrega" value="despacho_normal" <?php echo $checked;?>> Despacho Normal
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                        </fieldset>

                        <fieldset class="form-group despacho_normal">
                            <label class="col-sm-3 control-label" for="me_direccion">Dirección</label>
                            <div class="col-sm-9">
                                <input type="text" id="me_direccion" name="me_direccion" class="form-control" value="<?php echo isset($pedido->address->direccion) ? $pedido->address->direccion : '';?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group despacho_normal">
                            <label class="col-sm-3 control-label" for="me_region_id">Regi&oacute;n</label>
                            <div class="col-sm-9">
                                <select id="me_region_id" name="me_region_id">
                                    <?php if( ! empty($selects_region) ){ ?>
                                        <?php foreach($selects_region as $region_id => $region_name){ ?>
                                            <?php $selected = isset($selects_comuna[$pedido->address->comuna_id]) && $selects_comuna[$pedido->address->comuna_id]->region == $region_id ? 'selected="selected"' : '';?>
                                            <option value="<?php echo $region_id;?>" <?php echo $selected;?>><?php echo $region_name;?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </fieldset>

                        <fieldset class="form-group despacho_normal">
                            <label class="col-sm-3 control-label" for="me_comuna_id">Comuna</label>
                            <div class="col-sm-9">
                                <select id="me_comuna_id" name="me_comuna_id">
                                </select>
                                <input type="hidden" id="tmp_me_comuna_id" name="tmp_me_comuna_id" value="<?php echo $pedido->address->comuna_id;?>">
                            </div>
                        </fieldset>
                        
                        <fieldset class="form-group despacho_normal">
                            <label class="col-sm-3 control-label" for="me_comentarios">Referencia</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" id="me_comentarios" name="me_comentarios" <?php echo $pedido->order_delivered ? 'disabled' : ''; ?> ><?php echo $pedido->address->comentarios;?></textarea>
                            </div>
                        </fieldset>

                        <fieldset class="form-group retiro_local">
                            <label class="col-sm-3 control-label">Local</label>
                            <div class="col-sm-9">
                                <select id="me_local_id" name="me_local_id">
                                    <?php if( ! empty($selects_local) ){ ?>
                                        <?php foreach($selects_local as $local_id => $local_name){ ?>
                                            <?php $selected = isset($pedido->address->local_id) && $pedido->address->local_od == $local_id ? 'selected="selected"' : '';?>
                                            <option value="<?php echo $local_id;?>" <?php echo $selected;?>><?php echo $local_name;?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </form>
        </div>
        <?php } ?>
        
        <div class="col-sm-12">
            <?php echo form_open("pedidos/detalle", array("class" => "panel panel-default")); ?>
            <div class="panel-heading">
                <h2 class="panel-title">
                    Detalle de productos
                    <?php if( $this->user->is_super_admin || $this->user->is_admin ){ ?>
                        <?php if (! $pedido->order_delivered) { ?>
                            <div class="btn-group pull-right btn-group-sm">
                                <a href="<?php echo site_url("modals/agregar"); ?>" class="btn btn-default btn-sm" data-toggle="modal" data-backdrop="static" data-target="#modal"><i class="fa fa-plus"></i> Agregar</a>
                                <button class="btn btn-success btn-sm" ><i class="fa fa-save"></i> Guardar cambios</button>
                                <input type="hidden" name="order_id" value="<?php echo $pedido->id; ?>" />
                            </div>
                        <?php } ?>
                    <?php } ?>
                </h2>
            </div>

            <div class="panel-body nopadding table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Producto</th>
                        <th>Tamaño</th>
                        <th>Cantidad</th>
                        <th>Subtotal</th>
                    </tr>
                    </thead>
                    <tbody id="order-detail">
                    <?php foreach($pedido->products as $p){ ?>
                        <?php
                        $price = @$p->description->price_offer ? @$p->description->price_offer : @$p->description->price;
                        if( ! empty($discount_mail) ){
                            $discount = $price * ($discount_mail['percentage']/100);
                            $price = $price > $discount ? $price - $discount : $price;
                        }
                        ?>
                        <tr>
                            <td><?php echo @$p->description->brand ?> <?php echo @$p->description->name ? @$p->description->name : 'No existe el SKU : '.$p->sku_id;?> <?php if (! @$p->description->available) : ?> <span class="badge badge-warning"><span class="fa fa-exclamation"></span></span> <?php endif; ?></td>
                            <td><?php echo @$p->description->size ?></td>

                            <?php if (! $pedido->order_delivered) { ?>
                                <td>
                                    <input type="hidden" name="sku_id[]" value="<?php echo $p->sku_id; ?>" />
                                    <input type="number" name="qty[]" class="form-control input-sm" value="<?php echo $p->qty ?>" /></td>
                            <?php }else{ ?>
                                <td><input type="number" class="form-control input-sm" value="<?php echo $p->qty ?>" disabled/></td>
                            <?php } ?>

                            <td><?php echo $this->utils->numformat($price * $p->qty) ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>

                    <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <th>Envío:</th>
                        <th><?php echo $this->utils->numformat($pedido->order_shipping);?></th>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <th>Total:</th>
                        <th><?php echo $this->utils->numformat($pedido->order_total + $pedido->order_shipping);?></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            </form>
        </div>

    </div>


</div><!-- contentpanel -->

<script>
jQuery(function(){
    jQuery("#myform-pedido").validate({
        rules:{
            chofer  : {
                required: true
            },
            delivery_date : {
                required: true
            },
            sucursal : {
                required: function(element){
                    return $("#action").val() == 'emitir' ? true : false;
                }
            }
        },
        messages:{
            chofer  : '',
            delivery_date : '',
            sucursal : ''
        },
        submitHandler: function(form){
            form.submit();
        },
        highlight: function(element, errorClass) {
            if (element.type == "select-one") {
                var elem = $(element);
                if (elem.hasClass("select2-offscreen")) {
                    //$("#s2id_"+elem.attr("id")).addClass("errorClass");
                    $("#s2id_"+elem.attr("id")).addClass('has-error');
                } else {
                    jQuery(element).closest('.input-group').addClass('has-error');
                }
            }else{
                jQuery(element).closest('.input-group').addClass('has-error');
            }
        },
        unhighlight: function(element, errorClass) {
            if (element.type == "select-one") {
                var elem = $(element);
                if (elem.hasClass("select2-offscreen")) {
                    //$("#s2id_"+elem.attr("id")).addClass("errorClass");
                    $("#s2id_"+elem.attr("id")).removeClass('has-error');
                } else {
                    jQuery(element).closest('.input-group').removeClass('has-error');
                }
            }else{
                jQuery(element).closest('.input-group').removeClass('has-error');
            }
        }

    });

    $("input[name='metodo_entrega']").click(function(){
        if( $(this).is(":checked") ){
            if( $(this).val() == 'retiro_local' ){
                $(".retiro_local").show();
                $(".despacho_normal").hide();
            }else if( $(this).val() == 'despacho_normal'){
                $(".retiro_local").hide();
                $(".despacho_normal").show();
            }
        };
    });
    $("input[name='metodo_entrega']:checked").click();

    jQuery("#me_region_id").change(function(){
        $("#me_comuna_id").empty();
        $('#me_comuna_id').select2('destroy');
        $('#me_comuna_id').select2().val(null).trigger('change');
        
        var me_region_id = $(this).val();
        var tmp_me_comuna_id = $("#tmp_me_comuna_id").val();
        var selected='';
        $.ajax({
            type: "POST",
            url: "/ajax/selComunas",
            data: { 'me_region_id': me_region_id  },
            success: function(data){
                // Parse the returned json data
                var opts = $.parseJSON(data);
                // Use jQuery's each to iterate over the opts value
                //$('#me_comuna_id').append('<option value="">- Seleccione -</option>');
                $.each(opts, function(i, d) {
                    selected = tmp_me_comuna_id == d.id ? 'selected="selected"' : '';
                    // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                    $('#me_comuna_id').append('<option value="' + d.id + '" '+selected+'>' + d.nombre + '</option>');
                    if(selected!=''){
                        $('#me_comuna_id').val(tmp_me_comuna_id).trigger('change');
                    }
                });
                
                //$('.bs-select').select2('refresh');
            }
        });
        //ComponentsBootstrapSelect.init();
        //$('#comuna_id').selectpicker('refresh');
    });
    $('#me_region_id').trigger("change");
});
function printBoleta(){
    var printWindow = window.open("http://app2.bsale.cl/view/3311/7b8e14f621fb?sfd=99", "_blank");
    printWindow.print();
    printWindow.close();
}
</script>
<style>
.has-error .select2-container{
    border-color: #a94442 !important;
    display: block;
}
.form-inline .select2-container {
    width: 100%;
}
.despacho_normal, .retiro_local{
    display:none;
}
</style>