
<div class="contentpanel">
    <div class="row">
        <?php
        $attributes = array('id' => 'myform-pedido');
        echo form_open('pedidos/actualizar', $attributes);
        ?>
        
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                        <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                    </div>
                    <h2 class="panel-title">Información de Pedido  <?php if( $pedido->id ){ echo " #".$pedido->id; }?></h2>
                </div>

                <div class="panel-body nopadding">
                    <div class="form-horizontal form-bordered">
                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Cliente</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->customer->name ?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->customer->email ?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Monto</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->display_price; ?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Forma de pago</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" disabled value="<?php echo @$pedido->payment->name ?: 'Sin información' ?>" />
                            </div>

                            <label class="col-sm-3 control-label">Estado</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->order_paid ? 'Pagado' : 'Pendiente de pago' ?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Teléfono</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->customer->phone ?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Dirección</label>
                            <div class="col-sm-9">
                                <?php
                                if( isset($pedido->address->local_id) && $pedido->address->local_id ){
                                    $direccion = isset($locales[$pedido->address->local_id]) && ! empty($locales[$pedido->address->local_id]) ? $pedido->address->direccion.': '.$locales[$pedido->address->local_id]->nombre : '';
                                }else{
                                    $direccion =$pedido->address->direccion;
                                }
                                ?>
                                <input type="text" class="form-control" disabled value="<?php echo $direccion ?>" />
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Comuna</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->address->comuna;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">Fecha Pedido</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo date('d/m/Y H:i', strtotime($pedido->order_created));?>" />
                            </div>
                        </fieldset>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                        <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                    </div>
                    <h2 class="panel-title">Detalle Transacci&oacute;n</h2>
                </div>
                        
                <?php if( $pedido->paym_id == 4 ){ ?>
                <div class="panel-body nopadding">
                    <div class="form-horizontal form-bordered">
                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">TBK TIPO TRANSACCIÓN</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_webpay->Tbk_tipo_transaccion; ?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">TBK RESPUESTA</label>
                            <div class="col-sm-9">
                                <?php 
                                $value = ''; 
                                if( isset($pedido->detalle_webpay->id) && $pedido->detalle_webpay->id ){
                                    $codres = $pedido->detalle_webpay->Tbk_respuesta;

                                    if ($codres == 0) {
                                        $value = 'Transacción aprobada';
                                    } elseif ($codres == -1) {
                                        $value = 'Rechazo de transacción';
                                    } elseif ($codres == -2) {
                                        $value = 'Transacción debe reintentarse';
                                    } elseif ($codres == -3) {
                                        $value = 'Error en transacción';
                                    } elseif ($codres == -4) {
                                        $value = 'Rechazo de transacción';
                                    } elseif ($codres == -5) {
                                        $value = 'Rechazo por error de tasa';
                                    } elseif ($codres == -6) {
                                        $value = 'Excede cupo máximo mensual';
                                    } elseif ($codres == -7) {
                                        $value = 'Excede límite diario por transacción';
                                    } elseif ($codres == -8) {
                                        $value = 'Rubro no autorizado';
                                    } else {
                                        $value = $pedido->detalle_webpay->Tbk_respuesta;
                                    }
                                }else{
                                    $value = "Sin respuesta";
                                }
                                ?>
                                <input type="text" class="form-control" disabled value="<?php echo $value;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">TBK CÓDIGO AUTORIZACIÓN</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_webpay->Tbk_codigo_autorizacion;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">TBK MONTO</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_webpay->Tbk_monto;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">TBK ÚLTIMOS 4 DÍGITOS TARJETA</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_webpay->Tbk_numero_final_tarjeta;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">TBK FECHA EXPIRACIÓN</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_webpay->Tbk_fecha_expiracion;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">TBK FECHA TRANSACCIÓN</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_webpay->Tbk_fecha_transaccion;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">TBK HORA TRANSACCIÓN</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_webpay->Tbk_hora_transaccion;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">TBK TIPO PAGO</label>
                            <div class="col-sm-9">
                                <?php 
                                $value = '';
                                $tipopago = $pedido->detalle_webpay->Tbk_tipo_pago;
                                if ($tipopago == 'VD') {
                                    $value = 'Venta Débito';
                                } elseif ($tipopago == 'VN') {
                                    $value = 'Venta Normal';
                                } elseif ($tipopago == 'VC') {
                                    $value = 'Venta en cuotas';
                                } elseif ($tipopago == 'SI') {
                                    $value = '3 cuotas sin interés';
                                } elseif ($tipopago == 'S2') {
                                    $value = '2 cuotas sin interés';
                                } elseif ($tipopago == 'NC') {
                                    $value = 'N Cuotas sin interés';
                                }
                                ?>
                                <input type="text" class="form-control" disabled value="<?php echo $value;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">TBK NÚMERO CUOTAS</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_webpay->Tbk_numero_cuotas;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">TBK MONTO CUOTA</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_webpay->Tbk_monto_cuota;?>" />
                            </div>
                        </fieldset>

                    </div>
                </div>
                <?php } ?>

                <?php if( $pedido->paym_id == 5 || $pedido->paym_id == 6 ){ ?>
                <div class="panel-body nopadding">
                    <div class="form-horizontal form-bordered">
                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">TBK USERNAME</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_oneclick->username; ?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">TBK MONTO</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_oneclick->amount;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">TBK RESPUESTA</label>
                            <div class="col-sm-9">
                                <?php 
                                $value = '';
                                if( isset($pedido->detalle_oneclick->id) && $pedido->detalle_oneclick->id ){
                                    $codres = $pedido->detalle_oneclick->responseCode;
                                    if ($codres == 0) {
                                        $value = 'Transacción aprobada';
                                    } elseif ($codres == -1 || $codres == -2 || $codres == -3 || $codres == -4 || $codres == -5 || $codres == -6 || $codres == -7 || $codres == -8) {
                                        $value = 'Rechazo de transacción';
                                    } elseif ($codres == -97) {
                                        $value = 'Límites Oneclick, máximo monto diario de pago excedido';
                                    } elseif ($codres == -98) {
                                        $value = 'Límites Oneclick, máxima cantidad de pagos diarios excedido';
                                    } else {
                                        $value = $pedido->detalle_oneclick->responseCode;
                                    }
                                }else{
                                    $value = "Sin respuesta";
                                }
                                ?>
                                <input type="text" class="form-control" disabled value="<?php echo $value;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">TBK CÓDIGO AUTORIZACIÓN</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_oneclick->authCode;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">TBK TIPO TARJETA CRÉDITO</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_oneclick->creditCardType;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">TBK ÚLTIMOS 4 DÍGITOS TARJETA</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_oneclick->last4CardDigits;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">TBK FECHA TRANSACCIÓN</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_oneclick->created_at;?>" />
                            </div>
                        </fieldset>

                    </div>
                </div>
                <?php } ?>

                <?php if( $pedido->paym_id == 7 ){ ?>
                <div class="panel-body nopadding">
                    <div class="form-horizontal form-bordered">

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">RESPUESTA TRANSACCIÓN</label>
                            <div class="col-sm-9">
                                <?php
                                $value = '';
                                if (isset($pedido->detalle_puntopago->id) && $pedido->detalle_puntopago->id) {
                                    $codres = $pedido->detalle_puntopago->respuesta;
                                    if ($codres == null) {
                                        $value = $codres;
                                    }else if ($codres == '00' || $codres == 00) {
                                        $value = 'Transacción aprobada';
                                    } elseif ($codres == 1) {
                                        $value = 'Transaccion Rechazada';
                                    } elseif ($codres == 2) {
                                        $value = 'Transaccion Anulada';
                                    } elseif ($codres == 6) {
                                        $value = 'Transaccion Incompleta';
                                    } elseif ($codres == 7) {
                                        $value = 'Error del financiador';
                                    } else {
                                        $value = $pedido->detalle_puntopago->respuesta;
                                    }
                                }else{
                                    $value = "Sin respuesta";
                                }
                                ?>
                                <input type="text" class="form-control" disabled value="<?php echo $value;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">MEDIO DE PAGO</label>
                            <div class="col-sm-9">
                                <?php
                                $value = '';
                                $codmediopago = $pedido->detalle_puntopago->medio_pago;
                                if ($codmediopago == 2) {
                                    $value = 'Tarjeta Presto';
                                } elseif ($codmediopago == 3) {
                                    $value = 'Webpay Transbank (tarjetas de crédito y débito)';
                                } elseif ($codmediopago == 4) {
                                    $value = 'Botón de Pago Banco de Chile';
                                } elseif ($codmediopago == 5) {
                                    $value = 'Botón de Pago BCI';
                                } elseif ($codmediopago == 6) {
                                    $value = 'Botón de Pago TBanc';
                                } elseif ($codmediopago == 7) {
                                    $value = 'Botón de Pago Banco Estado';
                                } elseif ($codmediopago == 10) {
                                    $value = 'Tarjeta Ripley';
                                } elseif ($codmediopago == 15) {
                                    $value = 'Paypal';
                                } elseif ($codmediopago == 16) {
                                    $value = 'Botón de Pago BBVA';
                                } else {
                                    $value = $pedido->detalle_puntopago->medio_pago;
                                }
                                ?>
                                <input type="text" class="form-control" disabled value="<?php echo $value;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">MEDIO DE PAGO DESCRIPCIÓN</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_puntopago->medio_pago_descripcion;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">CÓDIGO DE AUTORIZACIÓN</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_puntopago->tipo_pago;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">ERROR DESCRIPCIÓN</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_puntopago->error;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">MONTO</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_puntopago->monto;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">FECHA DE APROBACIÓN</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_puntopago->fecha_aprobacion;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">NÚMERO DE CUOTAS</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_puntopago->num_cuotas;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">NÚMERO DE OPERACIÓN</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_puntopago->numero_operacion;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">ÚLTIMOS 4 DÍGITOS TARJETA</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_puntopago->numero_tarjeta;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">PRIMER VENCIMIENTO</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_puntopago->primer_vencimiento;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">TIPO CUOTAS</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_puntopago->tipo_cuotas;?>" />
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="col-sm-3 control-label">FECHA TRANSACCIÓN</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled value="<?php echo $pedido->detalle_puntopago->created_at;?>" />
                            </div>
                        </fieldset>

                    </div>
                </div>
                <?php } ?>

                <fieldset class="form-group">
                    <div class="col-md-12 text-center">
                        <a href="<?php echo base_url("pedidos/transacciones");?>" class="btn btn-default">
                            <span class="glyphicon glyphicon-circle-arrow-left"></span> Volver
                        </a>
                    </div>
                </fieldset>

            </div>
        </div>
        
        <?php echo form_close(); ?>

    </div>
</div><!-- contentpanel -->

<style>
.has-error .select2-container{
    border-color: #a94442 !important;
    display: block;
}
.form-inline .select2-container {
    width: 100%;
}
</style>