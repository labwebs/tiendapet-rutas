
<div class="modal-header">
    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
    <h4 class="modal-title">Agregar producto</h4>
</div>
<div class="modal-body nopadding">
  
  <form class="form-bordered" method="post">
     
    <! NUEVO PEDIDO>
    <div class="form-group order-builder" id="order-create">
      <div class="col-xs-7 col-sm-8">
        <select id="slct-prod-id" name="sku_id" placeholder="Seleccione un producto...">
          <option></option>
          <?php foreach($products as $p){ ?>
          <option value="<?php echo $p->id ?>"><?php echo "{$p->brand} {$p->name} {$p->size} – " . $this->utils->numformat($p->price); ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="col-xs-3 col-sm-2">
        <input class="form-control text-center" type="number" name="qty" id="prod-qty" min="1" value="1" />
      </div>
      <div class="col-xs-2 col-sm-2">
        <a id="btn-append-product" class="btn btn-success">
          <span class="hidden-xs">Agregar</span>
          <span class="visible-xs"><i class="fa fa-plus"></i></span>
        </a>
      </div>
    </div>
    <! /NUEVO PEDIDO>
 
  </form>
  
</div>