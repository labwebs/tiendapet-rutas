                    <div class="modal-body nopadding clearfix">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button" style="position: absolute; right: 10px; top: 5px; opacity: .5;">×</button>
                        <img class="img-responsive" style="border-top-left-radius: 2px; border-top-right-radius: 2px;" src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo $order->address->direccion; ?>,<?php echo $comuna; ?>&zoom=15&size=320x180&scale=2&maptype=roadmap&sensor=false&markers=<?php echo $order->address->direccion; ?>,<?php echo $comuna; ?>" />

                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-8">
                                    <h5><?php echo $order->customer->name; ?></h5>
                                    <a href="tel:<?php echo $order->customer->phone; ?>"><i class="fa fa-phone"></i> <?php echo $order->customer->phone ?></a>
                                </div>
                                <div class="col-xs-4">
                                    <span class="small pull-right text-muted" style="margin-top: 10px;"><i class="fa fa-clock-o"></i> <?php echo strftime('%d %h', strtotime($order->order_delivery_date)); ?>, <?php echo date('H:i', strtotime($order->order_delivery_time)); ?></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <p>
                                        <i class="fa fa-map-marker"></i> <?php echo $order->address->direccion; ?> <?php echo $comuna ? ", $comuna" : "" ; ?>
                                        <?php if ($comm = $order->address->comentarios){ ?>
                                            <br/><i class="fa fa-info-circle"></i> <?php echo $comm; ?>
                                        <?php } ?>
                                    </p>
                                </div>
                            </div>

                            <?php if($comentario = $order->order_comment) { ?>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h5>Comentarios</h5>
                                        <p><<?php echo $comentario; ?></p>
                                    </div>
                                </div>
                            <?php } ?>

                            <table id="quickorder-products-review" class="table table-stripped mb30">
                                <thead>
                                <tr>
                                    <th width="40%">Producto</th>
                                    <th>Cantidad</th>
                                    <th class="hidden-sm hidden-xs">Unitario</th>
                                    <th>Subtotal</th>
                                </thead>
                                <tbody>
                                <?php foreach($order->products as $p){ ?>
                                <?php
                                    $price = $p->description->price_offer ? $p->description->price_offer : $p->description->price;
                                    if( ! empty($discount_mail) ){
                                        $discount = $price * ($discount_mail['percentage']/100);
                                        $price = $price > $discount ? $price - $discount : $price;
                                    }
                                    ?>
                                    <tr>
                                        <td><?php echo $p->description->brand ?> <?php echo $p->description->name ?> <?php echo $p->description->size ?></td>
                                        <td><?php echo $p->qty ?></td>
                                        <td class="hidden-sm hidden-xs">$ <?php echo number_format( $price, 0, ',','.') ?></td>
                                        <td>$ <?php echo number_format( $price * $p->qty, 0, ',','.') ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th width="1%">Despacho:</th>
                                    <th width="1%" class="final-total"></th>
                                    <th width="1%" class="hidden-sm hidden-xs"></th>
                                    <th width="1%" class="final-total">$ <?php echo number_format( $order->order_shipping, 0,',','.') ?></th>
                                </tr>
                                <tr>
                                    <th width="1%">Total:</th>
                                    <th width="1%" class="final-total"></th>
                                    <th width="1%" class="hidden-sm hidden-xs"></th>
                                    <th width="1%" class="final-total"><?php echo $order->display_price?></th>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>

                    <?php if (! $order->order_delivered){ ?>

                        <?php echo form_open("rutero/confirmar", array("class" => "modal-footer form-horizontal")); ?>
                        <div class="row">
                            <?php
                            /*
                            <div class="col-xs-6 hide">
                                <select name="payment" class="form-control input-sm">
                                    <option value="0">Pago pendiente</option>
                                    <? foreach($pagos as $p) : ?>
                                        <option value="<?= $p->id; ?>"><?= $p->name; ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>
                            */
                            ?>

                            <input type="hidden" name="order_id" value="<?php echo $order->id; ?>" />

                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-success pull-right btn-sm">Confirmar entrega</button>
                            </div>

                        </div>
                        </form>

                    <?php }else{ ?>

                        <div class="modal-footer text-center">
                            <div class="text-center">Entregado el <?php echo strftime("%A %d de %B a las %H:%M", strtotime($order->order_delivered) ); ?></div>
                        </div>

                    <?php } ?>