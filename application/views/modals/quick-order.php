<script src="<?php echo base_url('assets/js/bootstrap-wizard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap3-typeahead.min.js'); ?>"></script>

<div class="modal-header">
    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
    <h4 class="modal-title">Nuevo pedido</h4>
</div>
<div class="modal-body nopadding">

    <form id="frm-pedidos" class="panel-wizard form-bordered" action="<?php echo site_url("pedidos/crear"); ?>"
          method="post">

        <ul class="nav nav-justified nav-wizard nav-pills">
            <li class="active">
                <a href="#tab-customer" data-toggle="tab"><i class="fa fa-user"></i> <span>Cliente</span></a>
            </li>
            <li>
                <a href="#tab-products" data-toggle="tab"><i class="fa fa-dropbox"></i> <span>Productos</span></a>
            </li>
            <li>
                <a href="#tab-shipping" data-toggle="tab"><i class="fa fa-truck"></i> <span>Despacho</span></a>
            </li>
            <li>
                <a href="#tab-confirm" data-toggle="tab"><i class="fa fa-check"></i> <span>Confirmación</span></a>
            </li>
        </ul>

        <div class="tab-content no-border nopadding">
            <div id="tab-customer" class="tab-pane active">
                <fieldset class="form-group vertical-spacing input-group">
                    <label class="control-label col-sm-3">
                        Cliente
                    </label>
                    <div class="col-sm-9">
                        <?php
                        /*
                        <input name="customer_id" id="slct-customer-id" placeholder="Seleccione cliente..." />
                        */
                        ?>

                        <input name="customer_id2" id="slct-customer-id2" type="text" class="form-control"
                               data-provide="typeahead" autocomplete="off" placeholder="Seleccione cliente..."/>
                        <input name="customer_id" id="customer_id" type="hidden" value=""/>

                    </div>
                    <div class="col-sm-9 col-sm-offset-3">
                        <input type="checkbox" id="chbox-new-customer" name="create_customer" value="create"/>
                        <label for="chbox-new-customer">Crear nuevo cliente</label>
                    </div>
                </fieldset>

                <fieldset id="existing-customer" class="input-group">
                    <div class="form-group vertical-spacing">
                        <label class="control-label col-sm-3">Dirección</label>
                        <div class="col-sm-9">
                            <input name="address_id" id="slct-address-id" placeholder="Seleccione una dirección..."/>
                        </div>

                        <div class="col-sm-9 col-sm-offset-3">
                            <input type="checkbox" id="chbox-new-address" name="create_address" value="existing"/>
                            <label for="chbox-new-address">Crear nueva dirección</label>
                        </div>

                    </div>

                </fieldset>

                <fieldset id="new-customer">

                    <fieldset class="input-group">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Nombre</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="cust_name" placeholder="Nombre completo"/>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="input-group">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Correo</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" name="cust_email" placeholder="Correo"/>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="input-group">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Teléfono</label>
                            <div class="col-sm-9">
                                <input type="tel" class="form-control" name="cust_phone" placeholder="Teléfono"/>
                            </div>
                        </div>
                    </fieldset>

                </fieldset>

                <fieldset id="append-address" class="-input-group">

                    <i></i><!-- Mongolidad para evitar el first:child del form-group siguiente -->

                    <div class="form-group vertical-spacing">

                        <div class="col-sm-12 input-group">
                            <label class="control-label col-sm-3">Nueva dirección</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="direccion"
                                       placeholder="Dirección completa"/>
                            </div>
                        </div>


                        <div class="col-sm-4 col-sm-offset-3">
                            <select name="comuna" id="slct-comuna">
                                <?php foreach ($comunas as $c) { ?>
                                    <option value="<?php echo $c->id ?>"><?php echo $c->nombre; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-sm-4 col-sm-offset-1">
                            <input type="text" class="form-control" name="ciudad" placeholder="Ciudad"/>
                        </div>


                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3">Comentarios de despacho</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" rows="3" name="comentarios"></textarea>
                        </div>
                    </div>
                </fieldset>

            </div>
            <! /#tab-customer >

            <div id="tab-products" class="tab-pane">

                <div id="order-source">
                    <div class="col-xs-6">
                        <label class="form-group checkbox">
                            <input type="radio" id="radio-repeat_order" name="order_repeat" value="repeat"> Repetir
                            pedido
                        </label>
                    </div>
                    <div class="col-xs-6">
                        <label class="form-group checkbox">
                            <input type="radio" id="radio-create_order" name="order_repeat" value="create"> Agregar
                            productos
                        </label>
                    </div>

                </div>

                <! REPETIR PEDIDO >
                <div class="form-group order-builder" id="order-repeat">

                    <div class="col-xs-12">
                        <input type="text" id="slct-order-id" name="sku_id"
                               placeholder="Seleccione un pedido anterior..."/>
                    </div>

                </div>
                <! /REPETIR PEDIDO>

                <! NUEVO PEDIDO>
                <div class="form-group order-builder" id="order-create">
                    <div class="col-xs-7 col-sm-8">
                        <select id="slct-prod-id" name="sku_id" placeholder="Seleccione un producto...">
                            <option></option>
                            <?php foreach ($products as $p) { ?>
                                <option value="<?php echo $p->id ?>"><?php echo "{$p->brand} {$p->name} {$p->size} – " . $this->utils->numformat($p->price); ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-xs-3 col-sm-2">
                        <input class="form-control text-center" type="number" name="qty" id="prod-qty" min="1"
                               value="1"/>
                    </div>
                    <div class="col-xs-2 col-sm-2">
                        <button id="btn-add-product" class="btn btn-success">
                            <span class="hidden-xs">Agregar</span>
                            <span class="visible-xs"><i class="fa fa-plus"></i></span>
                        </button>
                    </div>
                </div>
                <! /NUEVO PEDIDO>

                <table id="quickorder-products" class="table table-stripped mb30">
                    <thead>
                    <tr>
                        <th width="1%">Producto</th>
                        <th width="1%">
                            <span class="hidden-xs">Cantidad</span>
                            <span class="visible-xs-inline">Q</span>

                        </th>
                        <th width="1%" class="hidden-sm hidden-xs">Unitario</th>
                        <th width="1%">Total</th>
                        <th width="1%"></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <thead>
                    <tr>
                        <th width="1%"></th>
                        <th width="1%"></th>
                        <th width="1%" class="hidden-sm hidden-xs"></th>
                        <th width="1%">Despacho ($):</th>
                        <th width="1%"><input type="number" min="0" name="shipping"
                                              class="form-control input-sm order-delivery-input" value="0"/></th>
                    </tr>
                    <tr>
                        <th width="1%"></th>
                        <th width="1%"></th>
                        <th width="1%" class="hidden-sm hidden-xs"></th>
                        <th width="1%">Subtotal:</th>
                        <th width="1%" class="final-subtotal"></th>
                    </tr>
                    <tr>
                        <th width="1%"></th>
                        <th width="1%"></th>
                        <th width="1%" class="hidden-sm hidden-xs"></th>
                        <th width="1%">Descuento ($):</th>
                        <th width="1%"><input type="number" min="0" name="discount"
                                              class="form-control input-sm order-discount-input" value="0"/></th>
                    </tr>
                    <tr>
                        <th width="1%"></th>
                        <th width="1%"></th>
                        <th width="1%" class="hidden-sm hidden-xs"></th>
                        <th width="1%">Total:</th>
                        <th width="1%" class="final-total"></th>
                    </tr>
                    </thead>
                </table>
            </div>
            <! /#tab-products >

            <div id="tab-shipping" class="tab-pane form-horizontal">
                <img id="map-preview"
                     src="http://maps.googleapis.com/maps/api/staticmap?center=Pedro+de+Valdivia+1215+PROVIDENCIA&zoom=15&size=1140x250&maptype=roadmap&sensor=false&markers=Pedro+de+Valdivia+1215+PROVIDENCIA"
                     class="img-responsive" width="100%"/>

                <! COMUNA >
                <div class="form-group">
                    <label class="control-label col-sm-2">Comuna</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-map-marker"></span></span>
                            <input type="text" class="form-control" name="comuna_despacho"
                                   placeholder="Comuna de despacho" readonly/>
                        </div>
                    </div>
                </div>
                <! /COMUNA >

                <! CHOFER >
                <div class="form-group">
                    <label class="control-label col-sm-2">Chofer</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-truck"></span></span>
                            <select name="driver_id" id="driver_id" placholder="Seleccione chofer...">
                                <option value="">- Seleccione chofer -</option>
                                <?php /*<option>Por confirmar</option>*/ ?>
                                <?php foreach ($drivers as $d) { ?>
                                    <?php if ($d->inactivo) continue; ?>
                                    <?php $selected = $d->id == 12 ? 'selected="selected"' : ''; ?>
                                    <option value="<?php echo $d->id; ?>" <?php echo $selected; ?>><?php echo $d->name; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <! /CHOFER >

                <! FECHA Y HORA>
                <div class="form-group">
                    <label class="control-label col-sm-2">Fecha</label>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            <input type="text" class="datepicker form-control" name="delivery_date"
                                   placeholder="Fecha"/>
                        </div>
                    </div>

                    <?php
                    /*
                    <label class="control-label col-sm-2">Hora</label>
                    <div class="col-sm-4">
                        <select name="delivery_time" placeholder="Rango de hora...">
                            <option>08:00 – 10:00</option>
                            <option>10:00 – 12:00</option>
                            <option>12:00 – 14:00</option>
                            <option>14:00 – 16:00</option>
                            <option>16:00 – 18:00</option>
                            <option>18:00 – 20:00</option>
                        </select>
                        <?
                        //<div class="bootstrap-timepicker">
                        //  <input type="text" class="timepicker form-control" name="delivery_time" placeholder="Hora" />
                        //</div>
                        ?>
                    </div>
                    */
                    ?>
                </div>
                <! /FECHA Y HORA>

            </div>
            <! /#tab-shipping >

            <div id="tab-confirm" class="tab-pane">

                <div class="tab-content">
                    <h5>Datos de cliente</h5>
                    <table class="table table-responsive table-bordered">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Correo</th>
                            <th>Teléfono</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="review-customer"></td>
                            <td class="review-email"></td>
                            <td class="review-phone"></td>
                        </tbody>
                    </table>

                    <h5>Datos de despacho</h5>
                    <table class="table table-responsive table-bordered">
                        <thead>
                        <tr>
                            <th>Dirección</th>
                            <th>Fecha</th>
                            <th>Hora</th>
                            <th>Chofer</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="review-comuna"></td>
                            <td class="review-fecha"></td>
                            <td class="review-hora"></td>
                            <td class="review-chofer"><?php //= $drivers[2]->name; ?></td>
                        </tr>
                        <tr>
                            <th colspan="4">Observaciones de despacho</th>
                        </tr>
                        <tr>
                            <td class="review-comment" colspan="4"></td>
                        </tr>
                        </tbody>
                    </table>

                    <h5>Productos</h5>
                    <table id="quickorder-products-review" class="table table-stripped mb30">
                        <thead>
                        <tr>
                            <th width="1%">Producto</th>
                            <th width="1%">
                                <span class="hidden-xs">Cantidad</span>
                                <span class="visible-xs-inline">Q</span>
                            </th>
                            <th width="1%" class="hidden-sm hidden-xs">Unitario</th>
                            <th width="1%">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <thead>
                        <tr>
                            <th width="1%">Despacho:</th>
                            <th width="1%" class="final-despacho"></th>
                            <th width="1%" class="hidden-sm hidden-xs"></th>
                            <th width="1%"></th>
                        </tr>
                        <tr>
                            <th width="1%">Subtotal:</th>
                            <th width="1%" class="final-subtotal"></th>
                            <th width="1%" class="hidden-sm hidden-xs"></th>
                            <th width="1%"></th>
                        </tr>
                        <tr>
                            <th width="1%">Descuento:</th>
                            <th width="1%" class="final-descuento"></th>
                            <th width="1%" class="hidden-sm hidden-xs"></th>
                            <th width="1%"></th>
                        </tr>
                        <tr>
                            <th width="1%">Total:</th>
                            <th width="1%" class="final-total"></th>
                            <th width="1%" class="hidden-sm hidden-xs"></th>
                            <th width="1%"></th>
                        </tr>
                        </thead>
                    </table>

                    <h5>Agregar observaciones</h5>
                    <textarea class="form-control" name="comment"></textarea>

                    <h5>Seleccione una Bodega para poder Emitir la Boleta</h5>
                    <div class="form-group">
                        <!--<label class="control-label col-sm-2">Bodega</label>-->
                        <div class="col-sm-4">
                            <select name="sucursal2" id="sucursal2">
                                <option value="">- Seleccione Bodega -</option>
                                <?php foreach ($bodegas as $bodega_id => $nombre){ ?>
                                    <option value="<?php echo $bodega_id; ?>"><?php echo $nombre; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                </div>

            </div>
            <! /#tab-confirm >
        </div>

        <ul class="list-unstyled wizard">
            <li class="pull-left previous">
                <button type="button" class="btn btn-default">Anterior</button>
            </li>
            <li class="pull-right next">
                <button type="button" class="btn btn-primary">Siguiente</button>
            </li>
            <li class="pull-right finish hide">
                <button type="submit" name="accion" value="por_confirmar" class="btn btn-primary"
                        onclick="$('#action').val('por_confirmar')">Por Confirmar
                </button>
            </li>
            <li class="pull-right finish hide">&nbsp;</li>
            <li class="pull-right finish hide">
                <button type="submit" name="accion" value="emitir" class="btn btn-success"
                        onclick="$('#action').val('emitir')"><i class="fa fa-file-text-o"
                                                                aria-hidden="true"></i> <?php echo "Emitir Boleta"; ?>
                </button>
            </li>
        </ul>

        <input type="hidden" id="action" value=""/>

    </form>

</div>

<?php
/*
<script>
bindQuickOrder(<?= json_encode($customers); ?>);  
</script>
*/
?>

<script type="text/javascript">
    var customer = {};
    var _orders = [];
    var $validator;

    var direccion, comuna, chofer, fecha, hora, comment, customer, email, phone, user;

    jQuery(document).ready(function () {

        bindQuickOrder();

        //OCULTAR LOS PANELES DE USUARIO NUEVO Y EXISTENTE
        $('#existing-customer,#new-customer,#append-address,#quickorder-products,#order-source, #order-repeat').hide();

        // Actualizar la tabla de resumen al actualizar cantidades de productos
        $(document).on('change keyup blur', '.ipt-qty', function (e) {

            //¿Qué fila está afectada?
            var self = $(this),
                tr = self.parents('tr'),
                ix = tr.index(),
                val = self.val(),
                review = $('#quickorder-products-review tbody tr:eq(' + ix + ')'),
                cell = review.find('.td-qty'),
                price = tr.find('.ipt-price').val(),
                subt = tr.find('.ipt-subt');

            subt.val(price * val);
            var subtotal = '$ ' + number_format(price * val, 0, ',', '.');

            tr.find('.td-subtotal').text(subtotal);
            review.find('.td-subtotal').text(subtotal);
            cell.html(val);

            _doTotal();
        });

        $('#quickorder-products input').on('change keyup blur', _doTotal);


        var map = {};
        jQuery('#slct-customer-id2').typeahead({
            minLength: 3,
            items: 10,
            source: function (query, process) {
                $.ajax({
                    url: '/modals/customers',
                    type: 'POST',
                    dataType: 'JSON',
                    data: 'query=' + query,
                    success: function (json) {
                        var separator = " - ";
                        var data = [];
                        for (var i = 0; i < json.length; i++) {
                            var cadena = json[i].cust_name + separator + json[i].cust_email;
                            map[cadena] = json[i];
                            data.push(cadena);
                        }
                        process(data);
                    }
                });
            },
            updater: function (item) {
                //console.log(map[item].id);
                if (map[item].id) {
                    $('#customer_id').val(map[item].id);
                    //alert( map[item].cust_name );
                    $('.review-customer').text(map[item].cust_name);
                    $('.review-email').text(map[item].cust_email);
                    $('.review-phone').text(map[item].cust_phone);

                    loadDirecciones();

                    $('#order-source, #order-repeat').hide();
                    $('#order-create').show();
                    $('#quickorder-products-review tbody').html("");
                }
                return item;
            }
        });

        var onChange = function (event) {
            //console.log("Changed: "+event.target.value);
            var value = event.target.value;

            if (!value || !map[value]) {
                $('#customer_id').val("");
                $('#slct-customer-id2').val("");

                $('.review-customer').text("");
                $('.review-email').text("");
                $('.review-phone').text("");

                loadDirecciones();
            }//else{
            // Mostramos el panel correspondiente:
            $('#existing-customer').hide();
            $('#new-customer').hide();
            $('#quickorder-products').hide().find('tbody').empty();

            $('#order-source, #order-repeat').hide();
            $('#order-create').show();
            $('#quickorder-products-review tbody').html("");

            $("#chbox-new-address").attr('checked', false);
            $("#append-address").hide();
            //}
        };
        $('#slct-customer-id2').on('change', onChange);

        // BIND DE SELECTOR DE ÓRDENES
        $("#slct-order-id").on("change", function () {
            var order = _orders[$(this).val()];
            _renderProducts(order);
        });

        //Cuando se activa el check de nuevo usuario...
        $('#chbox-new-customer').change(function () {
            var chkbox = $(this), checked = chkbox.attr('checked');

            if (checked) {
                $('#slct-customer-id2').val("");
                $('#customer_id').val("");
                $('.review-customer').text("");
                $('.review-email').text("");
                $('.review-phone').text("");

                $('#slct-customer-id').select2('val', "");
                $('#existing-customer').hide();
                $('#new-customer, #append-address').show();
            } else {
                if ($('#slct-customer-id').val()) {
                    $('#existing-customer').show();
                    $('#new-customer, #append-address').hide();
                    $('#chbox-new-address').removeAttr('checked');
                } else {
                    $('#existing-customer,#new-customer, #append-address').hide();
                }
            }
        });

        /*
         //Al seleccionar una dirección:
         $('#slct-address-id').change(function() {
         var $slct = $(this),
         val   = $slct.val();

         $('#chbox-new-address').removeAttr('checked').trigger('change');
         });
         */

        //Al seleccionar una dirección:
        $('#slct-address-id').change(function () {
            var $slct = $(this),
                val = $slct.val();

            $('#chbox-new-address').removeAttr('checked').trigger('change');
        });

        //¿Pedido nuevo o repetir?
        $("[name=order_repeat]").click(function (e) {
            var val = $(this).val(),
                target = '#order-' + val;

            //No vaciemos la tabla, para poder personalizar el pedido.
            // $('#quickorder-products').hide().find('tbody').empty();
            $(target).show().siblings('.order-builder').hide();
        });

        //Tabla de productos:
        $('#btn-add-product').click(function (e) {
            e.preventDefault();

            var prodData = $('#slct-prod-id').select2('data'),
                $row = $("<tr><td class='td-name'></td><td class='td-qty'></td><td class='td-price hidden-sm hidden-xs'></td><td class='td-subtotal'></td><td class='td-actions'><a><i class='fa fa-trash-o'></i></a></td></tr>"),
                label = prodData.text.split(' – '),
                name = label[0],
                price = parseInt(label[1].replace('$ ', '').replace('.', '')),
                qty = $('#prod-qty').val(),
                inputId = $('<input type="hidden" class="ipt-id" name="sku[ids][]" />'),
                inputQty = $('<input type="number" class="ipt-qty form-control input-sm" min="1" name="sku[qty][]" />').val(qty),
                inputPrice = $('<input type="hidden" class="ipt-price" />'),
                inputSubt = $('<input type="hidden" class="ipt-subt" />'),
                clone;

            $row.find('.td-name').text(name);
            $row.find('.td-qty').append(inputQty);
            $row.find('.td-price').text('$ ' + number_format(price, 0, ',', '.'));
            $row.find('.td-subtotal').text('$ ' + number_format(price * qty, 0, ',', '.'));

            $clone = $row.clone();
            $clone.find('td').last().remove();
            $clone.find('.td-qty').html(qty);

            $row.find('td').last().append(inputId, inputSubt, inputPrice);

            inputId.val(prodData.id);
            inputQty.val(qty);
            inputPrice.val(price);
            inputSubt.val(qty * price);

            $('#quickorder-products').show();
            $('#quickorder-products tbody').append($row);
            $('#quickorder-products-review tbody').append($clone);
            $row.clone = $clone;

            $('.fa-trash-o', $row).click(function (e) {
                $row.clone.remove();
                $row.remove();
                _doTotal();
            });

            _doTotal();
        });

        $.validator.addMethod('validaCustomer', function (value, element, param) {
            var res = false;

            if ($("#chbox-new-customer").is(':checked') == false) {
                if ($("#customer_id").val()) res = true;
            } else {
                res = true;
            }

            return res;
        }, 'Invalid value');

        $.validator.addMethod('validaCustomerAdress', function (value, element, param) {
            var res = false;

            if ($("#chbox-new-customer").is(':checked') == false && $("#chbox-new-address").is(':checked') == false) {
                if ($("#customer_id").val() && value) res = true;
            } else {
                res = true;
            }

            return res;
        }, 'Invalid value');

        $validator = $("#frm-pedidos").validate({
            ignore: "",
            rules: {
                "customer_id2": {
                    validaCustomer: true
                },
                "address_id": {
                    validaCustomerAdress: true
                },
                "cust_name": {
                    required: function (element) {
                        return $("#chbox-new-customer").is(':checked') ? true : false;
                    }
                },
                "cust_email": {
                    required: function (element) {
                        return $("#chbox-new-customer").is(':checked') ? true : false;
                    }
                },
                "direccion": {
                    required: function (element) {
                        return $("#chbox-new-customer").is(':checked') || $("#chbox-new-address").is(':checked') ? true : false;
                    }
                },
                "sucursal2": {
                    required: function (element) {
                        return $("#action").val() == 'emitir' ? true : false;
                    }
                }
            },
            messages: {
                "customer_id2": "",
                "address_id": "",
                "cust_name": "",
                "cust_email": "",
                "direccion": "",
                "sucursal2": ""
            },
            highlight: function (element, errorClass) {
                if (element.type == "select-one" || element.name == "address_id" || element.name == "sucursal2") {
                    var elem = $(element);
                    if (elem.hasClass("select2-offscreen") || elem.hasClass("select2-container")) {
                        //$("#s2id_"+elem.attr("id")).addClass("errorClass");
                        $("#s2id_" + elem.attr("id")).addClass('has-error');
                    } else {
                        jQuery(element).closest('.input-group').addClass('has-error');
                    }
                } else {
                    jQuery(element).closest('.input-group').addClass('has-error');
                }
            },
            unhighlight: function (element, errorClass) {
                if (element.type == "select-one" || element.name == "address_id" || element.name == "sucursal2") {
                    var elem = $(element);
                    if (elem.hasClass("select2-offscreen") || elem.hasClass("select2-container")) {
                        //$("#s2id_"+elem.attr("id")).addClass("errorClass");
                        $("#s2id_" + elem.attr("id")).removeClass('has-error');
                    } else {
                        jQuery(element).closest('.input-group').removeClass('has-error');
                    }
                } else {
                    jQuery(element).closest('.input-group').removeClass('has-error');
                }
            }
        });

        $('[name=direccion]').keyup(function () {
            direccion = $(this).val();
        }).blur(drawMap);

        $('[name=create_address').change(function () {
            var chkbox = $(this), checked = chkbox.attr('checked');
            if (checked) {
                console.log("entre");
                $('#s2id_slct-address-id').select2('val', "");
            }
            $('#append-address').toggle($(this).is(':checked'));

        });

        $('[name=address_id]').change(function () {
            var addr = $(this).select2('data');

            $('[name=comuna_despacho').val(addr.comuna);

            direccion = addr.direccion;
            comuna = addr.comuna;
            comment = addr.comment;

            drawMap();
        });

        $('[name=cust_name').keyup(function (e) {
            customer = $(this).val();
            $('.review-customer').text(customer);
        });

        $('[name=cust_email').keyup(function (e) {
            email = $(this).val();
            $('.review-email').text(email);
        });

        $('[name=cust_phone').keyup(function (e) {
            phone = $(this).val();
            $('.review-phone').text(phone);
        });

        /*
        $('[name=driver_id]').change( function(e) {
            var data = $(this).select2('data');
            chofer = data.text;
        });
        */

        $('[name=comuna]').change(function () {
            comuna = $('option:selected', $(this)).text();
            $('[name=comuna_despacho').val(comuna);

            drawMap();
        });

        /*
        $('[name=driver_id]').change( function() {
            chofer = $('option:selected', $(this)).text();
            $('.review-chofer').text( chofer );
        });

        $('[name=delivery_date]').change( function() {
            fecha = $(this).val();
            $('.review-fecha').text( fecha );
        });

        $('[name=delivery_time]').change( function() {
            hora = $(this).val();
            $('.review-hora').text( hora );
        });
        */

    });

    //REGLAS DE FORMULARIO QUICK-ORDER
    function bindQuickOrder() {

        //var user  = {}, orders= [], c = [];

        //var direccion, comuna, chofer, fecha, hora, comment, customer, email, phone, user;

        $('.panel-wizard').bootstrapWizard({
            onTabShow: function (tab, navigation, index) {
                tab.prevAll().addClass('done');
                tab.nextAll().removeClass('done');
                tab.removeClass('done');

                var $total = navigation.find('li').length;
                var $current = index + 1;

                if ($current >= $total) {
                    $('.panel-wizard').find('.wizard .next').addClass('hide');
                    $('.panel-wizard').find('.wizard .finish').removeClass('hide');
                } else {
                    $('.panel-wizard').find('.wizard .next').removeClass('hide');
                    $('.panel-wizard').find('.wizard .finish').addClass('hide');
                }
            },
            onTabClick: function (tab, navigation, index) {
                return false;
            },
            onNext: function (tab, navigation, index) {

                $("#driver_id").rules("remove");

                if (index === 3) {
                    //var $valid = $('#frm-pedidos').valid();
                    $("#driver_id").rules("add", {
                        required: true,
                        messages: {
                            required: "",
                        }
                    });
                }

                var $valid = $('#frm-pedidos').valid();
                //var $valid = $('.panel-wizard').valid();

                $('#modal').animate({
                    scrollTop: '0px'
                }, 500);

                if (!$valid) {
                    $validator.focusInvalid();
                    return false;
                }
            }
        });
    }

    function loadDirecciones() {
        var customer_id = jQuery('#customer_id').val();

        var addr = [];
        var options = [];

        if (customer_id) {

            $.ajax({
                url: '/modals/customerLoadAddressesHasorders',
                type: 'POST',
                dataType: 'JSON',
                data: 'customer_id=' + customer_id,
                success: function (json) {
                    addr = $.map(json.addresses, function (el, i) {
                        return {
                            id: el.id,
                            text: el.direccion + ( el.comuna ? ', ' + el.comuna : ''),
                            direccion: el.direccion,
                            comuna: el.comuna,
                            comment: el.comentarios
                        }
                    });

                    $('#chbox-new-customer').attr('checked', false).trigger('change');

                    //productsPane();

                    $('#slct-address-id')
                        .select2('destroy')
                        .select2({
                            data: addr
                        });

                    // Mostramos el panel correspondiente:
                    $('#existing-customer').show();
                    $('#new-customer').hide();

                    //¿Tendrá pedidos para duplicar el amigo?
                    $('#order-source').toggle(json.has_orders);
                    if (json.has_orders) {
                        $('#radio-repeat_order').trigger('click');
                        _getCustomerOrders(customer_id, _renderOrders);
                    } else {
                        $('#radio-create_order').trigger('click');
                    }
                }
            });

        } else {
            $('#slct-address-id').select2('destroy').select2({
                data: addr
            });
            $('#slct-order-id').select2('destroy').select2({
                data: options
            });

            // Mostramos el panel correspondiente:
            $('#existing-customer').hide();
            $('#new-customer').hide();
            $('#quickorder-products').hide().find('tbody').empty();

            //OCULTAR LOS PANELES DE USUARIO NUEVO Y EXISTENTE
            $('#order-source, #order-repeat').hide();
            $('#order-create').show();
        }

    }

    // Genera select con órdenes anteriores para replicar
    function _renderOrders(json) {

        if (!json || !json.length) {
            $('#order-source').hide();
            $('#radio-create_order').trigger('click');
            return;
        }

        _orders = json;

        var options = _orders.map(function (order, index, array) {
            var count = 0;

            var date = moment(order.order_created).locale("es").format('DD MMMM');
            var total = "$" + number_format(order.order_total, 0);

            order.products.forEach(function (prod) {
                count += parseInt(prod.qty);
            });

            return {
                id: index,
                text: date + " – " + total + " (" + count + " productos)"
            }
        });

        $('#slct-order-id').select2('destroy').select2({
            data: options
        });
    }

    //Pregunta al servidor por órdenes anteriores para un ID de usuario
    function _getCustomerOrders(id, callback) {
        $.get('/api/users/' + id + '/orders', callback, 'json');
    }

    // Renderea tabla de productos de una orden existente
    function _renderProducts(order) {

        $('#quickorder-products tbody').empty();

        console.log(order);
        var shipping = order.order_shipping ? parseInt(order.order_shipping) : 0;
        $("input[name='shipping']").val(shipping);

        order.products.forEach(function (product, index) {

            var $row = $("<tr><td class='td-name'></td><td class='td-qty'></td><td class='td-price hidden-sm hidden-xs'></td><td class='td-subtotal'></td><td class='td-actions'><a><i class='fa fa-trash-o'></i></a></td></tr>"),
                name = product.name + ' ' + product.prod_name + ' ' + product.sku_description,
                price = parseInt(product.sku_price),
                qty = parseInt(product.qty),
                inputId = $('<input type="hidden" class="ipt-id" name="sku[ids][]" />'),
                inputQty = $('<input type="number" class="ipt-qty input-sm form-control" min="1" name="sku[qty][]" />'),
                inputPrice = $('<input type="hidden" class="ipt-price" />'),
                inputSubt = $('<input type="hidden" class="ipt-subt" />'),
                clone;

            $row.find('.td-name').text(name);
            $row.find('.td-qty').append(inputQty.val(qty));
            $row.find('.td-price').text('$ ' + number_format(price, 0, ',', '.'));
            $row.find('.td-subtotal').text('$ ' + number_format(price * qty, 0, ',', '.'));

            $clone = $row.clone();
            $clone.find('td').last().remove();
            $clone.find('.td-qty').html(qty);

            $row.find('td').last().append(inputId, inputSubt, inputPrice);

            inputId.val(product.id);
            inputQty.val(qty);
            inputPrice.val(price);
            inputSubt.val(qty * price);

            $('#quickorder-products tbody').append($row);
            $('#quickorder-products-review tbody').append($clone);
            $row.clone = $clone;

            $('.fa-trash-o', $row).click(function (e) {

                $row.clone.remove();
                $row.remove();

                _doTotal();
            });

        });

        $('#quickorder-products').show();
        _doTotal();
    }

    function _doTotal() {
        var subtotal = 0;

        if ($('.ipt-subt').length) {
            $('.ipt-subt').each(function (ix, el) {
                subtotal += parseInt($(el).val());
            });
        } else {
            subtotal = 0;
        }

        var descuento = parseInt($('.order-discount-input').val());
        if (!descuento) descuento = 0;

        var despacho = parseInt($('.order-delivery-input').val());
        if (!despacho) despacho = 0;

        var total = subtotal - descuento + despacho;


        $('.final-subtotal').text('$ ' + number_format(subtotal, 0, ',', '.'));
        $('.final-despacho').text('$ ' + number_format(despacho, 0, ',', '.'));
        $('.final-descuento').text('$ ' + number_format(descuento, 0, ',', '.'));
        $('.final-total').text('$ ' + number_format(total, 0, ',', '.'));
    }

    //PASAR DATOS PA'LANTE

    function drawMap() {
        var location = encodeURIComponent(direccion + "+" + comuna);
        var mapURL = "http://maps.googleapis.com/maps/api/staticmap?center=" + location + "&zoom=16&size=640x240&maptype=roadmap&sensor=false&scale=2&markers=" + location;

        $('#map-preview').attr('src', mapURL);
        $('.review-comuna').text(direccion + ', ' + comuna);
        $('.review-comment').text(comment)
        $('.review-chofer').text(chofer);

        //$('.review-customer').text(customer);
        //$('.review-email').text(email);
        //$('.review-phone').text(phone);

        //console.log(direccion, comuna, mapURL);
    }
</script>
