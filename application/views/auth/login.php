

    <div class="container">
      <div class="row">

        <div class="col-md-6 col-md-offset-3">
          <h1 class="text-center">Ingresar <i class="icon icon_user"></i></h1>

          <?php echo form_open("auth/login", array("id"=>"form-login", "class" => "form form-horizontal boxed"));?>

            <?php
            if( ! empty($this->session->flashdata('item')) ){
            $message = $this->session->flashdata('item');
            ?>
            <div class="alert alert-<?php echo $message['class'];?>"><?php echo $message['message'];?></div>
            <?php
            }
            ?>

            <div id="alertMessage" style="display:none;"></div>

            <div class="col-sm-12">
              <fieldset class="form-group">
                <label class="control-label">Rut</label>
                <div class="input-wrapper">
                  <?= form_input($identity); ?>
                </div>
              </fieldset>
            </div>

            <div class="col-sm-12">
              <fieldset class="form-group">
                <label class="control-label">Contraseña</label>
                <div class="input-wrapper">
                  <?= form_input($password); ?>
                </div>
              </fieldset>
            </div>

            <button type="submit" class="btn btn-lg btn-primary col-md-4 col-md-offset-4">Ingresar</button>

            <label for="identity" class="error" style="display:none;"></label>
            <label for="password" class="error" style="display:none;"></label>

          <?= form_close(); ?>


        </div>
      </div>
    </div>
  </body>
</html>

<script src="<?php echo base_url("assets/korua/js/vendor/jquery.js"); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url("assets/korua/js/plugins.js"); ?>"></script>
<script src="<?php echo base_url("assets/korua/js/main.js"); ?>"></script>
<script src="<?php echo base_url("assets/korua/js/utils.js"); ?>"></script>

<script src="<?php echo base_url('assets/korua/js/vendor/jquery.Rut.js'); ?>"></script>

<script>
jQuery(function(){
   jQuery("#form-login").validate({
      rules:{
         identity : {required:true, rut:true},
         password : {required:true}
      },
      messages:{
         identity : '',
         password : ''
      },
      submitHandler: function( form ) {
         var res_rut = verificaRutLogin();
         if( res_rut == 0 ){
            _alert("El Rut ingresado no se encuentra registrado en nuestro sistema.",2);
         }else{
            var res = verificaUsuarioActivo();
            if( res == 0 ){
               _alert("Tu cuenta se encuentra desactivado por el momento.",2);
            }else{
               form.submit();
            }
         }
         return false;
      }
   });
});
function verificaRutLogin(){
   var res = $.ajax({
      method: "POST",
      url: "<?php echo base_url();?>auth/rutCheck",
      data: { rut: jQuery("#identity").val() },
      async: false
   }).responseText;
   return res;
}
function verificaUsuarioActivo(){
   var res = $.ajax({
      method: "POST",
      url: "<?php echo base_url();?>auth/activoCheck",
      data: { rut: jQuery("#identity").val() },
      async: false
   }).responseText;
   return res;
}
</script>
