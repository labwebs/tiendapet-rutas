<div id="infoMessage"><?php echo $message;?></div>

<div class="container">
  <div class="row">

    <div class="col-md-12">
      <h1 class="text-center">Mi perfil <i class="icon icon_user"></i></h1>

      <?php echo form_open("panel/perfil", array("id"=>"form-registro", "name"=>"form-registro", "class" => "form form-horizontal boxed inverse"));?>
        <div class="col-sm-6">
          <fieldset class="form-group">
            <label class="control-label">Rut</label>
            <div class="input-wrapper">
               <input class="form-control" type="text" readonly="readonly" value="<?php echo $rut;?>">
            </div>
          </fieldset>
        </div>

        <div class="col-sm-6">
          <fieldset class="form-group">
            <label class="control-label"><?php echo lang('create_user_fname_label', 'first_name');?></label>
            <div class="input-wrapper">
              <?php echo form_input($first_name, null, array("class" => "form-control"));?>
            </div>
          </fieldset>
        </div>

        <div class="col-sm-6">
          <fieldset class="form-group">
          <label class="control-label"><?php echo lang('create_user_lname_label', 'last_name');?></label>
          <div class="input-wrapper">
            <?php echo form_input($last_name, null, array("class" => "form-control"));?>
          </div>
          </fieldset>
        </div>

        <div class="col-sm-6">
          <fieldset class="form-group">
          <label class="control-label">Apellido Materno</label>
          <div class="input-wrapper">
            <?php echo form_input($middle_name, null, array("class" => "form-control")); ?>
          </div>
          </fieldset>
        </div>

        <div class="col-sm-6">
          <fieldset class="form-group">
          <label class="control-label"><?php echo lang('create_user_email_label', 'email');?></label>
          <div class="input-wrapper">
            <input class="form-control" type="text" readonly="readonly" value="<?php echo $email;?>">
          </div>
          </fieldset>
        </div>

        <div class="col-sm-6">
          <fieldset class="form-group">
          <label class="control-label"><?php echo lang('create_user_phone_label', 'phone');?></label>
          <div class="input-wrapper">
            <?php echo form_input($phone, null, array("class" => "form-control"));?>
          </div>
          </fieldset>
        </div>

        <div class="col-sm-6">
          <fieldset class="form-group">
          <label class="control-label">Fecha nacimiento</label>
          <div class="input-wrapper">
            <?php echo form_input($birth_date, null, array("class" => "form-control datepicker"));?>
          </div>
          </fieldset>
        </div>

        <div class="col-sm-6">
          <fieldset class="form-group">
            <label class="control-label">Sexo</label>
            <div class="input-wrapper">
              <?php echo $sexo; ?>
          </fieldset>
        </div>

         <div class="col-sm-12">
            <fieldset class="form-group">
               <label class="control-label">Tipo de Plan</label>
               <div class="input-wrapper" style="text-align: center;">
                  <input type="text" readonly="readonly" class="form-control" value="<?php echo $plan;?>">
               </div>
            </fieldset>
         </div>


        <p class="text-center width100 lead">Para cambiar tu contraseña, ingresa una nueva a continuación. Si no deseas cambiarla, deja los campos en blanco.</p>

        <div class="col-sm-6">
          <fieldset class="form-group">
            <label class="control-label"><?php echo lang('create_user_password_label', 'password');?></label>
            <div class="input-wrapper">
              <?php echo form_input($password, null, array("class" => "form-control"));?>
            </div>
          </fieldset>
        </div>

        <div class="col-sm-6">
          <fieldset class="form-group">
            <label class="control-label"><?php echo lang('create_user_password_confirm_label', 'password_confirm');?></label>
            <div class="input-wrapper">
              <?php echo form_input($password_confirm, null, array("class" => "form-control"));?>
            </div>
          </fieldset>
        </div>

        <p class="text-center width100">&nbsp;</P>

        <h2 class="text-center col-xs-12">Contacto de Urgencia</h2>

            <p class="text-center width100">Ind&iacute;canos con qui&eacute;n nos contactamos en caso de una urgencia.</p>

            <p class="text-center width100">&nbsp;</P>

            <div class="col-sm-6">
               <fieldset class="form-group">
                  <label class="control-label">Nombre</label>
                  <div class="input-wrapper">
                     <?php echo form_input($nombre_contacto, null, array("class" => "form-control"));?>
                  </div>
                  <label for="nombre_contacto" class="error" style="display:none;"></label>
               </fieldset>
            </div>

            <div class="col-sm-6">
               <fieldset class="form-group">
                  <label class="control-label">Tel&eacute;fono</label>
                  <div class="input-wrapper">
                     <?php echo form_input($telefono_contacto, null, array("class" => "form-control"));?>
                  </div>
                  <label for="telefono_contacto" class="error" style="display:none;"></label>
               </fieldset>
            </div>

            <p class="text-center col-xs-12">&nbsp;</P>

        <button type="submit" class="btn btn-lg btn-primary col-md-4 col-md-offset-4">Guardar cambios</button>

            <p class="text-center col-xs-12">&nbsp;</P>

            <label for="first_name" class="error" style="display:none;"></label>
            <label for="last_name" class="error" style="display:none;"></label>
            <label for="middle_name" class="error" style="display:none;"></label>
            <label for="phone" class="error" style="display:none;"></label>
            <label for="birth_date" class="error" style="display:none;"></label>
            <label for="sexo" class="error" style="display:none;"></label>
            <label for="password" class="error" style="display:none;"></label>
            <label for="password_confirm" class="error" style="display:none;"></label>
            <label for="nombre_contacto" class="error" style="display:none;"></label>
            <label for="telefono_contacto" class="error" style="display:none;"></label>


        <?php echo form_hidden('id', $user->id);?>
        <?php echo form_hidden($csrf); ?>
      <?php echo form_close(); ?>


    </div>
  </div>
</div>

<script src="<?php echo base_url("assets/korua/js/vendor/jquery.js"); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url("assets/korua/js/plugins.js"); ?>"></script>
<script src="<?php echo base_url("assets/korua/js/main.js"); ?>"></script>
<script src="<?php echo base_url("assets/korua/js/utils.js"); ?>"></script>


<script src="<?php echo base_url('assets/korua/js/vendor/jquery.Rut.js'); ?>"></script>

<script>
<?php
/*
$.validator.addMethod("aFunction",
   function(value, element) {
      if (value == "none")
         return false;
      else
         return true;
   },
   "Please select a value");
*/
?>
jQuery(function(){

   jQuery("#form-registro").validate({
      rules:{
         //rut         : {required:true, rut:true},
         first_name  : {required:true},
         last_name   : {required:true},
         middle_name : {required:true},
         //email       : {required:true, email:true},
         phone       : {required:true, number:true},
         password    : {minlength:8},
         password_confirm  : {minlength:8, equalTo:"#password"},
         nombre_contacto   : {required:true},
         telefono_contacto : {required:true, number:true}
      },
      messages:{
         //rut         : '',
         first_name  : '',
         last_name   : '',
         middle_name : '',
         //email       : '',
         phone       : '',
         password    : '',
         password_confirm : '',
         nombre_contacto : '',
         telefono_contacto : ''
      },
      submitHandler: function( form ) {
         //var res_rut = verificaRut();
         //var res_email = verificaEmail();
         /*if( res_rut == 1 ){
            _alert("El Rut ingresado ya se encuentra registrado en nuestro sistema.",2);
         }else if( res_email == 1 ){
            _alert("El E-mail ingresado ya se encuentra registrado en nuestro sistema.",2);
         }else{
            form.submit();
         }
         */
         form.submit();
         return false;
      }
   });
});
function verificaEmail(){
   var res = $.ajax({
      method: "POST",
      url: "auth/emailCheck",
      data: { email: jQuery("#email").val() },
      async: false
   }).responseText;
   return res;
}

function verificaRut(){
   var res = $.ajax({
      method: "POST",
      url: "auth/rutCheck",
      data: { rut: jQuery("#rut").val() },
      async: false
   }).responseText;
   return res;
}
</script>


  </body>
</html>
