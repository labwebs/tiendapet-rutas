<div id="infoMessage"><?php echo $message;?></div>

<div class="container">
    <div class="row">

        <div class="col-md-12">
            <h1 class="text-center">Formulario de inscripci&oacute;n <i class="icon icon_check-circle"></i></h1>

            <?php echo form_open("registro", array("id"=>"form-registro", "name"=>"form-registro", "class" => "form form-horizontal boxed"));?>

            <p class="text-center lead">
                Te invitamos a crear tu perfil, es importante que completes todos los datos del formulario. Con tu perfil creado puede acceder a reservar las clases en l&iacute;nea.
            </p>

            <div id="alertMessage" style="display:none;"></div>

            <div class="col-sm-6">
                <fieldset class="form-group">
                    <label class="control-label">Rut</label>
                    <div class="input-wrapper">
                        <?php echo form_input($identity, null, array("class" => "form-control"));?>
                    </div>
                    <label for="rut" class="error" style="display:none;"></label>
                </fieldset>
            </div>

            <div class="col-sm-6">
                <fieldset class="form-group">
                    <label class="control-label"><?php echo lang('create_user_fname_label', 'first_name');?></label>
                    <div class="input-wrapper">
                        <?php echo form_input($first_name, null, array("class" => "form-control"));?>
                    </div>
                    <label for="first_name" class="error" style="display:none;"></label>
                </fieldset>
            </div>

            <div class="col-sm-6">
                <fieldset class="form-group">
                    <label class="control-label"><?php echo lang('create_user_lname_label', 'last_name');?></label>
                    <div class="input-wrapper">
                        <?php echo form_input($last_name, null, array("class" => "form-control"));?>
                    </div>
                    <label for="last_name" class="error" style="display:none;"></label>
                </fieldset>
            </div>

            <div class="col-sm-6">
                <fieldset class="form-group">
                    <label class="control-label">Apellido Materno</label>
                    <div class="input-wrapper">
                        <?php echo form_input($middle_name, null, array("class" => "form-control")); ?>
                    </div>
                    <label for="middle_name" class="error" style="display:none;"></label>
                </fieldset>
            </div>

            <div class="col-sm-6">
                <fieldset class="form-group">
                    <label class="control-label"><?php echo lang('create_user_email_label', 'email');?></label>
                    <div class="input-wrapper">
                        <?php echo form_input($email, null, array("class" => "form-control"));?>
                    </div>
                    <label for="email" class="error" style="display:none;"></label>
                </fieldset>
            </div>

            <div class="col-sm-6">
                <fieldset class="form-group">
                    <label class="control-label"><?php echo lang('create_user_phone_label', 'phone');?></label>
                    <div class="input-wrapper">
                        <?php echo form_input($phone, null, array("class" => "form-control"));?>
                    </div>
                    <label for="phone" class="error" style="display:none;"></label>
                </fieldset>
            </div>

            <div class="col-sm-6">
                <fieldset class="form-group">
                    <label class="control-label">Fecha nacimiento</label>
                    <div class="input-wrapper">
                        <?php echo form_input($birth_date, null, array("class" => "form-control datepicker"));?>
                    </div>
                </fieldset>
            </div>

            <div class="col-sm-6">
                <fieldset class="form-group">
                    <label class="control-label">Sexo</label>
                    <div class="input-wrapper">
                        <?php echo $sexo; ?>
                    </div>
                </fieldset>
                <label for="sexo" class="error" style="display:none;"></label>
            </div>

            <div class="col-sm-6">
                <fieldset class="form-group">
                    <label class="control-label" style="width:100%;">Tipo de Plan</label>
                </fieldset>
            </div>

            <div class="col-sm-6">
                <fieldset class="form-group">
                    <div class="input-wrapper2" style="width:100%;">
                        <?php echo $planes; ?>
                    </div>
                </fieldset>
                <label for="plan_id" class="error" style="display:none;"></label>
            </div>

            <p class="text-center width100 lead">La clave debe tener mínimo 8 caracteres.<br />Una clave segura mezcla letras con números y evita los números de tu dirección o cumpleaños</p>

            <div class="col-sm-6">
                <fieldset class="form-group">
                    <label class="control-label"><?php echo lang('create_user_password_label', 'password');?></label>
                    <div class="input-wrapper">
                        <?php echo form_input($password, null, array("class" => "form-control"));?>
                    </div>
                    <label for="password" style="display:none;"></label>
                </fieldset>
            </div>

            <div class="col-sm-6">
                <fieldset class="form-group">
                    <label class="control-label"><?php echo lang('create_user_password_confirm_label', 'password_confirm');?></label>
                    <div class="input-wrapper">
                        <?php echo form_input($password_confirm, null, array("class" => "form-control"));?>
                    </div>
                    <label for="password_confirm" style="display:none;"></label>
                </fieldset>
            </div>

            <p class="text-center width100">&nbsp;</P>

            <h2 class="text-center width100 celeste">Contacto de Urgencia</h2>

            <p class="text-center width100">Ind&iacute;canos con qui&eacute;n nos contactamos en caso de una urgencia.</p>

            <p class="text-center width100">&nbsp;</P>

            <div class="col-sm-6">
                <fieldset class="form-group">
                    <label class="control-label">Nombre</label>
                    <div class="input-wrapper">
                        <?php echo form_input($nombre_contacto, null, array("class" => "form-control"));?>
                    </div>
                    <label for="nombre_contacto" class="error" style="display:none;"></label>
                </fieldset>
            </div>

            <div class="col-sm-6">
                <fieldset class="form-group">
                    <label class="control-label">Tel&eacute;fono</label>
                    <div class="input-wrapper">
                        <?php echo form_input($telefono_contacto, null, array("class" => "form-control"));?>
                    </div>
                    <label for="telefono_contacto" class="error" style="display:none;"></label>
                </fieldset>
            </div>

            <input type="hidden" name="convenio_id" id="convenio_id" value="<?php echo isset($configuracion->convenio_id) ? $configuracion->convenio_id : '';?>"/>

            <p class="text-center width100">&nbsp;</P>

            <div class="col-sm-12">
                <div class="form-group text-center">
                    <input id="acepto_condiciones_contrato" type="checkbox" name="acepto_condiciones_contrato">&nbsp;&nbsp;<label for="tos" class="terms">Acepto las <a href="<?php echo base_url("assets/pdf/CONTRATO-CFK.pdf");?>" target="_blank" class="celeste">Condiciones de contrato</a></label>
                    <label for="acepto_condiciones_contrato" class="error" style="_display:none;"></label>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group text-center">
                    <input id="recibir_newsletter" type="checkbox" name="recibir_newsletter" value="1" checked="checked">&nbsp;&nbsp;<label for="tos" class="terms">Acepto recibir informaci&oacute;n de oferta y promociones</label>
                    <label for="recibir_newsletter" class="error" style="_display:none;"></label>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-lg btn-primary col-md-4 col-md-offset-4">Registrar</button>
                </div>
            </div>

            <p class="text-center width100">&nbsp;</P>

            <label for="password" class="error" style="display:none;"></label>
            <label for="password_confirm" class="error" style="display:none;"></label>

            <?php echo form_close(); ?>

        </div>
    </div>
</div>
<script src="<?php echo base_url("assets/korua/js/vendor/jquery.js"); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url("assets/korua/js/plugins.js"); ?>"></script>
<script src="<?php echo base_url("assets/korua/js/main.js"); ?>"></script>
<script src="<?php echo base_url("assets/korua/js/utils.js"); ?>"></script>


<script src="<?php echo base_url('assets/korua/js/vendor/jquery.Rut.js'); ?>"></script>

<script>
    <?php
    /*
    $.validator.addMethod("aFunction",
       function(value, element) {
          if (value == "none")
             return false;
          else
             return true;
       },
       "Please select a value");
    */
    ?>
    jQuery(function(){

        jQuery("#form-registro").validate({
            rules:{
                rut         : {required:true, rut:true},
                first_name  : {required:true},
                last_name   : {required:true},
                middle_name : {required:true},
                email       : {required:true, email:true},
                phone       : {required:true, number:true},
                password    : {required:true, minlength:8},
                password_confirm  : {required:true, minlength:8, equalTo:"#password"},
                nombre_contacto   : {required:true},
                telefono_contacto : {required:true, number:true},
                acepto_condiciones_contrato : {required:true}
            },
            messages:{
                rut         : '',
                first_name  : '',
                last_name   : '',
                middle_name : '',
                email       : '',
                phone       : '',
                password    : '',
                password_confirm : '',
                nombre_contacto : '',
                telefono_contacto : '',
                acepto_condiciones_contrato : 'Debes aceptar las condiciones de uso'
            },
            submitHandler: function( form ) {
                //jQuery("#msje").hide();
                var res_rut = verificaRut();
                var res_email = verificaEmail();
                //alert(res_rut );
                if( res_rut == 1 ){
                    _alert("El Rut ingresado ya se encuentra registrado en nuestro sistema.",2);
                }else if( res_email == 1 ){
                    _alert("El E-mail ingresado ya se encuentra registrado en nuestro sistema.",2);
                }else{
                    form.submit();
                }
                return false;
            }
        });
    });
    function verificaEmail(){
        var res = $.ajax({
            method: "POST",
            url: "auth/emailCheck",
            data: { email: jQuery("#email").val() },
            async: false
        }).responseText;
        return res;
    }

    function verificaRut(){
        var res = $.ajax({
            method: "POST",
            url: "auth/rutCheck",
            data: { rut: jQuery("#rut").val() },
            async: false
        }).responseText;
        return res;
    }
</script>

</body>

</html>