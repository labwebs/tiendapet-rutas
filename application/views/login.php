<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?php echo $this->site_name ?></title>

        <link href="<?php echo base_url('assets/css/style.default.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/custom.css'); ?>" rel="stylesheet">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="signin">
        
        <section>
            
            <div class="panel panel-signin">
                <div class="panel-body">
                    <div class="logo text-center">
                        <img src="<?php echo base_url('assets/images/logo.png') ;?>" alt="TiendaPet" class="img-responsive" >
                    </div>
                    <br />
                    <p class="text-center">Gestión de Rutas</p>
                    
                    <div class="mb30"></div>
                    
                    <?php if ( @$error ) : ?>
                    <p><?php echo $error ?></p>
                    <?php endif; ?>
                    
                    <?php echo form_open('auth/validate'); ?>
                        <div class="input-group mb15">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="text" name="username" class="form-control" placeholder="Nombre de usuario" required value="<?php echo set_value('username'); ?>">
                        </div><!-- input-group -->
                        <div class="input-group mb15">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input type="password" name="password" class="form-control" placeholder="Contraseña" required value="<?php echo set_value('password'); ?>">
                        </div><!-- input-group -->
                        
                        <div class="clearfix">
                            <div class="pull-left">
                            </div>
                            <div class="pull-right">
                                <button type="submit" class="btn btn-success">Entrar <i class="fa fa-angle-right ml5"></i></button>
                            </div>
                        </div>                      
                    </form>
                    
                </div><!-- panel-body -->
                <div class="panel-footer">
                  ©<?php echo date('Y'); ?> TiendaPet
                </div><!-- panel-footer -->
            </div><!-- panel -->
            
        </section>

        
        <script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/modernizr.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/pace.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/retina.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.cookies.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/custom.js'); ?>"></script>

    </body>
</html>