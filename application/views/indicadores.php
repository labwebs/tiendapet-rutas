<div class="indicadores contentpanel">

    <?php if (!$this->user->is_tomador_pedidos && !$this->user->is_tomador_pedidos_despacho) { ?>

	<!-- ESTADISTICAS -->
    <div class="row row-stat">

		<div class="row">
        	<div class="col-md-12">
			    <div class="col-md-6">
                    <div class="panel panel-success-alt noborder">
                        <div class="panel-heading noborder">
                            <div class="panel-btns" style="display: none;">
                                <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                            <!-- panel-btns -->

                            <div class="panel-icon"><i class="fa fa-users"></i></div>

                            <div class="media-body">
                  	            <h5 class="md-title nomargin"><b>Retención de Clientes</b></h5>
                  	            <h1 class="mt5"><?php echo $indicator_clients_retencion_percent; ?>%</h1>
                            </div>
                            <!-- media-body -->
                            <hr/>
                        </div>
                        
                        <!-- panel-body -->
                        <div class="panel-body noborder">
                            <div class="table-wrapper-data">
                                <table class="table table-striped table-dark">
                                <thead>
                                <tr>
                                    <th>Bodega</th>
                                    <th>0 y 90 d&iacute;as</th>
                                    <th>90 y 180 d&iacute;as</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($indicator_clients_x_bodega)) { ?>
                                    <?php foreach ($indicator_clients_x_bodega as $row) { ?>
                                    <tr>
                                        <td><?php echo $row['nombre']; ?></td>
                                        <td><?php echo $row['0y90']['percent']; ?>%</td>
                                        <td><?php echo $row['90y180']['percent']; ?>%</td>
                                    </tr>
                                    <?php } ?>
						        <?php } ?>
                                </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <a href="<?php echo base_url('dashboard/export/retencion-de-clientes'); ?>">exportar</a>
                        </div>
                    </div>
                    <!-- panel -->
                </div>
                <div class="col-md-6">
                    <div class="panel panel-success-alt noborder">
                        <div class="panel-heading noborder">
                            <div class="panel-btns" style="display: none;">
                                <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                            <!-- panel-btns -->

                            <div class="panel-icon"><i class="fa fa-cutlery"></i>
                        </div>

                        <div class="media-body">
                            <h5 class="md-title nomargin"><b>SKUs Disponibles</b></h5>
                            <h1 class="mt5"><?php echo $indicator_skus['skus_percent_disponible']; ?>%</h1></div>
                            <!-- media-body -->
                            <hr/>
                        </div>
              
			  	        <!-- panel-body -->
              	        <div class="panel-body noborder">
                	        <div class="table-wrapper-data">
                                <table class="table table-striped table-dark">
                                <thead>
                                <tr>
                                <th>Bodega</th>
                                <th>Cant. Disponible</th>
                                <th>Cant. Total</th>
                                <th>(%) Asignado</th>
                                </tr>
                                </thead>
                                <tbody>
						            <?php if (!empty($indicator_skus['bodegas'])) {?>
                        	            <?php foreach ($indicator_skus['bodegas'] as $row) {?>
                                        <tr>
                                            <td><?php echo $row['nombre']; ?></td>
                                            <td><?php echo $row['count']; ?></td>
                                            <td><?php echo $row['count_total']; ?></td>
                                            <td><?php echo $row['percent']; ?>%</td>
                                        </tr>
                                        <?php } ?>
                                    <?php } ?>
                                </tbody>
                  	            </table>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <a href="<?php echo base_url('dashboard/export/skus-por-bodega-available'); ?>">exportar todos</a> 
                            <a>|</a>
                            <a href="<?php echo base_url('dashboard/export/skus-no-disponibles'); ?>">exportar no disponibles</a>
                        </div>
                    </div>
                    <!-- panel -->
                </div>
                <div class="col-md-3"></div>
			    <div class="col-md-6">
            	    <div class="panel panel-success-alt noborder">
              		    <div class="panel-heading noborder">
                		    <div class="panel-btns" style="display: none;">
							    <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel">
                    		        <i class="fa fa-times"></i>
                  		        </a>
                	        </div>
                	        <!-- panel-btns -->

					        <div class="panel-icon"><i class="fa fa-dollar"></i></div>

					        <div class="media-body">
					            <h5 class="md-title nomargin"><b>Clientes Nuevos</b></h5>
					            <h1 class="mt5"><?php echo $indicator_clients_news['total']['percent']; ?>%</h1>
					        </div>
                            <!-- media-body -->
                	        <hr/>
                        </div>
              	        <!-- panel-body -->

				        <div class="panel-body noborder">
					        <div class="table-wrapper-data">
					            <table class="table table-striped table-dark table-bordered">
						        <thead>
                                <tr>
                                    <th rowspan="2" style="vertical-align:middle;">Bodega</th>
                                    <th colspan="2" style="border-bottom:solid;border-bottom-width:1px;text-align:center;">0 y 30 d&iacute;as</th>
                                    <th colspan="2" style="border-bottom:solid;border-bottom-width:1px;text-align:center;">30 y 60 d&iacute;as</th>
                                    <th colspan="2" style="border-bottom:solid;border-bottom-width:1px;text-align:center;">60 y 90 d&iacute;as</th>
                                </tr>
                                <tr> 
                                    <th>%</th>
                                    <th>q</th>
                                    <th>%</th>
                                    <th>q</th>
                                    <th>%</th>
                                    <th>q</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($indicator_clients_news['indicator_clients_news_x_bodega'])) { ?>
                                    <?php foreach ($indicator_clients_news['indicator_clients_news_x_bodega'] as $row) { ?>
                                    <tr>
                                        <td><?php echo $row['nombre']; ?></td>
                                        <td><?php echo $row['0y30']['percent']; ?>%</td>
                                        <td><?php echo $row['0y30']['count']; ?></td>
                                        <td><?php echo $row['31y60']['percent']; ?>%</td>
                                        <td><?php echo $row['31y60']['count']; ?></td>
                                        <td><?php echo $row['61y90']['percent']; ?>%</td>
                                        <td><?php echo $row['61y90']['count']; ?></td>
                                    </tr>
                                    <?php } ?>
                                <?php } ?>
						        </tbody>
					            </table>
					        </div>
				        </div>

                        <div class="panel-footer">
                            <a href="<?php echo base_url('dashboard/export/clients-news'); ?>">exportar</a>
                        </div>
                    </div>
                    <!-- panel -->
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="panel panel-success-alt noborder">
                        <div class="panel-heading noborder">
                            <div class="panel-btns" style="display: none;">
                                <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                            <!-- panel-btns -->

                            <div class="panel-icon">
                                <i class="fa fa-truck"></i>
                            </div>

                            <div class="media-body">
                                <h5 class="md-title nomargin"><b>Ventas Producto para Perro</b></h5>
                                <h1 class="mt5"><?php echo $indicators_perro['total']['percent']; ?>%</h1>
                            </div>
                            <!-- media-body -->
                            <hr/>
                        </div>
              
                        <!-- panel-body -->
                        <div class="panel-body noborder">
                            <div class="table-wrapper-data">
                                <table class="table table-striped table-dark table-bordered">
                                <thead>
                                <tr>
                                    <th rowspan="2" style="vertical-align:middle;">Categor&iacute;a</th>
                                    <th colspan="2" style="border-bottom:solid;border-bottom-width:1px;text-align:center;">0 y 90 d&iacute;as</th>
                                    <th colspan="2" style="border-bottom:solid;border-bottom-width:1px;text-align:center;">90 y 180 d&iacute;as</th>
                                </tr>
                                <tr> 
                                    <th>%</th>
                                    <th>$</th>
                                    <th>%</th>
                                    <th>$</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($indicators_perro['categorias'])) { ?>
                                    <?php 
                                    $sum_total_0y90days_percent = 0;
                                    $sum_total_0y90days_monto = 0;
                                    $sum_total_90y180days_percent = 0;
                                    $sum_total_90y180days_monto = 0;
                                    ?>
                                    <?php foreach ($indicators_perro['categorias'] as $row) { ?>
                                    <?php
                                        $sum_total_0y90days_percent += $row['0y90days']['percent'];
                                        $sum_total_0y90days_monto += $row['0y90days']['monto'];
                                        $sum_total_90y180days_percent += $row['90y180days']['percent'];
                                        $sum_total_90y180days_monto += $row['90y180days']['monto'];
                                    ?>
                                    <tr>
                                        <td><?php echo $row['nombre']; ?></td>
                                        <td><?php echo $row['0y90days']['percent']; ?>%</td>
                                        <td nowrap="nowrap"><?php echo $row['0y90days']['monto_formato']; ?></td>
                                        <td><?php echo $row['90y180days']['percent']; ?>%</td>
                                        <td nowrap="nowrap"><?php echo $row['90y180days']['monto_formato']; ?></td>
                                    </tr>
                                    <?php } ?>
                                <?php } ?>
                                </tbody>
                                <tfoot>
                                    <th>Total</th>
                                    <th><?php echo $sum_total_0y90days_percent;?>%</th>
                                    <th><?php echo $this->utils->numformat($sum_total_0y90days_monto);?></th>
                                    <th><?php echo $sum_total_90y180days_percent;?>%</th>
                                    <th><?php echo $this->utils->numformat($sum_total_90y180days_monto);?></th>
                                </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="panel-footer">
			   	            <a href="<?php echo base_url('dashboard/export/clients-buy-products-dog'); ?>">exportar</a>
                        </div>
                    </div>
                    <!-- panel -->
                </div>

                <div class="col-md-6">
                    <div class="panel panel-success-alt noborder">
                        <div class="panel-heading noborder">
                            <div class="panel-btns" style="display: none;">
                                <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                            <!-- panel-btns -->

                            <div class="panel-icon"><i class="fa fa-truck"></i></div>

                            <div class="media-body">
                                <h5 class="md-title nomargin"><b>Ventas Producto para Gato</b></h5>
                                <h1 class="mt5"><?php echo $indicators_gato['total']['percent']; ?>%</h1></div>
                                <!-- media-body -->
                                <hr/>
                            </div>
                            <!-- panel-body -->
                            <div class="panel-body noborder">
                                <div class="table-wrapper-data">
                                    <table class="table table-striped table-dark table-bordered">
                                    <thead>
                                    <tr>
                                        <th rowspan="2" style="vertical-align:middle;">Categor&iacute;a</th>
                                        <th colspan="2" style="border-bottom:solid;border-bottom-width:1px;text-align:center;">0 y 90 d&iacute;as</th>
                                        <th colspan="2" style="border-bottom:solid;border-bottom-width:1px;text-align:center;">90 y 180 d&iacute;as</th>
                                    </tr>
                                    <tr> 
                                        <th>%</th>
                                        <th>$</th>
                                        <th>%</th>
                                        <th>$</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if (!empty($indicators_gato['categorias'])) { ?>
                                        <?php 
                                        $sum_total_0y90days_percent = 0;
                                        $sum_total_0y90days_monto = 0;
                                        $sum_total_90y180days_percent = 0;
                                        $sum_total_90y180days_monto = 0;
                                        ?>
                                        <?php foreach ($indicators_gato['categorias'] as $row) { ?>
                                        <?php
                                        $sum_total_0y90days_percent += $row['0y90days']['percent'];
                                        $sum_total_0y90days_monto += $row['0y90days']['monto'];
                                        $sum_total_90y180days_percent += $row['90y180days']['percent'];
                                        $sum_total_90y180days_monto += $row['90y180days']['monto'];
                                        ?>
                                        <tr>
                                            <td><?php echo $row['nombre'];?></td>
                                            <td><?php echo $row['0y90days']['percent']; ?>%</td>
                                            <td nowrap="nowrap"><?php echo $row['0y90days']['monto_formato']; ?></td>
                                            <td><?php echo $row['90y180days']['percent']; ?>%</td>
                                            <td nowrap="nowrap"><?php echo $row['90y180days']['monto_formato']; ?></td>
                                        </tr>
                                        <?php } ?>
                                    <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <th>Total</th>
                                        <th><?php echo $sum_total_0y90days_percent;?>%</th>
                                        <th><?php echo $this->utils->numformat($sum_total_0y90days_monto);?></th>
                                        <th><?php echo $sum_total_90y180days_percent;?>%</th>
                                        <th><?php echo $this->utils->numformat($sum_total_90y180days_monto);?></th>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <a href="<?php echo base_url('dashboard/export/clients-buy-products-cat'); ?>">exportar</a>
                        </div>
                    </div>
                    <!-- panel -->
                </div>
			    
		    </div>
	    </div>

	    <div class="row">
            <div class="col-md-12" style="">
                <div class="col-md-12">
				    <div class="panel panel-success-alt noborder">
					    <div class="panel-heading noborder">
						    <div class="panel-btns" style="display: none;">
							    <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
						    </div>
						    <!-- panel-btns -->
						    <div class="panel-icon"><i class="fa fa-dollar"></i></div>

						    <div class="media-body">
							    <h5 class="md-title nomargin"><b>Ventas Retiro Local</b></h5>
							    <h1 class="mt5"><?php echo $indicators_order_x_retiro['0y30days']['percent']; ?>%</h1>
						    </div>
						    <!-- media-body -->
						    <hr/>
					    </div>

					    <!-- panel-body -->
					    <div class="panel-body noborder">
						    <div class="table-wrapper-data">
                                <table class="table table-striped table-dark table-bordered">
                                <thead>
                                <tr>
                                    <th rowspan="2" style="vertical-align:middle;">Local</th>
                                    <th colspan="2" style="border-bottom:solid;border-bottom-width:1px;text-align:center;">0 y 30 d&iacute;as</th>
                                    <th colspan="2" style="border-bottom:solid;border-bottom-width:1px;text-align:center;">30 y 60 d&iacute;as</th>
                                    <th colspan="2" style="border-bottom:solid;border-bottom-width:1px;text-align:center;">60 y 90 d&iacute;as</th>
                                    <th colspan="2" style="border-bottom:solid;border-bottom-width:1px;text-align:center;">90 y 120 d&iacute;as</th>
                                </tr>
                                <tr> 
                                    <th>%</th>
                                    <th>$</th>
                                    <th>%</th>
                                    <th>$</th>
                                    <th>%</th>
                                    <th>$</th>
                                    <th>%</th>
                                    <th>$</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($indicators_order_x_retiro['indicators_order_x_local']['locales'])) { ?>
                                    <?php 
                                    $sum_total_0y30_percent = 0;
                                    $sum_total_0y30_monto = 0;
                                    $sum_total_30y60_percent = 0;
                                    $sum_total_30y60_monto = 0;
                                    $sum_total_60y90_percent = 0;
                                    $sum_total_60y90_monto = 0;
                                    $sum_total_90y120_percent = 0;
                                    $sum_total_90y120_monto = 0;
                                    ?>
                                    <?php foreach ($indicators_order_x_retiro['indicators_order_x_local']['locales'] as $local_id => $row) { ?>
                                    <?php
                                        $sum_total_0y30_percent += $row['0y30']['percent'];
                                        $sum_total_0y30_monto += $row['0y30']['monto'];
                                        $sum_total_30y60_percent += $row['30y60']['percent'];
                                        $sum_total_30y60_monto += $row['30y60']['monto'];
                                        $sum_total_60y90_percent += $row['60y90']['percent'];
                                        $sum_total_60y90_monto += $row['60y90']['monto'];
                                        $sum_total_90y120_percent += $row['90y120']['percent'];
                                        $sum_total_90y120_monto += $row['90y120']['monto'];
                                    ?>
                                    <tr>
                                        <td><?php echo $row['nombre']; ?></td>
                                        <td><?php echo $row['0y30']['percent']; ?>%</td>
                                        <td nowrap="nowrap"><?php echo $row['0y30']['monto_formato']; ?></td>
                                        <td><?php echo $row['30y60']['percent']; ?>%</td>
                                        <td nowrap="nowrap"><?php echo $row['30y60']['monto_formato']; ?></td>
                                        <td><?php echo $row['60y90']['percent']; ?>%</td>
                                        <td nowrap="nowrap"><?php echo $row['60y90']['monto_formato']; ?></td>
                                        <td><?php echo $row['90y120']['percent']; ?>%</td>
                                        <td nowrap="nowrap"><?php echo $row['90y120']['monto_formato']; ?></td>
                                    </tr>
                                    <?php } ?>
                                <?php } ?>
                                </tbody>
                                <tfoot>
                                    <th>Total</th>
                                    <th><?php echo $sum_total_0y30_percent;?>%</th>
                                    <th><?php echo $this->utils->numformat($sum_total_0y30_monto);?></th>
                                    <th><?php echo $sum_total_30y60_percent;?>%</th>
                                    <th><?php echo $this->utils->numformat($sum_total_30y60_monto);?></th>
                                    <th><?php echo $sum_total_60y90_percent;?>%</th>
                                    <th><?php echo $this->utils->numformat($sum_total_60y90_monto);?></th>
                                    <th><?php echo $sum_total_90y120_percent;?>%</th>
                                    <th><?php echo $this->utils->numformat($sum_total_90y120_monto);?></th>
                                </tfoot>
                                </table>							
						    </div>
					    </div>
				
					    <div class="panel-footer">
						    <a href="<?php echo base_url('dashboard/export/clients-retiro-local'); ?>">exportar</a>
					    </div>
				    </div>
				    <!-- panel -->
			    </div>
            </div>
	    </div>

        <div class="row">
            <div class="col-md-12" style="">
                <div class="col-md-12">
                    <div class="panel panel-success-alt noborder">
                        <div class="panel-heading noborder">
                            <div class="panel-btns" style="display: none;">
                                <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                            </div>
                            <!-- panel-btns -->

                            <div class="panel-icon"><i class="fa fa-shopping-cart"></i></div>

                            <div class="media-body">
                                <h5 class="md-title nomargin"><b>Ventas por Despacho</b></h5>
                                <h1 class="mt5"><?php echo $indicators_order_x_despacho['total']['0y30days']['percent']; ?>%</h1>
                            </div>
                            <!-- media-body -->
                            <hr/>
                        </div>
                        <!-- panel-body -->
                        <div class="panel-body noborder">
                            <div class="table-wrapper-data">
                                <table class="table table-striped table-dark table-bordered">
                                <thead>
                                <tr>
                                    <th rowspan="2" style="vertical-align:middle;">Bodega</th>
                                    <th colspan="2" style="border-bottom:solid;border-bottom-width:1px;text-align:center;">0 y 30 d&iacute;as</th>
                                    <th colspan="2" style="border-bottom:solid;border-bottom-width:1px;text-align:center;">30 y 60 d&iacute;as</th>
                                    <th colspan="2" style="border-bottom:solid;border-bottom-width:1px;text-align:center;">60 y 90 d&iacute;as</th>
                                    <th colspan="2" style="border-bottom:solid;border-bottom-width:1px;text-align:center;">90 y 120 d&iacute;as</th>
                                </tr>
                                <tr> 
                                    <th>%</th>
                                    <th>$</th>
                                    <th>%</th>
                                    <th>$</th>
                                    <th>%</th>
                                    <th>$</th>
                                    <th>%</th>
                                    <th>$</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($indicators_order_x_despacho['bodegas'])) { ?>
                                    <?php 
                                    $sum_total_0y30_percent = 0;
                                    $sum_total_0y30_monto = 0;
                                    $sum_total_30y60_percent = 0;
                                    $sum_total_30y60_monto = 0;
                                    $sum_total_60y90_percent = 0;
                                    $sum_total_60y90_monto = 0;
                                    $sum_total_90y120_percent = 0;
                                    $sum_total_90y120_monto = 0;
                                    ?>
                                    <?php foreach ($indicators_order_x_despacho['bodegas'] as $bodega_id => $row) { ?>
                                    <?php
                                        $sum_total_0y30_percent += $row['0y30']['percent'];
                                        $sum_total_0y30_monto += $row['0y30']['monto'];
                                        $sum_total_30y60_percent += $row['30y60']['percent'];
                                        $sum_total_30y60_monto += $row['30y60']['monto'];
                                        $sum_total_60y90_percent += $row['60y90']['percent'];
                                        $sum_total_60y90_monto += $row['60y90']['monto'];
                                        $sum_total_90y120_percent += $row['90y120']['percent'];
                                        $sum_total_90y120_monto += $row['90y120']['monto'];
                                    ?>
                                    <tr>
                                        <td><?php echo $row['nombre']; ?></td>
                                        <td><?php echo $row['0y30']['percent']; ?>%</td>
                                        <td nowrap="nowrap"><?php echo $row['0y30']['monto_formato']; ?></td>
                                        <td><?php echo $row['30y60']['percent']; ?>%</td>
                                        <td nowrap="nowrap"><?php echo $row['30y60']['monto_formato']; ?></td>
                                        <td><?php echo $row['60y90']['percent']; ?>%</td>
                                        <td nowrap="nowrap"><?php echo $row['60y90']['monto_formato']; ?></td>
                                        <td><?php echo $row['90y120']['percent']; ?>%</td>
                                        <td nowrap="nowrap"><?php echo $row['90y120']['monto_formato']; ?></td>
                                    </tr>
                                    <?php } ?>
                                <?php } ?>
                                </tbody>
                                <tfoot>
                                    <th>Total</th>
                                    <th><?php echo $sum_total_0y30_percent;?>%</th>
                                    <th><?php echo $this->utils->numformat($sum_total_0y30_monto);?></th>
                                    <th><?php echo $sum_total_30y60_percent;?>%</th>
                                    <th><?php echo $this->utils->numformat($sum_total_30y60_monto);?></th>
                                    <th><?php echo $sum_total_60y90_percent;?>%</th>
                                    <th><?php echo $this->utils->numformat($sum_total_60y90_monto);?></th>
                                    <th><?php echo $sum_total_90y120_percent;?>%</th>
                                    <th><?php echo $this->utils->numformat($sum_total_90y120_monto);?></th>
                                </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <a href="<?php echo base_url('dashboard/export/clients-despacho'); ?>">exportar</a>
                        </div>

                    </div>
                    <!-- panel -->
                </div>
            </div>
        </div>

    </div>
    <!-- ESTADISTICAS -->
    <?php } else { ?>

    <?php } ?>

</div>
<script type="text/javascript">
  jQuery(function() {
    jQuery(".table-wrapper-data").mCustomScrollbar({
        theme: "ight-thin",
        //axis: "y",
        //axis:"yx",
        setHeight: "330px",
        axis:"yx",
        setWidth: "auto"
        //advanced:{ autoExpandHorizontalScroll:true }
    });
  })
</script>