<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Title</title>
</head>

<body style="-moz-box-sizing: border-box; -ms-text-size-adjust: 100%; -webkit-box-sizing: border-box; -webkit-text-size-adjust: 100%; Margin: 0; box-sizing: border-box; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; min-width: 100%; padding: 0; text-align: left; width: 100% !important;">
<style>
    @media screen and (max-width: 596px) {
        .contact .blocks {
            display: block;
        }
    }

    @media only screen {
        html {
            min-height: 100%;
            background: #f3f3f3;
        }
    }

    @media only screen and (max-width: 596px) {
        .small-float-center {
            margin: 0 auto !important;
            float: none !important;
            text-align: center !important;
        }
        .small-text-center {
            text-align: center !important;
        }
        .small-text-left {
            text-align: left !important;
        }
        .small-text-right {
            text-align: right !important;
        }
    }

    @media only screen and (max-width: 596px) {
        .hide-for-large {
            display: block !important;
            width: auto !important;
            overflow: visible !important;
            max-height: none !important;
            font-size: inherit !important;
            line-height: inherit !important;
        }
    }

    @media only screen and (max-width: 596px) {
        table.body table.container .hide-for-large,
        table.body table.container .row.hide-for-large {
            display: table !important;
            width: 100% !important;
        }
    }

    @media only screen and (max-width: 596px) {
        table.body table.container .callout-inner.hide-for-large {
            display: table-cell !important;
            width: 100% !important;
        }
    }

    @media only screen and (max-width: 596px) {
        table.body table.container .show-for-large {
            display: none !important;
            width: 0;
            mso-hide: all;
            overflow: hidden;
        }
    }

    @media only screen and (max-width: 596px) {
        table.body img {
            width: auto;
            height: auto;
        }
        table.body center {
            min-width: 0 !important;
        }
        table.body .container {
            width: 95% !important;
        }
        table.body .columns,
        table.body .column {
            height: auto !important;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            padding-left: 16px !important;
            padding-right: 16px !important;
        }
        table.body .columns .column,
        table.body .columns .columns,
        table.body .column .column,
        table.body .column .columns {
            padding-left: 0 !important;
            padding-right: 0 !important;
        }
        table.body .collapse .columns,
        table.body .collapse .column {
            padding-left: 0 !important;
            padding-right: 0 !important;
        }
        td.small-1,
        th.small-1 {
            display: inline-block !important;
            width: 8.33333% !important;
        }
        td.small-2,
        th.small-2 {
            display: inline-block !important;
            width: 16.66667% !important;
        }
        td.small-3,
        th.small-3 {
            display: inline-block !important;
            width: 25% !important;
        }
        td.small-4,
        th.small-4 {
            display: inline-block !important;
            width: 33.33333% !important;
        }
        td.small-5,
        th.small-5 {
            display: inline-block !important;
            width: 41.66667% !important;
        }
        td.small-6,
        th.small-6 {
            display: inline-block !important;
            width: 50% !important;
        }
        td.small-7,
        th.small-7 {
            display: inline-block !important;
            width: 58.33333% !important;
        }
        td.small-8,
        th.small-8 {
            display: inline-block !important;
            width: 66.66667% !important;
        }
        td.small-9,
        th.small-9 {
            display: inline-block !important;
            width: 75% !important;
        }
        td.small-10,
        th.small-10 {
            display: inline-block !important;
            width: 83.33333% !important;
        }
        td.small-11,
        th.small-11 {
            display: inline-block !important;
            width: 91.66667% !important;
        }
        td.small-12,
        th.small-12 {
            display: inline-block !important;
            width: 100% !important;
        }
        .columns td.small-12,
        .column td.small-12,
        .columns th.small-12,
        .column th.small-12 {
            display: block !important;
            width: 100% !important;
        }
        table.body td.small-offset-1,
        table.body th.small-offset-1 {
            margin-left: 8.33333% !important;
            Margin-left: 8.33333% !important;
        }
        table.body td.small-offset-2,
        table.body th.small-offset-2 {
            margin-left: 16.66667% !important;
            Margin-left: 16.66667% !important;
        }
        table.body td.small-offset-3,
        table.body th.small-offset-3 {
            margin-left: 25% !important;
            Margin-left: 25% !important;
        }
        table.body td.small-offset-4,
        table.body th.small-offset-4 {
            margin-left: 33.33333% !important;
            Margin-left: 33.33333% !important;
        }
        table.body td.small-offset-5,
        table.body th.small-offset-5 {
            margin-left: 41.66667% !important;
            Margin-left: 41.66667% !important;
        }
        table.body td.small-offset-6,
        table.body th.small-offset-6 {
            margin-left: 50% !important;
            Margin-left: 50% !important;
        }
        table.body td.small-offset-7,
        table.body th.small-offset-7 {
            margin-left: 58.33333% !important;
            Margin-left: 58.33333% !important;
        }
        table.body td.small-offset-8,
        table.body th.small-offset-8 {
            margin-left: 66.66667% !important;
            Margin-left: 66.66667% !important;
        }
        table.body td.small-offset-9,
        table.body th.small-offset-9 {
            margin-left: 75% !important;
            Margin-left: 75% !important;
        }
        table.body td.small-offset-10,
        table.body th.small-offset-10 {
            margin-left: 83.33333% !important;
            Margin-left: 83.33333% !important;
        }
        table.body td.small-offset-11,
        table.body th.small-offset-11 {
            margin-left: 91.66667% !important;
            Margin-left: 91.66667% !important;
        }
        table.body table.columns td.expander,
        table.body table.columns th.expander {
            display: none !important;
        }
        table.body .right-text-pad,
        table.body .text-pad-right {
            padding-left: 10px !important;
        }
        table.body .left-text-pad,
        table.body .text-pad-left {
            padding-right: 10px !important;
        }
        table.menu {
            width: 100% !important;
        }
        table.menu td,
        table.menu th {
            width: auto !important;
            display: inline-block !important;
        }
        table.menu.vertical td,
        table.menu.vertical th,
        table.menu.small-vertical td,
        table.menu.small-vertical th {
            display: block !important;
        }
        table.menu[align="center"] {
            width: auto !important;
        }
        table.button.small-expand,
        table.button.small-expanded {
            width: 100% !important;
        }
        table.button.small-expand table,
        table.button.small-expanded table {
            width: 100%;
        }
        table.button.small-expand table a,
        table.button.small-expanded table a {
            text-align: center !important;
            width: 100% !important;
            padding-left: 0 !important;
            padding-right: 0 !important;
        }
        table.button.small-expand center,
        table.button.small-expanded center {
            min-width: 0;
        }
    }
</style>
<!-- <style> -->
<table class="body" data-made-with-foundation="" style="Margin: 0; background: #f3f3f3; border-collapse: collapse; border-spacing: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; height: 100%; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
    <tbody>
    <tr style="padding: 0; text-align: left; vertical-align: top;">
        <td class="float-center" align="center" valign="top" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0 auto; border-collapse: collapse !important; color: #0a0a0a; float: none; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0 auto; padding: 0; text-align: center; vertical-align: top; word-wrap: break-word;">
            <center style="min-width: 580px; width: 100%;">
                <table class="container" style="Margin: 0 auto; background: #fefefe; border-collapse: collapse; border-spacing: 0; margin: 0 auto; padding: 0; text-align: inherit; vertical-align: top; width: 580px;">
                    <tbody>
                    <tr style="padding: 0; text-align: left; vertical-align: top;">
                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
                            <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
                                <tbody>
                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <th class="large-12 text-center" style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; padding-left: 8px; padding-right: 8px; text-align: center; width: 564px;">
                                        <img src="<?php echo base_url_cdn('assets/site/images/mail/header.png');?>" alt="" style="-ms-interpolation-mode: bicubic; clear: both; display: block; max-width: 100%; outline: none; text-decoration: none; width: auto;">
                                    </th>
                                </tr>
                                </tbody>
                            </table>

                            <table class="row welcome" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
                                <tbody>
                                <tr style="color: #ec8c00; font-size: 30px; font-style: italic; padding: 0; text-align: left; vertical-align: top;">
                                    <th class="large-12 text-center" style="Margin: 0; color: #ec8c00; font-family: Helvetica, Arial, sans-serif; font-size: 30px; font-style: italic; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; padding-left: 8px; padding-right: 8px; text-align: center; width: 564px;">
                                        <?php $nombre = utf8_decode(mb_convert_case($customer->name, MB_CASE_TITLE, "UTF-8"));?>
                                        <span style="color: #ec8c00; font-size: 30px; font-style: italic;">Estimado(a), <span class="name" style="color: #bbc51a; font-size: 30px; font-style: italic;"><?php echo $nombre;?></span></span>
                                    </th>
                                </tr>
                                </tbody>
                            </table>

                            <table class="row" style="border-collapse: collapse; border-spacing: 0; margin-top:10px;display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
                                <tbody>
                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <th class="small-12 large-12 colums" style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; padding-left: 8px; padding-right: 8px; text-align: left; width: 564px;">
                                        <p class="text-center" style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.5; margin: 0; margin-bottom: 10px; padding: 0; text-align: center;">
                                            Muchas gracias por elegir Tiendapet.cl, por favor evaluar nuestro servicio con las siguientes dos pregunta haciendo click en el siguiente bot&oacute;n.
                                        </p>
                                    </th>
                                </tr>
                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">
                                        <center data-parsed="" style="min-width: 532px; width: 100%;">
                                            <table class="button rounded float-center" style="Margin: 0 0 16px 0; border-collapse: collapse; border-spacing: 0; float: none; margin: 0 0 16px 0; padding: 0; text-align: center; vertical-align: top; width: auto;">
                                                <tbody>
                                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
                                                        <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                                                            <tbody>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background: #ec8c00 !important; border: none; border-collapse: collapse !important; border-radius: 500px; color: #fefefe; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;"><a href="https://www.tiendapet.cl/calificar/servicio/<?php echo $order_id;?>" style="Margin: 0; border: 0 solid #2199e8; border-radius: 3px; color: #fefefe; display: inline-block; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: bold; line-height: 1.3; margin: 0; padding: 8px 16px 8px 16px; text-align: left; text-decoration: none;">Evaluar Aqu&iacute;</a></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </center>
                                    </th>
                                    <th class="expander" style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0;"></th>
                                </tr>
                                </tbody>
                            </table>

                            <?php
                            /*
                            <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
                                <tbody>
                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <th class="large-12 text-center" style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; padding-left: 8px; padding-right: 8px; text-align: center; width: 564px;">
                                        <img src="<?php echo base_url_cdn('assets/site/images/mail/gato-perro.png');?>" alt="" class="main-image" style="-ms-interpolation-mode: bicubic; clear: both; display: block; max-width: 100%; outline: none; position: relative; text-decoration: none; width: auto; z-index: 9;">
                                    </th>
                                </tr>
                                </tbody>
                            </table>
                            */
                            ?>

                            <table class="row text-center contact" style="background: #ec8c00; border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: center; vertical-align: top; width: 100%;">
                                <tbody>
                                <tr style="color: #fff; padding: 0; text-align: left; vertical-align: top;">
                                    <th class="small-12 large-12 columns" style="Margin: 0 auto; color: #fff; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 8px; text-align: left; width: 564px;">
                                        <br style="color: #fff;">
                                        <a href="mailto:<?php echo $ajustes->mail;?>" style="text-decoration:none;color:#fff;"><img src="<?php echo base_url_cdn('assets/site/images/mail/email.png');?>" alt="" class="float-center" style="-ms-interpolation-mode: bicubic; Margin: 0 auto; clear: both; color: #fff; display: block; float: none; margin: 0 auto; max-width: 100%; outline: none; text-align: center; text-decoration: none; width: auto;"></a>
                                        <br style="color: #fff;">
                                        <p class="text-center" style="Margin: 0; Margin-bottom: 10px; color: #fff; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: center;"><a href="mailto:<?php echo $ajustes->mail;?>" style="Margin: 0; color: #fff; font-family: Helvetica, Arial, sans-serif; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left; text-decoration: none;"><?php echo $ajustes->mail;?></a></p>
                                    </th>
                                </tr>
                                </tbody>
                            </table>
                            <table class="row text-center contact" style="background: #ec8c00; border-collapse: collapse; border-spacing: 0; color: #fff; display: table; padding: 0; position: relative; text-align: center; vertical-align: top; width: 100%;">
                                <tbody>
                                <tr style="color: #fff; padding: 0; text-align: left; vertical-align: top;">
                                    <!--<th class="small-4 first columns" valign="middle">
                                                    <a href=""><img src="http://www.octano.cl/email/images/email.png" alt=""> ventas@tiendapet.cl</a>
                                                  </th>-->
                                    <th class="small-5 small-offset-1 large-offset-1 large-6 columns" style="Margin: 0 auto; color: #fff; font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 64.33333px; padding-right: 8px; text-align: left; width: 274px;">
                                        <?php
                                        $santiago  = str_replace(')','',str_replace('(','',preg_replace('[\s+]','', $ajustes->telefono_santiago)));
                                        $santiago2 = str_replace(')','',str_replace('(','',preg_replace('[\s+]','', $ajustes->telefono2_santiago)));
                                        ?>
                                        <ul class="r" style="color: #fff; list-style: none; padding: 0;font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: normal;">
                                            <li style="color: #fff;"><strong style="color: #fff;">Santiago</strong></li>
                                            <li style="color: #fff;"><span class="tel" style="color: #fff;"><a href="tel:<?php echo $santiago;?>" style="text-decoration:none;color:#fff;"><?php echo $ajustes->telefono_santiago;?></a></span></li>
                                            <li style="color: #fff;"><span class="tel" style="color: #fff;"><a href="tel:<?php echo $santiago2;?>" style="text-decoration:none;color:#fff;"><?php echo $ajustes->telefono2_santiago;?></a></span></li>
                                        </ul>
                                    </th>
                                    <th class="small-6 large-6 columns" style="Margin: 0 auto; color: #fff; font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 8px; text-align: left; width: 274px;">
                                        <?php
                                        $serena     = str_replace(')','',str_replace('(','',preg_replace('[\s+]','', $ajustes->telefono_serena)));
                                        $vregion    = str_replace(')','',str_replace('(','',preg_replace('[\s+]','', $ajustes->telefono_v_region)));
                                        $concepcion = str_replace(')','',str_replace('(','',preg_replace('[\s+]','', $ajustes->telefono_concepcion)));
                                        $temuco     = str_replace(')','',str_replace('(','',preg_replace('[\s+]','', $ajustes->telefono_temuco)));
                                        ?>
                                        <ul style="color: #fff; list-style: none; padding: 0; font-family: Helvetica, Arial, sans-serif; font-size: 13px; font-weight: normal;">
                                            <li style="color: #fff;"><span class="tel" style="color: #fff;"><strong style="color: #fff;">La serena: </strong><span class="blocks" style="color: #fff;"><a href="tel:<?php echo $serena;?>" style="text-decoration:none;color:#fff;"><?php echo $ajustes->telefono_serena;?></a></span></span>
                                            </li>
                                            <li style="color: #fff;"><span class="tel" style="color: #fff;"><strong style="color: #fff;">V Regi&oacute;n: </strong><span class="blocks" style="color: #fff;"><a href="tel:<?php echo $vregion;?>" style="text-decoration:none;color:#fff;"><?php echo $ajustes->telefono_v_region;?></a></span></span>
                                            </li>
                                            <li style="color: #fff;"><span class="tel" style="color: #fff;"><strong style="color: #fff;">Concepci&oacute;n: </strong><span class="blocks" style="color: #fff;"><a href="tel:<?php echo $concepcion;?>" style="text-decoration:none;color:#fff;"><?php echo $ajustes->telefono_concepcion;?></a></span></span>
                                            </li>
                                            <li style="color: #fff;"><span class="tel" style="color: #fff;"><strong style="color: #fff;">Temuco: </strong><span class="blocks" style="color: #fff;"><a href="tel:<?php echo $temuco;?>" style="text-decoration:none;color:#fff;"><?php echo $ajustes->telefono_temuco;?></a></span></span>
                                            </li>
                                        </ul>
                                    </th>

                                </tr>
                                </tbody>
                            </table>
                            <table class="row footer" style="background: #bbc51a; border-collapse: collapse; border-spacing: 0; color: #fff; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
                                <tbody style="color: #fff;">
                                <tr style="color: #fff; padding: 0; text-align: left; vertical-align: top;">
                                    <th class="small-12 columns" style="Margin: 0 auto; color: #fff; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; text-align: left;">
                                        <br style="color: #fff;">
                                        <p class="text-center" style="Margin: 0; Margin-bottom: 10px; color: #fff; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: center;">S&iacute;guenos en</p>
                                    </th>
                                </tr>
                                </tbody>
                            </table>
                            <table class="row footer" style="background: #bbc51a; border-collapse: collapse; border-spacing: 0; color: #fff; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
                                <tbody style="color: #fff;">
                                <tr style="color: #fff; padding: 0; text-align: left; vertical-align: top;">
                                    <th class="small-offset-3 small-2 columns" style="Margin: 0 auto; color: #fff; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; text-align: left;"><a href="<?php echo $ajustes->facebook;?>" title="Facebook"><img class="float-right" src="<?php echo base_url_cdn('assets/site/images/mail/facebook.png');?>" alt="" style="-ms-interpolation-mode: bicubic; clear: both; color: #fff; display: block; float: right; max-width: 100%; outline: none; text-align: right; text-decoration: none; width: auto;"></a></th>
                                    <th class="small-2 columns" style="Margin: 0 auto; color: #fff; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; text-align: left;"><a href="<?php echo $ajustes->twitter;?>" title="Twitter"><img class="float-center" src="<?php echo base_url_cdn('assets/site/images/mail/twitter.png');?>" alt="" style="-ms-interpolation-mode: bicubic; Margin: 0 auto; clear: both; color: #fff; display: block; float: none; margin: 0 auto; max-width: 100%; outline: none; text-align: center; text-decoration: none; width: auto;"></a></th>
                                    <th class="small-2 columns" style="Margin: 0 auto; color: #fff; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; text-align: left;"><a href="<?php echo $ajustes->instagram;?>" title="Instagram"><img class="float-left" src="<?php echo base_url_cdn('assets/site/images/mail/instagram.png');?>" alt="" style="-ms-interpolation-mode: bicubic; clear: both; color: #fff; display: block; float: left; max-width: 100%; outline: none; text-align: left; text-decoration: none; width: auto;"></a></th>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </center>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>