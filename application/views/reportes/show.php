<div class="contentpanel">

    <div class="row">

        <div class="col-md-12">

            <h2><?php echo $driver->name; ?>
                <small><?php echo $date ?></small>
                <div class="btn-group pull-right">
                    <a href="<?php echo site_url("reportes/guardar"); ?>" class="btn btn-default"><i
                                class="fa fa-floppy-o"></i> Guardar</a>
                    <a href="<?php echo site_url("reportes/imprimir"); ?>" class="btn btn-default"><i class="fa fa-print"></i>
                        Imprimir</a>
                </div>
            </h2>

            <?php foreach ($report as $payment) { ?>
                <p>&nbsp;</p>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo $payment['name']; ?></h3>
                    </div>

                    <div class="panel-body">

                        <?php if ($payment['orders']) { ?>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Cliente</th>
                                    <th>Dirección</th>
                                    <th>Productos</th>
                                    <th>Chofer</th>
                                    <th>Despacho</th>
                                    <th>Total</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php foreach ($payment['orders'] as $o) { ?>

                                    <tr>
                                        <td><?php echo $o->id; ?></td>
                                        <td>
                                            <strong><?php echo $o->customer->name; ?></strong>
                                            <p>
                                                <i class="fa fa-fw fa-phone"></i> <?php echo $o->customer->phone; ?> <br/>
                                                <i class="fa fa-fw fa-envelope"></i> <?php echo $o->customer->email; ?>
                                            </p>
                                        </td>
                                        <td><?php echo $o->address->direccion; ?>, <?php echo $o->address->comuna; ?></td>
                                        <td><?php echo $o->product_count; ?></td>
                                        <td><?php echo $o->driver->name; ?></td>
                                        <td>$<?php echo number_format($o->order_shipping, 0, ',', '.'); ?></td>
                                        <td><?php echo $o->display_price; ?></td>
                                    </tr>

                                <?php } ?>

                                </tbody>

                                <tfoot>
                                <tr>
                                    <th colspan="5"></th>
                                    <th class="text-right"><h5>Total <?php echo $payment['name']; ?>:</h5></th>
                                    <th><h5>$<?php echo number_format($payment['total'], 0, ',', '.'); ?></h5></th>
                                </tr>
                                </tfoot>
                            </table>
                        <?php }else{ ?>
                            <p class="text-center">No se registran pedidos para este tipo de pago</p>
                        <?php } ?>
                    </div>

                </div>
            <?php } ?>

        </div>

    </div>
</div><!-- contentpanel -->