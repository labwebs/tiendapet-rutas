<div class="contentpanel">

    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo form_open("reportes/generar", array("class" => "form-inline form-horizontal")); ?>
                    <div class="form-group">
                        <label class="sr-only" for="date">Desde</label>
                        <input type="text" name="date" class="form-control datepicker-max" id="date"
                               data-maxdate="<?php echo $date; ?>" placeholder="Fecha" value="<?php echo $date; ?>">
                    </div><!-- form-group -->

                    <div class="form-group">
                        <label class="sr-only" for="desde">Chofer</label>
                        <select name="driver" class="select" placeholder="Seleccione chofer">
                            <option></option>
                            <?php foreach ($drivers as $d) { ?>
                                <option value="<?php echo $d->id; ?>"><?php echo $d->name; ?></option>
                            <?php } ?>
                        </select>
                    </div><!-- form-group -->

                    <button type="submit" class="btn btn-primary mr5">Generar reporte</button>

                    </form>
                </div>
            </div>

            <div class="col-md-12">
            </div>
        </div><!-- contentpanel -->