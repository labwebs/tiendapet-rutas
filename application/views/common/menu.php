<div class="leftpanel">

  <h5 class="leftpanel-title">Menú</h5>
  <ul class="nav nav-pills nav-stacked">
    <li <?php echo $current == 'dashboard' && $method == 'index' ? 'class="active "' : 'class=""' ?>>
      <a href="<?php echo site_url('dashboard'); ?>">
        <i class="fa fa-dashboard"></i> <span>Panel de control</span>
      </a>
    </li>

	<?php if ($this->user->is_super_admin) { ?>
    <li <?php echo $current == 'dashboard' && $method == 'indicadoress' ? 'class="active "' : 'class=""' ?>>
        <a href="<?php echo site_url('dashboard/indicadores'); ?>">
            <i class="fa fa-dashboard"></i> <span>Indicadores</span>
        </a>
    </li>
	<?php } ?>

    <li <?php echo $current == 'pedidos' ? 'class="parent active "' : 'class="parent"' ?>>
      <a href="<?php echo site_url('pedidos'); ?>">
        <span class="pull-right badge"><?php echo $stats['today'] + $stats['pending'] + $stats['delivery']; ?></span>
        <i class="fa fa-shopping-cart"></i> <span>Pedidos</span>
      </a>
      <ul class="children">
        <li class="<?php echo $method == 'hoy' ? 'active' : ''; ?>">
          <a href="<?php echo site_url('pedidos/hoy'); ?>"><span class="pull-right badge"><?php echo $stats['today']; ?></span> Para hoy</a>
        </li>
        <li class="<?php echo $method == 'pendientes' ? 'active' : ''; ?>">
          <a href="<?php echo site_url('pedidos/confirmar'); ?>"><span class="pull-right badge"><?php echo $stats['pending']; ?></span> Por confirmar</a>
        </li>
        <li class="<?php echo $method == 'entregar' ? 'active' : ''; ?>">
          <a href="<?php echo site_url('pedidos/entregar'); ?>"><span class="pull-right badge"><?php echo $stats['delivery']; ?></span> Por entregar</a>
        </li>
        <li class="<?php echo $method == 'buscar' ? 'active' : ''; ?>">
          <a href="<?php echo site_url('pedidos/buscar'); ?>">Buscador</a>
        </li>
        <?php if ($this->user->role_id == 2 || $this->user->role_id == 5) {
    ?>
		<li class="<?php echo $method == 'cerrar' ? 'active' : ''; ?>">
            <a href="<?php echo site_url('pedidos/cerrar'); ?>"><span class="pull-right badge"><?php echo $stats['delivery']; ?></span> Por cerrar</a>
        </li>
        <li class="<?php echo $method == 'transacciones' ? 'active' : ''; ?>">
            <a href="<?php echo site_url('pedidos/transacciones'); ?>">Transacciones</a>
        </li>
          <li class="<?php echo $method == 'log' ? 'active' : ''; ?>">
            <a href="<?php echo site_url('pedidos/log'); ?>">Log</a>
          </li>
          <?php
}?>
      </ul>
    </li>
    <?php /*
<li <?= $current == 'clientes' ? 'class="active"' : '' ?>><a href="<?= site_url('clientes'); ?>"><i class="fa fa-users"></i> <span>Clientes</span></a></li>
 */?>
    <?php if (!$this->user->is_tomador_pedidos && !$this->user->is_tomador_pedidos_despacho) { ?>
        <li <?php echo $current == 'choferes' ? 'class="active"' : '' ?>><a href="<?php echo site_url('choferes'); ?>"><i class="fa fa-truck"></i> <span>Choferes</span></a></li>
        <li <?php echo $current == 'reportes' ? 'class="active"' : '' ?>><a href="<?php echo site_url('reportes'); ?>"><i class="fa fa-clipboard"></i> <span>Reportes</span></a></li>
        <?php /* <li <?= $current == 'admin' ? 'class="active"' : '' ?> id="menuadmin"><a href="<?= site_url('admin'); ?>"><i class="fa fa-gear"></i> <span>Administración</span></a></li> */?>
    <?php } ?>

        <?php if ($this->user->is_super_admin) { ?>
            <li <?php echo $current == 'exportar' ? 'class="active"' : '' ?>><a href="<?php echo site_url('exportar'); ?>"><i class="fa fa-file-excel-o"></i> <span>Exportar</span></a></li>
        <?php } ?>

        <?php //if ($this->user->is_super_admin) { ?>
        <li <?php echo $current == 'bsaleapi' ? 'class="parent active "' : 'class="parent"' ?>>
            <a href="<?php echo site_url('bsaleapi'); ?>">
                <i class="fa fa-dot-circle-o"></i> <span>BSale</span>
            </a>
            <ul class="children">
                <li class="<?php echo $method == 'searchbysku' ? 'active' : ''; ?>">
                    <a href="<?php echo site_url('bsaleapi/searchbysku'); ?>"><i class="glyphicon glyphicon-search"></i>  Buscar por SKU</a>
                </li>
            </ul>
        </li>
        <?php //} ?>

    </ul>

</div>
<!-- leftpanel -->

<div class="mainpanel">