<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa <?php echo $icon; ?>"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="<?php echo site_url($this->router->fetch_class()) ?>"><?php echo ucfirst($this->router->fetch_class() ) ?></a></li>
                <li><?php echo $method = $this->router->fetch_method() != 'index' ? ucfirst($this->router->fetch_method()) : '' ?></li>
            </ul>
            <h4>
              <?php echo ucfirst($this->router->fetch_class() ) ?>
              <?php if ($this->router->fetch_class() == "choferes") { ?>
              <a href="<?php echo site_url("choferes/crear"); ?>" class="btn btn-success pull-right">Agregar</a>
              <?php } ?>
            </h4>
            
        </div>
    </div><!-- media -->
    
</div>

<div class="contentpanel">
<?php if ( $error = $this->session->flashdata('error') ) { ?>
<div class="alert alert-danger col-md-12 alert-top">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <p><strong>Epa!</strong></p> <?php echo $error; ?>
</div>
<?php } ?>

<?php if ( $success = $this->session->flashdata('success') ) { ?>
<div class="alert alert-success col-md-12 alert-top">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <strong>Éxito!</strong> <?php echo $success; ?>
</div>
<?php } ?>
</div>
