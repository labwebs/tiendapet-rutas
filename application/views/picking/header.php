<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>
    <?php echo $this->site_name ?>
  </title>


  <link href="<?php echo base_url('assets/css/jquery-ui-1.10.3.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/style.default.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/select2.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/style.datatables.css'); ?>" rel="stylesheet">
  <link href="http://cdn.datatables.net/responsive/1.0.1/css/dataTables.responsive.css" rel="stylesheet" />
  <link href="<?php echo base_url('assets/css/bootstrap-timepicker.min.css'); ?>" rel="stylesheet" />
  <link href="<?php echo base_url('assets/css/custom.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/js/scrollbar/jquery.mCustomScrollbar.min.css'); ?>" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->
  <script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/modernizr.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/pace.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/retina.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.cookies.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/scrollbar/jquery.mCustomScrollbar.concat.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.validate.min.js'); ?>"></script>

  <script src="<?php echo base_url('assets/js/flot/jquery.flot.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/flot/flot.resize.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/flot/flot.symbol.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/flot/flot.crosshair.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/flot/flot.categories.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/flot/flot.pie.min.js'); ?>"></script>

  <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js'); ?>"></script>
  <script src="http://cdn.datatables.net/plug-ins/725b2a2115b/integration/bootstrap/3/dataTables.bootstrap.js"></script>
  <script src="http://cdn.datatables.net/responsive/1.0.1/js/dataTables.responsive.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
  <script src="//cdn.datatables.net/plug-ins/3cfcc339e89/sorting/datetime-moment.js"></script>
  <script src="<?php echo base_url('assets/js/moment.es.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap-timepicker.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/select2.min.js'); ?>"></script>
</head>

<body>

  <header>
    <div class="headerwrapper">
      <div class="header-left picking">
        <a href="<?php echo site_url('picking'); ?>" class="logo">
<img src="<?php echo site_url('assets/images/logo-min.png'); ?>" alt="" />
</a>
      </div>
      <!-- header-left -->

      <div class="header-right">

        <div class="pull-right">
          <div class="btn-group btn-group-option">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-caret-down"></i>
            </button>
            <ul class="dropdown-menu pull-right" role="menu">
              <li><a href="<?php echo site_url('auth/logout'); ?>"><i class="glyphicon glyphicon-log-out"></i>Salir</a></li>
            </ul>
          </div>
          <!-- btn-group -->

        </div>
        <!-- pull-right -->

      </div>
      <!-- header-right -->

    </div>
    <!-- headerwrapper -->
  </header>

  <section>
    <div class="mainwrapper picking">