
<div class="contentpanel">

    <?php if (!$this->user->is_tomador_pedidos && !$this->user->is_tomador_pedidos_despacho) {
    ?>
    <!-- ESTADISTICAS -->
    <div class="row row-stat">

        <div class="col-md-12">
            <div class="col-md-2"></div>
            <div class="col-md-8 text-center">
                <form id="myform-rutas" action="picking" method="post">
                    <fieldset class="form-group form-inline">
                        <label class="-sr-only"><h4>Rutas para</h4></label>
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            <input type="text" class="form-control" id="select_date" name="search[select_date]" placeholder="Fecha" readonly="readonly" value="<?php echo set_value('select_date', (isset($search['select_date']) && $search['select_date'] ? $search['select_date'] : date('Y-m-d'))); ?>" />
                        </div>
                        <label class="button-print-all">
                            <a href="<?php echo site_url("dashboard/printAll"); ?>" target="_blank" class="btn btn-xs btn-success btn-rounded -pull-left"><i class="fa fa-print"></i> Todos</a>
                        </label>

                        <input type="hidden" name="accion" value="search" />
                    </fieldset>
                </form>
            </div>
            <div class="col-md-2"></div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-success-alt noborder">
                <div class="panel-heading noborder">

                    <div class="panel-btns" style="display: none;">
                        <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                    </div><!-- panel-btns -->

                    <div class="panel-icon"><i class="fa fa-truck"></i></div>

                    <div class="media-body">
                        <h5 class="md-title nomargin"><b>Pedidos entregados</b></h5>
                        <h1 class="mt5"><?php echo $stats_dashboard['delivered']; ?></h1>
                    </div><!-- media-body -->

                </div><!-- panel-body -->
            </div><!-- panel -->
        </div>

        <div class="col-md-4">
            <div class="panel panel-danger-alt noborder">
                <div class="panel-heading noborder">

                    <div class="panel-btns" style="display: none;">
                        <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                    </div><!-- panel-btns -->

                    <div class="panel-icon"><i class="fa fa-clock-o"></i></div>

                    <div class="media-body">
                        <h5 class="md-title nomargin">Pedidos por entregar</h5>
                        <h1 class="mt5"><?php echo $stats_dashboard['delivery']; ?></h1>
                    </div><!-- media-body -->

                </div><!-- panel-body -->
            </div><!-- panel -->
        </div>


        <div class="col-md-4">
            <div class="panel panel-dark noborder">
                <div class="panel-heading noborder">

                    <div class="panel-btns" style="display: none;">
                        <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                    </div><!-- panel-btns -->

                    <div class="panel-icon"><i class="fa fa-dollar"></i></div>

                    <div class="media-body">
                        <h5 class="md-title nomargin">Ventas del día</h5>
                        <h1 class="mt5">$ <?php echo number_format($stats_dashboard['sales']['order_total'] + $stats_dashboard['sales']['order_shipping'], 0, ',', '.'); ?></h1>
                    </div><!-- media-body -->

                </div><!-- panel-body -->
            </div><!-- panel -->
        </div>

    </div><!-- ESTADISTICAS -->
    <?php
} else {
        ?>
    <div class="row row-stat">

        <div class="col-md-12">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
                <form id="myform-rutas" action="dashboard" method="post">
                    <fieldset class="form-group form-inline">
                        <label class="-sr-only"><h4>Rutas para</h4></label>
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            <input type="text" class="form-control" id="select_date" name="search[select_date]" placeholder="Fecha" readonly="readonly" value="<?php echo set_value('select_date', (isset($search['select_date']) && $search['select_date'] ? $search['select_date'] : date('Y-m-d'))); ?>" />
                        </div>
                        <input type="hidden" name="accion" value="search" />
                    </fieldset>
                </form>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
    <?php
    }?>

    <div class="row">

        <div class="col-md-6">
            <div class="panel panel-default widget-messaging">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <a href="<?php echo site_url("pedidos/imprimirporconfirmar/"); ?>" target="_blank" class="btn btn-xs btn-success btn-rounded pull-right"><i class="fa fa-print"></i></a>
                            <h3 class="panel-title pull-left">Pedidos por Confirmar: <?php echo !empty($orders_pending) ? count($orders_pending) : '0'; ?></h3>
                        </div>
                    </div>
                </div>

                <ul class="panel-body list-group nopadding tab-pane-scroll">
                    <?php if (!empty($orders_pending)) {
        foreach ($orders_pending as $o) {
            ?>
                        <?php
$background_color = '';
            $color = '';

            if (isset($o->despachoatupinta_bloque_id) && $o->despachoatupinta_bloque_id) {
                $background_color = 'background-color:#ffcc84;';
            } else {
                if ($o->order_iscyber == 1) {
                    $background_color = 'background-color:#1285ED;';
                    $color = 'color:#fff;';
                } elseif (in_array($o->paym_id, array(4, 5, 6, 7))) {
                    $background_color = 'background-color:#b5f1b5;';
                }
            } ?>

                        <li class="list-group-item" style="<?php echo $background_color; ?>">
                            <a href="<?php echo site_url("pedidos/editar/{$o->id}"); ?>" style="<?php echo $color; ?>">
                                <h4 class="sender"><i class="fa fa-fw fa-tag"></i> <?php echo $o->id; ?></h4>
                                <h4 class="sender"><i class="fa fa-fw fa-user"></i> <?php echo $o->customer->name; ?></h4>
                                <h4 class="sender">
                                    <i class="fa fa-map-marker fa-fw"></i> <?php echo $o->address->direccion ?>
                                    <small style="<?php echo $color; ?>">
                                        <?php
if ($o->address->comuna_id == 493) {
                echo isset($locales[$o->address->local_id]) && !empty($locales[$o->address->local_id]) ? $locales[$o->address->local_id]->nombre : '';
            } else {
                echo $o->address->comuna;
            } ?>
                                    </small>
                                    <?php /*<span class="pull-right text-<?php echo $o->paym_id == 4 ? 'success' : 'danger'; ?>"><i class="fa fa-circle"></i></span>*/?>
                                </h4>

                                <div class="row">
                                    <div class="col-xs-4">
                                        <i class="">&nbsp;</i> <?php echo $o->display_price; ?>
                                    </div>
                                    <div class="col-xs-4">
                                        <p><i class="fa fa-dropbox"></i> <?php echo $o->product_count; ?></p>
                                    </div>
                                    <div class="col-xs-4">
                                        <p><i class="fa fa-calendar"></i> <?php echo date('d-m-Y H:i:s', strtotime($o->order_created)); ?></p>
                                    </div>
                                </div>

                            </a>
                        </li>
                    <?php
        } ?>

                    <?php
    }?>
                </ul>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default widget-messaging">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="panel-title pull-left">Choferes</h3>
                        </div>
                    </div>
                </div>
                <ul class="panel-body list-group nopadding tab-pane-scroll">
                    <?php foreach ($drivers as $d) {
        ?>
                        <?php if ($d->inactivo == 1) {
            continue;
        } ?>
                    <li class="list-group-item">
                        <h4 class="sender"><i class="fa fa-fw fa-user"></i> <?php echo mb_convert_case($d->name, MB_CASE_TITLE, "UTF-8"); ?> <a style="margin-top: -5px;" class="btn btn-xs btn-danger btn-rounded pull-right"><?php echo count($d->orders); ?></a></h4>
                    </li>
                    <?php
    }?>
                </ul>
            </div>
        </div>

    </div>


    <?php
/*
<div class="row">
<?php $fecha_mas1day =  date('Y-m-d',strtotime("+1 days")); ?>
<?php $fecha_menos1day =  date('Y-m-d',strtotime("-1 days")); ?>
<form id="myform-rutas" action="dashboard" method="post">
<div class="col-md-12">
<h3 class="pull-left col-md-12">
Rutas para
<select class="-pull-left input-sm" id="search-order_delivery_date" name="search[order_delivery_date]" style="vertical-align: middle;">
<option value="" <?php echo $search['order_delivery_date']=="" ? 'selected="selected"' : '';?>>Hoy</option>
<option value="<?php echo $fecha_menos1day;?>" <?php echo $search['order_delivery_date']==$fecha_menos1day ? 'selected="selected"' : '';?>>Ayer</option>
<option value="<?php echo $fecha_mas1day;?>" <?php echo $search['order_delivery_date']==$fecha_mas1day ? 'selected="selected"' : '';?>>Ma&ntilde;ana</option>
</select>
</h3>
</div>
<input type="hidden" name="accion" value="search" />
</form>
</div>
 */
?>

    <?php //if( ! $this->user->is_tomador_pedidos && ! $this->user->is_tomador_pedidos_despacho){?>
    <!-- CHOFERES -->
    <div class="row dash-choferes">

        <?php foreach ($drivers as $d) {
    ?>
            <?php if ($d->inactivo == 1) {
        continue;
    } ?>
            <div class="col-md-4">
                <div class="panel panel-default widget-messaging">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="title pull-left">
                                    <h3 class="panel-title pull-left"><?php echo $d->name; ?>: <?php echo count($d->orders); ?></h3>
                                </div>
                                <div class="icons pull-right">
                                    <a href="<?php echo site_url("choferes/imprimir/{$d->id}"); ?>" target="_blank" class="btn btn-xs btn-success btn-rounded pull-right" title="Imprimir Pedidos"><i class="fa fa-print"></i></a>
                                    <div class="input-group">
                                        <a id="btn-calendar_<?php echo $d->id; ?>" class="btn btn-xs btn-dark btn-rounded pull-right btn-calendar" style="margin: 0 4px;" title="Asignar Pedidos Automáticamente">
                                            <i class="fa fa-clock-o"></i>
                                        </a>
                                        <input type="text" data-driverid="<?php echo $d->id; ?>" class="form-control select_date_assignorder" style="" id="select_date_assignorder_<?php echo $d->id; ?>" name="aaa" placeholder="Fecha" readonly="readonly" value="" />
                                    </div>
                                    &nbsp;
                                    <a href="<?php echo site_url("choferes/imprimirproductosagrupados/{$d->id}"); ?>" target="_blank" class="btn btn-xs btn-warning btn-rounded pull-right" title="Imprimir Pedidos"><i class="fa fa-print"></i> <i class="fa fa-truck"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <ul class="panel-body list-group nopadding">
                        <?php if ($d->orders) {
                                foreach ($d->orders as $o) {
                        ?>
                            <li class="list-group-item">
                                <a href="<?php echo site_url("pedidos/editar/{$o->id}"); ?>">
                                    <h4 class="sender"><i class="fa fa-fw fa-user"></i> <?php echo $o->customer->name; ?></h4>
                                    <h4 class="sender">
                                        <i class="fa fa-map-marker fa-fw"></i> <?php echo $o->address->direccion ?>
                                        <small>
                                            <?php
                                            if ($o->address->comuna_id == 493) {
                                                echo isset($locales[$o->address->local_id]) && !empty($locales[$o->address->local_id]) ? $locales[$o->address->local_id]->nombre : '';
                                            } else {
                                                echo $o->address->comuna;
                                            } 
                                            ?>
                                        </small>
                                        <span class="pull-right text-<?php echo $o->order_delivered ? 'success' : 'danger'; ?>"><i class="fa fa-circle"></i></span>
                                    </h4>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p><?php echo $o->display_price; ?></p>
                                        </div>
                                        <div class="col-xs-4">
                                            <p><i class="fa fa-dropbox"></i> <?php echo $o->product_count; ?></p>
                                        </div>

                                    </div>
                                </a>
                            </li>
                        <?php
        }
    } else {
        ?>
                            <p>&nbsp;</p>
                            <p class="text-center">No hay despachos para <?php echo isset($search['order_delivery_date']) && $search['order_delivery_date'] ? "ma&ntilde;ana" : "hoy"; ?></p>
                            <p>&nbsp;</p>
                        <?php
    } ?>
                    </ul>

                    <div class="panel-footer text-center small">

                        <?php if (@$d->last_seen->order_delivered) {
        ?>
                            Última entrega: <?php echo date('d M, H:i', strtotime($d->last_seen->order_delivered)); ?>
                        <?php
    } else {
        ?>
                            No hay información de última entrega.
                        <?php
    } ?>
                    </div>
                </div>
            </div>
        <?php
}?>
    </div>
    <?php //}?>

    <form id="myform-assign" action="dashboard/assignOrderToDriver" method="post">
        <input type="hidden" id="driver_id_assign" name="driver_id_assign" />
        <input type="hidden" id="date_assign" name="date_assign" />
    </form>

</div>
<script type="text/javascript">
    jQuery(function () {
        jQuery("#search-order_delivery_date").change(function(){
            jQuery("#myform-rutas").submit();
        });
        $('#select_date').datepicker({
            dateFormat: "yy-mm-dd",
            monthNames: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
            monthNamesShort: ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"],
            dayNames: ["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"],
            dayNamesMin: ["Do","Lu","Ma","Mi","Ju","Vi","Sá"],
            dayNamesShort: ["Dom","Lun","Mar","Mié","Jue","Vie","Sáb"],
            onSelect : function (dateText, inst) {
                $('#myform-rutas').submit(); // <-- SUBMIT
        }});
        <?php
$mindate_day = 0;
if (date('H') == 12) {
    if (date('m') >= 30) {
        $mindate_day = 1;
    }
} elseif (date('H') > 12) {
    $mindate_day = 1;
}
?>
        <?php foreach ($drivers as $d) {
    ?>
        $('#select_date_assignorder_<?php echo $d->id; ?>').datepicker({
            dateFormat: "yy-mm-dd",
            monthNames: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
            monthNamesShort: ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"],
            dayNames: ["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"],
            dayNamesMin: ["Do","Lu","Ma","Mi","Ju","Vi","Sá"],
            dayNamesShort: ["Dom","Lun","Mar","Mié","Jue","Vie","Sáb"],
            minDate: '+<?php echo $mindate_day; ?>d',
            autoclose: true,
            onSelect : function (dateText, inst) {
                $("#driver_id_assign").val($(this).attr("data-driverid"));
                $("#date_assign").val($(this).val());
                $('#myform-assign').submit(); // <-- SUBMIT
        }});
        $('#btn-calendar_<?php echo $d->id; ?>').on('click', function() {
            $('#select_date_assignorder_<?php echo $d->id; ?>').datepicker('show');
        });
        <?php
}?>

        jQuery(".tab-pane-scroll").mCustomScrollbar({
            theme:"ight-thin",
            setHeight: '500px'
        });

    })
</script>