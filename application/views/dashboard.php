
<div class="contentpanel">

    <?php if( ! $this->user->is_tomador_pedidos && ! $this->user->is_tomador_pedidos_despacho){ ?>
    <!-- ESTADISTICAS -->
    <div class="row row-stat">
        
        <?php /*$this->session->set_flashdata('success', 'Se agreg&oacute; correctamente los datos.');*/ ?>

        <?php /*if ($error = $this->session->flashdata('error')) { ?>
        <div class="alert alert-danger col-md-12 alert-top">
            <button class="close" data-close="alert"></button>
            <?php echo $error; ?>
        </div>
        <?php } ?>

        <?php if ($success = $this->session->flashdata('warning')) { ?>
        <div class="alert alert-warning col-md-12 alert-top">
            <button class="close" data-close="alert"></button>
            <?php echo $success; ?>
        </div>
        <?php } ?>

        <?php if ($success = $this->session->flashdata('success')) { ?>
        <div class="alert alert-success col-md-12 alert-top">
            <button class="close" data-close="alert"></button>
            <?php echo $success; ?>
        </div>
        <?php } */?>

        <div class="col-md-12">
            <div class="col-md-2"></div>
            <div class="col-md-8 text-center">
                <form id="myform-rutas" action="dashboard" method="post">
                    <fieldset class="form-group form-inline">
                        <label class="-sr-only"><h4>Rutas para</h4></label>
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            <input type="text" class="form-control" id="select_date" name="search[select_date]" placeholder="Fecha" readonly="readonly" value="<?php echo set_value('select_date', (isset($search['select_date'])&&$search['select_date'] ? $search['select_date'] : date('Y-m-d'))); ?>" />
                        </div>
                        <label class="button-print-all">
                            <a href="<?php echo site_url("dashboard/printAll"); ?>" target="_blank" class="btn btn-xs btn-success btn-rounded -pull-left"><i class="fa fa-print"></i> Todos</a>
                        </label>

                        <input type="hidden" name="accion" value="search" />
                    </fieldset>
                </form>
            </div>
            <div class="col-md-2"></div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-success-alt noborder">
                <div class="panel-heading noborder">

                    <div class="panel-btns" style="display: none;">
                        <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                    </div><!-- panel-btns -->

                    <div class="panel-icon"><i class="fa fa-truck"></i></div>

                    <div class="media-body">
                        <h5 class="md-title nomargin"><b>Pedidos entregados</b></h5>
                        <h1 class="mt5"><?php echo $stats_dashboard['delivered'];?></h1>
                    </div><!-- media-body -->

                </div><!-- panel-body -->
            </div><!-- panel -->
        </div>

        <div class="col-md-4">
            <div class="panel panel-danger-alt noborder">
                <div class="panel-heading noborder">

                    <div class="panel-btns" style="display: none;">
                        <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                    </div><!-- panel-btns -->

                    <div class="panel-icon"><i class="fa fa-clock-o"></i></div>

                    <div class="media-body">
                        <h5 class="md-title nomargin">Pedidos por entregar</h5>
                        <h1 class="mt5"><?php echo $stats_dashboard['delivery'];?></h1>
                    </div><!-- media-body -->

                </div><!-- panel-body -->
            </div><!-- panel -->
        </div>

        <div class="col-md-4">
            <div class="panel panel-dark noborder">
                <div class="panel-heading noborder">

                    <div class="panel-btns" style="display: none;">
                        <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                    </div><!-- panel-btns -->

                    <div class="panel-icon"><i class="fa fa-dollar"></i></div>

                    <div class="media-body">
                        <h5 class="md-title nomargin">Ventas del día</h5>
                        <h1 class="mt5">$ <?php echo number_format($stats_dashboard['sales']['order_total']+$stats_dashboard['sales']['order_shipping'], 0, ',', '.');?></h1>
                    </div><!-- media-body -->

                </div><!-- panel-body -->
            </div><!-- panel -->
        </div>

    </div><!-- ESTADISTICAS -->

    <?php }else{ ?>

    <div class="row row-stat">
        <div class="col-md-12">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
                <form id="myform-rutas" action="dashboard" method="post">
                    <fieldset class="form-group form-inline">
                        <label class="-sr-only"><h4>Rutas para</h4></label>
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            <input type="text" class="form-control" id="select_date" name="search[select_date]" placeholder="Fecha" readonly="readonly" value="<?php echo set_value('select_date', (isset($search['select_date'])&&$search['select_date'] ? $search['select_date'] : date('Y-m-d'))); ?>" />
                        </div>
                        <input type="hidden" name="accion" value="search" />
                    </fieldset>
                </form>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
    <?php } ?>

    <div class="row">

        <div class="col-md-6 dash-orders-to-beconfirmed">
            <div class="panel panel-default widget-messaging">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <a href="<?php echo site_url("pedidos/imprimirporconfirmar/"); ?>" target="_blank" class="btn btn-xs btn-success btn-rounded pull-right"><i class="fa fa-print"></i></a>
                            <h3 class="panel-title pull-left">Pedidos por Confirmar: <?php echo ! empty($orders_pending) ? count($orders_pending) : '0';?></h3>
                        </div>
                    </div>
                </div>

                <form id="myform-asingar-chofer-a-pedidos" name="myform-asingar-chofer-a-pedidos" class="form-assign form-horizontal" action="<?php echo base_url('dashboard/assignOrderConfirmedToDriver');?>" method="post">
                    <div class="form-group form-filter">
                        <div class="row-">
                            <div class="col-md-1">
                                <div class="form-group">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input checkAll" type="checkbox" name="checkallorders" id="checkallorders">
                                        <label class="form-check-label checkAll-label" for="checkallorders"> Check Todos</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span id="btn_calendar_assign_order_confirmed" class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        <input type="text" class="form-control" id="select_date_assign_order_confirmed" name="date_assign_order_confirmed" placeholder="Fecha" readonly="readonly" value="<?php date('Y-m-d');?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select name="driver_id_assign_order_confirmed" class="select2 form-control-">
                                    <?php foreach($drivers_selects as $select){ ?>
                                        <option value="<?php echo $select->id;?>"><?php echo $select->admin_name; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group text-center">
                                    <button class="btn-group-ok">
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> 
                                        <span class="span-text-aplicar"> Aplicar</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
            
                    <ul class="panel-body list-group nopadding tab-pane-scroll">
                    <?php if ( ! empty($orders_pending) ) { ?>
                        <?php foreach($orders_pending as $o) { ?>
                        <?php
                            //$o = $this->orders->get_products($o);
                            $background_color = '';
                            $color = '';
                            if( isset($o->despachoatupinta_bloque_id) && $o->despachoatupinta_bloque_id ){
                                $background_color = 'background-color:#ffcc84;';
                            }else{
                                if( $o->order_iscyber == 1 ){
                                    $background_color = 'background-color:#1285ED;';
                                    $color = 'color:#fff;';
                                }else if( in_array($o->paym_id, array(4, 5, 6, 7)) ){
                                    $background_color = 'background-color:#b5f1b5;';
                                }else if( in_array($o->paym_id, array(101)) ){
                                    $background_color = 'background-color:#ffc9c7;';
                                }
                            }
                            if( isset($o->products) && ! empty($o->products) ){
                                foreach ($o->products as $indice => $pro) {
                                    $categorias = isset($pro->description->categorias) ? $pro->description->categorias : array();
                                    if( ! empty($categorias) ){
                                        foreach ($categorias as $cat) {
                                            $paths = isset($cat->path) && ! empty($cat->path) ? array_keys($cat->path) : array();
                                            if ( in_array(124, $paths) || in_array(125, $paths) ) {
                                                $background_color = 'background-color:#ffc18e;';
                                                break 2;
                                            }
                                        }
                                    }
                                }
                            }
                        ?>

                        <li class="list-group-item" style="<?php echo $background_color;?>">
                            <a href="<?php echo site_url("pedidos/editar/{$o->id}"); ?>" style="<?php echo $color;?>">
                                <div>
                                    <input type="checkbox" name="checks[]" value="<?php echo $o->id;?>" class="check-child-assign">
                                </div>
                                <h4 class="sender"><i class="fa fa-fw fa-tag"></i> <?php echo $o->id;?></h4>
                                <h4 class="sender"><i class="fa fa-fw fa-user"></i> <?php echo $o->customer->name; ?></h4>
                                <h4 class="sender">
                                    <i class="fa fa-map-marker fa-fw"></i> <?php echo $o->address->direccion ?>
                                    <small style="<?php echo $color;?>">
                                        <?php
                                        if($o->address->comuna_id == 493){
                                            echo isset($locales[$o->address->local_id]) && ! empty($locales[$o->address->local_id]) ? $locales[$o->address->local_id]->nombre : '';
                                        }else{
                                            echo $o->address->comuna;
                                        }
                                        ?>
                                    </small>
                                    <?php /*<span class="pull-right text-<?php echo $o->paym_id == 4 ? 'success' : 'danger'; ?>"><i class="fa fa-circle"></i></span>*/?>
                                </h4>

                                <div class="row">
                                    <div class="col-xs-4">
                                        <i class="">&nbsp;</i> <?php echo $o->display_price; ?>
                                    </div>
                                    <div class="col-xs-4">
                                        <p><i class="fa fa-dropbox"></i> <?php echo $o->product_count; ?></p>
                                    </div>
                                    <div class="col-xs-4">
                                        <p><i class="fa fa-calendar"></i> <?php echo date('d-m-Y H:i:s', strtotime($o->order_created));?></p>
                                    </div>
                                </div>

                            </a>
                        </li>
                        <?php } ?>

                    <?php }else{ ?>
                        <li>
                            <p>&nbsp;</p>
                            <p class="text-center">No hay despachos para <?php echo isset($search['order_delivery_date']) && $search['order_delivery_date'] ? "ma&ntilde;ana" : "hoy";?></p>
                            <p>&nbsp;</p>
                        </li>
                    <?php } ?>
                    </ul>
                </form>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default widget-messaging">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="panel-title pull-left">Choferes</h3>
                        </div>
                    </div>
                </div>
                <ul class="panel-body list-group nopadding tab-pane-scroll">
                    <?php foreach($drivers as $d){ ?>
                        <?php if($d->inactivo == 1) continue; ?>
                    <li class="list-group-item">
                        <h4 class="sender"><i class="fa fa-fw fa-user"></i> <?php echo mb_convert_case($d->name, MB_CASE_TITLE, "UTF-8");?> <a style="margin-top: -5px;" class="btn btn-xs btn-danger btn-rounded pull-right"><?php echo count($d->orders);?></a></h4>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>

    </div>


    <?php
    /*
    <div class="row">
        <?php $fecha_mas1day =  date('Y-m-d',strtotime("+1 days")); ?>
        <?php $fecha_menos1day =  date('Y-m-d',strtotime("-1 days")); ?>
        <form id="myform-rutas" action="dashboard" method="post">
            <div class="col-md-12">
                <h3 class="pull-left col-md-12">
                    Rutas para
                    <select class="-pull-left input-sm" id="search-order_delivery_date" name="search[order_delivery_date]" style="vertical-align: middle;">
                        <option value="" <?php echo $search['order_delivery_date']=="" ? 'selected="selected"' : '';?>>Hoy</option>
                        <option value="<?php echo $fecha_menos1day;?>" <?php echo $search['order_delivery_date']==$fecha_menos1day ? 'selected="selected"' : '';?>>Ayer</option>
                        <option value="<?php echo $fecha_mas1day;?>" <?php echo $search['order_delivery_date']==$fecha_mas1day ? 'selected="selected"' : '';?>>Ma&ntilde;ana</option>
                    </select>
                </h3>
            </div>
            <input type="hidden" name="accion" value="search" />
        </form>
    </div>
    */
    ?>

    <?php //if( ! $this->user->is_tomador_pedidos && ! $this->user->is_tomador_pedidos_despacho){ ?>
    
    <!-- CHOFER SIMPLIROUTE -->
    <?php if( ! $this->user->is_tomador_pedidos && ! $this->user->is_tomador_pedidos_despacho){ ?>
        <?php if( isset($orders_simpleroute->id) && $orders_simpleroute->id){ ?>
        <div class="row dash-simpliroute">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-default widget-messaging">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12 p-0">
                                <div class="title pull-left center-vertical">
                                    <h3 class="panel-title pull-left"><?php echo $orders_simpleroute->name; ?>: <?php echo count($orders_simpleroute->orders); ?></h3>
                                </div>
                                <div class="icons pull-right">
                                    <a href="<?php echo site_url("choferes/imprimir/{$orders_simpleroute->id}"); ?>" target="_blank" class="btn btn-xs btn-success btn-rounded pull-right" title="Imprimir Pedidos"><i class="fa fa-print"></i></a>
                                    <div class="input-group">
                                        <a id="btn-calendar_<?php echo $orders_simpleroute->id;?>" class="btn btn-xs btn-dark btn-rounded pull-right btn-calendar" style="margin: 0 4px;" title="Asignar Pedidos Automáticamente">
                                            <i class="fa fa-clock-o"></i>
                                        </a>
                                        <input type="text" data-driverid="<?php echo $orders_simpleroute->id;?>" class="form-control select_date_assignorder" style="" id="select_date_assignorder_<?php echo $orders_simpleroute->id;?>" name="aaa" placeholder="Fecha" readonly="readonly" value="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <form id="myform-action" name="myform-action" class="form-move form-horizontal" action="<?php echo base_url('dashboard/moveOrderToDriver');?>" method="post">
                        <div class="form-group form-filter">
                            <div class="col-md-12">
                                <div class="form-radio-inline">
                                    <div class="col-xs-4 col-sm-4 text-center">
                                        <label class="radio-inline"><input type="radio" name="optaction" data-action="move" value="<?php echo base_url('dashboard/moveOrderToDriver');?>" checked>Mover Pedidos</label>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 text-center">
                                        <label class="radio-inline"><input type="radio" name="optaction" data-action="geocode" value="<?php echo base_url('dashboard/geocodeSimpliroute');?>">Georeferenciar</label>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 text-center">
                                        <label class="radio-inline"><input type="radio" name="optaction" data-action="send" value="<?php echo base_url('dashboard/sendToSimpliroute');?>">Enviar a SimpliRoute</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-filter">
                            <div class="col-md-6 wrapper-move">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span id="btn_calendar_move_<?php echo $orders_simpleroute->id;?>" class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        <input type="text" class="form-control" id="select_date_move_<?php echo $orders_simpleroute->id;?>" name="date_move" placeholder="Fecha" readonly="readonly" value="<?php date('Y-m-d');?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 wrapper-move">
                                <div class="form-group">
                                    <select name="driver_id_move" class="select2 form-control-">
                                    <?php foreach($drivers_selects as $select){ ?>
                                        <?php $selected = $select->id == $orders_simpleroute->id ? 'selected="selected"' : ''; ?> 
                                        <option value="<?php echo $select->id;?>" <?php echo $selected;?>><?php echo $select->admin_name; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <?php if( $driversAllSR ){ ?>
                            <div class="col-md-12 wrapper-driver" style="display:none;">
                                <div class="panel-group" id="accordion">
                                    <div class="panel panel-default">
                                        <div class="panel-heading accordion">
                                            <h4 class="panel-title text-center">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="title-accordion">
                                                    Choferes
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse out">
                                            <div class="panel-body">
                                                <ul class="panel-body list-group nopadding">
                                                <?php foreach($driversAllSR as $key => $dri){ ?>
                                                    <li class="list-group-item">
                                                        <h4 class="sender">
                                                            <i class="fa fa-fw fa-user"></i> <?php echo mb_convert_case($dri->admin_name, MB_CASE_TITLE, "UTF-8");?>
                                                            <div class="pull-right text-center" style="margin-top: -4px;">
                                                                <?php /*<input type="checkbox" name="checksr_<?php echo $dri->id;?>" data-id="<?php echo $dri->id;?>" id="checksr_<?php echo $dri->id;?>" value="1" class="checksr_dsr" style="display:none;">*/ ?>
                                                                <div id="toogle_<?php echo $dri->id;?>" data-choferid="<?php echo $dri->chofer_id;?>" class="toggle toggle-success" style="margin: 0 auto;width: 78px; height: 24px;"></div>
                                                            </div>
                                                        </h4>
                                                    </li>
                                                <?php } ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div><!-- panel -->
                                    
                                </div><!-- panel-group -->
                            </div>
                            <?php } ?>

                            <div class="col-xs-12 col-sm-12 col-md-12 wrapper-driver" style="display:none;">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <div class="form-group text-center">
                                        <label for="tipo_opt">Tipo de Optimizaci&oacute;n</label>
                                        <select id="tipo_opt" name="tipo_opt" class="select2">
                                        <?php foreach($tipo_opt_sr as $tipo){ ?>
                                            <option value="<?php echo $tipo['tipo'];?>"><em><?php echo $tipo['nombre']; ?></em></option>
                                        <?php } ?>
                                        </select>
                                        <p>&nbsp;</p>
                                    </div>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 wrapper-driver" style="display:none;">
                                <?php foreach($tipo_opt_sr as $tipo){ ?>
                                    <p id="tipo-<?php echo $tipo['tipo'];?>" class="tipo-opt-texto text-center" style="display:none;"><?php echo $tipo['texto']; ?></p>
                                <?php } ?>
                                <p>&nbsp;</p>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group pull-right">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input checkAll" type="checkbox" name="checkall" id="check-all-<?php echo $orders_simpleroute->id;?>" data-driverid="<?php echo $orders_simpleroute->id;?>">
                                        <label class="form-check-label" for="check-all-<?php echo $orders_simpleroute->id;?>"> Check Todos</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group pull-left">
                                    <button class="btn-group-ok">
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> 
                                        <span> Aplicar</span>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <ul class="panel-body list-group nopadding -tab-pane-scroll-">
                            <?php if ( ! empty($orders_simpleroute->orders) ) { ?> 
                                <?php foreach($orders_simpleroute->orders as $o) { ?>
                                <li class="list-group-item">
                                    <a href="<?php echo site_url("pedidos/editar/{$o->id}"); ?>">
                                        <div>
                                            <input type="checkbox" name="checks[]" value="<?php echo $o->id;?>" class="check-child-<?php echo $orders_simpleroute->id;?>">
                                        </div>
                                        <h4 class="sender"><i class="fa fa-fw fa-user"></i> <?php echo $o->customer->name; ?></h4>
                                        <h4 class="sender">
                                            <?php if( $o->address->latitude && $o->address->longitude ){ ?>
                                                <span class="badge badge-success"><i class="fa fa-map-marker fa-fw"></i>  </span>
                                            <?php }else{ ?>
                                                <span class="badge badge-danger"><i class="fa fa-map-marker fa-fw"></i>  </span>
                                            <?php } ?>
                                            <?php echo $o->address->direccion ?>
                                            <small>
                                                <?php
                                                if($o->address->comuna_id == 493){
                                                    echo isset($locales[$o->address->local_id]) && ! empty($locales[$o->address->local_id]) ? $locales[$o->address->local_id]->nombre : '';
                                                }else{
                                                    echo $o->address->comuna;
                                                }
                                                ?>
                                            </small>
                                            <span class="pull-right text-<?php echo $o->order_delivered ? 'success' : 'danger'; ?>"><i class="fa fa-circle"></i></span>
                                        </h4>
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <p><?php echo $o->display_price; ?></p>
                                            </div>
                                            <div class="col-xs-4">
                                                <p><i class="fa fa-dropbox"></i> <?php echo $o->product_count; ?></p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <?php } ?>
                            <?php }else{ ?>
                                <p>&nbsp;</p>
                                <p class="text-center">No hay despachos para <?php echo isset($search['order_delivery_date']) && $search['order_delivery_date'] ? "ma&ntilde;ana" : "hoy";?></p>
                                <p>&nbsp;</p>
                            <?php } ?>
                        </ul>
                    </form>

                    <div class="panel-footer text-center small">
                        <?php if (@$d->last_seen->order_delivered) { ?>
                            Última entrega: <?php echo date('d M, H:i', strtotime($d->last_seen->order_delivered)); ?>
                        <?php }else{ ?>
                            No hay información de última entrega.
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>    
        <?php } ?>
    <?php } ?>

    <!-- CHOFERES -->
    <div class="row dash-choferes">

        <?php foreach($drivers as $d){ ?>
            <?php if($d->username == 'simpliroute') continue; ?>
            <?php if($d->inactivo == 1) continue; ?>
            <div class="col-md-4">
                <div class="panel panel-default widget-messaging">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12 p-0">
                                <div class="title pull-left center-vertical">
                                    <h3 class="panel-title pull-left"><?php echo $d->name; ?>: <?php echo count($d->orders); ?></h3>
                                </div>
                                <div class="icons pull-right">
                                    <a href="<?php echo site_url("choferes/imprimir/{$d->id}"); ?>" target="_blank" class="btn btn-xs btn-success btn-rounded pull-right" title="Imprimir Pedidos"><i class="fa fa-print"></i></a>
                                    <div class="input-group">
                                        <a id="btn-calendar_<?php echo $d->id;?>" class="btn btn-xs btn-dark btn-rounded pull-right btn-calendar" style="margin: 0 4px;" title="Asignar Pedidos Automáticamente">
                                            <i class="fa fa-clock-o"></i>
                                        </a>
                                        <input type="text" data-driverid="<?php echo $d->id;?>" class="form-control select_date_assignorder" style="" id="select_date_assignorder_<?php echo $d->id;?>" name="aaa" placeholder="Fecha" readonly="readonly" value="" />
                                    </div>
                                    &nbsp;
                                    <a href="<?php echo site_url("choferes/imprimirproductosagrupados/{$d->id}"); ?>" target="_blank" class="btn btn-xs btn-warning btn-rounded pull-right" title="Imprimir Pedidos"><i class="fa fa-print"></i> <i class="fa fa-truck"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <form id="myform-move-<?php echo $d->id;?>" name="myform-move-<?php echo $d->id;?>" class="form-move form-horizontal" action="<?php echo base_url('dashboard/moveOrderToDriver');?>" method="post">
                        <div class="--filtro-group-fecha form-group form-filter">
                            <div class="row-">
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input checkAll" type="checkbox" name="checkall" id="check-all-<?php echo $d->id;?>" data-driverid="<?php echo $d->id;?>">
                                            <label class="form-check-label checkAll-label" for="check-all-<?php echo $d->id;?>"> Check Todos</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span id="btn_calendar_move_<?php echo $d->id;?>" class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input type="text" class="form-control" id="select_date_move_<?php echo $d->id;?>" name="date_move" placeholder="Fecha" readonly="readonly" value="<?php date('Y-m-d');?>" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select name="driver_id_move" class="select2 form-control-">
                                        <?php foreach($drivers_selects as $select){ ?>
                                            <?php $selected = $select->id == $d->id ? 'selected="selected"' : ''; ?> 
                                            <option value="<?php echo $select->id;?>" <?php echo $selected;?>><?php echo $select->admin_name; ?></option>
                                        <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group text-center">
                                        <button class="btn-group-ok">
                                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> 
                                            <span class="span-text-aplicar"> Aplicar</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="panel-body list-group nopadding -tab-pane-scroll-">
                            <?php if ($d->orders ) { foreach($d->orders as $o) { ?>
                                <li class="list-group-item">
                                    <a href="<?php echo site_url("pedidos/editar/{$o->id}"); ?>">
                                        <div>
                                            <input type="checkbox" name="checks[]" value="<?php echo $o->id;?>" class="check-child-<?php echo $d->id;?>">
                                        </div>
                                        <h4 class="sender"><i class="fa fa-fw fa-user"></i> <?php echo $o->customer->name; ?></h4>
                                        <h4 class="sender">
                                            <i class="fa fa-map-marker fa-fw"></i> <?php echo $o->address->direccion ?>
                                            <small>
                                                <?php
                                                if($o->address->comuna_id == 493){
                                                    echo isset($locales[$o->address->local_id]) && ! empty($locales[$o->address->local_id]) ? $locales[$o->address->local_id]->nombre : '';
                                                }else{
                                                    echo $o->address->comuna;
                                                }
                                                ?>
                                            </small>
                                            <span class="pull-right text-<?php echo $o->order_delivered ? 'success' : 'danger'; ?>"><i class="fa fa-circle"></i></span>
                                        </h4>
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <p><?php echo $o->display_price; ?></p>
                                            </div>
                                            <div class="col-xs-4">
                                                <p><i class="fa fa-dropbox"></i> <?php echo $o->product_count; ?></p>
                                            </div>

                                        </div>
                                    </a>
                                </li>
                            <?php } }else{ ?>
                                <p>&nbsp;</p>
                                <p class="text-center">No hay despachos para <?php echo isset($search['order_delivery_date']) && $search['order_delivery_date'] ? "ma&ntilde;ana" : "hoy";?></p>
                                <p>&nbsp;</p>
                            <?php } ?>
                        </ul>
                    </form>

                    <div class="panel-footer text-center small">

                        <?php if (@$d->last_seen->order_delivered) { ?>
                            Última entrega: <?php echo date('d M, H:i', strtotime($d->last_seen->order_delivered)); ?>
                        <?php }else{ ?>
                            No hay información de última entrega.
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <?php //} ?>

    <form id="myform-assign" action="dashboard/assignOrderToDriver" method="post">
        <input type="hidden" id="driver_id_assign" name="driver_id_assign" />
        <input type="hidden" id="date_assign" name="date_assign" />
    </form>

</div>
<script type="text/javascript">
    jQuery(function () {

        <?php foreach($driversAllSR as $key => $dri){ ?>
            jQuery('#toogle_<?php echo $dri->id;?>').toggles({
                on: <?php echo $dri->inactivo_sr == 1 ? "false" : "true";?>,
                text:{
                    on: 'Activo',
                    off: 'Inactivo'
                },
                //checkbox: jQuery('#checksr_<?php echo $dri->id;?>')
            });
            // Getting notified of changes, and the new state:
            $('#toogle_<?php echo $dri->id;?>').on('toggle', function(e, active) {
                jQuery.ajax({
                    type: "POST",
                    url: "/ajax/updStatusChoferSR",
                    data: {"checked":active, "chofer_id": $(this).attr("data-choferid")},
                    dataType: 'json',
                    success: function (json) {

                    }
                });
                if (active) {
                    console.log('Toggle is now ON!');
                } else {
                    console.log('Toggle is now OFF!');
                }
            });
        <?php } ?>
        /*
        jQuery(".checksr_dsr").each(function(){
            var thisCheckAll = $(this);
            thisCheckAll.change(function(){
                if ( this.checked ) {
                    alert("checked");
                } else {
                    alert("no-checked");
                }       
            });
        });
        */
        jQuery("#search-order_delivery_date").change(function(){
            jQuery("#myform-rutas").submit();
        });
        $('#select_date').datepicker({
            dateFormat: "yy-mm-dd",
            monthNames: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
            monthNamesShort: ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"],
            dayNames: ["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"],
            dayNamesMin: ["Do","Lu","Ma","Mi","Ju","Vi","Sá"],
            dayNamesShort: ["Dom","Lun","Mar","Mié","Jue","Vie","Sáb"],
            onSelect : function (dateText, inst) {
                $('#myform-rutas').submit(); // <-- SUBMIT
        }});

        $("input[name=optaction]").change(function () {	 
            $("#myform-action").attr("action", $(this).val());
            if( $(this).attr('data-action') == 'move' ){
                $(".wrapper-move").show();
                $(".wrapper-driver").hide();
            }else if( $(this).attr('data-action') == 'send' ){
                $(".wrapper-move").hide();
                $(".wrapper-driver").show();
            }else{
                $(".wrapper-move").hide();
                $(".wrapper-driver").hide();
            }
        });
        $("input[name=optaction]:checked").trigger("change");

        $("#tipo_opt").change(function(){
            $(".tipo-opt-texto").hide();
            $("#tipo-"+$(this).val()).show();
        });
        $("#tipo_opt").trigger("change");

        <?php
        $mindate_day=0;
        if( date('H') == 12 ){
            if(date('m') >= 30){
                $mindate_day=1;
            }
        }else if( date('H') > 12 ){
            $mindate_day=1;
        }

        $mindate_day_masivo=0;
        if( date('H') >= 18 ){
            $mindate_day_masivo=1;
        }
        ?>
        <?php foreach($drivers as $d){ ?>
            $('#select_date_assignorder_<?php echo $d->id;?>').datepicker({
                dateFormat: "yy-mm-dd",
                monthNames: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
                monthNamesShort: ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"],
                dayNames: ["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"],
                dayNamesMin: ["Do","Lu","Ma","Mi","Ju","Vi","Sá"],
                dayNamesShort: ["Dom","Lun","Mar","Mié","Jue","Vie","Sáb"],
                minDate: '+<?php echo $mindate_day;?>d',
                autoclose: true,
                onSelect : function (dateText, inst) {
                    $("#driver_id_assign").val($(this).attr("data-driverid"));
                    $("#date_assign").val($(this).val());
                    $('#myform-assign').submit(); // <-- SUBMIT
            }});
            $('#btn-calendar_<?php echo $d->id;?>').on('click', function() {
                $('#select_date_assignorder_<?php echo $d->id;?>').datepicker('show');
            });

            $('#select_date_move_<?php echo $d->id;?>').datepicker({
                dateFormat: "yy-mm-dd",
                monthNames: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
                monthNamesShort: ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"],
                dayNames: ["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"],
                dayNamesMin: ["Do","Lu","Ma","Mi","Ju","Vi","Sá"],
                dayNamesShort: ["Dom","Lun","Mar","Mié","Jue","Vie","Sáb"],
                minDate: '+<?php echo $mindate_day_masivo;?>d',
                autoclose: true,
                onSelect : function (dateText, inst) {
                    //$("#driver_id_assign").val($(this).attr("data-driverid"));
                    //$("#date_assign").val($(this).val());
                    //$('#myform-assign').submit(); // <-- SUBMIT
            }});
            $('#btn_calendar_move_<?php echo $d->id;?>').on('click', function() {
                $('#select_date_move_<?php echo $d->id;?>').datepicker('show');
            });
        <?php } ?>

        $('#select_date_assign_order_confirmed').datepicker({
            dateFormat: "yy-mm-dd",
            monthNames: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
            monthNamesShort: ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"],
            dayNames: ["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"],
            dayNamesMin: ["Do","Lu","Ma","Mi","Ju","Vi","Sá"],
            dayNamesShort: ["Dom","Lun","Mar","Mié","Jue","Vie","Sáb"],
            minDate: '+<?php echo $mindate_day;?>d',
            autoclose: true,
            onSelect : function (dateText, inst) {
                
        }});
        $('#btn_calendar_assign_order_confirmed').on('click', function() {
            $('#select_date_assign_order_confirmed').datepicker('show');
        });

        jQuery(".tab-pane-scroll").mCustomScrollbar({
            theme:"ight-thin",
            setHeight: '500px'
        });
        
        jQuery(".dash-orders-to-beconfirmed .checkAll").each(function(){
            var thisCheckAll = $(this);
            thisCheckAll.change(function(){
                if ( this.checked ) {
                    $('.check-child-assign').attr('checked','checked');
                } else {
                    $('.check-child-assign').removeAttr('checked');
                }       
            });
        });

        jQuery(".dash-choferes .checkAll").each(function(){
            var thisCheckAll = $(this);
            var driverid = thisCheckAll.attr("data-driverid");
            thisCheckAll.change(function(){
                if ( this.checked ) {
                    $('.check-child-'+driverid).attr('checked','checked');
                } else {
                    $('.check-child-'+driverid).removeAttr('checked');
                }       
            });
        });
        jQuery(".dash-simpliroute .checkAll").each(function(){
            var thisCheckAll = $(this);
            var driverid = thisCheckAll.attr("data-driverid");
            thisCheckAll.change(function(){
                if ( this.checked ) {
                    $('.check-child-'+driverid).attr('checked','checked');
                } else {
                    $('.check-child-'+driverid).removeAttr('checked');
                }       
            });
        });
    })
</script>