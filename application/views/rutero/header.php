<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?php echo $this->site_name ?></title>
        
        
        <link href="<?php echo site_url('assets/css/jquery-ui-1.10.3.css'); ?>" rel="stylesheet">
        <link href="<?php echo site_url('assets/css/style.default.css'); ?>" rel="stylesheet">
        <link href="<?php echo site_url('assets/css/select2.css'); ?>" rel="stylesheet">
        <link href="<?php echo site_url('assets/css/custom.css'); ?>" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        <script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/modernizr.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/toggles.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/custom.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/ruteros.tiendapet.js'); ?>"></script>
    </head>

    <body>
        
        <header>
            <div class="headerwrapper">
                <div class="header-left rutero">
                    <a href="<?php echo site_url('rutero'); ?>" class="logo">
                        <img src="<?php echo site_url('assets/images/logo-min.png'); ?>" alt="" />
                    </a>
                </div><!-- header-left -->
                
                <div class="header-right">
                    
                    <div class="pull-right">
                        <div class="btn-group btn-group-option">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                              <i class="fa fa-caret-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right" role="menu">
                              <?php if( $this->user->is_tomador_pedidos_despacho ){ ?>
                              <li><a href="<?php echo site_url('/'); ?>"><i class="fa fa-dashboard"></i>Ir al Dashboard</a></li>
                              <?php } ?>
                              <li><a href="<?php echo site_url('rutero/presente'); ?>"><i class="fa fa-truck"></i>Entregas para hoy</a></li>
                              <li><a href="<?php echo site_url('rutero/pasado'); ?>"><i class="fa fa-check"></i>Entregas anteriores</a></li>
                              <li class="divider"></li>
                              <li><a href="<?php echo site_url('rutero/reporte'); ?>"><i class="fa fa-calendar-check-o"></i>Reporte <?php echo $hoy; ?></a></li>
                              <li><a href="<?php echo site_url('rutero/reporte/anterior'); ?>"><i class="fa fa-calendar-check-o"></i>Reporte <?php echo $ayer; ?></a></li>
                              <li class="divider"></li>
                              <li><a href="<?php echo site_url('auth/logout'); ?>"><i class="glyphicon glyphicon-log-out"></i>Salir</a></li>
                            </ul>
                        </div><!-- btn-group -->
                        
                    </div><!-- pull-right -->
                    
                </div><!-- header-right -->
                
            </div><!-- headerwrapper -->
        </header>

        <section>
            <div class="mainwrapper rutero">