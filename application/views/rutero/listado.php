<div class="panel panel-default widget-messaging">
    <div class="panel-heading" style="padding-bottom: 10px;">
        <div class="row">
            <div class="col-xs-8">
                <h4 class="panel-title"><?php echo $title; ?></h4>
                <?php if (isset($text)) { ?>
                    <p><?php echo $text; ?></p>
                <?php } ?>
            </div>
            <div class="col-xs-4">
                <div class="control-label">
                    <div class="toggle toggle-primary" id="entregados-toggle"></div>
                </div>
                <label class="help-block">Entregados</label>
            </div>
        </div>
    </div>

    <?php if (!$pedidos){ ?>
        <div class="panel-body">
            <p class="lead text-center"><i class="fa fa-info-circle fa-5x text-muted "></i></p>
            <p class="lead text-center">No tienes pedidos para mostrar</p>
        </div>
    <?php }else{ ?>

        <ul class="list-group">

            <?php foreach ($pedidos as $p) { ?>
                <a class="list-group-item entrega <?php echo $p->order_delivered ? "entregado" : ""; ?>"
                   href="<?php echo site_url("modals/order/{$p->id}"); ?>" data-toggle="modal" data-target="#modal">
                    <div class="row">
                        <div class="col-xs-8">
                            <h4 class="sender"><?php echo $p->customer->name; ?></h4>
                            <p><?php echo $p->address->direccion; ?>, <?php echo $p->address->comuna; ?></p>

                            <?php if ($p->order_comment) { ?>
                                <em class="ellipsis"><i class="fa fa-comment"></i> <?php echo $p->order_comment; ?></em>
                            <?php } ?>

                        </div>
                        <div class="col-xs-4 clearfix">
                            <span class="label label-<?php echo $p->order_delivered ? "success" : "danger"; ?>"><?php echo $p->order_delivered ? "Entregado" : "Pendiente"; ?></span>
                            <p class="small"><i
                                        class="fa fa-clock-o"></i> <?php echo strftime('%d %h', strtotime($p->order_delivery_date)); ?>
                                , <?php echo date('H:i', strtotime($p->order_delivery_time)); ?></p>
                        </div>
                    </div>
                </a>
            <?php } ?>
        </ul>

    <?php } ?>

</div>

<script>
    $('.toggle').toggles({
        on: true,
        text: {
            on: 'SI',
            off: 'NO'
        }
    });

    $('#entregados-toggle').on('toggle', function (e, active) {

        $('.entrega.entregado').toggle(active);

    });
</script>