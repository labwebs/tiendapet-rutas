<div class="panel panel-default">
    <div class="panel-heading" style="padding-bottom: 10px;">
        <h4 class="panel-title">Reporte para <?= $fecha; ?></h4>
    </div>

    <div class="panel-body">

        <?php foreach ($report as $payment) { ?>
            <h3 class="lg-title"><?php echo $payment['name']; ?></h3>

            <?php if ($payment['orders']) { ?>
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Cliente</th>
                        <th>Dirección</th>
                        <th>Total</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php foreach ($payment['orders'] as $o) { ?>
                        <tr class="small">
                            <td><?php echo $o->id; ?></td>
                            <td><?php echo $o->customer->name; ?></td>
                            <td><?php echo $o->address->direccion; ?>, <?php echo $o->address->comuna; ?></td>
                            <td><?php echo $o->display_price; ?></td>
                        </tr>
                    <?php } ?>

                    </tbody>

                    <tfoot>
                    <tr>
                        <th class="text-right" colspan="3"><h5 class="md-title">Total <?php echo $payment['name']; ?>:</h5>
                        </th>
                        <th><h5 class="md-title">$<?php echo number_format($payment['total'], 0, ',', '.'); ?></h5></th>
                    </tr>
                    </tfoot>
                </table>
            <?php }else{ ?>
                <p class="text-center">No se registran pedidos para este tipo de pago</p>
            <?php } ?>

            <hr/>

        <?php } ?>

    </div>

</div>