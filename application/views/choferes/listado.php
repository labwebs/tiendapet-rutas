<div class="contentpanel">
  <div class="row">
    <div class="col-md-12">
      <table class="table data-table table-striped table-responsive no-footer">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Correo</th>
            <th>Teléfono</th>
            <th>Última entrega</th>
            <th>Fecha y hora</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($choferes as $c) { ?>
          <tr>
            <td><?php echo @$c->name; ?>
            <td><?php echo @$c->email ?></td>
            <td><?php echo @$c->phone ?></td>
            <td><?php echo isset($c->last_seen->address->direccion) && $c->last_seen ? "{$c->last_seen->address->direccion}, {$c->last_seen->address->comuna}" : "Sin información" ?></td>
            <td><?php echo isset($c->last_seen->order_delivered) && $c->last_seen->order_delivered ? date('d/m/Y H:i:s', strtotime($c->last_seen->order_delivered)) : "Sin información" ?></td>
            <td>
              <a href="<?php echo base_url("choferes/editar/{$c->id}"); ?>" class="btn btn-xs btn-primary">Modificar</a>
<?php /*              <div class="btn-group">
                
                <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">
                  <span class="caret"></span>
                  <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu pull-right" role="menu">
                  <li><a href="<?= base_url("choferes/editar/{$c->id}"); ?>">Modificar</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-menu-danger"><a href="<?= base_url("choferes/eliminar/{$c->id}"); ?>">Eliminar</a></li>;
                </ul>
              </div>
                */ ?>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
</div><!-- contentpanel -->