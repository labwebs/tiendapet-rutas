<?php
$search = $this->session->userdata('search');
$fecha = isset($search['select_date']) && $search['select_date'] ? $search['select_date'] : date('Y-m-d');
?>
<head>
    <link href="<?php echo base_url("assets/css/bootstrap.min.css"); ?>" rel="stylesheet" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<?php echo base_url("assets/css/print.css"); ?>" rel="stylesheet" />
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js'); ?>"></script>
</head>
<body>
<div class="container page">
    <div class="page-header">
        <h1><?php echo $driver->name; ?> <small class="text-right"><?php echo strftime("%d de %B de %Y", strtotime($fecha)); ?></small></h1>
    </div>
    <table class="table tabla__imprimir-">
        <thead>
        <tr>
            <th>&nbsp;</th>
            <th style="width:10%">#</th>
            <th>Cantidad</th>
            <th>Producto</th>
            <th>&nbsp;</th>
        </tr>
        </thead>

        <tbody>
        <?php if( !empty($products) ){ ?>
            <?php $i = 1;?>
            <?php foreach ($products as $p) { ?>
                <tr>
                    <td width="20%">&nbsp;</td>
                    <td width="10%"><?php echo $i; ?></td>
                    <td width="5%"><?php echo $p['qty']; ?></td>
                    <td width="35%" nowrap>
                        <label for="for_<?php echo $i;?>" style="font-weight:normal;">
                            <?php
                            $checked = 'checked="checked"';
                            if( ! empty($p['pinkings']) ){
                                foreach ($p['pinkings'] as $order_sku_id => $check) {
                                    if( $check == 0 ){
                                        $checked = '';
                                    }
                                }
                            } 
                            ?>
                            <input id="<?php echo 'for_'.$i;?>" type="checkbox" data-id="<?php echo ! empty($p['orders']) ? implode(',', $p['orders']) : ''; ?>" data-userid="<?php echo $this->user->id; ?>" <?php echo $checked; ?>/> &nbsp; 
                            <?php echo $p['producto']['brand']; ?> <?php echo $p['producto']['name']; ?> <?php echo $p['producto']['size']; ?>
                        </label>
                    </td>
                    <td width="30%">&nbsp;</td>
                </tr>
                <?php $i++;?>
            <?php } ?>
        <?php }else{ ?>
            <tr><td colspan="5" align="center">No existen productos.</td></tr>
        <?php } ?>
        </tbody>

    </table>
</div>
<script>
$(function(){
    $('input[type="checkbox"]').click(function(){
        var checked = $(this).is(':checked') == true ? 1 : 0;
        var userid = $(this).attr('data-userid');
        var sku_orders_id = $(this).attr('data-id');

        var data = 'sku_orders_id='+sku_orders_id+'&checked='+checked+'&userid='+userid;

        jQuery.ajax({
            type: "POST",
            url: "/ajax/picking",
            data: data,
            dataType: 'json',
            success: function (json) {

            }
        });
    });
});
</script>
</body>