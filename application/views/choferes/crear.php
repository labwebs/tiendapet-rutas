<div class="contentpanel">
    <div class="row">
        <div class="col-md-12">

            <?php echo form_open("choferes/guardar", array("id"=>"frm-chofer", "class" => "panel panel-default")) ?>

            <div class="panel-heading">
                <h4 class="panel-title">Nuevo Chofer</h4>
            </div>

            <div class="panel-body form-horizontal">


                <div class="errorForm">
                    <?php echo validation_errors(); ?>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Nombre completo <span class="asterisk">*</span></label>
                    <div class="col-sm-8">
                        <input type="text" name="admin_name" class="form-control"
                               value="<?php echo set_value('admin_name'); ?>" placeholder="Nombre completo"
                               required="required">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Nombre de usuario <span class="asterisk">*</span></label>
                    <div class="col-sm-8">
                        <input type="text" name="admin_login" class="form-control"
                               value="<?php echo set_value('admin_login'); ?>" placeholder="Nombre de usuario"
                               required="required"/>
                        <span class="help-block">Usado para iniciar sesión. Utilice sólo minúsculas sin tilde o números.</span>
                    </div>
                </div>

                <hr class="row"/>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Regi&oacute;n <span class="asterisk">*</span></label>
                    <div class="col-sm-4">
                        <select id="region_id" name="region_id" class="select2 form-control2" data-live-search="true" required="required">
                            <option value="">- Seleccione -</option>
                            <?php
                            if( ! empty($selectsRegiones) ){
                                foreach ($selectsRegiones as $select ){
                                    $selected =  set_value('region_id') == $select->codigo ? 'selected="selected"' : '';
                                    echo '<option value="'.$select->codigo.'" '.$selected.'>'.$select->nombre.'</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Comuna <span class="asterisk">*</span></label>
                    <div class="col-sm-4">
                        <select id="comuna_id" name="comuna_id[]" class="select2 form-control2" multiple="multiple" data-live-search="true">
                        </select>
                    </div>
                    <?php $comuna_ids = set_value('comuna_id'); ?>
                    <?php $comuna_ids = ! empty($comuna_ids) ? implode(',',$comuna_ids) : '';?>
                    <input type="hidden" id="tmp_comuna_ids" value="<?php echo $comuna_ids;?>">
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Bodega <span class="asterisk">*</span></label>
                    <div class="col-sm-4">
                        <select id="chofer_bodega_id" name="chofer_bodega_id" class="select2 form-control2">
                            <option value="">- Seleccione -</option>
                            <?php
                            if( ! empty($selectsBodegas) ){
                                foreach ($selectsBodegas as $id => $bodega) {
                                    $selected =  set_value('chofer_bodega_id') == $id ? 'selected="selected"' : '';
                                    echo '<option value="'.$id.'" '.$selected.'>'.$bodega.'</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <hr class="row"/>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Direcci&oacute;n Partida <span class="asterisk">*</span></label>
                    <div class="col-sm-8">
                        <input type="text" id="address_start" name="address_start" class="form-control" placeholder="Ingrese direcci&oacute;n" value="<?php echo set_value('address_start', @$driver->address_start);?>" autocomplete="off">
                        <span class="help-block">Usado para el sistema de Simpliroute. Utilice sólo minúsculas sin tilde.</span>
                    </div>
                </div>
                
                <div class="form-group">
                    <div id="row-my-map">
                        <div class="col-md-4"></div>
                        <div class="col-md-8">
                            <div id="my_map_address_start" class="form-control" style="width:100%; height: 300px;"></div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <label>Latitud</label>
                        <input type="text" name="latitude_start" id="lat_start" class="form-control" value="<?php echo set_value('latitude_start');?>" placeholder="" readonly />
                    </div>
                    <div class="col-md-4">
                        <label>Longitud</label>
                        <input type="text" name="longitude_start" id="lng_start" class="form-control" value="<?php echo set_value('longitude_start');?>" placeholder="" readonly />
                    </div>
                </div>

                <hr class="row"/>
                
                <div class="form-group">
                    <label class="col-sm-4 control-label">Direcci&oacute;n T&eacute;rmino <span class="asterisk">*</span></label>
                    <div class="col-sm-8">
                        <input type="text" id="address_end" name="address_end" class="form-control" placeholder="Ingrese direcci&oacute;n" value="<?php echo set_value('address_end', @$driver->address_end);?>" autocomplete="off">
                        <span class="help-block">Usado para el sistema de Simpliroute. Utilice sólo minúsculas sin tilde.</span>
                    </div>
                </div>
                
                <div class="form-group">
                    <div id="row-my-map">
                        <div class="col-md-4"></div>
                        <div class="col-md-8">
                            <div id="my_map_address_end" class="form-control" style="width:100%; height: 300px;"></div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <label>Latitud</label>
                        <input type="text" name="latitude_end" id="lat_end" class="form-control" value="<?php echo set_value('latitude_end');?>" placeholder="" readonly />
                    </div>
                    <div class="col-md-4">
                        <label>Longitud</label>
                        <input type="text" name="longitude_end" id="lng_end" class="form-control" value="<?php echo set_value('longitude_end');?>" placeholder="" readonly />
                    </div>
                </div>
                
                <hr class="row"/>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Horario que comienza su ruta <span class="asterisk">*</span></label>
                    <div class="col-sm-8">
                        <div class="input-group mb15">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            <div class="bootstrap-timepicker"><input id="shift_start" name="shift_start" type="text" class="form-control" value="<?php echo set_value('shift_start');?>"/></div>
                        </div>
                        <span class="help-block">(formato 24 horas)</span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Horario que termina su ruta <span class="asterisk">*</span></label>
                    <div class="col-sm-8">
                        <div class="input-group mb15">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            <div class="bootstrap-timepicker"><input id="shift_end" name="shift_end" type="text" class="form-control" value="<?php echo set_value('shift_end');?>"/></div>
                        </div>
                        <span class="help-block">(formato 24 horas)</span>
                    </div>
                </div>

                <hr class="row"/>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Horario que comienza su descanso o colaci&oacute;n<span class="asterisk">*</span></label>
                    <div class="col-sm-8">
                        <div class="input-group mb15">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            <div class="bootstrap-timepicker"><input id="window_start_2" name="window_start_2" type="text" class="form-control" value="<?php echo set_value('window_start_2');?>"/></div>
                        </div>
                        <span class="help-block">(formato 24 horas)</span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Horario que termina su descanso o colaci&oacute;na<span class="asterisk">*</span></label>
                    <div class="col-sm-8">
                        <div class="input-group mb15">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            <div class="bootstrap-timepicker"><input id="window_end_2" name="window_end_2" type="text" class="form-control" value="<?php echo set_value('window_end_2');?>"/></div>
                        </div>
                        <span class="help-block">(formato 24 horas)</span>
                    </div>
                </div>

                <hr class="row"/>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Duraci&oacute;n Servicio <span class="asterisk">*</span></label>
                    <div class="col-sm-8">
                        <div class="input-group mb15">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            <div class="bootstrap-timepicker"><input id="duration" name="duration" type="number" min="1" max="60" step="1" class="form-control" value="<?php echo set_value('duration');?>"/></div>
                            <span class="input-group-addon">minutos</span>
                        </div>
                        <span class="help-block">(es el tiempo de servicio desde que el chofer llega al lugar y entrega el pedido.)</span>
                        <label for="duration" class="error"></label>
                    </div>
                </div>

                <hr class="row"/>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Correo electrónico <span class="asterisk">*</span></label>
                    <div class="col-sm-8">
                        <input type="email" name="email" class="form-control" value="<?php echo set_value('email'); ?>"
                               placeholder="correo@servidor.cl" required="required"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Teléfono <span class="asterisk">*</span></label>
                    <div class="col-sm-8">
                        <input type="phone" name="phone" class="form-control" value="<?php echo set_value('phone'); ?>"
                               placeholder="+56 9 2222 5555" required="required"/>
                    </div>
                </div>

                <hr class="row"/>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Contraseña <span class="asterisk">*</span></label>
                    <div class="col-sm-8">
                        <input type="password" id="password" name="password" class="form-control" placeholder="Contraseña" required="required"/>
                        <p class="help-block">Mínimo 6 caracteres</p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Repita contraseña <span class="asterisk">*</span></label>
                    <div class="col-sm-8">
                        <input type="password" name="password_match" class="form-control" placeholder="Contraseña" required="required"/>
                    </div>
                </div>

                <hr class="row" />
                
                <div class="form-group">
                    <label class="col-sm-4 control-label">Activo en Rutas<span class="asterisk">*</span></label>
                    <div class="col-sm-8">
                        <div class="mt-radio-inline">
                            <label class="mt-radio mt-radio-outline">
                                <?php $checked = set_value('inactivo') == 0 ? 'checked="checked"' : '';?>
                                <input type="radio" name="inactivo" value="0" <?php echo $checked;?>> Sí
                                <span></span>
                            </label>
                            <label class="mt-radio mt-radio-outline">
                                <?php $checked = set_value('inactivo') == 1 ? 'checked="checked"' : '';?>
                                <input type="radio" name="inactivo" value="1" <?php echo $checked;?>> No
                                <span></span>
                            </label>
                        </div>
                    </div>
                </div>

                <hr class="row" />

                <div class="form-group">
                    <label class="col-sm-4 control-label">Activo en SimpliRoute<span class="asterisk">*</span></label>
                    <div class="col-sm-8">
                        <div class="mt-radio-inline">
                            <label class="mt-radio mt-radio-outline">
                                <?php $checked = set_value('inactivo_sr') == 0 ? 'checked="checked"' : '';?>
                                <input type="radio" name="inactivo_sr" value="0" <?php echo $checked;?>> Sí
                                <span></span>
                            </label>
                            <label class="mt-radio mt-radio-outline">
                                <?php $checked = set_value('inactivo_sr') == 1 ? 'checked="checked"' : '';?>
                                <input type="radio" name="inactivo_sr" value="1" <?php echo $checked;?>> No
                                <span></span>
                            </label>
                        </div>
                    </div>
                </div>

                <hr class="row" />

                <div class="form-group">
                    <div class="text-center">
                        <div class="col-md-4"></div>
                        <div class="panel panel-info-head col-md-4 text-center">
                            <div class="panel-heading">
                                <h3 class="panel-title">Simpli Route - Crear</h3>
                            </div>
                            <div class="panel-body list-group nopadding">
                                <div class="form-group text-center">
                                    <label class="col-md-12 control-label" style="text-align:center;">Crear chofer en la plataforma de SimpliRoute</label>
                                </div>
                                <div class="form-group text-center">
                                    <div class="col-xs-12 col-md-12 control-label text-center">
                                        <input type="checkbox" name="checksr" id="checksr" value="create" style="display:none;">
                                        <div class="toggle toggle-success" style="margin: 0 auto;width: 58px; height: 34px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>  

            </div>

            <div class="panel-footer">
                <a href="<?php echo site_url("choferes"); ?>" class="btn btn-default">Cancelar</a>
                <button type="submit" class="btn btn-success pull-right">Guardar</button>
            </div>

            </form>
        </div>
    </div>
</div>

<script src="<?php echo site_url("assets/js/jquery.validate.min.js"); ?>"></script>
<script>
/*
jQuery("form").validate({
    highlight: function (element) {
        jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        jQuery(element).closest('.form-group').removeClass('has-error');
    }
});
*/
jQuery("#frm-chofer").validate({
    rules:{
        "admin_name":{ required:true },
        "admin_login":{ required:true },
        "region_id":{ required:true },
        "comuna_id[]":{ required:true },
        "email":{ required:true, email:true },
        "phone":{ required:true },
        "address_start":{ 
            required:{       
                depends: function(element) {
                    return $("#checksr").is(':checked') ? true : false;
                }
            },
        },
        "address_end":{ 
            required:{       
                depends: function(element) {
                    return $("#checksr").is(':checked') ? true : false;
                }
            },
        },
        "shift_start":{ 
            required:{       
                depends: function(element) {
                    return $("#checksr").is(':checked') ? true : false;
                }
            },
        },
        "shift_end":{ 
            required:{       
                depends: function(element) {
                    return $("#checksr").is(':checked') ? true : false;
                }
            },
        },
        "password":{ 
            required:{       
                depends: function(element) {
                    return $("#checksr").is(':checked') ? true : false;
                }
            },
        },
        "password_match": {
            required:{       
                depends: function(element) {
                    return $("#checksr").is(':checked') ? true : false;
                }
            },
            minlength: 6,
            equalTo: "#password"
        }
    },
    messages:{
        "admin_name": {
            required: " "
        },
        "admin_login": {
            required: " "
        },
        "region_id": {
            required: " "
        },
        "comuna_id[]": {
            required: " "
        },
        "email": {
            required: " ",
            email: " "
        },
        "phone": {
            required: " "
        },
        "address_start": {
            required: " "
        },
        "address_end": {
            required: " "
        },
        "password": {
            required: " "
        },
        "password_match": {
            required: " "
        }
    },
    highlight: function (element) {
        jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        jQuery(element).closest('.form-group').removeClass('has-error');
    }
});
</script>
<script>
$(function(){
    $("#region_id").change(function(){
        var region_id = $(this).val();

        $.ajax({
            type: "POST",
            url: "/choferes/selComunas",
            data: { 'region_id': region_id  },
            success: function(data){
                var opts = $.parseJSON(data);
                //var comuna_id = $("#tmp_comuna_id").val();
                var comuna_ids = $("#tmp_comuna_ids").val();
                comuna_ids = comuna_ids.split(',');
                // Use jQuery's each to iterate over the opts value
                //$('#comuna_id').append('<option value="">- Seleccione -</option>');
                $.each(opts, function(i, d) {
                    var selected = inArray(d.id, comuna_ids) ? 'selected="selected"' : '';
                    // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                    $('#comuna_id').append('<option value="' + d.id + '" '+selected+'>' + d.nombre + '</option>');
                });
                //$('.bs-select').selectpicker('refresh');
            }
        });
        $('#comuna_id').select2();
    });
    $('#region_id').trigger("change");

    $('.toggle').toggles({
        on: false,
        text:{
            on: 'Sí',
            off: 'No'
        },
        checkbox: $('#checksr')
    });
        
    $("#address_start").geocomplete({
        map: "#my_map_address_start",
        details: "#addressDetails",
        blur: true,
        mapOptions:{
            mapTypeControl: false
        },
        markerOptions: {
            //icon : image,
            draggable: true
        }
        //types: ["geocode", "establishment"]
    }).bind("geocode:result", function(event, result){
        $("#lat_start").val(result.geometry.location.lat());
        $("#lng_start").val(result.geometry.location.lng());
    });

    $("#address_start").bind("geocode:dragged", function(event, latLng){
        $("#lat_start").val(latLng.lat());
        $("#lng_start").val(latLng.lng());
    });
    
    $('#address_start').on('input', function() {
        if($("#address_start").val() == "")
        {
            $("#lat_start").val("");
            $("#lng_start").val("")    
        }
    });

    if( $("#lat_start").val() && $("#lng_start").val() ){
        $("#address_start").geocomplete("find", $("#lat_start").val() + "," + $("#lng_start").val())
    }else{
        $("#address_start").trigger("geocode");
    }

    $("#address_end").geocomplete({
        map: "#my_map_address_end",
        details: "#addressDetails",
        blur: true,
        mapOptions:{
            mapTypeControl: false
        },
        markerOptions: {
            //icon : image,
            draggable: true
        }
        //types: ["geocode", "establishment"]
    }).bind("geocode:result", function(event, result){
        $("#lat_end").val(result.geometry.location.lat());
        $("#lng_end").val(result.geometry.location.lng());
    });

    $("#address_end").bind("geocode:dragged", function(event, latLng){
        $("#lat_end").val(latLng.lat());
        $("#lng_end").val(latLng.lng());
    });
    
    $('#address_end').on('input', function() {
        if($("#address_end").val() == "")
        {
            $("#lat_end").val("");
            $("#lng_end").val("")    
        }
    });

    if( $("#lat_end").val() && $("#lng_end").val() ){
        $("#address_end").geocomplete("find", $("#lat_end").val() + "," + $("#lng_end").val())
    }else{
        $("#address_end").trigger("geocode");
    }

    jQuery('#shift_start').timepicker({showMeridian: false, defaultTIme: false});
    jQuery('#shift_end').timepicker({showMeridian: false, defaultTIme: false});

    jQuery('#window_start_2').timepicker({showMeridian: false, defaultTIme: false});
    jQuery('#window_end_2').timepicker({showMeridian: false, defaultTIme: false});
});
</script>