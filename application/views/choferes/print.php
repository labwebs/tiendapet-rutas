<?php
$search = $this->session->userdata('search');
$fecha = isset($search['select_date']) && $search['select_date'] ? $search['select_date'] : date('Y-m-d');
?>
<head>
    <link href="<?php echo base_url("assets/css/bootstrap.min.css"); ?>" rel="stylesheet" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<?php echo base_url("assets/css/print.css"); ?>" rel="stylesheet" />
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js'); ?>"></script>
</head>
<body>
<div class="container page">
    <div class="page-header">
        <h1><?php echo $driver->name; ?> <small class="text-right"><?php echo strftime("%d de %B de %Y", strtotime($fecha)); ?></small></h1>
    </div>
    <table class="table tabla__imprimir">
        <thead>
        <tr>
            <th>Cliente</th>
            <th>Direcci&oacute;n</th>
            <th style="width:5%">#</th>
            <th>Productos</th>
            <th>Total</th>
        </tr>
        </thead>

        <tbody>
        <?php $i = 1;?>
        <?php foreach ($driver->orders as $o) { ?>
            <?php
                $pago_webpay = 0;
                if ($o->paym_id == 4 || $o->paym_id == 5 || $o->paym_id == 6 || $o->paym_id == 7) {
                    $pago_webpay = 1;
                } 
                $pago_gifcard = 0;
                if ($o->paym_id == 101) {
                    $pago_gifcard = 1;
                }
            ?>
            <tr>
                <td width="20%">
                    <?php if (isset($o->bsale_nro_boleta) && $o->bsale_nro_boleta) { ?>
                        <b>Boleta </b><?php echo $o->bsale_nro_boleta; ?><?php } ?>
                        <address>
                            <strong><?php echo $o->customer->name; ?></strong><br />
                            Tel: <?php echo $o->customer->phone; ?><br />
                            <?php /*Mail: <?php echo $o->customer->email; ?><br />*/?>
                        </address>
                </td>
                <td width="20%">
                    <address>
                        <?php if (isset($o->address->local_id) && $o->address->local_id && $o->address->comuna_id == 493) { ?>
                            <strong><?php echo $o->address->direccion; ?>, <?php echo isset($locales[$o->address->local_id]->nombre) ? $locales[$o->address->local_id]->nombre : $o->address->comuna; ?></strong><br />
                        <?php } else { ?>
                            <strong><?php echo $o->address->direccion; ?>, <?php echo $o->address->comuna; ?></strong><br />
                        <?php } ?>
                        <p><i><?php echo $o->address->comentarios; ?></i></p>
                        <?php
                        $comment = "";
                        if (!empty($o->order_comment)) {
                            $comment = $o->order_comment;
                        } else {
                            if ($o->paym_id == 1) { // Efectivo o Cheque
                                $comment = "Cliente debe cancelar con efectivo o cheque // ";
                            } elseif ($o->paym_id == 2) {
                                $comment = "Entregar solo una vez confirmación previa vía transferencia // ";
                            }
                        }
                        if (!empty($o->comment)) {
                            $comment = $comment . '<br/>' . $o->comment;
                        }
                        ?>
                        <p><i><?php echo $comment; ?></i></p>
                        <?php if (isset($o->despachoatupinta_bloque_id) && $o->despachoatupinta_bloque_id) { ?>
                            <?php $text = $this->orders->getMsjeDespachoatuPinta($o->id);?>
                            <p><i><?php echo $text; ?></i></p>
                        <?php } ?>
                    </address>
                </td>
                <td width="10%"><?php echo $i; ?></td>
                <td width="30%">
                    <ul class="item__lista--picking">
                        <?php foreach ($o->products as $p) { ?>
                            <li>
                                <label for="for_<?php echo $p->id;?>" style="font-weight:normal;">
                                    <?php $checked = $p->picking_check == 1 ? 'checked="checked"' : '';?>
                                    <input id="for_<?php echo $p->id;?>" type="checkbox" data-id="<?php echo $p->id; ?>" data-userid="<?php echo $this->user->id; ?>" <?php echo $checked; ?>>
                                    <span class="item__lista--descripcion"><?php echo $p->qty; ?>&times; <?php echo $p->description->brand; ?> <?php echo $p->description->name; ?> <?php echo $p->description->size; ?></span>
                                </label>
                            </li>
                        <?php } ?>
                    </ul>
                    <?php
/*
    <ul class="item__lista--imprimir">
    <?php foreach( $o->products as $p){ ?>
    <li><span class="item__lista--descripcion"><?php echo $p->qty; ?>&times; <?php echo $p->description->brand; ?> <?php echo $p->description->name; ?> <?php echo$p->description->size; ?></span></li>
    <?php } ?>
    </ul>
     */
    ?>
                </td>
                <td width="20%">
                    <?php echo $o->display_price; ?>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="3">
                    <ul class="formapago__lista">
                        <li><i class=""></i>Efectivo</li>
                        <li><i class=""></i>Cheque</li>
                        <li><i class=""></i>Transferencia</li>
                        <li><i class="<?php echo $pago_webpay ? 'fa fa-times' : ''; ?>"></i>Webpay Plus</li>
                        <li><i class="<?php echo $pago_gifcard ? 'fa fa-times' : ''; ?>"></i>Gif Card</li>
                    </ul>
                    <?php
/*echo "<ul class='formapago__lista'>";
    if( ! empty($payments) ){
    foreach ($payments as $indice => $p ){
    if( $p->paym_inactivo == 1 ) continue;
    echo '<li><i class="'.( $pago_webpay && $p->id == 4 ? 'fa fa-times' : '' ).'"></i>'.$p->paym_name.'</li>';
    }
    }
    echo "</ul>";
     */
    ?>
                </td>
                <td class="forma__pago--box">
                    <i></i>
                    Entregado
                </td>
            </tr>
            <?php $i++;?>
        <?php
}?>
        </tbody>

    </table>
</div>
<script>
$(document).ready(function(){
    $('input[type="checkbox"]').click(function(){
        //picking
        //sku_orders_id
        //checked
        var checked = $(this).is(':checked') == true ? 1 : 0;
        var userid = $(this).attr('data-userid');
        var sku_orders_id = $(this).attr('data-id');

        var data = 'sku_orders_id='+sku_orders_id+'&checked='+checked+'&userid='+userid;

        jQuery.ajax({
            type: "POST",
            url: "/ajax/picking",
            data: data,
            dataType: 'json',
            success: function (json) {

            }
        });
    });
});
/*
var data = $("#registrate-form").serialize();
jQuery.ajax({
    type: "POST",
    url: "/ajax/register",
    data: data,
    dataType: 'json',
    success: function (json) {
        var $msje = $("#msje-registrate");
        if( json.res == 1 ){
            $msje.addClass("alert-success");
            $("#registrate-form").hide();
            $("#registrate-form input").val("");
            $msje.html('<i class="fa fa-check-circle"></i>'+json.msje).show();
            window.location.reload(true);
        }else{
            $("#btnLoadingRegistro").hide();
            $("#btnEnviarRegistro").show();
            $msje.addClass("alert-warning");
            $msje.html('<i class="fa fa-exclamation-triangle"></i>'+json.msje).show();
        }
        jQuery("#btnEnviarRegistro").addClass("activo");
        //jQuery("html, body").animate({scrollTop:jQuery("#registrate-form").offset().top}, 800);

        setTimeout(function(){
            $msje.hide().removeClass("alert-success").removeClass("alert-warning");
            $("#btnLoadingRegistro").hide();
            $("#btnEnviarRegistro").show();
            $("#registrate-form").show();
        }, 6000);
    }
});
*/
</script>
</body>