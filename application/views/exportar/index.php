<div class="contentpanel">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo form_open("exportar/generar", array("class" => "form-inline form-horizontal")); ?>
                        <div class="form-group">
                            <label class="sr-only" for="date">Fecha</label>
                            <input type="text" name="search_exportar[date]" class="form-control datepicker-max" id="date" placeholder="Fecha" value="" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="desde">Chofer</label>
                            <select name="search_exportar[driver_id]" class="select" placeholder="Seleccione chofer">
                                <option></option>
                                <?php foreach ($drivers as $d) { ?>
                                    <option value="<?php echo $d->id; ?>"><?php echo $d->name; ?></option>
                                <?php } ?>
                            </select>
                        </div><!-- form-group -->
                        <input type="hidden" name="accion" value="exportXls"/>
                        <button type="submit" class="btn btn-primary mr5">Generar</button>
                    </form>
                </div>
            </div>

            <div class="col-md-12">
            </div>
        </div><!-- contentpanel -->
