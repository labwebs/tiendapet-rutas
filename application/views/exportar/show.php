<div class="contentpanel">
    <div class="row">
        <div class="col-md-12">
            <h2>
                Listado de Pedidos por Despachar 
                <a href="<?php echo site_url("exportar/"); ?>" class="btn btn-default"><i class="fa fa-arrow-left"></i> Volver </a>
                <div class="btn-group pull-right">
                    <a href="<?php echo site_url("exportar/generarXLS"); ?>" class="btn btn-default"><i class="fa fa-file-excel-o"></i> Exportar</a>
                </div>
            </h2>
            <p>&nbsp;</p>
            <div class="panel panel-default">
                <div class="panel-body table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Dirección</th>
                            <th>Comuna</th>
                            <th>Número</th>
                            <th>Pagado</th>
                            <th>Uno</th>
                            <th>Nro.Boleta</th>
                            <th>Monto Total</th>
                            <th>Teléfono</th>
                            <th>E-mail</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($orders) { ?>
                            <?php foreach ($orders as $o) { ?>
                                <tr>
                                    <td><?php echo $o->id; ?></td>
                                    <td><strong><?php echo $o->customer->name; ?></strong></td>
                                    <td><?php echo $o->address->direccion; ?></td>
                                    <td><?php echo $o->address->comuna; ?></td>
                                    <td>24</td>
                                    <td>P</td>
                                    <td>1</td>
                                    <!--<td><?php echo $o->driver->name; ?></td>-->
                                    <td><?php echo $o->bsale_nro_boleta; ?></td>
                                    <td>$<?php echo ($o->order_total + $o->order_shipping - $o->order_purcharse_valor_desc); ?></td>
                                    <td><?php echo $o->customer->phone; ?></td>
                                    <td><?php echo $o->customer->email; ?></td>
                                </tr>
                            <?php } ?>
                        <?php }else{ ?>
                            <td colspan="10" class="text-center">No se registran pedidos por despachar.</td>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div><!-- contentpanel -->