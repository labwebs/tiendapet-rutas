<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = 'default';
$active_record = TRUE;

/**
 * DB desarrollo servidor Tienda PET
 * =================================== */
$db['vps_dev']['hostname'] = 'localhost';
$db['vps_dev']['username'] = 'tienda18_dev';
$db['vps_dev']['password'] = 'octano2015';
$db['vps_dev']['database'] = 'tienda18_dev';
$db['vps_dev']['dbdriver'] = 'mysql';
$db['vps_dev']['dbprefix'] = '';
$db['vps_dev']['pconnect'] = FALSE;
$db['vps_dev']['db_debug'] = TRUE;
$db['vps_dev']['cache_on'] = FALSE;
$db['vps_dev']['cachedir'] = '';
$db['vps_dev']['char_set'] = 'utf8';
$db['vps_dev']['dbcollat'] = 'utf8_general_ci';
$db['vps_dev']['swap_pre'] = '';
$db['vps_dev']['autoinit'] = TRUE;
$db['vps_dev']['stricton'] = FALSE;


$db['default']['hostname'] = 'localhost';
//$db['default']['username'] = 'tiendape_tienda';
//$db['default']['password'] = 'TgcEy01TvrZ8';
$db['default']['username'] = 'root';
$db['default']['password'] = 'jav3x11';
$db['default']['database'] = 'tiendape_tienda18_tiendapetv2';
$db['default']['dbdriver'] = 'mysqli';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = FALSE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;


$db['simon']['hostname'] = 'localhost';
$db['simon']['username'] = 'root';
$db['simon']['password'] = '';
$db['simon']['database'] = 'tiendape_tienda';
$db['simon']['dbdriver'] = 'mysql';
$db['simon']['dbprefix'] = '';
$db['simon']['pconnect'] = FALSE;
$db['simon']['db_debug'] = TRUE;
$db['simon']['cache_on'] = FALSE;
$db['simon']['cachedir'] = '';
$db['simon']['char_set'] = 'utf8';
$db['simon']['dbcollat'] = 'utf8_general_ci';
$db['simon']['swap_pre'] = '';
$db['simon']['autoinit'] = TRUE;
$db['simon']['stricton'] = FALSE;


/* End of file database.php */
/* Location: ./application/config/database.php */
