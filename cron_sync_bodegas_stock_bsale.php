<?php
@session_start();

define("PATH_SITE", dirname(__FILE__));
$modo_debug = FALSE;
$_SESSION['cron'] = true;

//if ((isset($argv[1])) && ($argv[1] =='cron'))
/*
if (PHP_SAPI === 'cli') {
    $_SESSION['cron'] = TRUE;
}
else
{
    $_SESSION['cron'] = FALSE;
    // No ejecutado desde CrON
    die( "No permitido ");
}
*/

$CRON_OUTPUT = "INICIANDO CRON @ ".date("m-d-Y H:i:s")."\r\n";
$CRON_OUTPUT .= "\r\n";

ob_start();

//$_SERVER['CI_ENV'] = 'production'; //'production development';
$_SERVER['CI_ENV'] = 'development'; //'production development';

include(PATH_SITE.'/index.php');


ob_end_clean();
$CI =& get_instance();

//error_reporting(E_ALL);
//ini_set("display_errors", true);

$CI->load->library('session');
$CI->load->library('bsale');

$CI->load->model(array('products', 'bodegas'));

$results = $CI->products->all();
/*
$results[0] = new stdClass();
$results[0]->id = 149;
$results[0]->prod_id = 96;
$results[0]->size = '3 kg.';
$results[0]->price = 16900;
$results[0]->name = 'Puppy Small';
$results[0]->available = 1;
$results[0]->brand = 'Eukanuba';

$results[1] = new stdClass();
$results[1]->id = 5;
$results[1]->prod_id = 96;
$results[1]->size = '3 kg.';
$results[1]->price = 16900;
$results[1]->name = 'Puppy Small';
$results[1]->available = 1;
$results[1]->brand = 'Eukanuba';

$results[2] = new stdClass();
$results[2]->id = 16;
$results[2]->prod_id = 96;
$results[2]->size = '3 kg.';
$results[2]->price = 16900;
$results[2]->name = 'Puppy Small';
$results[2]->available = 1;
$results[2]->brand = 'Eukanuba';
*/

echo "Inicio del CRON: ".date("m-d-Y H:i:s")."\n";

echo "Se han econtrado ".strval(count($results))." Skus encontrados ! "."\n";
$CRON_OUTPUT .= "Se han econtrado ".strval(count($results))." skus registrados ! ".date("m-d-Y H:i:s")."\n";

$cont = 0;
if( ! empty($results) ){
    
    $empRyA = $CI->bodegas->getEmpresa(1);
    
    $bsGolani = new Bsale();
    $bsGolani->setToken($empRyA->token);

    $bs = new Bsale();

    //Empresas Bsale 
    $empresas = $CI->bodegas->getBsaleEmpresasAll();
    
    $resultBodegasSyncAjustes = $CI->bodegas->getBodegasSyncAjustes();
    $bodegasSyncAjustes = array();
    if( ! empty($resultBodegasSyncAjustes) ){
        foreach($resultBodegasSyncAjustes as $bsa){
            $bodegasSyncAjustes[$bsa['bodega_id']] = $bsa['bodega_id'];
        }

        foreach ($results as $key => $result){
            if( ! empty($empresas) ){
                $sku_bodegas = $CI->bodegas->getBySku($result->id);
                
                $sku_rel_bodegas_site = array();
                if( ! empty($sku_bodegas) ){
                    foreach($sku_bodegas as $skubodega){
                        if (in_array($skubodega['bodega_id'], $bodegasSyncAjustes)) {
                            $sku_rel_bodegas_site[$skubodega['bodega_id']] = $skubodega['bodega_id'];
                        }
                    }
                }

                if( ! empty($sku_rel_bodegas_site) ){
                    $msje = "ENCONTRAMOS LA RELACIÓN CON LAS SIGUIENTES BODEGAS ID:".(implode(',', $sku_rel_bodegas_site)).", PARA EL SKU:".$result->id."...";
                    $CRON_OUTPUT .= "[STATUS: Site_Join_Bodegas, BODEGAS IDS: ".(implode(',', $sku_rel_bodegas_site)).", SKU: ".$result->id."] " . $msje . "\r\n";    
                }

                // Se agregar la condicion para pregntar si existe y si tiene stock en la bodega Gonali
                $stockBodegaGolani = $bsGolani->getStockProductBySku($result->id, 12); // 12 : es la bodega o sucursal de la empresa RyA
                if( isset($stockBodegaGolani->id) && $stockBodegaGolani->id && $stockBodegaGolani->quantity > 0 ){
                    $stockAvailableBodegasBasle = array();
                    foreach($empresas as $indice => $empresa){
                        $bodegasBsale = $CI->bodegas->getBsaleBodegasByEmpresa($empresa->id);
                        if( ! empty($bodegasBsale) ){
                            $bs->setToken($empresa->token);
                            foreach($bodegasBsale as $bodega){
                                if (in_array($bodega->bodega_id, $bodegasSyncAjustes)) {
                                    if( $bodega->bodega_id != 0 && $bodega->bodega_id ){
                                        $stockBodega = $bs->getStockProductBySku($result->id, $bodega->bsale_bodega_id);
                                        if( isset($stockBodega->id) && $stockBodega->id ){
                                            if( isset($stockBodega->quantity) && $stockBodega->quantity > 0 ){
                                                $stockAvailableBodegasBasle[$bodega->bodega_id] = $bodega->bodega_id;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    //Syncronizamos las bodegas del SKU, si no existe stock en bodegas de BSALE
                    if( ! empty($stockAvailableBodegasBasle) ){
                        $msje = "ENCONTRAMOS LA RELACIÓN CON LAS SIGUIENTES BODEGAS ID:".(implode(',', $stockAvailableBodegasBasle)).", PARA EL SKU:".$result->id."...";
                        $CRON_OUTPUT .= "[STATUS: Bsale_Bodegas_Disponibles, BODEGAS IDS: ".(implode(',', $stockAvailableBodegasBasle)).", SKU: ".$result->id."] " . $msje . "\r\n";    

                        if( ! empty($sku_rel_bodegas_site) ){
                            foreach($sku_rel_bodegas_site as $key => $bodega_id){
                                if( ! in_array($bodega_id, $stockAvailableBodegasBasle) ){
                                    $msje = "ELIMINAMOS LA RELACIÓN CON LA BODEGA ID:".$bodega_id.", PARA EL SKU:".$result->id."...";
                                    $CRON_OUTPUT .= "[STATUS: Eliminar, BODEGA_ID: ".$bodega_id.", SKU: ".$result->id."] " . $msje . "\r\n";    
                                    $CI->bodegas->deleteBodegaRelSKU($bodega_id, $result->id);
                                }
                            }       
                        }
                        foreach ($stockAvailableBodegasBasle as $key => $sucursal_id) {
                            if( ! in_array($sucursal_id, $sku_rel_bodegas_site) ){
                                $msje = "INSERTAMOS LA RELACIÓN CON LA BODEGA ID:".$sucursal_id.", PARA EL SKU:".$result->id."...";
                                $CRON_OUTPUT .= "[STATUS: Insertar, BODEGA_ID: ".$sucursal_id.", SKU: ".$result->id."] " . $msje . "\r\n";    
                                $CI->bodegas->insertBodegaRelSKU($sucursal_id, $result->id);
                            }
                        }
                        $CI->bodegas->updSku($result->id, array('sku_available'=>1));
                        $msje = "ACTUALIZANDO LA DISPONIBILIDAD DEL SKU:".$result->id."...";
                        $CRON_OUTPUT .= "[STATUS: Disponible, SKU: ".$result->id."] " . $msje . "\r\n";    
                    }else{
                        if( ! empty($sku_rel_bodegas_site) ){
                            foreach($sku_rel_bodegas_site as $key => $bodega_id){
                                $msje = "ELIMINAMOS LA RELACIÓN CON LA BODEGA ID:".$bodega_id.", PARA EL SKU:".$result->id."...";
                                $CRON_OUTPUT .= "[STATUS: Eliminar, BODEGA_ID: ".$bodega_id.", SKU: ".$result->id."] " . $msje . "\r\n";    
                                $CI->bodegas->deleteBodegaRelSKU($bodega_id, $result->id);
                            }       
                        }
                        $sku_bodegas = $CI->bodegas->getBySku($result->id);
                        if( count($sku_bodegas) == 0 ){
                            $CI->bodegas->updSku($result->id, array('sku_available'=>0));
                            $msje = "ACTUALIZANDO LA DISPONIBILIDAD DEL SKU:".$result->id."...";
                            $CRON_OUTPUT .= "[STATUS: No Disponible, SKU: ".$result->id."] " . $msje . "\r\n";    
                        }
                    }
                }else{
                    if( ! empty($sku_rel_bodegas_site) ){
                        foreach($sku_rel_bodegas_site as $key => $bodega_id){
                            $msje = "ELIMINAMOS LA RELACIÓN CON LA BODEGA ID:".$bodega_id.", PARA EL SKU:".$result->id."...";
                            $CRON_OUTPUT .= "[STATUS: Eliminar, BODEGA_ID: ".$bodega_id.", SKU: ".$result->id."] " . $msje . "\r\n";    
                            $CI->bodegas->deleteBodegaRelSKU($bodega_id, $result->id);
                        }       
                    }
                    $sku_bodegas = $CI->bodegas->getBySku($result->id);
                    if( count($sku_bodegas) == 0 ){
                        $CI->bodegas->updSku($result->id, array('sku_available'=>0));
                        $msje = "ACTUALIZANDO LA DISPONIBILIDAD DEL SKU:".$result->id."...";
                        $CRON_OUTPUT .= "[STATUS: No Disponible, SKU: ".$result->id."] " . $msje . "\r\n";    
                    }
                }
            }
        }
    }
}

$CRON_OUTPUT .= "\r\n";
$CRON_OUTPUT .= "TERMINANDO CRON @ ".date("m-d-Y H:i:s")."\r\n";
$CRON_OUTPUT .= "\r\n";
//echo dirname(__FILE__); 

$fh = fopen(PATH_SITE.'/cron_log/cron_sync_bodegas_stock_bsale_'.date('d-m-Y_his').'.log', 'a+');
fwrite($fh, $CRON_OUTPUT);
fclose($fh);
echo "Fin del CRON: ".date("m-d-Y H:i:s")."\n";
//echo "\n";
//exit;
?>