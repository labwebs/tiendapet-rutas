var oTableAjax;
var TableDatatables = function() {

    var initTableAjax = function() {

        var tableAjax = $('.table-list-ajax');

        var controller = $("#list_action_ajax").html();

        oTableAjax = tableAjax.dataTable({
            "language": {
                "emptyTable": "No hay información disponible para mostrar",
                "info": "Mostrando registros del _START_ al _END_ de _TOTAL_ totales",
                "infoEmpty": "Sin registros para mostrar.",
                "infoFiltered": "(filtrados de un total de _MAX_ registros)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ registros",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "No se encontraron registros que coincidan",
                "paginate": {
                    "first": "Primero",
                    "last": "Ãšltimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "aria": {
                    "sortAscending": ": active para ordenar esta columna ascendentemente",
                    "sortDescending": ": active para ordenar esta columna descendentemente"
                }
            },
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/" + controller + "/listajax",
                onDataLoad: function(grid) {
                        // execute some code on ajax data load
                    }
                    /*"data": function ( d ) {
                        //d.myKey = "myValue";
                        // d.custom = $('#myInput').val();
                        // etc
                    }*/
            },
            loadingMessage: 'Cargando...',
            buttons: [
                { extend: 'print', className: 'btn dark btn-outline' },
                { extend: 'copy', className: 'btn red btn-outline' },
                { extend: 'pdf', className: 'btn green btn-outline' },
                { extend: 'excel', className: 'btn yellow btn-outline ' },
                { extend: 'csv', className: 'btn purple btn-outline ' },
                { extend: 'colvis', className: 'btn dark btn-outline', text: 'Columns' }
            ],
            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: true,
            "searching": false,
            "ordering": true,
            //"ordering": false, disable column ordering
            //"paging": false, disable pagination
            "columnDefs": [{
                "targets": 'no-sort',
                "orderable": false,
            }],
            "order": [
                [0, 'desc']
            ],
            "fnInitComplete": function() {
                $('.table-list-ajax tbody tr').each(function() {
                    /*
                    if (controller == 'autocompra') {
                        $(this).find('td:eq(1)').attr('nowrap', 'nowrap');
                        $(this).find('td:eq(8)').attr('nowrap', 'nowrap');
                    }*/
                    $(this).find('td:last').attr('nowrap', 'nowrap');
                    //$(this).find('td:eq(10)').attr('nowrap', 'nowrap');
                });
                if ($('.table-list-ajax tbody tr').length > 0) {
                    $("#container-actions").removeClass('hide').show();
                }
            },
            "lengthMenu": [
                [5, 10, 15, 20, 50, 100, -1],
                [5, 10, 15, 20, 50, 100, "Todos"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            //"dom": "<'row' <'col-md-12'>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
            // So when dropdowns used the scrollable div should be removed.
            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        });
        /*
        oTableAjax.on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
            console.log( 'Details for row '+row.index()+' '+(showHide ? 'shown' : 'hidden') );
        } );
        */
        // handle datatable custom tools
        /*
        $('#sample_3_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });
        */
        $('#tools_export_list > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTableAjax.DataTable().button(action).trigger();
        });
        /*
        oTableAjax.on('draw', function () {
            alert( 'Table redrawn' );
        });
        oTableAjax.fnDraw();
        */
        //oTableAjax.columns.adjust().responsive.recalc();
        //$('.table-list-ajax').css( 'display', 'table' );
        //$('.table-list-ajax').responsive.recalc();
    }

    return {

        //main function to initiate the module
        init: function() {
            if (!jQuery().dataTable) {
                return;
            }

            initTableAjax();
        }
    };
}();


jQuery(document).ready(function() {

    "use strict";

    // Tooltip
    jQuery('.tooltips').tooltip({ container: 'body' });

    //jQuery('.toggle').toggles({on: true});

    // Popover
    jQuery('.popovers').popover();

    // Show panel buttons when hovering panel heading
    jQuery('.panel-heading').hover(function() {
        jQuery(this).find('.panel-btns').fadeIn('fast');
    }, function() {
        jQuery(this).find('.panel-btns').fadeOut('fast');
    });

    // Close Panel
    jQuery('.panel .panel-close').click(function() {
        jQuery(this).closest('.panel').fadeOut(200);
        return false;
    });

    // Minimize Panel
    jQuery('.panel .panel-minimize').click(function() {
        var t = jQuery(this);
        var p = t.closest('.panel');
        if (!jQuery(this).hasClass('maximize')) {
            p.find('.panel-body, .panel-footer').slideUp(200);
            t.addClass('maximize');
            t.find('i').removeClass('fa-minus').addClass('fa-plus');
            jQuery(this).attr('data-original-title', 'Maximize Panel').tooltip();
        } else {
            p.find('.panel-body, .panel-footer').slideDown(200);
            t.removeClass('maximize');
            t.find('i').removeClass('fa-plus').addClass('fa-minus');
            jQuery(this).attr('data-original-title', 'Minimize Panel').tooltip();
        }
        return false;
    });

    jQuery('.leftpanel .nav .parent > a').click(function() {

        var coll = jQuery(this).parents('.collapsed').length;

        if (!coll) {
            jQuery('.leftpanel .nav .parent-focus').each(function() {
                jQuery(this).find('.children').slideUp('fast');
                jQuery(this).removeClass('parent-focus');
            });

            var child = jQuery(this).parent().find('.children');
            if (!child.is(':visible')) {
                child.slideDown('fast');
                if (!child.parent().hasClass('active'))
                    child.parent().addClass('parent-focus');
            } else {
                child.slideUp('fast');
                child.parent().removeClass('parent-focus');
            }
        }
        return false;
    });


    // Menu Toggle
    jQuery('.menu-collapse').click(function() {
        if (!$('body').hasClass('hidden-left')) {
            if ($('.headerwrapper').hasClass('collapsed')) {
                $('.headerwrapper, .mainwrapper').removeClass('collapsed');
            } else {
                $('.headerwrapper, .mainwrapper').addClass('collapsed');
                $('.children').hide(); // hide sub-menu if leave open
            }
        } else {
            if (!$('body').hasClass('show-left')) {
                $('body').addClass('show-left');
            } else {
                $('body').removeClass('show-left');
            }
        }
        return false;
    });

    // Add class nav-hover to mene. Useful for viewing sub-menu
    jQuery('.leftpanel .nav li').hover(function() {
        $(this).addClass('nav-hover');
    }, function() {
        $(this).removeClass('nav-hover');
    });

    // For Media Queries
    jQuery(window).resize(function() {
        hideMenu();
    });

    hideMenu(); // for loading/refreshing the page
    function hideMenu() {

        if ($('.header-right').css('position') == 'relative') {
            $('body').addClass('hidden-left');
            $('.headerwrapper, .mainwrapper').removeClass('collapsed');
        } else {
            $('body').removeClass('hidden-left');
        }

        // Seach form move to left
        if ($(window).width() <= 360) {
            if ($('.leftpanel .form-search').length == 0) {
                $('.form-search').insertAfter($('.profile-left'));
            }
        } else {
            if ($('.header-right .form-search').length == 0) {
                $('.form-search').insertBefore($('.btn-group-notification'));
            }
        }
    }

    collapsedMenu(); // for loading/refreshing the page
    function collapsedMenu() {

        if ($('.logo').css('position') == 'relative') {
            $('.headerwrapper, .mainwrapper').addClass('collapsed');
        } else {
            $('.headerwrapper, .mainwrapper').removeClass('collapsed');
        }
    }

    if (jQuery("#list_action_ajax").attr("id") == 'list_action_ajax') {
        $($.fn.dataTable.tables(true)).DataTable().responsive.recalc();
        TableDatatables.init();
    }

});

function inArray(needle, haystack) {
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
        if (haystack[i] == needle) return true;
    }
    return false;
}