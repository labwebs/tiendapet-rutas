function number_format( number, decimals, dec_point, thousands_sep ) {
    // http://kevin.vanzonneveld.net
    // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +     bugfix by: Michael White (http://crestidg.com)
    // +     bugfix by: Benjamin Lupton
    // +     bugfix by: Allan Jensen (http://www.winternet.no)
    // +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)    
    // *     example 1: number_format(1234.5678, 2, '.', '');
    // *     returns 1: 1234.57     
  
    var n = number, c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;
    var d = dec_point == undefined ? "," : dec_point;
    var t = thousands_sep == undefined ? "." : thousands_sep, s = n < 0 ? "-" : "";
    var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
  
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

var configs = {

  api: '//rutas.tiendapet.cl/api/',
//  api: '//rutas.dev/api/',
  
  datepicker: {
    dateFormat: "yy-mm-dd",
    monthNames: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
    monthNamesShort: ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"],
    dayNames: ["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"],
    dayNamesMin: ["Do","Lu","Ma","Mi","Ju","Vi","Sá"],
    dayNamesShort: ["Dom","Lun","Mar","Mié","Jue","Vie","Sáb"]
  },
  
  timepicker: {
    defaultTime: 'current',
    showMeridian: false
  },
  
  datatable: {
    language: {
      "emptyTable":     "No hay información disponible para mostrar",
      "info":           "Mostrando registros del _START_ al _END_ de _TOTAL_ totales",
      "infoEmpty":      "Sin registros para mostrar.",
      "infoFiltered":   "(filtrados de un total de _MAX_ registros)",
      "infoPostFix":    "",
      "thousands":      ",",
      "lengthMenu":     "Mostrar _MENU_ registros",
      "loadingRecords": "Cargando...",
      "processing":     "Procesando...",
      "search":         "Buscar:",
      "zeroRecords":    "No se encontraron registros que coincidan",
      "paginate": {
          "first":      "Primero",
          "last":       "Último",
          "next":       "Siguiente",
          "previous":   "Anterior"
      },
      "aria": {
          "sortAscending":  ": active para ordenar esta columna ascendentemente",
          "sortDescending": ": active para ordenar esta columna descendentemente"
      }
    },
    "paging":   true,
    "info":     true,
    "sScrollX": "100%",
    "sScrollXInner": "100%",
    "bScrollCollapse": true,
    "responsive": true
  }
}

$.fn.dataTable.moment('D/MM/YYYY');

$('.form-group select').select2();

$('.data-table').DataTable(configs.datatable);
$('.timepicker').timepicker(configs.timepicker);
$('.datepicker').datepicker(configs.datepicker);
$('.datepicker-max').each( function(ix, el) {
  
  var maxdate = $(el).attr('data-maxdate');
  
  var settings = configs.datepicker;
  
  settings.maxDate = maxdate;
  $(el).datepicker(settings);
  
});

//add by jeans
$.datepicker.regional['es'] = {
    closeText: 'Cerrar',
    prevText: '< Ant',
    nextText: 'Sig >',
    currentText: 'Hoy',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
};
$.datepicker.setDefaults($.datepicker.regional['es']);

$(function(){
  $('.confirmation').on('click', function () {
    return confirm('Estás seguro que desea eliminar ó anular el pedido?');
  });

    //modulo exportar
    $('.datepicker').datepicker(configs.datepicker);
    /*
    $('#fecha_ini').datepicker({
        //minDate: 0,
        dateFormat: "dd-mm-yy",
        beforeShow: function () {
            jQuery(this).datepicker('option', 'maxDate', jQuery('#fecha_fin').val());
        },
        language: 'es'
    });
    $('#fecha_fin').datepicker({
        //defaultDate: "+1w",
        dateFormat: "dd-mm-yy",
        beforeShow: function () {
            jQuery(this).datepicker('option', 'minDate', jQuery('#fecha_ini').val());
            if (jQuery('#fecha_ini').val() === '') jQuery(this).datepicker('option', 'minDate', 0);
        },
        language: 'es'
    });
    */
});

$('body').on('hidden.bs.modal', '.modal', function () {
  $(this).removeData('bs.modal');
});

$('#modal').on('loaded.bs.modal', function(e) {
  $('#modal select').select2();
  $('#modal .timepicker').timepicker(configs.timepicker);
  
  var datepicker = configs.datepicker;
  
  var now = new Date();

  if (now.getHours() == 12 && now.getMinutes() >= 30 ) {
    datepicker.defaultDate = +1;
  } else if( now.getHours() > 12 ) {
    datepicker.defaultDate = +1;
  } else {
    datepicker.defaultDate = new Date();
  }

  $('#modal .datepicker').datepicker(datepicker).datepicker('setDate', datepicker.defaultDate);
  $('#btn-append-product').on('click', appendProduct);
});

//AGREGAR PRODUCTO A PEDIDO EXISTENTE
function appendProduct() {
  var sku = $('#slct-prod-id').val(),
      q   = parseInt($('#prod-qty').val());
  
  $.get(configs.api + 'product/' + sku + '/show', function(data) {
    console.log(q);
    var row   = $('<tr></tr>'),
        name  = $('<td></td>').text(data.brand + ' ' + data.name + ' '),
        size  = $('<td></td>').text(data.size),
        id    = $('<input type="hidden" name="sku_id[]" />').val(sku),
        qty   = $('<input type="number" name="qty[]" class="form-control input-sm">').val(q),
        qty   = $('<td></td>').append(qty),
        price = $('<td></td>').text( '$ ' + number_format(data.price * q, 0, ',','.') );
    
    row
      .append(name)
      .append(size)
      .append(qty.append(id))
      .append(price);
    
    $('#order-detail').append(row);
    $('#modal').modal('hide');
      
  },'json');
  
}


//REGLAS DE FORMULARIO QUICK-ORDER
/*function bindQuickOrder(customers) {
  
  var user  = {},
      orders= [],
      c     = [];
  
  var direccion, comuna, chofer, fecha, hora, comment, customer, email, phone, user;
  
  $('.panel-wizard').bootstrapWizard({
      onTabShow: function(tab, navigation, index) {
          tab.prevAll().addClass('done');
          tab.nextAll().removeClass('done');
          tab.removeClass('done');
          
          var $total = navigation.find('li').length;
          var $current = index + 1;
          
          if($current >= $total) {
              $('.panel-wizard').find('.wizard .next').addClass('hide');
              $('.panel-wizard').find('.wizard .finish').removeClass('hide');
          } else {
              $('.panel-wizard').find('.wizard .next').removeClass('hide');
              $('.panel-wizard').find('.wizard .finish').addClass('hide');
          }
      },
      onTabClick: function(tab, navigation, index) {
          return false;
      },
      onNext: function(tab, navigation, index) {
          var $valid = $('.panel-wizard').valid();
          
          $('#modal').animate({
            scrollTop: '0px'
          },500);
          
          if (!$valid) {
              $validator.focusInvalid();
              return false;
          }
      }
  });
  */
  //Formateamos los clientes (c) para mayor comodidad...
  /*
  customer.forEach(function(el, i) {
    c[el.id] = {
      addr: $.map(el.addresses, function(el, i) {
        return {
          id: el.id,
          text: el.direccion + ( el.comuna ? ', ' + el.comuna : ''),
          comment: el.comentarios
        }
      }),
      
      has_orders: el.has_orders ? true : false
    }
  }); */

   /*
  var c = $.map(customers, function(el, i) {
    return {
      id: el.id,
      has_orders: el.has_orders ? true : false,
      text: el.name + ' – ' + el.email,
      phone: el.phone,
      email: el.email,
      name: el.name,
      addr: $.map(el.addresses, function(el, i) {
        return {
          id: el.id,
          text: el.direccion + ( el.comuna ? ', ' + el.comuna : el.old_comuna ? ', ' + el.old_comuna : ''),
          comment: el.comentarios,
          direccion : el.direccion,
          comuna: el.comuna ? el.comuna : el.old_comuna ? el.old_comuna : 'n/a',
          test: 'hola'
        }
      })
    }
  });
  */
  
  /*
  $('[name=customer_id]').select2({
    data: c
  });
  */

  //OCULTAR LOS PANELES DE USUARIO NUEVO Y EXISTENTE 
  //$('#existing-customer,#new-customer,#append-address,#quickorder-products,#order-source, #order-repeat').hide();
  
  /*
  // Acá empieza la locura: cuando se selecciona un cliente...
  $('#slct-customer-id').change( function(e) {
    
    user = $(this).select2('data');
    console.log(user);
    
    customer  = user.name;
    email     = user.email;
    phone     = user.phone;
    
    $('.review-customer').text( customer );
    $('.review-email').text( email );
    $('.review-phone').text( phone );
    
    var cId = $(this).val();
    if ( cId ) {
      
      // ...significa que ya no vamos a crear cliente:
      $('#chbox-new-customer').attr('checked', false).trigger('change');
      
      productsPane();

      // Como select2 es un imbécil, destruimos, vaciamos, reconstruimos y reinstanciamos el select.
      $('#slct-address-id')
        .select2('destroy')
        .select2({
          data: user.addr
        });
      
      // Mostramos el panel correspondiente:
      $('#existing-customer').show();
      $('#new-customer').hide();
      
      
    }
  });
  */

  /*
  // Actualizar la tabla de resumen al actualizar cantidades de productos
  $(document).on('change keyup blur', '.ipt-qty', function(e) {
    
    //¿Qué fila está afectada?
    var self    = $(this),
        tr      = self.parents('tr'),
        ix      = tr.index(),
        val     = self.val(),
        review  = $('#quickorder-products-review tbody tr:eq(' + ix + ')' ),
        cell    = review.find('.td-qty'),
        price   = tr.find('.ipt-price').val(),
        subt    = tr.find('.ipt-subt');
    
    subt.val(price * val);    
    var subtotal = '$ ' + number_format(price * val, 0, ',', '.');
    
    tr.find('.td-subtotal').text(subtotal);
    review.find('.td-subtotal').text(subtotal);
    cell.html( val );
    
    doTotal();
  });
  
  $('#quickorder-products input').on('change keyup blur', doTotal);
  */

  /*
  // Renderea tabla de productos de una orden existente
  function renderProducts(order) {
    
    $('#quickorder-products tbody').empty();
    
    order.products.forEach(function(product, index) {
      
      var $row = $("<tr><td class='td-name'></td><td class='td-qty'></td><td class='td-price hidden-sm hidden-xs'></td><td class='td-subtotal'></td><td class='td-actions'><a><i class='fa fa-trash-o'></i></a></td></tr>"),
          name = product.name + ' ' + product.prod_name + ' ' + product.sku_description,
          price = parseInt(product.sku_price),
          qty = parseInt(product.qty),
          inputId =  $('<input type="hidden" class="ipt-id" name="sku[ids][]" />'),
          inputQty = $('<input type="number" class="ipt-qty input-sm form-control" min="1" name="sku[qty][]" />'),
          inputPrice = $('<input type="hidden" class="ipt-price" />'),
          inputSubt = $('<input type="hidden" class="ipt-subt" />'),
          clone;
      
      $row.find('.td-name').text( name );
      $row.find('.td-qty').append( inputQty.val(qty) );
      $row.find('.td-price').text('$ ' + number_format(price, 0, ',', '.') );
      $row.find('.td-subtotal').text('$ ' + number_format(price * qty, 0, ',', '.') );
      
      $clone = $row.clone();
      $clone.find('td').last().remove();
      $clone.find('.td-qty').html(qty);
      
      $row.find('td').last().append(inputId, inputSubt, inputPrice);
      
      inputId.val(product.id);
      inputQty.val(qty);
      inputPrice.val(price);
      inputSubt.val(qty*price);
      
      $('#quickorder-products tbody').append($row);
      $('#quickorder-products-review tbody').append($clone);
      $row.clone = $clone;
      
      $('.fa-trash-o', $row).click( function(e) {
        
        $row.clone.remove();
        $row.remove();
        
        doTotal();
      });
      
    });
    
    $('#quickorder-products').show();
    doTotal();
  }
  
  // Genera select con órdenes anteriores para replicar
  function renderOrders(json) {
    
    if (!json || !json.length) {
      $('#order-source').hide();
      $('#radio-create_order').trigger('click');
      return;
    }
    
    orders = json;
    
    var options = orders.map(function(order, index, array) {
      
      var count = 0;
      
      var date = moment(order.order_created).locale("es").format('DD MMMM');
      var total = "$" + number_format(order.order_total, 0);
      
      order.products.forEach(function(prod) {
        count += parseInt(prod.qty);
      });
      
      return {
        id: index,
        text: date + " – " + total + " (" + count + " productos)"
      }
    });
    
    $('#slct-order-id').select2('destroy').select2({
      data: options
    });
  }
  */

  //Pregunta al servidor por órdenes anteriores para un ID de usuario
  /*
  function getCustomerOrders(id, callback) {
    $.get(configs.api + 'users/' + id + '/orders', callback, 'json');
  }
  */

  /*
  //Cuando se activa el check de nuevo usuario...
  $('#chbox-new-customer').change(function() {
    var chkbox = $(this),
        checked = chkbox.attr('checked');
    
    if (checked) {
      $('#slct-customer-id2').val("");
      $('#customer_id').val("");

      $('#slct-customer-id').select2('val', "");
      $('#existing-customer').hide();
      $('#new-customer, #append-address').show();
    } else {
      if ( $('#slct-customer-id').val() ) {
        $('#existing-customer').show();
        $('#new-customer, #append-address').hide();
        $('#chbox-new-address').removeAttr('checked');
      } else {
         $('#existing-customer,#new-customer, #append-address').hide();
      }
    }
  });
  */

  //Al seleccionar una dirección:
  /*
  $('#slct-address-id').change(function() {
    var $slct = $(this),
        val   = $slct.val();
    
    $('#chbox-new-address').removeAttr('checked').trigger('change');
    
  });
  */
  
  /*
  function productsPane() {
    //¿Tendrá pedidos para duplicar el amigo?
    $('#order-source').toggle(user.has_orders);
    console.log(user);
    if (user.has_orders) {
      $('#radio-repeat_order').trigger('click');
      
      getCustomerOrders(user.id, renderOrders);
      
    } else {
      $('#radio-create_order').trigger('click');
    }
    
    
  }
  */
  /*
  function doTotal() {
    var subtotal = 0;
    
    if ( $('.ipt-subt').length ) {
      $('.ipt-subt').each(function(ix, el) {
        subtotal += parseInt($(el).val());
      });
    } else {
      subtotal = 0;
    }
    
    var descuento = parseInt( $('.order-discount-input').val() );
    if (!descuento) descuento = 0;
    
    var despacho  = parseInt( $('.order-delivery-input').val() );
    if (!despacho) despacho = 0;
    
    var total     = subtotal - descuento + despacho;
    
    
    $('.final-subtotal').text('$ ' + number_format(subtotal, 0, ',', '.'));
    $('.final-despacho').text('$ ' + number_format(despacho, 0, ',', '.'));
    $('.final-descuento').text('$ ' + number_format(descuento, 0, ',', '.'));
    $('.final-total').text('$ ' + number_format(total, 0, ',', '.'));
  }
  */
  /*
  //¿Pedido nuevo o repetir?
  $("[name=order_repeat]").click( function(e) {
    
    var val = $(this).val(),
        target = '#order-' + val;

   
  //No vaciemos la tabla, para poder personalizar el pedido.
  // $('#quickorder-products').hide().find('tbody').empty();
    
    $(target).show().siblings('.order-builder').hide();
  });
  */
  
  /*
  //Tabla de productos:
  $('#btn-add-product').click(function(e) {
    e.preventDefault();
    
    var prodData = $('#slct-prod-id').select2('data'),
        $row = $("<tr><td class='td-name'></td><td class='td-qty'></td><td class='td-price hidden-sm hidden-xs'></td><td class='td-subtotal'></td><td class='td-actions'><a><i class='fa fa-trash-o'></i></a></td></tr>"),
        label = prodData.text.split(' – '),
        name = label[0],
        price = parseInt(label[1].replace('$ ', '').replace('.', '')),
        qty = $('#prod-qty').val(),
        inputId =  $('<input type="hidden" class="ipt-id" name="sku[ids][]" />'),
        inputQty = $('<input type="number" class="ipt-qty form-control input-sm" min="1" name="sku[qty][]" />').val(qty),
        inputPrice = $('<input type="hidden" class="ipt-price" />'),
        inputSubt = $('<input type="hidden" class="ipt-subt" />'),
        clone;
    
    $row.find('.td-name').text( name );
    $row.find('.td-qty').append(inputQty);
    $row.find('.td-price').text('$ ' + number_format(price, 0, ',', '.') );
    $row.find('.td-subtotal').text('$ ' + number_format(price * qty, 0, ',', '.') );
    
    $clone = $row.clone();
    $clone.find('td').last().remove();
    $clone.find('.td-qty').html(qty);
    
    $row.find('td').last().append(inputId, inputSubt, inputPrice);
    
    inputId.val(prodData.id);
    inputQty.val(qty);
    inputPrice.val(price);
    inputSubt.val(qty*price);
    
    $('#quickorder-products').show();
    $('#quickorder-products tbody').append($row);
    $('#quickorder-products-review tbody').append($clone);
    $row.clone = $clone;
    
    
    $('.fa-trash-o', $row).click( function(e) {
      
      $row.clone.remove();
      $row.remove();
      
      doTotal();
    });
    
    doTotal();
  });
  */
  
  //PASAR DATOS PA'LANTE
  /*
  function drawMap() {
    
    var location = encodeURIComponent(direccion + "+" + comuna);
    var mapURL = "http://maps.googleapis.com/maps/api/staticmap?center=" + location + "&zoom=16&size=640x240&maptype=roadmap&sensor=false&scale=2&markers="+location;
    
    $('#map-preview').attr('src', mapURL);
    $('.review-comuna').text(direccion + ', ' + comuna);
    $('.review-comment').text(comment)
    $('.review-chofer').text(chofer);
    
    $('.review-customer').text(customer);
    $('.review-email').text(email);
    $('.review-phone').text(phone);
    
    console.log(direccion, comuna, mapURL);
  }
  */
  
  /*
  $('[name=direccion]').keyup(function () {
    direccion = $(this).val(); 
  }).blur(drawMap);
  
  $('[name=create_address').change( function() {
    $('#append-address').toggle( $(this).is(':checked') );
  });
  
  $('[name=address_id]').change( function() {
    var addr = $(this).select2('data');
    
    $('[name=comuna_despacho').val(addr.comuna);
    
    direccion = addr.direccion;
    comuna = addr.comuna;
    comment = addr.comment;
    
    drawMap();
  });
  
  $('[name=cust_name').keyup(function (e) {
    customer = $(this).val();
    $('.review-customer').text( customer );
  });
  
  $('[name=cust_email').keyup(function (e) {
    email = $(this).val();
    $('.review-email').text( email );
  });
  
  $('[name=cust_phone').keyup(function (e) {
    phone = $(this).val();
    $('.review-phone').text( phone );
  });
  
  $('[name=driver_id]').change( function(e) {
    var data = $(this).select2('data');
    chofer = data.text;
  });
  
  $('[name=comuna]').change( function() {
    comuna = $('option:selected', $(this)).text();
    $('[name=comuna_despacho').val( comuna );
    
    drawMap();
  });
  
  $('[name=driver_id]').change( function() {
    chofer = $('option:selected', $(this)).text();
    $('.review-chofer').text( chofer );
  });
  
  $('[name=delivery_date]').change( function() {
    fecha = $(this).val();
    $('.review-fecha').text( fecha );
  });
  
  $('[name=delivery_time]').change( function() {
    hora = $(this).val();
    $('.review-hora').text( hora );
  });
  */

//}

//DEV
//$('header .header-right a.btn[data-toggle=modal]').trigger('click');