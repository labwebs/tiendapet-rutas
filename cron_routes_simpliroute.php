<?php
@session_start();
echo dirname(__FILE__); exit;
//define("PATH_SITE", '/Users/jeans/Sites/tiendapet-rutas-new');
//define("PATH_SITE", '/home/tiendapetcl/public_html/rutas');
define("PATH_SITE", dirname(__FILE__));
$modo_debug = FALSE;
$_SESSION['cron'] = true;

//if ((isset($argv[1])) && ($argv[1] =='cron'))
/*
if (PHP_SAPI === 'cli') {
    $_SESSION['cron'] = TRUE;
}
else
{
    $_SESSION['cron'] = FALSE;
    // No ejecutado desde CrON
    die( "No permitido ");
}
*/

$CRON_OUTPUT = "INICIANDO CRON @ ".date("m-d-Y H:i:s")."\r\n";
$CRON_OUTPUT .= "\r\n";

ob_start();

//$_SERVER['CI_ENV'] = 'production'; //'production development';
$_SERVER['CI_ENV'] = 'development'; //'production development';

include(PATH_SITE.'/index.php');


ob_end_clean();
$CI =& get_instance();

//error_reporting(E_ALL);
//ini_set("display_errors", true);


$CI->load->library('session');
$CI->load->library('notificacion');

//$CI->load->library('sendmail');
$CI->load->library('simpliroute');

$CI->load->model('orders');
$CI->load->model('drivers');
$CI->load->model('simpliroute_model');

//$current_date = date('Y-m-d');
$date_less_1day = date('Y-m-d', strtotime("-1day")); //-1
//$date_less_1day = date('Y-m-d');

$search = array(
    'status' => 1,
    'start_date' => $date_less_1day,
    'end_date' => $date_less_1day,
    'cron' => 0
);
$ids = $CI->simpliroute_model->srch($search);
//print_a($search);
//print_a($ids);
//exit;
$results = $CI->simpliroute_model->get($ids);

//echo "Se han econtrado ".strval(count($results))." planes ingresados ! ".date("m-d-Y H:i:s")."\n";
$CRON_OUTPUT .= "Se han econtrado ".strval(count($results))." planes ingresados ! ".date("m-d-Y H:i:s")."\n";

$cont = 0;
if( ! empty($results) ){
    $driver = $CI->drivers->findByUserRolChofer('simpliroute');
    $sr = new Simpliroute();

    foreach ($results as $key => $result){
        if( ! empty($result->routes) ){
            foreach ($result->routes as $indice => $r) {
                $result_visits = $sr->consultRoutes($r->route);
                
                if (! $result_visits['error']) {
                    $visits = $result_visits['response'];
                    if( ! empty($visits) ){
                        foreach ($visits as $key => $v) {
                            $order_id = $v->reference;
                            
                            if( $v->status == 'completed' ){
                                // Actualizamos
                                $chofer_id = isset($result->driver[$order_id]->chofer_id) ? $result->driver[$order_id]->chofer_id : 0;
                                
                                $data = array(
                                    "order_delivered" => $date_less_1day,
                                    "driver_id" => $chofer_id,
                                );
                                $CI->orders->update($order_id, $data);
                                
                                if( $chofer_id ){
                                    $CI->drivers->update($chofer_id, array(
                                        'last_delivery' => $order_id,
                                    ));
                                }

                                $notificacion = new Notificacion();
                                $notificacion->qualifyService($order_id);

                                $msje = "ACTUALIZANDO EL PEDIDO COMPLETADO #".$order_id."...";
                                $CRON_OUTPUT .= "[STATUS: Completed, DRIVER_ID: ".$chofer_id.", ORDER_DELIVERED: ".$date_less_1day."] " . $msje . "\r\n";    
                            }else{
                                // Actualizamos
                                /*
                                $driver = $CI->drivers->findByUserRolChofer('pedidosporconseguir');
                                $data = array(
                                    //"order_delivery_date" => $date_current,
                                    "driver_id" => $driver->id,
                                );
                                $CI->orders->update($order_id, $data);
                                */
                                $msje = "SE DEJA CON EL MISMO CHOFER, PEDIDO NO COMPLETADO #".$order_id."...";
                                $chofer_id = isset($result->driver[$order_id]->chofer_id) ? $result->driver[$order_id]->chofer_id : 0;
                                $CRON_OUTPUT .= "[STATUS: ".ucfirst($v->status).", DRIVER_ID: ".$chofer_id.", ORDER_DELIVERY_DATE: ".$date_less_1day."] " . $msje . "\r\n";   
                            }
                        }
                    }else{
                        $msje = "NO SE ENCONTRARON VISITAS AL CONSULTAR EL PLAN VÍA API";
                        $CRON_OUTPUT .= "[PLAN:" . $result->ident . ", MSJE: ".$result_visits['message']."] " . $msje . "\r\n";    
                    }
                }else{
                    $msje = "PROBLEMAS DE COMUNICACIÓN CON LA API AL CONSULTAR LOS DATOS DE UNA ROUTE";
                    $CRON_OUTPUT .= "[ROUTE:" . $r->route . ", MSJE: ".$result_visits['message']."] " . $msje . "\r\n";
                }
            }
            $CI->simpliroute_model->update($result->id, array('cron'=>1));
        }
    }
}

$CRON_OUTPUT .= "\r\n";
$CRON_OUTPUT .= "TERMINANDO CRON @ ".date("m-d-Y H:i:s")."\r\n";
$CRON_OUTPUT .= "\r\n";
//echo dirname(__FILE__); 

$fh = fopen(PATH_SITE.'/cron_log/cron_routes_simpliroute_'.date('d-m-Y_his').'.log', 'a+');
fwrite($fh, $CRON_OUTPUT);
fclose($fh);
//echo "Fin del CRON: ".date("m-d-Y H:i:s")."\n";
//echo "\n";
//exit;
?>
